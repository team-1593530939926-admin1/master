trigger LeadTrigger on Lead (After update) {
    new TriggerDispatcher().run('LeadTriggerHandler');
}