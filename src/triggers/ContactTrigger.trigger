trigger ContactTrigger on Contact (after insert,after update) {
    new TriggerDispatcher().run('ContactTriggerHandler');
}