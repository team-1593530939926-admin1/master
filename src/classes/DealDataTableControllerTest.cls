@isTest
private class DealDataTableControllerTest
{    
    @testSetup
    private static void testSetupData()
    { 
        TestUtil.insertBusinessApplicationWrapper(); 
    }
     
    private static Account getCompany()
    {
        return [SELECT Id, Company_Code__c FROM Account LIMIT 1];
    }
    
    private static Opportunity getDeal()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity LIMIT 1];
    }
    
    @isTest
    private static void testGetListOfDealRecordIfRecordExists()
    {
        
        
        DealDataTableController.DealDataTableWrappper dealDataTableWrappperInstance = new DealDataTableController.DealDataTableWrappper();
        
        Test.startTest(); 
            dealDataTableWrappperInstance = DealDataTableController.getListOfDealRecord();        
        Test.stopTest();
        
        System.assertEquals(1, dealDataTableWrappperInstance.data.size(), 'Record is not retrieved');
    }
    
    @isTest
    private static void testGetListOfDealRecordIfRecordNotExists() 
    {
        List<Opportunity> listOfOpportunityToDelete = [SELECT Id From Opportunity];
        delete listOfOpportunityToDelete;
        DealDataTableController.DealDataTableWrappper dealDataTableWrappperInstance = new DealDataTableController.DealDataTableWrappper();
        Test.startTest(); 
        try
        {
            dealDataTableWrappperInstance = DealDataTableController.getListOfDealRecord(); 
        }
        catch(Exception exceptionObj)
        {  
        }
        Test.stopTest();
        
        System.assertEquals(0, dealDataTableWrappperInstance.data.size(), 'Record is retrieved');
    }
    
    @isTest
    private static void testGetListOfSearchRecordIfRecordExists()
    {
        DealDataTableController.DealDataTableWrappper dealDataTableWrappperInstance = new DealDataTableController.DealDataTableWrappper();
        
        Test.startTest(); 
            dealDataTableWrappperInstance = DealDataTableController.getListOfSearchRecord();        
        Test.stopTest();
        
        System.assertEquals(null , dealDataTableWrappperInstance.data, 'Record is not retrieved');        
    } 
    
    @isTest
    private static void testGetListOfPendingDealRecordIfRecordExists()
    {
        
        
        DealDataTableController.DealDataTableWrappper dealDataTableWrappperInstance = new DealDataTableController.DealDataTableWrappper();
        
        Test.startTest(); 
            dealDataTableWrappperInstance = DealDataTableController.getListOfPendingDealRecord();        
        Test.stopTest();
        
        System.assertEquals(1, dealDataTableWrappperInstance.data.size(), 'Record is not retrieved');
    }
    
    @isTest
    private static void testGetListOfPendingDealRecordIfRecordNotExists()
    {
        List<Opportunity> listOfOpportunityToDelete = [SELECT Id From Opportunity];
        delete listOfOpportunityToDelete;
        DealDataTableController.DealDataTableWrappper dealDataTableWrappperInstance = new DealDataTableController.DealDataTableWrappper();
        Test.startTest(); 
        try
        {
            dealDataTableWrappperInstance = DealDataTableController.getListOfPendingDealRecord(); 
        }
        catch(Exception exceptionObj)
        { 
        }
        Test.stopTest();
        
        System.assertEquals(0, dealDataTableWrappperInstance.data.size(), 'Record is retrieved');
    }
    
    @isTest
    private static void testgetListOfDealRecordBySearchKeyIfSearchKeyExists()
    {
        Test.startTest();
            List<Opportunity> obj =  DealDataTableController.getListOfDealRecordBySearchKey('Test','5','0','CreatedDate','DESC', '', '');
        Test.stopTest();
        
        System.assertEquals(1, obj.size(),'Search key does not exist');
    }
    
    @isTest
    private static void testgetListOfDealRecordBySearchKeyIfSearchKeyDoesNotExist()
    {
        Test.startTest();
           List<Opportunity> obj =  DealDataTableController.getListOfDealRecordBySearchKey('xyz','5','0','CreatedDate','DESC', '', '');
        Test.stopTest();
        
        System.assertEquals(0, obj.size(),'Search Key Exists');
    }
   
    @isTest
    private static void testGetListOfPendingDealRecordBySearchKeyIfSearchKeyExists()
    {
        Test.startTest();
            List<Opportunity> obj =  DealDataTableController.getListOfPendingDealRecordBySearchKey('Test','5','0','CreatedDate','DESC', '', '');
        Test.stopTest();
        
        System.assertEquals(1, obj.size(),'Search key does not exist');
    }
    
    @isTest
    private static void testGetListOfPendingDealRecordBySearchKeyIfSearchKeyDoesNotExist()
    {
        Test.startTest();
            List<Opportunity> obj =  DealDataTableController.getListOfPendingDealRecordBySearchKey('xyz','5','0','CreatedDate','DESC', '', '');
        Test.stopTest();
        
        System.assertEquals(1, obj.size(),'Search key exists');
    }
    
    @isTest
    private static void testgetSelectOptionsByListOfDataIfListIsNotEmpty()
    {
        List<String> pickValues = new List<String>{'2', '5', '10', '20', '50', '100'};
        
        Test.startTest();
            List<DealDataTableController.PicklistWrapper> picklistWrapperInstance = DealDataTableController.getSelectOptionsByListOfData(pickValues);
        Test.stopTest();
        
        System.assertEquals(6, picklistWrapperInstance.size(), 'List is empty');            
            
    }  
 
    @isTest
    private static void testgetSelectOptionsByListOfDataIfListIsEmpty()
    {
        List<String> pickValues = new List<String>{};
        
        Test.startTest();
            List<DealDataTableController.PicklistWrapper> picklistWrapperInstance = DealDataTableController.getSelectOptionsByListOfData(pickValues);
        Test.stopTest();
        
        System.assertEquals(0, picklistWrapperInstance.size(), 'List is not empty');           
            
    }   
    
    
    @isTest
    private static void testGetSubmittedApplicationOnViewIfDataExists()
    {
        
        Account accountRecord = getCompany();
        Opportunity opportunityRecord = getDeal();
        BusinessApplicationWrapper businessApplicationWrapperInstance = new BusinessApplicationWrapper();
        
        Test.startTest();
            businessApplicationWrapperInstance = DealDataTableController.getSubmittedApplicationOnView(opportunityRecord.Application_Id__c, accountRecord.Company_Code__c);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', businessApplicationWrapperInstance.companyWrapper.fullLegalCompanyName, 'Required records not found');
    } 
    
     @isTest
    private static void testGetSubmittedApplicationOnViewIfDataNotExist()
    {
        BusinessApplicationWrapper businessApplicationWrapperInstance = new BusinessApplicationWrapper();
       
        Test.startTest();
            businessApplicationWrapperInstance = DealDataTableController.getSubmittedApplicationOnView('COMP-00212', 'DEAL-0007');
        Test.stopTest();
        
        System.assertEquals('Company Code does not exist', businessApplicationWrapperInstance.companyWrapper.errorMessage, 'Required records found');
    }
}