@isTest(isParallel=true)
private class FCSApiServiceTest {

    private static final String EMPTY_STATE_CODE_ASSERT_MESSAGE = 'Must Throw Custom Exception when state code is not provided.';
    private static final String EMPTY_BUSINESS_NAME_ASSERT_MESSAGE = 'Must Throw Custom Exception when Business Name is not provided.';
    private static final String EMPTY_TRANSACTION_ID_ASSERT_MESSAGE = 'Must Throw Custom Exception when Transaction Id is not provided.';
    private static final String EMPTY_ENTITY_ID_ASSERT_MESSAGE = 'Must Throw Custom Exception when Entity Id is not provided.';
    private static final String MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE = 'Exception must throw when error response occur.';
    private static final String FAILURE_REASON_FOR_ERROR_RESPONSE = 'Failure Reason.';
    private static final String THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE = 'There must be proper response.';
    private static final String THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE = 'Exception should be return here.';
    private static final String SUCCESS_STATUS_CODE_ASSERT_MESSAGE = 'Status Code Should be '+FCSAPIConstant.STATUS_CODE_SUCCESS+'.';
    
    private static final String BODY_ERROR_RESPONSE = '{"Message":"'+FAILURE_REASON_FOR_ERROR_RESPONSE+'"}';

    private static final String STATE_INFO_BODY = '[{"StateCode" : "CA"}]';

    private static final String BUSINESS_INFO_BODY = '{ "Header": { "TransactionID": 1, "StateCode": "CA" },'+
                            '"Results": [ {"CompanyName": "xyz","EntityID": "e1234" } ] }';

    private static final String BUSINESS_DETAILS_BODY = '{ "Header": { "TransactionID": 1, "StateCode": "CA" },'+
                            '"Results": [ {"BusinessReport": {} } ] }';
    
    private static final String STATE_CODE = 'CA';
    private static final String BUSINESS_NAME = 'XYZ';
    private static final String REFERENCE = 'Ams';
    private static final String PASS_THROUGH = 'ACK';
    private static final String REFERENCE_EMPTY = '';
    private static final String PASS_THROUGH_EMPTY= '';
    private static final String TRANSACTION_ID = 'T1234';
    private static final String ENTITY_ID = 'E1234';
    private static final String ADDITIONAL_SEARCH_PARAMETERS = '';
    
    @isTest
    private static void getStateInfoSuccessTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, STATE_INFO_BODY));

        Test.startTest();
        	HttpResponse actualRes = FCSApiService.getStateInfo(STATE_CODE);
        Test.stopTest();

        System.assertEquals(FCSAPIConstant.STATUS_CODE_SUCCESS, actualRes.getStatusCode(), SUCCESS_STATUS_CODE_ASSERT_MESSAGE);
        System.assertEquals(STATE_INFO_BODY, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
    }
    
    @isTest
    private static void searchBusinessByStateAndBusinessNameTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_INFO_BODY));
        
        Test.startTest();
        	HttpResponse actualRes = FCSApiService.searchBusinessByStateAndBusinessName(STATE_CODE, BUSINESS_NAME, REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
        Test.stopTest();
        
        System.assertEquals(FCSAPIConstant.STATUS_CODE_SUCCESS, actualRes.getStatusCode(), SUCCESS_STATUS_CODE_ASSERT_MESSAGE);
        System.assertEquals(BUSINESS_INFO_BODY, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
    }
    
    @isTest
    private static void searchBusinessByStateAndBusinessNameExceptionWhenEmptyStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_INFO_BODY));
        try {

            Test.startTest();
            	HttpResponse actualRes = FCSApiService.searchBusinessByStateAndBusinessName('', BUSINESS_NAME, REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
            Test.stopTest();

            System.assert(false, EMPTY_STATE_CODE_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void searchBusinessByStateAndBusinessNameExceptionWhenEmptyBusinessNameTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_INFO_BODY));
        try {

            Test.startTest();
            	HttpResponse actualRes = FCSApiService.searchBusinessByStateAndBusinessName(STATE_CODE, '', REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
            Test.stopTest();

            System.assert(false, EMPTY_BUSINESS_NAME_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void getBusinessDetailByEntityAndTransactionIdTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        
        Test.startTest();
        	HttpResponse actualRes = FCSApiService.getBusinessDetailByEntityAndTransactionId(TRANSACTION_ID, ENTITY_ID, ADDITIONAL_SEARCH_PARAMETERS, PASS_THROUGH_EMPTY);
        Test.stopTest();
        
        System.assertEquals(FCSAPIConstant.STATUS_CODE_SUCCESS, actualRes.getStatusCode(), SUCCESS_STATUS_CODE_ASSERT_MESSAGE);
        System.assertEquals(BUSINESS_DETAILS_BODY, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void getBusinessDetailByEntityAndTransactionIdExceptionWhenEmptyTransactionIDTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        try {

            Test.startTest();
                HttpResponse actualRes = FCSApiService.getBusinessDetailByEntityAndTransactionId('', ENTITY_ID, ADDITIONAL_SEARCH_PARAMETERS, PASS_THROUGH_EMPTY); 
            Test.stopTest();
            
            System.assert(false, EMPTY_TRANSACTION_ID_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void getBusinessDetailByEntityAndTransactionIdExceptionWhenEmptyEntityIDTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        try {

            Test.startTest();
                HttpResponse actualRes = FCSApiService.getBusinessDetailByEntityAndTransactionId(TRANSACTION_ID, '', ADDITIONAL_SEARCH_PARAMETERS, PASS_THROUGH_EMPTY);            	
            Test.stopTest();

            System.assert(false, EMPTY_ENTITY_ID_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void searchEntityIdWithStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        
        Test.startTest();
        	HttpResponse actualRes = FCSApiService.searchEntityIdWithStateCode(STATE_CODE, ENTITY_ID, REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
        Test.stopTest();
        
        System.assertEquals(FCSAPIConstant.STATUS_CODE_SUCCESS, actualRes.getStatusCode(), SUCCESS_STATUS_CODE_ASSERT_MESSAGE);
        System.assertEquals(BUSINESS_DETAILS_BODY, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), THERE_MUST_BE_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void searchEntityIdWithStateCodeExceptionWhenEmptyStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        try {

            Test.startTest();
                HttpResponse actualRes = FCSApiService.searchEntityIdWithStateCode('', ENTITY_ID, REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
            Test.stopTest();

            System.assert(false, EMPTY_STATE_CODE_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void searchEntityIdWithStateCodeExceptionWhenEmptyEntityIdTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, BUSINESS_DETAILS_BODY));
        try {

            Test.startTest();
                HttpResponse actualRes = FCSApiService.searchEntityIdWithStateCode(STATE_CODE, '', REFERENCE_EMPTY, PASS_THROUGH_EMPTY);
            Test.stopTest();
            
            System.assert(false, EMPTY_ENTITY_ID_ASSERT_MESSAGE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void handleErrorResponseBadRequestTest() {
        HttpResponse res = new HttpResponse();
        res.setHeader(FCSAPIConstant.CONTENT_TYPE_HEADER, FCSAPIConstant.CONTENT_TYPE_JSON);
        res.setBody(BODY_ERROR_RESPONSE);
        res.setStatusCode(FCSAPIConstant.STATUS_CODE_BAD_REQUEST);
        try {

            Test.startTest();
                FCSApiService.handleFCSErrorResponse(res);
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(FAILURE_REASON_FOR_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void handleErrorResponseRequestEntityTooLargeTest() {
        HttpResponse res = new HttpResponse();
        res.setHeader(FCSAPIConstant.CONTENT_TYPE_HEADER, FCSAPIConstant.CONTENT_TYPE_JSON);
        res.setBody(BODY_ERROR_RESPONSE);
        res.setStatusCode(FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE);
        try {

            Test.startTest();
                FCSApiService.handleFCSErrorResponse(res);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(FAILURE_REASON_FOR_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void handleErrorResponseInternalErrorTest() {
        HttpResponse res = new HttpResponse();
        res.setHeader(FCSAPIConstant.CONTENT_TYPE_HEADER, FCSAPIConstant.CONTENT_TYPE_JSON);
        res.setBody(BODY_ERROR_RESPONSE);
        res.setStatusCode(FCSAPIConstant.STATUS_CODE_INTERNAL_ERROR);
        try {

            Test.startTest();
                FCSApiService.handleFCSErrorResponse(res);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(FCSApiService.FCSAPIException actualException) {
            System.assertEquals(BODY_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
}