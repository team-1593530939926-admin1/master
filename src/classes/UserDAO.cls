public with sharing class UserDAO implements IUserDAO {    
    public Interface IUserDAO {
        List<User> insertUser(List<User> listOfUser);
        void insertUserFutureMethod(String listOfUserJSON);
        List<User> getUser();
        List<PicklistWrapper> getLanguageLocaleKeyPicklist();
        List<PicklistWrapper> getLocaleSidKeyPicklist();
        List<PicklistWrapper> gettimezonesidkeyPicklist();
        List<User> updateUser(List<User> listOfUser);
        List<User> getUserToEdit();
        User getUserById(Id ownerId);
        User getContactByUserId();
    }
    
    public static List<User> getUser()
    {
        List<User> listOfUser = [SELECT Id, username, toLabel(timezonesidkey), toLabel(localesidkey), toLabel(languagelocalekey),
                                 UserType, communityNickname, firstName, email, lastName, phone, title,
                                 extension, fax, mobilephone, street, city, state, postalcode, country, 
                                 Name FROM User WHERE Id =: userInfo.getUserId()];
        return listOfUser;
    }
    
    public static List<User> insertUser(List<User> listOfUser){
        System.debug('UserDAO listOfUser: '+listOfUser);
        insert listOfUser;
        return listOfUser;
    }

    @future
    public static void insertUserFutureMethod(String listOfUserJSON) {
        List<User> listOfUser = (List<User>)JSON.deserialize(listOfUserJSON, List<User>.class);
        System.debug('UserDAO future listOfUser: '+listOfUser);
        insert listOfUser;
    }
    
    public static List<PicklistWrapper> getLanguageLocaleKeyPicklist(){
       List<PicklistWrapper> languagePicklist = new List<PicklistWrapper>();
         for (Schema.PicklistEntry p: User.LanguageLocaleKey.getDescribe().getPicklistValues()) {
            languagePicklist.add(new PicklistWrapper(p.label, p.value));
        }
        return languagePicklist;
    }
   
      public static List<PicklistWrapper> getLocaleSidKeyPicklist(){
       List<PicklistWrapper> localeSidPicklist = new List<PicklistWrapper>();
         for (Schema.PicklistEntry p: User.LocaleSidKey.getDescribe().getPicklistValues()) {
            localeSidPicklist.add(new PicklistWrapper(p.label, p.value));
        }
        return localeSidPicklist;
    }
   
      public static List<PicklistWrapper> gettimezonesidkeyPicklist(){
       List<PicklistWrapper> timeZonePicklist = new List<PicklistWrapper>();
         for (Schema.PicklistEntry p: User.TimeZoneSidKey.getDescribe().getPicklistValues()) {
            timeZonePicklist.add(new PicklistWrapper(p.label, p.value));
        }
        return timeZonePicklist;
    }
   
     public static List<User> getUserToEdit()
    {
        List<User> listOfUser = [SELECT Id, username, timezonesidkey, localesidkey, languagelocalekey,
                                 communityNickname, UserType, firstName, email, lastName, phone, title,
                                 extension, fax, mobilephone, street, city, state, postalcode, country, 
                                 Name FROM User WHERE Id =: userInfo.getUserId()];
        return listOfUser;
    }
    
      
    public static List<User> updateUser(List<User> listOfUser){
        System.debug('UserDAO listOfUser: '+listOfUser);
        update listOfUser;
        return listOfUser;
    }
    
    public static User getUserById(Id ownerId) {
        User userInstance = [SELECT Id, Name, username FROM User WHERE Id = :ownerId];
        return userInstance;
    }
    
     public static User getContactByUserId() {
        User userInstance = [SELECT ContactId ,Id FROM User WHERE Id = :userInfo.getUserId() Limit 1];
        return userInstance;
    }
}