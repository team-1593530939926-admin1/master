public class GuarantorWrapper{
    @AuraEnabled
    public String companyName{get;set;} 
    @AuraEnabled
    public String fullSearchName{get;set;}
    @AuraEnabled
    public String existingId{get;set;}
    @AuraEnabled
    public String firstNameGuarantor{get;set;}
    @AuraEnabled
    public String middleNameGuarantor{get;set;}
    @AuraEnabled
    public String lastNameGuarantor{get;set;}
    @AuraEnabled
    public String mailingStreetGurantor{get;set;}
    @AuraEnabled
    public String mailingCountryGuarantor{get;set;}
    @AuraEnabled
    public String mailingCityGuarantor{get;set;}
    @AuraEnabled
    public String mailingStateGuarantor{get;set;}
    @AuraEnabled
    public String mailingPostalCodeGuarantor{get;set;}
    @AuraEnabled
    public String homePhoneGuarantor{get;set;}
    @AuraEnabled
    public String emailAddressGuarantor{get;set;}
    @AuraEnabled
    public String faxGuarantor{get;set;}
    @AuraEnabled
    public String federalTaxId{get;set;}
    @AuraEnabled
    public String natureofBusiness{get;set;}
    @AuraEnabled
    public String primaryGuarantor{get;set;}
    @AuraEnabled
    public String contigent{get;set;}
    @AuraEnabled
    public String dOB{get;set;}
    @AuraEnabled
    public String socialSecurity{get;set;}
    @AuraEnabled
    public String guarantor{get;set;}
    @AuraEnabled
    public String otherStreetAddress{get;set;}
    @AuraEnabled
    public String otherCity{get;set;}
    @AuraEnabled
    public String otherCounty{get;set;}
    @AuraEnabled
    public String otherState{get;set;}
    @AuraEnabled
    public String otherZip{get;set;}
    @AuraEnabled
    public String guarantorType{get;set;}
    @AuraEnabled
    public String recordTypeId{get;set;}
    @AuraEnabled
    public String prefix{get;set;}
    @AuraEnabled
    public Boolean isReviewEnabled{get;set;}
    @AuraEnabled
    public Integer rowId{get;set;}
    @AuraEnabled
    public Integer count{get;set;}
    @AuraEnabled
    public Boolean isDeleteRequire{get;set;}
    @AuraEnabled
    public Boolean isGuarantor{get;set;}
    @AuraEnabled
    public Boolean isCreatedByMe{get;set;}
    @AuraEnabled
    public String maskedOtherStreetAddress{get;set;}    
    @AuraEnabled
    public String maskedHomePhoneGuarantor{get;set;}
    @AuraEnabled
    public String maskedEmailAddressGuarantor{get;set;}    
    @AuraEnabled
    public String maskedFirstNameGuarantor{get;set;}
    @AuraEnabled
    public String maskedMiddleNameGuarantor{get;set;}
    @AuraEnabled
    public String maskedLastNameGuarantor{get;set;}    
    @AuraEnabled
    public String maskedDob{get;set;}
    @AuraEnabled
    public String maskedSocialSecurity{get;set;}
    @AuraEnabled
    public String maskedMailingStreetGurantor{get;set;}
    @AuraEnabled
    public AddressInformationWrapper addressInfo{get;set;}
    @AuraEnabled
    public Boolean companyDataRequire{get;set;}
    @AuraEnabled
    public Boolean ismaskedOtherStreetAddressRequired{get;set;}    
    @AuraEnabled
    public Boolean ismaskedHomePhoneGuarantorRequired{get;set;}
    @AuraEnabled
    public Boolean ismaskedEmailAddressGuarantorRequired{get;set;}    
    @AuraEnabled
    public Boolean ismaskedDobRequired{get;set;}
    @AuraEnabled
    public Boolean ismaskedSocialSecurityRequired{get;set;}
    @AuraEnabled
    public Boolean ismaskedMailingStreetRequired{get;set;}
    @AuraEnabled
    public Boolean multipleGuarantor{get;set;}
    @AuraEnabled
    public Boolean individualDataRequire{get;set;}
    @AuraEnabled
    public Boolean isCorporate{get;set;}
    public Contact dealGuarantorInfo{get;set;}
    public Account dealCorporateGuarantorInfo{get;set;}
    public Communication_Address__c gurantorAddress{get; set;}
    public Relationship__c guarantorRelationship{get; set;}
    public static RecordTypeDAO.IRecordTypeDAO recordTypeDAOInstance = new RecordTypeDAO();
    private static final Id BUSINESS_CONTACT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Contact', 'Crestmark Contact');
    private static final Id BUSINESS_ACCOUNT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Account', 'Business');
    private static final Id LOGIN_USER_ID = UserInfo.getUserId();
    
    public GuarantorWrapper(GuarantorWrapper guarantorWrapperInfo, String externalId, Boolean isPersonal, String suffixKeyWord){
        Relationship__c relationshipDetails;
        if(isPersonal){
            if(!String.isEmpty(guarantorWrapperInfo.existingId)){
                relationshipDetails = new Relationship__c(Related_Opportunity__r = new Opportunity(External_System_Id__c = externalId), Person__c = guarantorWrapperInfo.existingId);
            }else
            {
                relationshipDetails = new Relationship__c(Related_Opportunity__r = new Opportunity(External_System_Id__c = externalId), Person__r = new Contact(External_System_Id__c = externalId + suffixKeyWord));
            }            
            relationshipDetails.External_System_Id__c = externalId + suffixKeyWord;
            relationshipDetails.Primary_Guarantor__c = guarantorWrapperInfo.primaryGuarantor;
            relationshipDetails.Relationship_Type__c = 'Personal Guarantee';
            relationshipDetails.RecordTypeId = recordTypeDAOInstance.recordTypeIdByObjectNameAndRecordTypeName('Relationship__c', 'Individual');
            Contact guarantorRecord =new Contact();
            if(!String.isEmpty(guarantorWrapperInfo.existingId)){
                guarantorRecord.Id = guarantorWrapperInfo.existingId;
            }
            guarantorRecord.Fax = guarantorWrapperInfo.faxGuarantor;
            guarantorRecord.FirstName = guarantorWrapperInfo.firstNameGuarantor;
            guarantorRecord.MiddleName = guarantorWrapperInfo.middleNameGuarantor;
            guarantorRecord.LastName = guarantorWrapperInfo.lastNameGuarantor;
            guarantorRecord.MailingStreet = guarantorWrapperInfo.mailingStreetGurantor;
            guarantorRecord.MailingCity = guarantorWrapperInfo.mailingCityGuarantor;
            guarantorRecord.MailingState = guarantorWrapperInfo.mailingStateGuarantor;
            guarantorRecord.MailingPostalCode = guarantorWrapperInfo.mailingPostalCodeGuarantor;
            guarantorRecord.MailingCountry = guarantorWrapperInfo.mailingCountryGuarantor;
            guarantorRecord.Phone = guarantorWrapperInfo.homePhoneGuarantor;
            guarantorRecord.SSN__c = guarantorWrapperInfo.socialSecurity;
            guarantorRecord.Nature_of_Business__c = guarantorWrapperInfo.natureofBusiness;
            guarantorRecord.External_System_Id__c = externalId + suffixKeyWord;        
            if(!String.isEmpty(guarantorWrapperInfo.otherState)){
                Communication_Address__c otherGuarantorAddress;
                if(!String.isEmpty(guarantorWrapperInfo.existingId)){
                    otherGuarantorAddress = new Communication_Address__c(Guarantors__c = guarantorWrapperInfo.existingId);
                }else{
                    otherGuarantorAddress = new Communication_Address__c(Guarantors__r = new Contact(External_System_Id__c = externalId + suffixKeyWord));
                }                
                otherGuarantorAddress.Address_Type__c = 'Office Address';
                otherGuarantorAddress.Street_Address_1__c = guarantorWrapperInfo.otherStreetAddress;
                otherGuarantorAddress.City__c = guarantorWrapperInfo.otherCity;
                otherGuarantorAddress.State__c = guarantorWrapperInfo.otherState;
                otherGuarantorAddress.County__c = guarantorWrapperInfo.otherCounty;
                otherGuarantorAddress.Postal_Code__c = guarantorWrapperInfo.otherZip;
                this.gurantorAddress = otherGuarantorAddress;
            }
            guarantorRecord.Email = guarantorWrapperInfo.emailAddressGuarantor;
            guarantorRecord.Relationship_Type__c = 'Guarantor';
            guarantorRecord.RecordTypeId = BUSINESS_CONTACT_RECORD_TYPE_ID;
            if(!String.isEmpty(guarantorWrapperInfo.dOB)){
                guarantorRecord.Birthdate = date.valueOf(guarantorWrapperInfo.dOB);
            }
            if(!String.isEmpty(guarantorWrapperInfo.contigent)){
                relationshipDetails.Percent_of_Ownership__c = decimal.valueOf(guarantorWrapperInfo.contigent);
            }
            this.dealGuarantorInfo = guarantorRecord;
        }
        else{
            if(!String.isEmpty(guarantorWrapperInfo.existingId)){
                relationshipDetails = new Relationship__c(Related_Opportunity__r = new Opportunity(External_System_Id__c = externalId), Business__c = guarantorWrapperInfo.existingId);
            }else
            {
                relationshipDetails = new Relationship__c(Related_Opportunity__r = new Opportunity(External_System_Id__c = externalId), Business__r = new Account(External_System_Id__c = externalId + suffixKeyWord));
            }            
            relationshipDetails.External_System_Id__c = externalId + suffixKeyWord;
            relationshipDetails.Relationship_Type__c = 'Business Guarantee';
            relationshipDetails.Primary_Guarantor__c = guarantorWrapperInfo.primaryGuarantor;
            relationshipDetails.RecordTypeId = recordTypeDAOInstance.recordTypeIdByObjectNameAndRecordTypeName('Relationship__c', 'Corporation');
            Account guarantorRecord =new Account();
            guarantorRecord.Fax = guarantorWrapperInfo.faxGuarantor;
            guarantorRecord.Name = guarantorWrapperInfo.companyName;
            guarantorRecord.RecordTypeId = BUSINESS_ACCOUNT_RECORD_TYPE_ID;
            if(!String.isEmpty(guarantorWrapperInfo.existingId)){
                guarantorRecord.Id = guarantorWrapperInfo.existingId;
            }
            guarantorRecord.Name = guarantorWrapperInfo.companyName;
            if(!String.isEmpty(guarantorWrapperInfo.lastNameGuarantor)){
                Contact contactRecord = new Contact(Account = new Account(External_System_Id__c = externalId + suffixKeyWord));
                contactRecord.FirstName = guarantorWrapperInfo.firstNameGuarantor;
                contactRecord.MiddleName = guarantorWrapperInfo.middleNameGuarantor;
                contactRecord.LastName = guarantorWrapperInfo.lastNameGuarantor;
                contactRecord.Email = guarantorWrapperInfo.emailAddressGuarantor;
                contactRecord.MailingStreet = guarantorWrapperInfo.mailingStreetGurantor;
                contactRecord.MailingCity = guarantorWrapperInfo.mailingCityGuarantor;
                contactRecord.MailingState = guarantorWrapperInfo.mailingStateGuarantor;
                contactRecord.MailingPostalCode = guarantorWrapperInfo.mailingPostalCodeGuarantor;
                contactRecord.MailingCountry = guarantorWrapperInfo.mailingCountryGuarantor;
                this.dealGuarantorInfo = contactRecord;
            }
            guarantorRecord.Industry = guarantorWrapperInfo.natureofBusiness;
            guarantorRecord.BillingStreet = guarantorWrapperInfo.mailingStreetGurantor;
            guarantorRecord.BillingCity = guarantorWrapperInfo.mailingCityGuarantor;
            guarantorRecord.BillingState = guarantorWrapperInfo.mailingStateGuarantor;
            guarantorRecord.BillingPostalCode = guarantorWrapperInfo.mailingPostalCodeGuarantor;
            guarantorRecord.BillingCountry = guarantorWrapperInfo.mailingCountryGuarantor;
            guarantorRecord.Phone = guarantorWrapperInfo.homePhoneGuarantor;
            guarantorRecord.Federal_Tax_ID__c = guarantorWrapperInfo.homePhoneGuarantor;
            guarantorRecord.External_System_Id__c = externalId + suffixKeyWord;
            if(!String.isEmpty(guarantorWrapperInfo.contigent)){
                relationshipDetails.Percent_of_Ownership__c = decimal.valueOf(guarantorWrapperInfo.contigent);
            }
            this.dealCorporateGuarantorInfo = guarantorRecord;
        }
        this.existingId = guarantorWrapperInfo.existingId;
        this.guarantorRelationship = relationshipDetails;
        
    }
    
    public GuarantorWrapper(){}
    
   
    
    public GuarantorWrapper(GuarantorWrapper guarantorRecordWrapper , Boolean isVisible){
        this.isCorporate = false;
        this.companyName = guarantorRecordWrapper.companyName;  
        this.firstNameGuarantor = guarantorRecordWrapper.firstNameGuarantor;   
        this.middleNameGuarantor = guarantorRecordWrapper.middleNameGuarantor;   
        this.lastNameGuarantor = guarantorRecordWrapper.lastNameGuarantor;   
        this.mailingStreetGurantor = guarantorRecordWrapper.mailingStreetGurantor;   
        this.mailingCountryGuarantor = guarantorRecordWrapper.mailingCountryGuarantor;   
        this.mailingCityGuarantor = guarantorRecordWrapper.mailingCityGuarantor;   
        this.mailingStateGuarantor = guarantorRecordWrapper.mailingStateGuarantor;   
        this.mailingPostalCodeGuarantor = guarantorRecordWrapper.mailingPostalCodeGuarantor;   
        this.homePhoneGuarantor = guarantorRecordWrapper.homePhoneGuarantor;   
        this.emailAddressGuarantor = guarantorRecordWrapper.emailAddressGuarantor;   
        this.faxGuarantor = guarantorRecordWrapper.faxGuarantor;   
        this.federalTaxId = guarantorRecordWrapper.federalTaxId;   
        this.natureofBusiness = guarantorRecordWrapper.natureofBusiness;   
        this.primaryGuarantor = guarantorRecordWrapper.primaryGuarantor;   
        this.contigent = guarantorRecordWrapper.contigent;   
        this.dOB = guarantorRecordWrapper.dOB;    
        this.socialSecurity = guarantorRecordWrapper.socialSecurity;    
        this.guarantor = guarantorRecordWrapper.guarantor; 
        this.recordTypeId = guarantorRecordWrapper.recordTypeId;
        this.guarantorType = guarantorRecordWrapper.guarantorType;
        this.otherStreetAddress = guarantorRecordWrapper.otherStreetAddress;
        this.otherCity = guarantorRecordWrapper.otherCity;
        this.otherCounty = guarantorRecordWrapper.otherCounty;
        this.otherState = guarantorRecordWrapper.otherState;
        this.otherZip = guarantorRecordWrapper.otherZip;  
        this.prefix = guarantorRecordWrapper.prefix;
        this.existingId = guarantorRecordWrapper.existingId;
        if(guarantorRecordWrapper.guarantorType == 'Corporate'){
            this.isCorporate = true;
        }
        this.isCreatedByMe = guarantorRecordWrapper.isCreatedByMe;
        this.maskedDob = this.dOB != null ? CVFSerciceUtil.emcryptedData(this.dOB,'DOB') : null;  
        this.maskedSocialSecurity = this.socialSecurity != null ? CVFSerciceUtil.emcryptedData(this.socialSecurity,'SSN') : null;
        this.maskedMailingStreetGurantor = this.mailingStreetGurantor != null ? CVFSerciceUtil.emcryptedData(this.mailingStreetGurantor,'Street') : null;
        this.maskedOtherStreetAddress = this.otherStreetAddress != null ? CVFSerciceUtil.emcryptedData(this.otherStreetAddress,'Street') : null;
        this.maskedHomePhoneGuarantor = this.homePhoneGuarantor != null ? CVFSerciceUtil.emcryptedData(this.homePhoneGuarantor,'Full') : null;
        this.maskedEmailAddressGuarantor = this.emailAddressGuarantor != null ? CVFSerciceUtil.emcryptedData(this.emailAddressGuarantor,'Full') : null;
        this.maskedFirstNameGuarantor = this.firstNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.firstNameGuarantor,'Full') : null;
        this.maskedMiddleNameGuarantor = this.middleNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.middleNameGuarantor,'Full') : null;
        this.maskedLastNameGuarantor = this.lastNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.lastNameGuarantor,'Full') : null;         
        this.ismaskedDobRequired = guarantorRecordWrapper.ismaskedDobRequired;
        this.ismaskedSocialSecurityRequired = guarantorRecordWrapper.ismaskedSocialSecurityRequired;
        this.ismaskedMailingStreetRequired = guarantorRecordWrapper.ismaskedMailingStreetRequired;        
        this.ismaskedOtherStreetAddressRequired = guarantorRecordWrapper.ismaskedOtherStreetAddressRequired;
        this.ismaskedHomePhoneGuarantorRequired = guarantorRecordWrapper.ismaskedHomePhoneGuarantorRequired;
        this.ismaskedEmailAddressGuarantorRequired = guarantorRecordWrapper.ismaskedEmailAddressGuarantorRequired;
    }   
    
    public GuarantorWrapper(Contact guarantorRecord, Relationship__c relationshipRecord){
        this.isCorporate = false; 
        this.existingId = guarantorRecord.Id;
        if(guarantorRecord.FirstName != null){
            this.firstNameGuarantor = guarantorRecord.FirstName;
        }
        this.middleNameGuarantor = guarantorRecord.MiddleName;   
        if(guarantorRecord.LastName != null){
        this.lastNameGuarantor = guarantorRecord.LastName;
        }
        else{
            this.lastNameGuarantor = '';
        }
        this.fullSearchName = getValues(guarantorRecord.FirstName) + ' ' + getValues(guarantorRecord.MiddleName) + ' ' + getValues(guarantorRecord.LastName);
        if(guarantorRecord.MailingStreet != null){
          this.mailingStreetGurantor = guarantorRecord.MailingStreet; 
        }
        else{
             this.mailingStreetGurantor = '';
        }
        if(guarantorRecord.MailingCountry != null){
        this.mailingCountryGuarantor = guarantorRecord.MailingCountry;   
        }
        else{
            this.mailingCountryGuarantor = '';
        }
        if(guarantorRecord.MailingCity != null){
        this.mailingCityGuarantor = guarantorRecord.MailingCity; 
        }
        else{
            this.mailingCityGuarantor = '';
        }
        if(guarantorRecord.MailingState != null){
        this.mailingStateGuarantor = guarantorRecord.MailingState; 
         }
        else{
            this.mailingStateGuarantor = '';
        }
        if(guarantorRecord.MailingPostalCode != null){
        this.mailingPostalCodeGuarantor = guarantorRecord.MailingPostalCode;  
        }
        else{
            this.mailingPostalCodeGuarantor = '';
        }
        this.homePhoneGuarantor = guarantorRecord.Phone;   
        this.emailAddressGuarantor = guarantorRecord.Email;   
        this.faxGuarantor = guarantorRecord.Fax; 
        this.guarantorType = 'Individual';
        this.natureofBusiness = guarantorRecord.Nature_of_Business__c; 
        if(relationshipRecord != null){
            this.contigent = relationshipRecord.Percent_of_Ownership__c != null ? String.valueOf(relationshipRecord.Percent_of_Ownership__c) : null;   
            this.primaryGuarantor = relationshipRecord.Primary_Guarantor__c;
        }else{
            this.primaryGuarantor = 'Yes';
        }
        this.dOB = guarantorRecord.Birthdate != null ? String.valueOf(guarantorRecord.Birthdate) : null; 
        if(guarantorRecord.SSN__c != null){
          this.socialSecurity = guarantorRecord.SSN__c;    
        }
        else{
            this.socialSecurity = '';
        }
        this.guarantor = guarantorRecord.Id; 
        this.recordTypeId = guarantorRecord.RecordTypeId;
        if(!guarantorRecord.Guarantor_Address__r.isEmpty()){
            this.otherCity = guarantorRecord.Guarantor_Address__r.get(0).City__c;
            this.otherCounty = guarantorRecord.Guarantor_Address__r.get(0).County__c;
            this.otherState = guarantorRecord.Guarantor_Address__r.get(0).State__c;
            this.otherStreetAddress = guarantorRecord.Guarantor_Address__r.get(0).Street_Address_1__c;
            this.otherZip = guarantorRecord.Guarantor_Address__r.get(0).Postal_Code__c;
        }else{
            this.otherCity = '';
            this.otherCounty = '';
            this.otherState = '';
            this.otherStreetAddress = '';
            this.otherZip = '';
        }
        this.isCreatedByMe  = true;
        if(guarantorRecord.CreatedById == LOGIN_USER_ID){
            this.isCreatedByMe = true;
        }else{
            this.isCreatedByMe = false;
            if(String.isEmpty(this.dOB)){
                this.ismaskedDobRequired = true;
            }else{
                this.ismaskedDobRequired = false;
            }
            if(String.isEmpty(this.socialSecurity)){
                this.ismaskedSocialSecurityRequired = true;
            }else{
                this.ismaskedSocialSecurityRequired = false;
            }
            if(String.isEmpty(this.mailingStreetGurantor)){
                this.ismaskedMailingStreetRequired = true;
            }else{
                this.ismaskedMailingStreetRequired = false;
            }
            if(String.isEmpty(this.otherStreetAddress)){
                this.ismaskedOtherStreetAddressRequired = true;
            }else{
                this.ismaskedOtherStreetAddressRequired = false;
            }
            if(String.isEmpty(this.homePhoneGuarantor)){
                this.ismaskedHomePhoneGuarantorRequired = true;
            }else{
                this.ismaskedHomePhoneGuarantorRequired = false;
            }
            if(String.isEmpty(this.emailAddressGuarantor)){
                this.ismaskedEmailAddressGuarantorRequired = true;
            }else{
                this.ismaskedEmailAddressGuarantorRequired = false;
            }
        }
        this.maskedDob = this.dOB != null ? CVFSerciceUtil.emcryptedData(this.dOB,'DOB') : null;  
        this.maskedSocialSecurity = this.socialSecurity != null ? CVFSerciceUtil.emcryptedData(this.socialSecurity,'SSN') : null;
        this.maskedMailingStreetGurantor = this.mailingStreetGurantor != null ? CVFSerciceUtil.emcryptedData(this.mailingStreetGurantor,'Street') : null;
        this.maskedOtherStreetAddress = this.otherStreetAddress != null ? CVFSerciceUtil.emcryptedData(this.otherStreetAddress,'Street') : null;
        this.maskedHomePhoneGuarantor = this.homePhoneGuarantor != null ? CVFSerciceUtil.emcryptedData(this.homePhoneGuarantor,'Full') : null;
        this.maskedEmailAddressGuarantor = this.emailAddressGuarantor != null ? CVFSerciceUtil.emcryptedData(this.emailAddressGuarantor,'Full') : null;
        this.maskedFirstNameGuarantor = this.firstNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.firstNameGuarantor,'Full') : null;
        this.maskedMiddleNameGuarantor = this.middleNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.middleNameGuarantor,'Full') : null;
        this.maskedLastNameGuarantor = this.lastNameGuarantor != null ? CVFSerciceUtil.emcryptedData(this.lastNameGuarantor,'Full') : null;      
        
    }
    
    public GuarantorWrapper(Account guarantorRecord, Relationship__c relationshipRecord){
        this.isCorporate = true;
        this.companyName = guarantorRecord.Name;  
        this.fullSearchName = guarantorRecord.Name;
        if(!guarantorRecord.Contacts.isEmpty()){
            this.firstNameGuarantor = guarantorRecord.Contacts.get(0).FirstName;
            this.middleNameGuarantor = guarantorRecord.Contacts.get(0).MiddleName;
            this.lastNameGuarantor = guarantorRecord.Contacts.get(0).LastName;
            this.emailAddressGuarantor = guarantorRecord.Contacts.get(0).Email;
        }else{
            this.firstNameGuarantor = '';
            this.middleNameGuarantor = '';
            this.lastNameGuarantor = '';
            this.emailAddressGuarantor = '';
        }  
        if(guarantorRecord.BillingStreet != null){
            this.mailingStreetGurantor = guarantorRecord.BillingStreet;
        }
        else{
            this.mailingStreetGurantor = '';
        }
        if(guarantorRecord.BillingCountry != null){
            this.mailingCountryGuarantor = guarantorRecord.BillingCountry;   
        }
        else{
            this.mailingCountryGuarantor = '';
        }
        if(guarantorRecord.BillingCity != null){
             this.mailingCityGuarantor = guarantorRecord.BillingCity;   
        }
        else{
            this.mailingCityGuarantor = '';
        }
        if(guarantorRecord.BillingState != null){
            this.mailingStateGuarantor = guarantorRecord.BillingState;   
        
        }
        else{
             this.mailingStateGuarantor = '';
        }
         if(guarantorRecord.BillingPostalCode != null){
             this.mailingPostalCodeGuarantor = guarantorRecord.BillingPostalCode;   
         }
        else{
            this.mailingPostalCodeGuarantor = '';
        }
           
        this.homePhoneGuarantor = guarantorRecord.Phone;
        this.faxGuarantor = guarantorRecord.Fax;   
        if(guarantorRecord.Federal_Tax_ID__c != null){
          this.federalTaxId = guarantorRecord.Federal_Tax_ID__c;   
         }
        else{
            this.federalTaxId = '';
        }
        this.natureofBusiness = guarantorRecord.Industry;   
        if(relationshipRecord != null){
            this.contigent = relationshipRecord.Percent_of_Ownership__c != null ? String.valueOf(relationshipRecord.Percent_of_Ownership__c) : null;   
            this.primaryGuarantor = relationshipRecord.Primary_Guarantor__c;
        }else{
            this.primaryGuarantor = 'Yes';
        }
        this.guarantor = guarantorRecord.Id; 
        this.recordTypeId = guarantorRecord.RecordTypeId;
        this.guarantorType = 'Corporate';
        this.existingId = guarantorRecord.Id;
        
    }
    
    private String getValues(String stringVal){
        if(!String.isEmpty(stringVal)){
            return stringVal;
        }else{
            return '';
        }
    }
}