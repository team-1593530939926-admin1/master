public class FCSAuthProvider extends Auth.AuthProviderPluginClass{

    @TestVisible
    private static final String RESOURCE_CALLBACK = '/services/authcallback/';
    @TestVisible
    private static final String DEFAULT_TOKEN_TYPE = 'Bearer';
    @TestVisible
    private static final String ENCODING_XML = 'application/x-www-form-urlencoded;charset=UTF-8';
    @TestVisible
    private static final String ENCODING_JSON = 'application/json';
    @TestVisible
    private static final String DUMMY_CODE = '999';
    @TestVisible
    private static final String DOUBLEQUOTE = '"';

    // This class is dependant on this Custom Metadata Type created to hold custom parameters
    @TestVisible
    private static final String CUSTOM_MDT_NAME = 'FCS_Auth_Provider__mdt'; 
    @TestVisible
    private static final String CMT_FIELD_CALLBACK_URL = 'Callback_URL__c';
    @TestVisible
    private static final String CMT_FIELD_PROVIDER_NAME = 'Auth_Provider_Name__c';
    @TestVisible
    private static final String CMT_FIELD_AUTHTOKEN_URL = 'Access_Token_URL__c';
    @TestVisible
    private static final String CMT_FIELD_CLIENT_ID = 'Client_Id__c';
    @TestVisible
    private static final String CMT_FIELD_CLIENT_SECRET = 'Client_Secret__c';
    @TestVisible
    private static final String CMT_FIELD_USE_JSON = 'Use_JSON_Encoding__c';
    @TestVisible
    private static final String CMT_FIELD_SCOPE = 'Scope__c';
    @TestVisible
    private static final String CMT_FIELD_USERNAME = 'Username__c';
    @TestVisible
    private static final String CMT_FIELD_USER_PASSWORD = 'User_Password__c';

    @TestVisible
    private static final String GRANT_TYPE_PARAM = 'grant_type';
    @TestVisible
    private static final String CLIENT_ID_PARAM = 'client_id';
    @TestVisible
    private static final String CLIENT_SECRET_PARAM = 'client_secret';
    @TestVisible
    private static final String SCOPE_PARAM = 'scope';
    @TestVisible
    private static final String GRANT_TYPE_CLIENT_CREDS = 'password';
    @TestVisible
    private static final String USERNAME = 'username';
    @TestVisible
    private static final String USER_PASSWORD = 'password';

    private static final String EQUAL_OPERATOR = '=';
    private static final String COLON = ':';
    private static final String EMPTY_SPACE = '';
    private static final String AMPERSAND = '&';
    private static final String COMMA_WITH_SPACE = ', ';


    /**
     Added Constructor purely for debugging purposes to have visibility as to when the class
     is being instantiated.
    **/
    public FCSAuthProvider() {
        super();
        System.debug(LoggingLevel.INFO, 'Constructor called');
    }
    
    
    /**
        Name of custom metadata type to store this auth provider configuration fields
        This method is required by its abstract parent class.
    **/
    public String getCustomMetadataType() {
        return CUSTOM_MDT_NAME;
    } 
    
    /**
    Initiate callback. No End User authorization required in this flow so skip straight to the Token request.
    The interface requires the callback url to be defined. 
    Eg: https://test.salesforce.com/services/authcallback/<authprovidername>
    **/
    public PageReference initiate(Map<String,String> config, String stateToPropagate) {
        System.debug(LoggingLevel.INFO, 'initiate');

        final PageReference pageRef = new PageReference(getCallbackUrl(config)); //NOSONAR
        pageRef.getParameters().put('state',stateToPropagate);
        pageRef.getParameters().put('code',DUMMY_CODE); // Empirically found this is required, but unused
        System.debug(LoggingLevel.INFO, pageRef.getUrl());
        return pageRef;
    } 

    /**
      This method composes the callback URL automatically UNLESS it has been overridden through Configuration.
      Normally one should not override the callback URL, but it's there in case the generated URL doesn't work.
    **/
    private String getCallbackUrl(Map<String,String> config) {
        // https://{salesforce-hostname}/services/authcallback/{urlsuffix}
        final String overrideUrl = config.get(CMT_FIELD_CALLBACK_URL);
        final String generatedUrl = URL.getSalesforceBaseUrl().toExternalForm() + RESOURCE_CALLBACK + config.get('Auth_Provider_Name__c');

        return String.isEmpty(overrideUrl) ? generatedUrl : overrideUrl;
    }
    
    /**
    Handle callback (from initial loop back "code" step in the flow).
    In the Client Credentials flow, this method retrieves the access token directly.
    Required by parent class.
    Error handling here is a bit painful as the UI never displays the exception or error message 
    supplied here.  The exception is thrown for Logging/Debugging purposes only. 
    **/
    public Auth.AuthProviderTokenResponse handleCallback(Map<String,String> config, Auth.AuthProviderCallbackState state ) {
        System.debug(LoggingLevel.INFO, 'handleCallback');
        final TokenResponse response = retrieveToken(config);
		
        if (response.isError()) {
            throw new TokenException(response.getErrorMessage());
        }
        return new Auth.AuthProviderTokenResponse(config.get(CMT_FIELD_PROVIDER_NAME)
                                                  , response.access_token
                                                  , config.get(CMT_FIELD_CLIENT_SECRET) // No Refresh Token
                                                  , state.queryParameters.get('state'));
    } 
    
    /**
        Refresh is required by the parent class and it's used if the original Access Token has expired.
        In the Client Credentials flow, there is no Refresh token, so its implementation is exactly the
        same as the Initiate() step.
    **/
    public override Auth.OAuthRefreshResult refresh(Map<String,String> config, String refreshToken) {
        System.debug(LoggingLevel.INFO, 'refresh');
        final TokenResponse response = retrieveToken(config); 
        if (response.isError()) {
            throw new TokenException(response.getErrorMessage());
        }
        return new Auth.OAuthRefreshResult(response.access_token, response.token_type);
    }

       
    /**
        getUserInfo is required by the Parent class, but not fully supported by this provider.
        Effectively the Client Credentials flow is only useful for Server-to-Server API integrations
        and cannot be used for other contexts such as a Registration Handler for Communities.
     **/
    public Auth.UserData getUserInfo(Map<String,String> config, Auth.AuthProviderTokenResponse response) {
        System.debug(LoggingLevel.INFO, 'getUserInfo-was-called');
        final TokenResponse token = retrieveToken(config);

        final Auth.UserData userData = new Auth.UserData(
              null // identifier
            , null // firstName
            , null // lastName
            , null // fullName
            , null // email
            , null // link
            , null // userName
            , null  //locale
            , config.get(CMT_FIELD_PROVIDER_NAME) //provider
            , null // siteLoginUrl
            , new Map<String,String>());


        return userData;
    }
    
    
    /**
       Private method that gets the Auth Token using the Client Credentials Flow.
    **/
     private TokenResponse retrieveToken(Map<String,String> config) {
         
        System.debug(LoggingLevel.INFO, 'retrieveToken');

        final Boolean useJSONEncoding = Boolean.valueOf(config.get(CMT_FIELD_USE_JSON));
        final HttpRequest req = new HttpRequest();
        final PageReference endpoint = new PageReference(config.get(CMT_FIELD_AUTHTOKEN_URL)); //NOSONAR -- Protected by RemoteSite Setting
      

        // Determine whether or not to use JSON encoding
        final String encoding = useJSONEncoding ? ENCODING_JSON : ENCODING_XML;
        final String encodedParams = encodeParameters(config,encoding);
		
         System.debug('encodedParams>> ' + encodedParams);
        req.setEndpoint(endpoint.getUrl()); 
        req.setHeader('Content-Type',encoding); 
        req.setMethod('POST'); 
        req.setBody(encodedParams);
		
        final HTTPResponse res = new Http().send(req); 

        final Integer statusCode = res.getStatusCode();

        if ( statusCode == 200) {
            TokenResponse token =  deserializeToken(res.getBody());
            // Ensure values for key fields
            token.token_type = (token.token_type == null) ? DEFAULT_TOKEN_TYPE : token.token_type;
            return token;

        } else  {
            return deserializeToken(res.getBody());
        }

    }
    
    //deserialise response and return token
    @testVisible
    private TokenResponse deserializeToken(String responseBody) {
        System.debug(LoggingLevel.INFO, 'token response:' + responseBody);
        
        // use default parsing for everything we can.
        if (responseBody == null) {
            throw new TokenException('Server is busy please try again.');
        }
        TokenResponse parsedResponse = (TokenResponse) System.JSON.deserialize(responseBody, TokenResponse.class);
		
        return parsedResponse;
    }

    /**
        Conditionally encode parameters as URL-style or JSON
    **/
    @testVisible
    private String encodeParameters(Map<String,String> config,String encoding) {

        // Pull out the subset of configured parameters that will be sent
        Map<String,String> params = new Map<String,String>();
        params.put(GRANT_TYPE_PARAM,GRANT_TYPE_CLIENT_CREDS);
        params.put(CLIENT_ID_PARAM, config.get(CMT_FIELD_CLIENT_ID));
        params.put(CLIENT_SECRET_PARAM, config.get(CMT_FIELD_CLIENT_SECRET));
        params.put(USERNAME, config.get(CMT_FIELD_USERNAME));
        params.put(USER_PASSWORD, config.get(CMT_FIELD_USER_PASSWORD));  
        final String scope = config.get(CMT_FIELD_SCOPE);
        if (!String.isEmpty(scope)) {
            params.put(SCOPE_PARAM,scope);
        }

        return encoding == ENCODING_JSON ? encodeAsJSON(params) : encodeAsURL(params);
    }

    private String encodeAsJSON(Map<String,String> params) {
        String output = '{';
        for (String key : params.keySet()) {
            output += (output == '{' ? EMPTY_SPACE : COMMA_WITH_SPACE);
            output += DOUBLEQUOTE + key + DOUBLEQUOTE + COLON;
            output += DOUBLEQUOTE + params.get(key) + DOUBLEQUOTE;
        }
        output += '}';
        return output;
    }

    private String encodeAsURL(Map<String,String> params) {
        String output = '';
        for (String key : params.keySet()) {
            output += (String.isEmpty(output) ? EMPTY_SPACE : AMPERSAND);
            output += key + EQUAL_OPERATOR + params.get(key);
        }
        return output;
    }

    /**
    OAuth Response is a JSON body like this on a Successful call
    {
      "token_type" : "BearerToken",
      "access_token" : "kRxqmPr2b223uzTUGnndQhXWv8F4",
      "expires_in" : "3599",
    }
    On failure, the following structure from FCS API 400 and 401
    { 
      "error" : "Invalid_grant"
    }
    { 
      "Message”:”Authorization has been denied for this request."
    }
    The following response class is the Union of all responses
    **/

    public class TokenResponse {
        
        public String token_type {get;set;}
        public String access_token {get;set;}
        public String expires_in {get;set;}

        public String Error {get; set;}
        public String ErrorCode {get; set;}
        public String Message {get; set;}

        public Boolean isError() {
            return Error != null || Message != null;
        }

        public String getErrorMessage() {
            if (Error != null) {
                return ErrorCode;
            }
            if (Message != null) {
                return Message;
            }
            return null;
        }
    }

    public class Detail {
        public String Message {get;set;}
    }

    /**
        Custom exception type so we can wrap and rethrow
    **/
    public class TokenException extends Exception {

    }
}