public with sharing class BankReferenceDAO implements IBankReferenceDAO 
{
    public Interface IBankReferenceDAO
    {
        List<Bank_Reference__c> insertBankReference (List<Bank_Reference__c> listofBankDetails);
        List<Bank_Reference__c> getBankRecordByApplicationId(String applicationId);
    }
    
    public static List<Bank_Reference__c> insertBankReference (List<Bank_Reference__c> listofBankDetails)
    {
        insert listofBankDetails;
        return listofBankDetails;
    }
    
    public static List<Bank_Reference__c> getBankRecordByApplicationId(String applicationId)
    {
        return [Select Id, CreatedById, Bank_Name__c, Address_Line_1__c, City__c, State__c, Zip__c, Bank_Contact_Person__c,
               Phone_Number__c, Account_Number__c, Country__c
               FROM Bank_Reference__c];
    }
}