@isTest
private class FCSEntityVerificationsTest {
    
    private static final String ENTITY_ID = 'C1905389';
    private static final String ENTITY_STANDING = 'Active';
    private static final String ENTITY_NAME = 'MCDONALD';
    private static final String PARTY_TYPE = 'Officers';
    private static final String TITLE = 'CEO'; 
    
    private static final String LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE = 'List Should not be Empty';
    private static final String VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE = 'Value Should not be Empty or null';
    private static final String SHOULD_GET_RESULTS_ASSERT_MESSAGE = 'Should get Results';

    @TestSetup
    static void testSetup(){
        TestUtil.createAccountData(1, true);
        TestUtil.createLeadData();
    }

    private static Account getCompany() {
        return [SELECT Id, Name FROM Account LIMIT 1];
    }

    private static List<Lead> getLeads() {
        return [SELECT Id, Status, LastName, Company, State, Source__c, Location__c, Street, PostalCode, City, Country, IsConverted FROM Lead LIMIT 1];
    }

    private static FCS_Entity_Information__c getEntityInformation() {
        FCS_Entity_Information__c fcsEntityInfo = [SELECT Id, Entity_Id__c, Entity_Name__c,
            Domicile_Country__c, Domicile_State__c, Company__c, Lead__c FROM FCS_Entity_Information__c LIMIT 1];
        return fcsEntityInfo;
    }

    private static List<FCS_Entity_Party_Agent__c> getEntityPartyAgentInformation() {
        List<FCS_Entity_Party_Agent__c> listOfPartyAgentInfo = [SELECT Id, Name__c, Title__c, Type__c, 
            Entity_Information__c, RecordTypeId FROM FCS_Entity_Party_Agent__c LIMIT 2];
        return listOfPartyAgentInfo;
    }

    private static List<Communication_Address__c> getCommunicationAddresses() {
        List<Communication_Address__c> listOfAddresses = [SELECT City__c, Country_Parish__c, County__c, 
            Postal_Code__c, State__c, Street_Address_1__c, Entity_Party_Agent__c, 
            Street_Address_2__c FROM Communication_Address__c LIMIT 3];
        return listOfAddresses;
    }

    @isTest
    private static void generateEntityObjectFromWrapperTest() {

        Test.startTest();
            List<FCSEntityVerifications.FCSEntityVerificationWrapper> fcsEntityVerificationWrapper = FCSEntityVerifications.generateEntityObjectFromWrapper(FCSTestDataFactory.generateFCSBusinessDetailsWrapper());
        Test.stopTest();

        System.assertEquals(1, fcsEntityVerificationWrapper.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(2, fcsEntityVerificationWrapper.get(0).listOfEntityPartyOrAgent.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(2, fcsEntityVerificationWrapper.get(0).listOfEntityPartyOrAgent.get(0).listOfPartyOrAgentCommunicationAddress.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(1, fcsEntityVerificationWrapper.get(0).listOfEntityCommunicationAddress.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationWrapper.get(0).entityInformation, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationWrapper.get(0).listOfEntityPartyOrAgent.get(0).entityPartyOrAgent, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void generateAddressSObjectFromResponseTest() {

        Test.startTest();
            List<Communication_Address__c> listOfAddress = FCSEntityVerifications.generateAddressSObjectFromResponse(FCSTestDataFactory.generateAddressWrapper());
        Test.stopTest();

        System.assertEquals(1, listOfAddress.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('ENCINITAS', listOfAddress.get(0).City__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('United States', listOfAddress.get(0).County__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('92024', listOfAddress.get(0).Postal_Code__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('CA', listOfAddress.get(0).State__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(0, listOfAddress.size(), SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void generateEntityPartySObjectFromResponseTest() {

        Test.startTest();
            List<FCSEntityVerifications.FCSPartyAgentWrapper> listOfPartyAgentWrapper = FCSEntityVerifications.generateEntityPartySObjectFromResponse(FCSTestDataFactory.generateFCSPartiesWrapper());
        Test.stopTest();

        System.assertEquals(1, listOfPartyAgentWrapper.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(PARTY_TYPE, listOfPartyAgentWrapper.get(0).entityPartyOrAgent.Type__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(TITLE, listOfPartyAgentWrapper.get(0).entityPartyOrAgent.Title__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(0, listOfPartyAgentWrapper.size(), SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void generateEntityAgentSObjectFromResponseTest() {

        Test.startTest();
            List<FCSEntityVerifications.FCSPartyAgentWrapper> listOfPartyAgentWrapper = FCSEntityVerifications.generateEntityAgentSObjectFromResponse(FCSTestDataFactory.generateFCSAgentsWrapper());
        Test.stopTest();

        System.assertEquals(1, listOfPartyAgentWrapper.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_NAME, listOfPartyAgentWrapper.get(0).entityPartyOrAgent.Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(0, listOfPartyAgentWrapper.size(), SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    private static List<FCSEntityVerifications.FCSEntityVerificationWrapper> getFCSEntityVerificationWrapper(FCS_Entity_Information__c fcsEntityInfo) {
        List<Communication_Address__c> listOfAddressForfcsEntityInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);

        List<FCSEntityVerifications.FCSPartyAgentWrapper> listOfEntityPartyOrAgent = new List<FCSEntityVerifications.FCSPartyAgentWrapper>();
        
        FCSEntityVerifications.FCSPartyAgentWrapper fcsPartyWrapper = new FCSEntityVerifications.FCSPartyAgentWrapper();
        FCS_Entity_Party_Agent__c fcsEntityParty = FCSTestDataFactory.generateFCSPartyInfo();
        List<Communication_Address__c> listOfAddressForFcsPartyInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);
        fcsPartyWrapper.entityPartyOrAgent = fcsEntityParty;
        fcsPartyWrapper.listOfPartyOrAgentCommunicationAddress = listOfAddressForFcsPartyInfo;
        listOfEntityPartyOrAgent.add(fcsPartyWrapper);

        FCSEntityVerifications.FCSPartyAgentWrapper fcsAgentWrapper = new FCSEntityVerifications.FCSPartyAgentWrapper();
        FCS_Entity_Party_Agent__c fcsEntityAgent = FCSTestDataFactory.generateFCSAgentInfo();
        List<Communication_Address__c> listOfAddressForFcsAgentInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);
        fcsAgentWrapper.entityPartyOrAgent = fcsEntityAgent;
        fcsAgentWrapper.listOfPartyOrAgentCommunicationAddress = listOfAddressForFcsAgentInfo;
        listOfEntityPartyOrAgent.add(fcsAgentWrapper);

        List<FCSEntityVerifications.FCSEntityVerificationWrapper> listOfEntityVerificationWrapper = new List<FCSEntityVerifications.FCSEntityVerificationWrapper>();
        FCSEntityVerifications.FCSEntityVerificationWrapper fcsEntityVerificationWrapper = new FCSEntityVerifications.FCSEntityVerificationWrapper(fcsEntityInfo, listOfAddressForfcsEntityInfo, listOfEntityPartyOrAgent);
        
        listOfEntityVerificationWrapper.add(fcsEntityVerificationWrapper);
        return listOfEntityVerificationWrapper;
    }

    @isTest
    private static void saveEntityVerificationInformationByCompanyIdTest() {
        Account company = getCompany();
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        List<FCSEntityVerifications.FCSEntityVerificationWrapper> listOfEntityVerificationWrapper = getFCSEntityVerificationWrapper(fcsEntityInfo);

        Test.startTest();
            FCSEntityVerifications.saveEntityVerificationInformation(listOfEntityVerificationWrapper, company.Id);
        Test.stopTest();
        
        FCS_Entity_Information__c fcsEntityInfo1 = getEntityInformation(); 

        System.assertEquals(fcsEntityInfo.Entity_Id__c, fcsEntityInfo1.Entity_Id__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityInfo1.Entity_Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityInfo1.Domicile_Country__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityInfo1.Domicile_State__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityInfo1, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void saveEntityVerificationInformationByLeadIdTest() {
        Lead lead = getLeads().get(0);
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        List<FCSEntityVerifications.FCSEntityVerificationWrapper> listOfEntityVerificationWrapper = getFCSEntityVerificationWrapper(fcsEntityInfo);

        Test.startTest();
            FCSEntityVerifications.saveEntityVerificationInformation(listOfEntityVerificationWrapper, lead.Id);
        Test.stopTest();
        
        FCS_Entity_Information__c fcsEntityInfo1 = getEntityInformation(); 

        System.assertEquals(fcsEntityInfo.Entity_Id__c, fcsEntityInfo1.Entity_Id__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityInfo1.Entity_Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityInfo1.Domicile_Country__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityInfo1.Domicile_State__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityInfo1, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    private static FCS_Entity_Information__c insertEntityVerificationObjectWithItsRelatedRecords(String recordId) {
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        if (String.isNotBlank(recordId)) {

			if (Id.valueOf(recordId).getSobjectType() == Account.getSObjectType()) {
                fcsEntityInfo.Company__c = recordId;
                insert fcsEntityInfo;
			}

			if (Id.valueOf(recordId).getSobjectType() == Lead.getSObjectType()) {
                fcsEntityInfo.Lead__c = recordId;
                insert fcsEntityInfo;
            }
            fcsEntityInfo = getEntityInformation();
            List<Communication_Address__c> listOfAddressForfcsEntityInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);
            Communication_Address__c addr = listOfAddressForfcsEntityInfo.get(0);
            addr.Entity_Information__c = fcsEntityInfo.Id;
            insert addr;
            FCS_Entity_Party_Agent__c fcsEntityParty = FCSTestDataFactory.generateFCSPartyInfo();
            fcsEntityParty.Entity_Information__c = fcsEntityInfo.Id;
            insert fcsEntityParty;

            FCS_Entity_Party_Agent__c fcsEntityAgent = FCSTestDataFactory.generateFCSAgentInfo();
            fcsEntityAgent.Entity_Information__c = fcsEntityInfo.Id;
            insert fcsEntityAgent;
        }
        return fcsEntityInfo;
    }

    @isTest
    private static void getEntityInformationWithInMonthByCompanyTest() {
        Account company = getCompany();
        FCS_Entity_Information__c fcsEntityInfo = insertEntityVerificationObjectWithItsRelatedRecords(company.Id);

        Test.startTest();
            FCSEntityVerifications.FCSEntityVerificationWrapper fcsEntityVerificationWrapper = FCSEntityVerifications.getEntityInformationWithInMonthByCompany(company.Id);
        Test.stopTest();

        System.assertEquals(fcsEntityInfo.Company__c, fcsEntityVerificationWrapper.entityInformation.Company__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityVerificationWrapper.entityInformation.Entity_Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityVerificationWrapper.entityInformation.Domicile_Country__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityVerificationWrapper.entityInformation.Domicile_State__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationWrapper, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void getEntityInformationWithInMonthByLeadTest() {
        Lead leadInstance = getLeads().get(0);
        FCS_Entity_Information__c fcsEntityInfo = insertEntityVerificationObjectWithItsRelatedRecords(leadInstance.Id);

        Test.startTest();
            FCSEntityVerifications.FCSEntityVerificationWrapper fcsEntityVerificationWrapper = FCSEntityVerifications.getEntityInformationWithInMonthByLead(leadInstance.Id);
        Test.stopTest();

        System.assertEquals(fcsEntityInfo.Lead__c, fcsEntityVerificationWrapper.entityInformation.Lead__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityVerificationWrapper.entityInformation.Entity_Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityVerificationWrapper.entityInformation.Domicile_Country__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityVerificationWrapper.entityInformation.Domicile_State__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationWrapper, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void getPartiesAddressByPartiesIdTest() {
        FCS_Entity_Party_Agent__c fcsEntityParty = FCSTestDataFactory.generateFCSPartyInfo();
        insert fcsEntityParty;
        FCS_Entity_Party_Agent__c fcsEntityParty1 = getEntityPartyAgentInformation().get(0);
        List<Communication_Address__c> listOfAddressForFcsPartyInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);
        Communication_Address__c addr = listOfAddressForFcsPartyInfo.get(0);
        addr.Entity_Party_Agent__c = fcsEntityParty1.Id;
        insert addr;

        Test.startTest();
            List<FCSEntityVerifications.FCSPartyAgentWrapper> fcsPartyAgentWrapper = FCSEntityVerifications.getPartiesAddressByPartiesId(new List<FCS_Entity_Party_Agent__c>{fcsEntityParty});
        Test.stopTest();

        System.assertEquals(1, fcsPartyAgentWrapper.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals(1, fcsPartyAgentWrapper.get(0).listOfPartyOrAgentCommunicationAddress.size(), LIST_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('TEST1', fcsPartyAgentWrapper.get(0).entityPartyOrAgent.Name__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('CEO1', fcsPartyAgentWrapper.get(0).entityPartyOrAgent.Title__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertEquals('type1', fcsPartyAgentWrapper.get(0).entityPartyOrAgent.Type__c, VALUE_SHOULD_NOT_BE_EMPTY_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsPartyAgentWrapper.get(0), SHOULD_GET_RESULTS_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsPartyAgentWrapper.get(0).entityPartyOrAgent, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void isSFEntityDataAvailableInSFForThisMonthRangeByCompanyIdTest() {
        Account company = getCompany();
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        fcsEntityInfo.Company__c = company.Id;
        insert fcsEntityInfo;

        Test.startTest();
            Boolean isSFEntityDataAvailable = FCSEntityVerifications.isSFEntityDataAvailableInSFForThisMonthRange(fcsEntityInfo.Company__c);
        Test.stopTest();

        System.assertEquals(true, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
        System.assertNotEquals(false, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void isSFEntityDataAvailableInSFForThisMonthRangeByLeadIdTest() {
        Lead leadInstance = getLeads().get(0);
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        fcsEntityInfo.Lead__c = leadInstance.Id;
        insert fcsEntityInfo;

        Test.startTest();
            Boolean isSFEntityDataAvailable = FCSEntityVerifications.isSFEntityDataAvailableInSFForThisMonthRange(leadInstance.Id);
        Test.stopTest();

        System.assertEquals(true, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
        System.assertNotEquals(false, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }

    @isTest
    private static void isSFEntityDataAvailableInSFForThisMonthRangeByDifferentObjectIdTest() {
        TestUtil.createOpportunityData();
        Opportunity opp = [SELECT Id, Name FROM Opportunity];
        
        Test.startTest();
            Boolean isSFEntityDataAvailable = FCSEntityVerifications.isSFEntityDataAvailableInSFForThisMonthRange(opp.Id);
        Test.stopTest();

        System.assertEquals(false, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
        System.assertNotEquals(true, isSFEntityDataAvailable, SHOULD_GET_RESULTS_ASSERT_MESSAGE);
    }
}