public class BankDetailsWrapper {  
    @AuraEnabled
    public String existingId{get;set;} 
    @AuraEnabled
    public Boolean isCreatedByMe{get;set;}
    @AuraEnabled
    public String maskedAccountNumber{get;set;}    
    @AuraEnabled
    public Boolean isDisable{get;set;}    
    @AuraEnabled
    public Integer rowId{get;set;}
    @AuraEnabled
    public Integer count{get;set;}
    @AuraEnabled
    public string bankName{get;set;}
    @AuraEnabled
    public string addressLine1{get;set;}
    @AuraEnabled
    public string city {get;set;}
    @AuraEnabled
    public string state{get;set;}
    @AuraEnabled
    public string zip {get;set;}
    @AuraEnabled
    public string bankContactPerson{get;set;}
    @AuraEnabled
    public string phoneNumber{get;set;}
    @AuraEnabled
    public string accountNumber{get;set;}
    @AuraEnabled
    public string country{get;set;}
    @AuraEnabled
    public string bankId{get;set;}
    public Bank_Reference__c dealBankDetails{get;set;}
    private static final Id LOGIN_USER_ID = UserInfo.getUserId();
    
    public BankDetailsWrapper(BankDetailsWrapper bankInfoWrapper, Account accountInfo){
        Bank_Reference__c bankInfo = new Bank_Reference__c(Bank_Details__r = new Account(External_System_Id__c = accountInfo.External_System_Id__c));
        bankInfo.Account_Number__c = bankInfoWrapper.accountNumber;
        bankInfo.Address_Line_1__c = bankInfoWrapper.addressLine1;
        bankInfo.Bank_Contact_Person__c = bankInfoWrapper.bankContactPerson;
        bankInfo.Bank_Name__c = bankInfoWrapper.bankName;
        bankInfo.Name = bankInfoWrapper.bankName;
        bankInfo.City__c = bankInfoWrapper.city;
        bankInfo.Country__c = bankInfoWrapper.country;
        bankInfo.Phone_Number__c = bankInfoWrapper.phoneNumber;
        bankInfo.State__c = bankInfoWrapper.state;
        bankInfo.Zip__c = bankInfoWrapper.zip;
        if(!String.isEmpty(bankInfoWrapper.existingId)){
            bankInfo.Id = bankInfoWrapper.existingId;
            this.existingId = bankInfoWrapper.existingId;
        }
        this.dealBankDetails = bankInfo;
    }
    public BankDetailsWrapper(){}

    public BankDetailsWrapper(Integer count){
        this.rowId = count;
        this.count = count + 1;
    }
    
    public BankDetailsWrapper(Bank_Reference__c bankRecord){
        this.bankName = bankRecord.Bank_Name__c;
        this.addressLine1 = bankRecord.Address_Line_1__c; 
        this.city  = bankRecord.City__c;   
        this.state = bankRecord.State__c;   
        this.zip  = bankRecord.Zip__c;  
        this.bankContactPerson = bankRecord.Bank_Contact_Person__c;   
        this.phoneNumber = bankRecord.Phone_Number__c;   
        this.accountNumber = bankRecord.Account_Number__c;      
        this.country = bankRecord.Country__c;
        this.bankId = bankRecord.Id;
        this.existingId = bankRecord.Id;
        this.isCreatedByMe  = true;
        if(bankRecord.CreatedById == LOGIN_USER_ID){
            this.isCreatedByMe = true;
        }else{
            this.isCreatedByMe = false;
        }  
        this.maskedAccountNumber = this.accountNumber != null ? CVFSerciceUtil.emcryptedData(this.accountNumber,'Full') : null;
    }
    
    public BankDetailsWrapper(BankDetailsWrapper bankRecordWrapper , Boolean isVisible){
        this.bankName = bankRecordWrapper.bankName;
        this.addressLine1 = bankRecordWrapper.addressLine1; 
        this.city  = bankRecordWrapper.city;   
        this.state = bankRecordWrapper.state;   
        this.zip  = bankRecordWrapper.zip;  
        this.bankContactPerson = bankRecordWrapper.bankContactPerson;   
        this.phoneNumber = bankRecordWrapper.phoneNumber;   
        this.accountNumber = bankRecordWrapper.accountNumber;      
        this.country = bankRecordWrapper.country;
        this.bankId = bankRecordWrapper.bankId;
        this.existingId = bankRecordWrapper.existingId;
        this.isCreatedByMe  = bankRecordWrapper.isCreatedByMe;
        this.maskedAccountNumber = this.accountNumber != null ? CVFSerciceUtil.emcryptedData(this.accountNumber,'Full') : null;
    }
    
}