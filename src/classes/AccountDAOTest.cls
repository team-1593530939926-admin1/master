@isTest
private class AccountDAOTest
{
    @testSetup  
    private static void testSetupData() 
    { 
        TestUtil.insertBusinessApplicationWrapper();
    }
    
    private static Opportunity getDeal()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity LIMIT 1];
    }
    private static Account getCompany()
    {
        return [SELECT Id, Name, Company_Code__c FROM Account LIMIT 1];
    }
    
    public static Account getTradeReferenceAccount() {
        return [SELECT Id, Name, Trade_Reference__r.Company_Code__c FROM Account WHERE Name='Test tradeReference' LIMIT 1];
    }
    
    @isTest
    private static void getExistingCopmanyDetails() {
        Account objectOfAccount = new Account();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '123';
        insert objectOfAccount;
        
        Test.startTest();
            Account accountInstance = AccountDAO.getExistingCopmanyDetails(objectOfAccount);
        Test.stopTest();

        System.assertEquals(objectOfAccount.Id, accountInstance.Id, 'Id does not exist');
    }
    
    @isTest
    private static void getExistingCopmanyDetailsNullRecord() {

        Test.startTest();
            Account accountInstance = AccountDAO.getExistingCopmanyDetails(null);
        Test.stopTest();

        System.assertEquals(null, accountInstance, 'Id does not exist');        
    }
    
    @isTest
    private static void getExistingCopmanyDetailsRecordNotFound() {
        Account objectOfAccount = new Account();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '123';
        //insert objectOfAccount;
        
        Test.startTest();
            Account accountInstance = AccountDAO.getExistingCopmanyDetails(objectOfAccount);
        Test.stopTest();

        System.assertEquals(null, accountInstance, 'Id does not exist');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdTest() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            Account accountInstance1 = AccountDAO.getGuarantorRecordFromGuarantorId(accountInstance.Id);
        Test.stopTest();

        System.assertEquals(accountInstance.Name, accountInstance1.Name, 'Id does not exist');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdRecordNotFoundTest() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            Account accountInstance1 = AccountDAO.getGuarantorRecordFromGuarantorId('');
        Test.stopTest();

        System.assertEquals(null, accountInstance1, 'Record Not Found.');
    }
    
    @isTest
    private static void getAccountRecordByCompanyIdTest() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            Account accountInstance1 = AccountDAO.getAccountRecordByCompanyId(accountInstance.Id);
        Test.stopTest();

        System.assertEquals(accountInstance.Name, accountInstance1.Name, 'Id does not exist');
    }
    
    @isTest
    private static void getAccountRecordByCompanyIdRecordNotFoundTest() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            Account accountInstance1 = AccountDAO.getAccountRecordByCompanyId('');
        Test.stopTest();

        System.assertEquals(null, accountInstance1, 'Record Not Found.');
    }
    
    @isTest
    private static void testGetListOfAccountsIfIdExists()
    {
        Set<Id> OriginalAccIds = new Set<Id>();
        List<Account> listOfAccounts = [Select id from Account where Name = 'Test Business Account'];
        OriginalAccIds.add(listOfAccounts[0].Id);
        
        Test.startTest();
            List<Account> printListOfAccount = AccountDAO.getListOfAccounts(OriginalAccIds);
        Test.stopTest();

        System.assertEquals(1, printListOfAccount.size(), 'Id does not exist');
    }
    
    @isTest
    private static void testgetListOfCorporateGuarantors()
    {
        Set<Id> OriginalAccIds = new Set<Id>();
        List<Account> listOfAccounts = [Select id from Account where Name = 'Test Business Account'];
        OriginalAccIds.add(listOfAccounts[0].Id);
        
        Test.startTest();
            List<Account> printListOfAccount = AccountDAO.getListOfCorporateGuarantors(OriginalAccIds);
        Test.stopTest();

        System.assertEquals(1, printListOfAccount.size(), 'Id does not exist');
    }
    
    @isTest
    private static void testGetListOfAccountsIfIdNotExists()
    {
        Set<Id> OriginalAccIds = new Set<Id>();
        List<Account> listOfAccounts = [Select id from Account where Name = 'Test Account'];
        
        Test.startTest();
            List<Account> printListOfAccount = AccountDAO.getListOfAccounts(OriginalAccIds);
        Test.stopTest();
        
        System.assertEquals(0, printListOfAccount.size(), 'Id do exist');
    }
    
    @isTest
    private static void testInsertAccountsIfListIsAccepted()
    {
        List<Account> accountsList = new List<Account>();
        Account objectOfAccount = new Account();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '123';
        
        accountsList.add(objectOfAccount);
        
        Test.startTest();
            List<Account> listOfAccounts = AccountDAO.insertAccounts(accountsList);
        Test.stopTest();
        
        System.assertEquals(1, listOfAccounts.size(), 'Account record not inserted');
    }
    
    @isTest
    private static void testUpsertAccountsIfListIsAccepted()
    {
        List<Account> accountsList = new List<Account>();
        Account objectOfAccount = new Account();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '123';
        
        accountsList.add(objectOfAccount);
        
        Test.startTest();
            List<Account> listOfAccounts = AccountDAO.upsertAccounts(accountsList);
        Test.stopTest();
        
        System.assertEquals(1, listOfAccounts.size(), 'Account record not inserted');
    }
    
    @isTest
    private static void testInsertAccountsIfListIsNotAccepted()
    {
        List<Account> accountsList = new List<Account>();
        Account objectOfAccount = new Account();
        
        Test.startTest();
            List<Account> listOfAccounts = AccountDAO.insertAccounts(accountsList);
        Test.stopTest();
        
        System.assertEquals(0, listOfAccounts.size(), 'Account record inserted');
    }
    
    
    @isTest
    private static void testGetAccountRecordFromCompanyCodeIfCompanyCodeIsValid()
    {
        Account accountRecord = getCompany();
       
        Test.startTest();
            Account accList = AccountDAO.getAccountRecordFromCompanyCode(accountRecord.Company_Code__c);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', accList.Name, 'Account record is not found');
    }
    
    @isTest
    private static void testGetAccountRecordFromCompanyCodeIfCompanyCodeIsNotValid()
    {       
        String cCode = 'COMP-00212';    
        
        Test.startTest();
            Account accList = AccountDAO.getAccountRecordFromCompanyCode(cCode);
        Test.stopTest();
        
        System.assertEquals(null, accList, 'Account record found');
    }
    
    @isTest
    private static void testGetAccountRecordFromApplicationIdIfApplicationIdIsValid()
    { 
        Account accountRecord = getTradeReferenceAccount();
        
        Test.startTest();
            List<Account> accList = AccountDAO.getAccountRecordFromApplicationId(accountRecord.Trade_Reference__r.Company_Code__c);
        Test.stopTest();
        
        System.assertEquals(1, accList.size(), 'Account record is not found');
    }
    
    @isTest
    private static void testGetAccountRecordFromApplicationIdIfApplicationIdIsNotValid()
    {
        
        Test.startTest();
            List<Account> accList = AccountDAO.getAccountRecordFromApplicationId('DEAL-0007');
        Test.stopTest();
        
        System.assertEquals(0, accList.size(), 'Account record is not found');
    }
    
    @isTest
    private static void testSearchPositive()
    {
        Test.startTest();
            List<sObject> result = AccountDAO.search('Test Business Account', 'Account','','Business');
        Test.stopTest();
        System.assertEquals(1, result.size());
    }
    
        @isTest
    private static void testSearchNegative()
    {
        Test.startTest();
            List<sObject> result = AccountDAO.search(null, 'Account', '','Business');
        Test.stopTest();
        //System.assertEquals(2, result.size());
    }
}