@isTest
private class ContactDAOTest
{
    @TestSetup
    private static void testSetup() {
        TestUtil.createContactData();
    }
    
    public static Contact getContact() {
        return [SELECT Id, LastName, AccountId FROM Contact LIMIT 1];
    }
    
    @isTest
    private static void searchTest() {
        String searchTerm = 'LastName';
        String myObject = 'Contact';
        String filter = '';
        
        Test.startTest();
        	List<sObject> listOfContacts = ContactDAO.search(searchTerm, myObject, filter,'Crestmark Contact');
        Test.stopTest();
        
        Contact contactInstance = (Contact)listOfContacts.get(0);
        System.assertEquals(1, listOfContacts.size(), 'List can not be empty.');
        System.assertEquals('FirstName 1 LastName 1', contactInstance.Name, 'List can not be empty.');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdTest() {
        Contact contactInstance = getContact();
        
        Test.startTest();
        	Contact contactInstance1 = ContactDAO.getGuarantorRecordFromGuarantorId(contactInstance.Id);
        Test.stopTest();
        
        System.assertEquals(contactInstance.Id, contactInstance1.Id, 'Record can not be null or empty.');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdEmptyRecordTest() {
        Contact contactInstance = getContact();
        
        Test.startTest();
        	Contact contactInstance1 = ContactDAO.getGuarantorRecordFromGuarantorId('');
        Test.stopTest();
        
        System.assertEquals(null, contactInstance1, 'Record must be there.');
    }
    
    @isTest
    private static void searchEmptySearchTermAndFilterTest() {
        String searchTerm = '';
        String myObject = 'Contact';
        String filter = '';
        
        Test.startTest();
        	List<sObject> listOfContacts = ContactDAO.search(searchTerm, myObject, filter,'Crestmark Contact');
        Test.stopTest();
        
        //Contact contactInstance = (Contact)listOfContacts.get(0);
        //System.assertEquals(1, listOfContacts.size(), 'List can not be empty.');
        //System.assertEquals('FirstName 1 LastName 1', contactInstance.Name, 'List can not be empty.');
    }
    
    @isTest
    private static void testInsertContactsIfListIsAccepted()
    {
        List<contact> contactsList = new List<contact>();
        Contact contactRecord = new Contact();
        
        contactRecord.FirstName = 'FirstName';
        contactRecord.LastName = 'LastName';
        contactRecord.MailingCity = 'testCity';
        contactRecord.MailingCountry = 'USA';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingStreet = 'testMailStreet';
        contactRecord.MailingPostalCode = '111';
        contactsList.add(contactRecord);
        
        Test.startTest();
            List<contact> listOfContacts = ContactDAO.insertContacts(contactsList);
        Test.stopTest();
        
        System.assertEquals(1, listOfContacts.size(), 'Contact record is not inserted');
    }
	
    @isTest 
    private static void testInsertContactsDataIfListIsNotAccepted()
    {
        List<contact> contactsList = new List<contact>();
        Contact contactRecord = new Contact();
        
        Test.startTest();
            List<contact> listOfContacts = ContactDAO.insertContacts(contactsList);
        Test.stopTest();
        
        System.assertEquals(0, listOfContacts.size(), 'Contact record is inserted');
    }
    
    @isTest
    private static void testGetContactDetailsByApplicationIdTest()
    {
        Contact contactInstance = getContact();
        
        Test.startTest();
            List<contact> listOfContacts = ContactDAO.getContactDetailsByApplicationId(contactInstance.AccountId);
        Test.stopTest();
        
        System.assertEquals(1, listOfContacts.size());
    }
    
    @isTest
    private static void getListOfPersonalGuarantorTest() {
        Contact contactInstance = getContact();
        
        Test.startTest();
        	List<contact> listOfContacts = ContactDAO.getListOfPersonalGuarantor(new Set<Id>{contactInstance.Id});
        Test.stopTest();
        
        System.assertEquals(1, listOfContacts.size());
    }
}