@isTest
private class ContactTriggerTest {
    
    private static List<Contact> getContact()
    {
        return [Select Id, FirstName, LastName From Contact Limit 1];
    }
    @isTest
    private static void testContactTriggerBeforeInsert(){
        Test.startTest();
            TestUtil.createContactData();
        Test.stopTest();
        
        System.assertEquals(1, getContact().size(), 'Contact Not Inserted.');
    }
    @isTest
    private static void testContactTriggerBeforeUpdate(){
        TestUtil.createContactData();
        Contact updateRecord = getContact()[0];
        updateRecord.FirstName = 'TestName';
        Test.startTest();
            update updateRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getContact()[0].FirstName, 'Contact Not Updated.');
    }
    @isTest
    private static void testContactTriggerBeforeDelete(){
        TestUtil.createContactData();
        Contact deleteRecord = getContact()[0];
        deleteRecord.FirstName = 'TestName';
        Test.startTest();
            delete deleteRecord;
        Test.stopTest();
        
        System.assertEquals(0, getContact().size(), 'Contact Not Deleted.');
    }
    @isTest
    private static void testContactTriggerBeforeUnDelete(){
        TestUtil.createContactData();        
        Contact undeleteRecord = getContact()[0];
		undeleteRecord.FirstName = 'TestName';
        delete undeleteRecord;
        undelete undeleteRecord;
        Test.startTest();
        	update undeleteRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getContact()[0].FirstName, 'Contact Not Deleted.');
    }
}