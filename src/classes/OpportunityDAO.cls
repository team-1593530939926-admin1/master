public with sharing class OpportunityDAO implements IOpportunityDAO {
    
    public  interface IOpportunityDAO 
    {
        List<Opportunity> getListOfOpportunityByCurrentUser(String searchParam, List<String> column, 
        String pageSize, String offset, String sortOrderField, String sortDirection ,String fromDate ,String toDate );
        Opportunity updateOpportunityRecord(Opportunity opportunityRecord);
        List<Opportunity> insertOpportunities(List<Opportunity> listOfOpportunities);
        List<Opportunity> getOpportunityByAccountId(Set<String> setOfIds);
        List<Opportunity> getListOfOpportunityByStageName(String searchParam, Set<String> stageName, List<String> column,String pageSize, String offset,
                                                          String sortOrderField, String sortDirection,
                                                          String fromDate ,String toDate);
        List<Opportunity> getOpportunityDetailsByApplicationId(String applicationId);
        List<Opportunity> getOpportunityById(Id opportunityId);
        Opportunity getOpportunityByIdforAddingBrokerComment(BrokerCommentInformationController.BrokerComment brokerInput);
        Opportunity getOpportunityByIdforCheckingBrokerComment(BrokerCommentInformationController.BrokerComment brokerInput);
        Opportunity getOpportunityCreditConditions (String opportunityId);
    }

    public static List<Opportunity> getListOfOpportunityByCurrentUser(String searchParam, List<String> column, 
        String pageSize, String offset, String sortOrderField, String sortDirection ,String fromDate ,String toDate ) {
            String userId = UserInfo.getUserId();
            String recordTypeName = 'CVF';
            String query = 'SELECT Id, ' + String.join(column, ',')  + ' FROM Opportunity ';
            String whereClause = 'RecordType.Name = :recordTypeName'; //'CreatedById = :userId';

            System.debug('searchParam from DAO  ' + searchParam);
            if (!String.isBlank(searchParam)) {
                String searchParamKey =  '%' + searchParam + '%';
                whereClause = whereClause + ' AND (Account.Name  LIKE :searchParamKey OR Account.Company_Code__c LIKE :searchParamKey OR Application_Id__c LIKE :searchParamKey OR Name LIKE :searchParamKey)';
           }            
            if (!String.isBlank(fromDate)) {
                //Date fromDateOpportunity = Date.valueOf(fromDate);
                //Date toDateOpportunity = Date.valueOf(toDate);
                String fromDateOpportunity = fromDate + 'T00:00:00.000+0000';
                String toDateOpportunity = toDate + 'T23:59:00.000+0000';
             //   String searchFromKey =   fromDateOpportunity ; 
         //       String searchtoKey = + toDateOpportunity + '%';
                whereClause = whereClause + ' AND createdDate >= ' +fromDateOpportunity+ ' and createdDate <= '+ toDateOpportunity;
            }
 
            if (!String.isBlank(whereClause)) {
                query = query + ' WHERE ' + whereClause;
            }

            if(!String.isBlank(sortOrderField)){
                query = query + ' ORDER BY ' + sortOrderField + ' ' + (String.isBlank(sortDirection) ? 'ASC' : sortDirection);
            }
            
            if (!String.isBlank(pagesize)) {
                query = query + ' LIMIT ' + pageSize;
            }

            if (!String.isBlank(offset)) {
                query = query + ' OFFSET ' + offset;
            }
            
            System.debug(query);
           return Database.query(query);
    }
    
    public static List<Opportunity> getListOfOpportunityByStageName(String searchParam, Set<String> stageName, List<String> column, 
        String pageSize, String offset, String sortOrderField, String sortDirection,String fromDate ,String toDate) {
            String userId = UserInfo.getUserId();
            String recordTypeName = 'CVF';
            String query = 'SELECT Id, ' + String.join(column, ',')  + ' FROM Opportunity ';
            String whereClause = 'RecordType.Name = :recordTypeName AND (StageName NOT IN :stageName)'; //'CreatedById = :userId';

            if (!String.isBlank(searchParam)) {
                
                whereClause = whereClause + ' AND (StageName NOT IN :stageName)';
            }            
           if (!String.isBlank(fromDate)) {
                //Date fromDateOpportunity = Date.valueOf(fromDate);
                //Date toDateOpportunity = Date.valueOf(toDate);
               String fromDateOpportunity = fromDate + 'T00:00:00.000+0000';
               String toDateOpportunity = toDate + 'T23:59:00.000+0000';
             //   String searchFromKey =   fromDateOpportunity ; 
         //       String searchtoKey = + toDateOpportunity + '%';
                whereClause = whereClause + ' AND createdDate >= ' +fromDateOpportunity+ ' and createdDate <= '+ toDateOpportunity;
            }

            if (!String.isBlank(whereClause)) {
                query = query + ' WHERE ' + whereClause;
            }

            if(!String.isBlank(sortOrderField)){
                query = query + ' ORDER BY ' + sortOrderField + ' ' + (String.isBlank(sortDirection) ? 'ASC' : sortDirection);
            }
            
            if (!String.isBlank(pagesize)) {
                query = query + ' LIMIT ' + pageSize;
            }

            if (!String.isBlank(offset)) {
                query = query + ' OFFSET ' + offset;
            }
            
            System.debug(query);
           return Database.query(query);
    }
    
    public static List<Opportunity> insertOpportunities(List<Opportunity> listOfOpportunities)
    {
        insert listOfOpportunities;
        return listOfOpportunities;        
    }
    
    public static List<Opportunity> getOpportunityDetailsByApplicationId(String applicationId)
    {
        return [SELECT Id, Name,Documentation_Type__c,CreatedById, LeaseInfo_DealBackground__c, StageName, CloseDate, AccountId, Company_Name__c, Third_Party_Name__c, Broker_Comment__c,
               Lease_Term__c, LeaseInfo_SoftwareAmount__c, Amount, LeaseInfo_EquipmentDescription__c, Lease_Type__c, Total_Cash_Price__c,
               Lease_Type_Request_6__c, RecordTypeId,Lease_Type_Request_1__c,Lease_Term_Request_1__c,                              
               (SELECT Id,CreatedById, City__c, County__c, Opportunity__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c, Street_Address_2__c
               FROM Communication_Addresses__r)
               FROM Opportunity Where Application_Id__c = :applicationId];
    }
    
    public static List<Opportunity> getOpportunityById(Id opportunityId)
    {
        return [SELECT Id, Name, StageName, CreatedById, Application_Id__c,Documentation_Type__c, LeaseInfo_DealBackground__c, AccountId, 
                Account.Company_Code__c,Account.TamarackPI__Paynet_Id__c From Opportunity Where Id =: opportunityId];
    }
    
      
     public static Opportunity getOpportunityByIdforAddingBrokerComment(BrokerCommentInformationController.BrokerComment brokerInput)
    {
        system.debug('opp'+brokerInput);
        Id convertedOpportunityId = Id.valueOf(brokerInput.applicationId);
       Opportunity opportunityRecord = [SELECT Id,Broker_Comment__c, Broker_Date_Time_Stamp__c, Application_Id__c From Opportunity 
                Where Id =: convertedOpportunityId];
        opportunityRecord.Broker_Comment__c = brokerInput.comment;
        opportunityRecord.Broker_Date_Time_Stamp__c = DateTime.now();
        system.debug('opp'+opportunityRecord);
        upsert opportunityRecord;
        return opportunityRecord;
    }
    
     public static Opportunity getOpportunityByIdforCheckingBrokerComment(BrokerCommentInformationController.BrokerComment brokerInput)
    {
       Id convertedOpportunityId = Id.valueOf(brokerInput.applicationId);
       Opportunity opportunityRecord = [SELECT Id,Broker_Comment__c, Name,Documentation_Type__c, LeaseInfo_EquipmentDescription__c, Company_Name__c, 
                                        Total_Cash_Price__c, Broker_Date_Time_Stamp__c, Account.Company_Code__c, Account.Name,
                                        LeaseInfo_SoftwareAmount__c, Amount, Application_Id__c From Opportunity Where Id =: convertedOpportunityId];
        return opportunityRecord;
    }
    
     public static Opportunity getOpportunityCreditConditions (String opportunityId)
    {
       Id convertedOpportunityId = Id.valueOf(opportunityId);
       Opportunity opportunityRecord = [SELECT Id, Broker_Comment__c, Name, LeaseInfo_EquipmentDescription__c, Company_Name__c, 
                                        Total_Cash_Price__c, Broker_Date_Time_Stamp__c, Account.Company_Code__c, Account.Name,
                                        LeaseInfo_SoftwareAmount__c, Amount, Application_Id__c, Credit_Condition__c,Other_Credit_Condition__c From Opportunity Where Id =: convertedOpportunityId];
        return opportunityRecord;
    }
    
    public static Opportunity updateOpportunityRecord(Opportunity opportunityRecord)
    {
        update opportunityRecord;
        return opportunityRecord;
    }
    
    public static List<Opportunity> getOpportunityByAccountId(Set<String> setOfIds)
    {
        return [SELECT Id ,Name,Documentation_Type__c,(Select Id, CreatedById From Automation_Applicant_Infos__r Where Last_Stage__c = 'Complete') FROM Opportunity 
                WHERE AccountId IN :setOfIds  
                AND RecordType.DeveloperName = 'CVF' Order By CreatedDate desc Limit 1 ];
    }//AND createdBy.Profile.Name = 'Crestmark Broker Profile'
}