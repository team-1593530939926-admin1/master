@istest
private class OpportunitiesTest {   
        
    private static final Id OPPORTUNITY_CVF_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');
    
    @TestSetup
    private static void setupTestData()
    {
        TestUtil.opportunityRelatedAccountData();
    }
    
    private static List<Account> getAccount()
    {
        return [SELECT Id , Name From Account];
    }
    
     private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id , Name, Company_Name__c, Application_Id__c,StageName, Tier__c, RecordTypeId, Deal_Status__c,AccountId,Is_Automation_Completed__c From Opportunity];
    }
    
    @isTest
    private static void updateOpportunityStatusTest() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Map<Id, Opportunity> oldObjMap = new Map<Id, Opportunity>(listOfOpportunity);
        Opportunity opportunityInstance = listOfOpportunity.get(0);
        opportunityInstance.Score__c = 721;
        opportunityInstance.Is_Automation_Completed__c = true;
        update opportunityInstance;
        List<Opportunity> listOfNewSObject = getOpportunity();
        
        Test.startTest();
            Opportunities.updateOpportunityStatus(listOfNewSObject, oldObjMap);
        Test.stopTest();
    }
    
    @isTest
    private static void updateOpportunityStatusTierEqualsFourTest() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Map<Id, Opportunity> oldObjMap = new Map<Id, Opportunity>(listOfOpportunity);
        Opportunity opportunityInstance = listOfOpportunity.get(0);
        opportunityInstance.Score__c = 671;
        opportunityInstance.Is_Automation_Completed__c = true;
        update opportunityInstance;
        List<Opportunity> listOfNewSObject = getOpportunity();
        
        Test.startTest();
            Opportunities.updateOpportunityStatus(listOfNewSObject, oldObjMap);
        Test.stopTest();
    }
    
    @isTest
    private static void updateOpportunityStatusTierEqualsDeclineTest() {
        
        List<Opportunity> listOfOpportunity = getOpportunity();
        Map<Id, Opportunity> oldObjMap = new Map<Id, Opportunity>(listOfOpportunity);
        Opportunity opportunityInstance = listOfOpportunity.get(0);
        opportunityInstance.Score__c = 600;
        opportunityInstance.Is_Automation_Completed__c = true;
        update opportunityInstance;
        List<Opportunity> listOfNewSObject = getOpportunity();
        
        Test.startTest();
            Opportunities.updateOpportunityStatus(listOfNewSObject, oldObjMap);
        Test.stopTest();
    }
    
    @isTest
    private static void testGetCvfRecordTypeIdIfIdExist()
    {
        Test.startTest();
        RecordTypeWrappers wrapper = Opportunities.getCvfRecordTypeId();
        Test.stopTest();
        System.assertEquals(false, String.isEmpty(wrapper.recordTypeId), 'RecordType Id does not exist');
    }
    
    @isTest
    private static void testGetOpportunityDetailsByApplicationIdIfApplicationIdExist()
    {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Opportunity opprtunityRecord = listOfOpportunity[0];
        
        Test.startTest();
            Opportunity opportunityResult = Opportunities.getOpportunityDetailsByApplicationId(opprtunityRecord.Application_Id__c);
        Test.stopTest();
        System.assertEquals('Pre Qual', opportunityResult.StageName);            
    }
    
    @isTest
    private static void testGetOpportunityDetailsByApplicationIdIfApplicationIdNotExist()
    {        
        Test.startTest();
            Opportunity opportunityResult = Opportunities.getOpportunityDetailsByApplicationId('');
        Test.stopTest();
        System.assertEquals(null, opportunityResult);            
    }
    
    @isTest
    private static void testGetOpportunityByIdIfExist()
    {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Opportunity opprtunityRecord = listOfOpportunity[0];
        
        Test.startTest();
            Opportunity opportunityResult = Opportunities.getOpportunityById(opprtunityRecord.Id);
        Test.stopTest();
        System.assertEquals(opprtunityRecord.Application_Id__c, opportunityResult.Application_Id__c);            
    }    

    @isTest
    private static void testGetOpportunityByIdIfNotExist()
    {       
        Test.startTest();
            Opportunity opportunityResult = Opportunities.getOpportunityById('0065C000005jsOpQAJ');
        Test.stopTest();
        System.assertEquals(null, opportunityResult);            
    }    
    
/*    
    @isTest 
    private static void testEquipmentDataInputIfDataIsAccepted()
    {
        Account accountRecord = getAccount()[0];
        
        EquipmentInformationWrapper equipmentWrapper = new EquipmentInformationWrapper();
        
        equipmentWrapper.dealName = 'TestDealName';
        equipmentWrapper.supplierName = 'Shivansh';
        equipmentWrapper.contactName = 'Rajat';
        equipmentWrapper.leaseTermMonths = '48';
        equipmentWrapper.estimatedEquipmentCost = String.valueOf(66);
        equipmentWrapper.equipmentDescription = 'testDesc';
        equipmentWrapper.endOfLeasePurchaseOption = 'Other';
        equipmentWrapper.otherText = 'testOtherTest';
        equipmentWrapper.totalCashPrice = String.valueOf(55);
        
        equipmentWrapper.otherStreetAddress = 'testOtherStreetAddress';
        equipmentWrapper.otherCity = 'testOtherCity';
        equipmentWrapper.otherState = 'testOtherState';
        equipmentWrapper.otherCounty = 'testOtherCounty';
        equipmentWrapper.otherZip = '45678';        
        
        Test.startTest();
        Opportunities.equipmentDataInput(equipmentWrapper, accountRecord.Id );
        Test.stopTest();
        List<Opportunity> listOfOpportunity = getOpportunity();
        
        System.assertEquals(1, listOfOpportunity.size());    
    }
    
     @isTest
    private static void testEquipmentDataInputIfDataIsNotAccepted()
    {
        Account accountRecord = getAccount()[0];
        
        EquipmentInformationWrapper equipmentWrapper = new EquipmentInformationWrapper();
        
        
        Test.startTest();
        try
        {
        Opportunities.equipmentDataInput(equipmentWrapper, accountRecord.Id );
        }
        catch(Exception exceptionObj)
        {            
        }
        Test.stopTest();
        List<Opportunity> listOfOpportunity = [SELECT Id From Opportunity];
        System.assertEquals(0, listOfOpportunity.size());    
    }
*/
}