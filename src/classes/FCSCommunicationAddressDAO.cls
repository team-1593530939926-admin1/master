public class FCSCommunicationAddressDAO implements IFCSCommunicationAddressDAO {
    public interface IFCSCommunicationAddressDAO {
        List<Communication_Address__c> getCommunicationAddressById(Set<Id> setOfIds);
        List<Communication_Address__c> getCommunicationAddressByEntityInformationId(Set<Id> setOfEntityIds);
        List<Communication_Address__c> getCommunicationAddressByPartyAgentId(Set<Id> setOfPartyAgentIds);
        void saveCommunicationAddress(List<Communication_Address__c> communicationAddresses);
    }

    public static List<Communication_Address__c> getCommunicationAddressById(Set<Id> setOfIds){
        return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
         Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
         Address_Type__c, Street_Address_2__c  
         FROM Communication_Address__c WHERE Id = :setOfIds];
    }

    public static List<Communication_Address__c> getCommunicationAddressByEntityInformationId(Set<Id> setOfEntityIds){
        return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
         Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
         Address_Type__c, Street_Address_2__c 
         FROM Communication_Address__c WHERE Entity_Information__c = :setOfEntityIds];
    }

    public static List<Communication_Address__c> getCommunicationAddressByPartyAgentId(Set<Id> setOfPartyAgentIds){
        return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
         Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
         Address_Type__c, Street_Address_2__c 
         FROM Communication_Address__c WHERE Entity_Party_Agent__c = :setOfPartyAgentIds];
    }

    public static void saveCommunicationAddress(List<Communication_Address__c> communicationAddresses){
        insert communicationAddresses;
    }
}