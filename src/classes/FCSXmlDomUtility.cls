public class FCSXmlDomUtility {
    
    public static ProcessInfoRequestResult parseFCSBusinessDetailXmlResponse(String xml) {
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(xml);

        Dom.XmlNode rootNode = xmlDoc.getRootElement(); 
        ProcessInfoRequestResult xmlResponseWrapper = new ProcessInfoRequestResult();
        parseXml(rootNode, xmlResponseWrapper);

        if (xmlResponseWrapper != null) {
            if (xmlResponseWrapper.recordInfo != null && 
            xmlResponseWrapper.recordInfo.entityInfo != null && 
            xmlResponseWrapper.recordInfo.entityInfo.listOfDates != null) {
                String dateOfIncorporation;
                for(Dates dateObject : xmlResponseWrapper.recordInfo.entityInfo.listOfDates) {
                    if ('DateOfIncorporation'.equalsIgnoreCase(dateObject.dateType)) {
                        dateOfIncorporation = dateObject.dateValue;
                    }
                }

                if (dateOfIncorporation != null) {
                    Date inCorporationDateInstance = FCSUtil.convertFCSDateInSFDate(dateOfIncorporation);
                    Integer daysInBetween = 0;
                    if (inCorporationDateInstance != null) {
                        daysInBetween = inCorporationDateInstance.daysBetween(Date.Today());
                    } 
                   
                    if (xmlResponseWrapper.recordInfo.entityInfo.entityType != null) {
                        xmlResponseWrapper.recordInfo.entityInfo.entityType.timeInService = daysInBetween;
                    }
                }
            }
        }
        System.debug(xmlResponseWrapper);
        return xmlResponseWrapper;
    }

    @TestVisible
    private static void parseXML(DOM.XMLNode node, ProcessInfoRequestResult xmlResponseWrapper) {
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if('header'.equalsIgnoreCase(node.getName())) {
                Header headerInfoObject = new Header();
                parseHeaderInfo(node, headerInfoObject);
                xmlResponseWrapper.headerInfo = headerInfoObject;
            }

            if('record'.equalsIgnoreCase(node.getName())) {
                Record recordInfoObject = new Record();
                parseRecordInfo(node, recordInfoObject); 
                xmlResponseWrapper.recordInfo = recordInfoObject;               
            }
              
        }

        for (Dom.XMLNode child: node.getChildElements()) {
            parseXML(child, xmlResponseWrapper);
        }
    }

    @TestVisible
    private static void parseHeaderInfo (Dom.XmlNode headerXmlNode, Header headerInfoObject) {
        if (headerXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {
            if ('PacketNum'.equalsIgnoreCase(headerXmlNode.getName())) {
                headerInfoObject.packetNumber = headerXmlNode.getText().trim();
            }
        }

        for (Dom.XMLNode headerChildNode: headerXmlNode.getChildElements()) {
            parseHeaderInfo(headerChildNode, headerInfoObject);
        }
    }


    @TestVisible
    private static void parseRecordInfo (Dom.XmlNode recordXmlNode, Record recordInfo) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {
            if ('SeqNumber'.equalsIgnoreCase(recordXmlNode.getName())) {
                recordInfo.sequenceNumber = recordXmlNode.getText().trim();
            }

            if ('EntityInfo'.equalsIgnoreCase(recordXmlNode.getName())) {
                EntityInfo entityInfoObject = new EntityInfo();
                parseEntityInfo(recordXmlNode, entityInfoObject);
                recordInfo.entityInfo = entityInfoObject;
            }
        }

        for (Dom.XMLNode recordChildNode: recordXmlNode.getChildElements()) {
            parseRecordInfo(recordChildNode, recordInfo);
        }
    }

    @TestVisible
    private static void parseEntityInfo (Dom.XmlNode recordXmlNode, EntityInfo entityInfoDetails) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {
            if ('Dates'.equalsIgnoreCase(recordXmlNode.getName())) {
                Dates datesObject = new Dates();
                parseDateInfo(recordXmlNode, datesObject);
                entityInfoDetails.listOfDates.add(datesObject);
            }

            if ('EntityType'.equalsIgnoreCase(recordXmlNode.getName())) {
                entityType entityTypeObject = new entityType();
                parseEntityType(recordXmlNode, entityTypeObject);
                entityInfoDetails.entityType = entityTypeObject;
            }

            if ('Parties'.equalsIgnoreCase(recordXmlNode.getName())) {
                Parties partyTypeObject = new Parties();

                parsePartyInfo(recordXmlNode, partyTypeObject);
                entityInfoDetails.listOfParties.add(partyTypeObject);
            }

            if ('Agent'.equalsIgnoreCase(recordXmlNode.getName())) {
                 Agent agentTypeObject = new Agent();
                parseAgentInfo(recordXmlNode, agentTypeObject);
                entityInfoDetails.listOfAgents.add(agentTypeObject);
            }
        }

        for (Dom.XMLNode entityChildNode: recordXmlNode.getChildElements()) {
            parseEntityInfo(entityChildNode, entityInfoDetails);
        }
    }

    @TestVisible
     private static void parseAgentInfo(Dom.XmlNode recordXmlNode, Agent agentInfoObject) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('Names'.equalsIgnoreCase(recordXmlNode.getName())) {
                Names nameInfoObject = new Names();
                parseNamesInfo(recordXmlNode, nameInfoObject);
                agentInfoObject.listOfAgentsName.add(nameInfoObject);
            }
        }

        for (Dom.XMLNode dateChildNode: recordXmlNode.getChildElements()) {
            parseAgentInfo(dateChildNode, agentInfoObject);
        }
    }

    @TestVisible
     private static void parsePartyInfo(Dom.XmlNode recordXmlNode, Parties partyInfoObject) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('PartiesType'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    partyInfoObject.partiesType = recordXmlNode.getAttributeValue('Type', null);
                }
            }
            
            if ('Title'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    partyInfoObject.title = recordXmlNode.getAttributeValue('Type', null);
                }
            }
         
            if ('Names'.equalsIgnoreCase(recordXmlNode.getName())) {
                Names nameInfoObject = new Names();
                parseNamesInfo(recordXmlNode, nameInfoObject);
                partyInfoObject.listOfPartiesName.add(nameInfoObject);
            }
        }

        for (Dom.XMLNode dateChildNode: recordXmlNode.getChildElements()) {
            parsePartyInfo(dateChildNode, partyInfoObject);
        }
    }

    @TestVisible
    private static void parseDateInfo(Dom.XmlNode recordXmlNode, Dates dateInfoObject) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if (recordXmlNode.getAttributeCount() > 0) {
                dateInfoObject.dateType = recordXmlNode.getAttributeValue('Type', null);
            }
            if ('Value'.equalsIgnoreCase(recordXmlNode.getName())) {
                dateInfoObject.dateValue = recordXmlNode.getText().trim();
            }
        }

        for (Dom.XMLNode dateChildNode: recordXmlNode.getChildElements()) {
            parseDateInfo(dateChildNode, dateInfoObject);
        }
    }


    @TestVisible
    private static void parseEntityType(Dom.XmlNode recordXmlNode, EntityType entityTypeInfoObject) {
        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('EntityType'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    entityTypeInfoObject.entityType = recordXmlNode.getAttributeValue('Type', null);
                }
            }

            if ('Domicile'.equalsIgnoreCase(recordXmlNode.getName())) {
                Domicile domicileInfoObject = new Domicile();
                parseDomicileInfo(recordXmlNode, domicileInfoObject);
                entityTypeInfoObject.domicileDetails = domicileInfoObject;
            }

             if ('EntityID'.equalsIgnoreCase(recordXmlNode.getName())) {
                entityTypeInfoObject.entityId = recordXmlNode.getText().trim();
            }

             if ('EntityStanding'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    entityTypeInfoObject.entityStanding = recordXmlNode.getAttributeValue('Standing', null);
                }
            }

            if ('Names'.equalsIgnoreCase(recordXmlNode.getName())) {
                Names nameInfoObject = new Names();
                parseNamesInfo(recordXmlNode, nameInfoObject);
                entityTypeInfoObject.listOfEntityName.add(nameInfoObject);
            }
        }

        for (Dom.XMLNode childNode: recordXmlNode.getChildElements()) {
            parseEntityType(childNode, entityTypeInfoObject);
        }
    }

    @TestVisible
    private static void parseDomicileInfo(Dom.XmlNode recordXmlNode, Domicile domicileInfoObject) {

        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('Domicile'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    domicileInfoObject.domicileType = recordXmlNode.getAttributeValue('Type', null);
                }
            }

            if ('State'.equalsIgnoreCase(recordXmlNode.getName())) {
                domicileInfoObject.state = recordXmlNode.getText().trim();
            }

            if ('country'.equalsIgnoreCase(recordXmlNode.getName())) {
                domicileInfoObject.country = recordXmlNode.getText().trim();
            }
        }

        for (Dom.XMLNode childNode: recordXmlNode.getChildElements()) {
            parseDomicileInfo(childNode, domicileInfoObject);
        }
    }


    @TestVisible
    private static void parseNamesInfo(Dom.XmlNode recordXmlNode, Names namesInfoObject) {

        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('EntityName'.equalsIgnoreCase(recordXmlNode.getName())) {
                namesInfoObject.entityName = recordXmlNode.getText().trim();
            }

            if ('Address'.equalsIgnoreCase(recordXmlNode.getName())) {
                Address addressInfoObject = new Address();
                parseAddressInfo(recordXmlNode, addressInfoObject);
                namesInfoObject.listOfAddress.add(addressInfoObject);
            }
        }

        for (Dom.XMLNode childNode: recordXmlNode.getChildElements()) {
            parseNamesInfo(childNode, namesInfoObject);
        }
    }

    @TestVisible
    private static void parseAddressInfo(Dom.XmlNode recordXmlNode, Address addressInfoObject) {

        if (recordXmlNode.getNodeType() == DOm.XmlNodeType.ELEMENT) {

            if ('AddressType'.equalsIgnoreCase(recordXmlNode.getName())) {
                if (recordXmlNode.getAttributeCount() > 0) {
                    addressInfoObject.addressType = recordXmlNode.getAttributeValue('Type', null);
                }
            }

            if ('Name'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.name = recordXmlNode.getText().trim();
            }

            if ('Address1'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.address1 = recordXmlNode.getText().trim();
            }

            if ('Address2'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.address2 = recordXmlNode.getText().trim();
            }

            if ('City'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.city = recordXmlNode.getText().trim();
            }

             if ('StateOrProvince'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.stateOrProvince = recordXmlNode.getText().trim();
            }

             if ('PostalCode'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.postalCode = recordXmlNode.getText().trim();
            }
            if ('CountryOrParish'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.countryOrParish = recordXmlNode.getText().trim();
            }

            if ('Country'.equalsIgnoreCase(recordXmlNode.getName())) {
                addressInfoObject.country = recordXmlNode.getText().trim();
            }
        }

        for (Dom.XMLNode childNode: recordXmlNode.getChildElements()) {
            parseAddressInfo(childNode, addressInfoObject);
        }
    }

    


    public class ProcessInfoRequestResult {
        @AuraEnabled
        public Header headerInfo {get; set;}
        @AuraEnabled
        public Record recordInfo {get; set;}
    }

    public class Header {
        @AuraEnabled
        public String packetNumber {get; set;}
    }

    public class Record {
        @AuraEnabled
        public String sequenceNumber {get; set;}
        @AuraEnabled
        public EntityInfo entityInfo {get; set;}
    }

    public class EntityInfo {
        @AuraEnabled
        public List<Dates> listOfDates {get; set;}
        @AuraEnabled
        public EntityType entityType {get; set;}
        @AuraEnabled
        public List<Parties> listOfParties {get; set;}
        @AuraEnabled
        public List<Agent> listOfAgents {get; set;}

        public EntityInfo() {
            this.listOfDates = new List<Dates>();
            this.listOfParties = new List<Parties>();
            this.listOfAgents = new List<Agent>();
        }
    }

    public class Dates {
        @AuraEnabled
        public String dateType {get; set;}
        @AuraEnabled
        public String dateValue {get; set;}
    }

    public class EntityType {
        @AuraEnabled
        public String entityId {get; set;}
        @AuraEnabled
        public String entityStanding {get; set;}
        @AuraEnabled
        public String entityType {get; set;}
        @AuraEnabled
        public Integer timeInService {get;set;}
        @AuraEnabled
        public List<Names> listOfEntityName {get; set;}
        @AuraEnabled
        public Domicile domicileDetails  {get; set;}
        
        public entityType () {
            this.listOfEntityName = new List<Names>();
        }
    }

    public class Domicile {
        @AuraEnabled
        public  String domicileType {get;set;}
        @AuraEnabled
        public String state {get; set;}
        @AuraEnabled
        public String country {get; set;}
    }

    public class Parties {
        @auraEnabled 
        public String partyKey {get;set;}
        @AuraEnabled
        public String partiesType {get; set;}
        @AuraEnabled
        public String title {get; set;}
        @AuraEnabled
        public List<Names> listOfPartiesName {get; set;}
        
        public Parties () {
            this.partyKey = FCSUtil.generateRandomKey('PartyKey');
            this.listOfPartiesName = new List<Names>();
        }
    }

    public class Agent {
        @auraEnabled 
        public String agentKey {get;set;}
        @AuraEnabled
        public List<Names> listOfAgentsName {get; set;}
        public Agent () {
            this.agentKey = FCSUtil.generateRandomKey('agentKey');
            this.listOfAgentsName = new List<Names>();
        }
    }


    public class Names {
        @AuraEnabled
        public String entityName {get; set;}
        @AuraEnabled
        public List<Address> listOfAddress {get; set;}

        public Names() {
            this.listOfAddress = new List<Address>();
        }
    }

    public class Address {  
        @auraEnabled 
        public String addressKey {get;set;}
        @AuraEnabled
        public String addressType {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String address1 {get; set;}
        @AuraEnabled
        public String address2 {get; set;}
        @AuraEnabled
        public String city {get; set;}
        @AuraEnabled
        public String stateOrProvince {get; set;}
        @AuraEnabled
        public String postalCode {get; set;}
        @AuraEnabled
        public String countryOrParish {get; set;}
        @AuraEnabled
        public String country {get; set;}

        public Address() {
             this.addressKey = FCSUtil.generateRandomKey('addressKey');
        }
    }
}