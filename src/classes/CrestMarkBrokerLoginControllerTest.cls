@isTest
private class CrestMarkBrokerLoginControllerTest {
    @isTest
    private static void testCrestMarkBrokerLoginControllerCustructor(){
        CrestMarkBrokerLoginController crestMarkBrokerLoginControllerInstance;
        
        Test.startTest();
            crestMarkBrokerLoginControllerInstance = new CrestMarkBrokerLoginController();
        Test.stopTest();
        
        System.assertEquals(false, String.isEmpty(crestMarkBrokerLoginControllerInstance.startURL), 'Start URL is empty.');
    }
    
    @isTest
    private static void testDoLoginGetSiteHomePageAfterLogin(){
        CrestMarkBrokerLoginController crestMarkBrokerLoginControllerInstance = new CrestMarkBrokerLoginController();
        crestMarkBrokerLoginControllerInstance.username = 'testusername';
        crestMarkBrokerLoginControllerInstance.password = 'testpassword';
        crestMarkBrokerLoginControllerInstance.startURL = 'www.salesforce.com';
        
        Test.startTest();
            PageReference pagRef = crestMarkBrokerLoginControllerInstance.doLogin();
        Test.stopTest();
        
        System.assertEquals(null, pagRef, 'Credentail are correct.');
    }
    
    @isTest
    private static void testDoLoginGetPasswordErrorIfPasswordIsBlank(){
        CrestMarkBrokerLoginController crestMarkBrokerLoginControllerInstance = new CrestMarkBrokerLoginController();
        crestMarkBrokerLoginControllerInstance.username = 'testusername';
        crestMarkBrokerLoginControllerInstance.startURL = 'www.salesforce.com';
        
        Test.startTest();
            PageReference pagRef = crestMarkBrokerLoginControllerInstance.doLogin();
        Test.stopTest();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        
        System.assertEquals(1, pageMessages.size(), 'Password is not empty.');
    }
    
    @isTest
    private static void testDoLoginGetUseraameErrorIfUsernameIsBlank(){
        CrestMarkBrokerLoginController crestMarkBrokerLoginControllerInstance = new CrestMarkBrokerLoginController();
        crestMarkBrokerLoginControllerInstance.password = 'testpassword';
        crestMarkBrokerLoginControllerInstance.startURL = 'www.salesforce.com';
        
        Test.startTest();
            PageReference pagRef = crestMarkBrokerLoginControllerInstance.doLogin();
        Test.stopTest();
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        
        System.assertEquals(1, pageMessages.size(), 'Username is not empty.');
    }
}