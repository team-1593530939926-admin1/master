@isTest
private class TransactionLogHandlerTest {
    
    private static Transaction_Log__c getTransactionLog() {
        return [SELECT Id, Error_Log__c, Exception_Time__c, Process_Name__c, Class_Name__c FROM Transaction_Log__c WHERE Process_Name__c = 'BrokerApplicationFormController'];
    }
    
    @isTest
    private static void doHandleExceptionTest() {
        
        try {
            Account accountInstance;
            insert accountInstance;
            System.assert(false, 'Should throw Exception.');
        } catch(System.Exception ex) {
            
            Test.startTest();
            	TransactionLogHandler.doHandleException(ex , 'BrokerApplicationFormController');
            Test.stopTest();
            
            Transaction_Log__c transactionLog = getTransactionLog();
            System.assertEquals('BrokerApplicationFormController', transactionLog.Process_Name__c, 'Value should not be empty or null');
            System.assertEquals('BrokerApplicationFormController', transactionLog.Class_Name__c, 'Value should not be empty or null');
            System.assertNotEquals(null, transactionLog, 'Object should be present.');
            //throw new AuraHandledException(ex.getMessage()); 
        }
    }
}