public with sharing class FCSMultipleApiCalloutsMock implements HttpCalloutMock {
    
    private static final Integer STATUS_CODE_SUCCESS = 200;
    
    private String CONTENT_TYPE_HEADER = 'Content-Type';
    private String TRANSACTION_ID = 'TransactionID';

    private String contentType;
    private String businessInfoBody;
    private Integer businessInfoStatusCode;
    private String businessDetailsBody;
    private Integer businessDetailsStatusCode;

    public FCSMultipleApiCalloutsMock(Integer businessInfoStatusCode, String businessInfoBody, Integer businessDetailsStatusCode, String businessDetailsBody, String contentType){
        this.businessInfoStatusCode = businessInfoStatusCode;
        this.businessInfoBody = businessInfoBody;
        this.businessDetailsStatusCode = businessDetailsStatusCode;
        this.businessDetailsBody = businessDetailsBody;
        this.contentType = contentType;
    }
    
	public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader(CONTENT_TYPE_HEADER, contentType);
        if(businessInfoStatusCode == STATUS_CODE_SUCCESS){
            if(businessDetailsStatusCode == STATUS_CODE_SUCCESS){
                if (req.getBody().contains(TRANSACTION_ID)) {
                    res.setBody(businessDetailsBody);
                    res.setStatusCode(businessDetailsStatusCode);
                } else {
                    res.setBody(businessInfoBody);
                    res.setStatusCode(businessInfoStatusCode);
                }
            } else {
                if (req.getBody().contains(TRANSACTION_ID)) {
                    res.setBody(businessDetailsBody);
                    res.setStatusCode(businessDetailsStatusCode);
                } else {
                    res.setBody(businessInfoBody);
                    res.setStatusCode(businessInfoStatusCode);
                }
            }            
        } else {
            res.setBody(businessInfoBody);
            res.setStatusCode(businessInfoStatusCode);
        }
        return res;
    }
}