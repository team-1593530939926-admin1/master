@isTest
private class DealDocumentUploadTableControllerTest {
    
   @testSetup
    private static void getContentVersionData() { 
        TestUtil.getContentVersionData();
    }
    
    @isTest
     static void testDealDocumentUploadTableController() {
     List<DealDocumentUploadTableController.DealDocumentUploadWrapper> wrap = new List<DealDocumentUploadTableController.DealDocumentUploadWrapper>();
        List<Contentversion> ContentId = [select contentDocumentId from Contentversion];
         List<Id> ConId = new List<Id>();
         List<Id> ConEmptyId = new List<Id>();
         
         for(Contentversion contId :ContentId)
         {
             ConId.add(contId.contentDocumentId);
         }
         wrap=DealDocumentUploadTableController.relatedFiles(null);
         Test.startTest();
         
         wrap=DealDocumentUploadTableController.relatedFiles(ConId);
         DealDocumentUploadTableController.setFileDetails(ContentId[0].contentDocumentId,'File Description','Signed Application');
         DealDocumentUploadTableController.deleteFiles(ContentId[0].contentDocumentId);
         
         Test.stopTest();
         
         System.assertEquals(0, [select count() from contentversion]);
        
            
    }


}