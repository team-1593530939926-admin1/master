@isTest
private class AdditionalAddressDAOTest 
{
    @isTest
    private static void testInsertAdditionalAddressIfListIsAccepted()
    {
        List<Additional_Address__c> AdditionalAddressList = new List<Additional_Address__c>();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
        Account acc = new Account();
		
        acc.Name = 'Test Account';
        acc.billingCity='Test';
        acc.BillingCountry='test';
        acc.BillingState='NY';
        acc.BillingStreet='test';
        acc.BillingPostalCode='111';
		
        insert acc;
		
        objectsOfAdditionalAddress.Name = 'Test Name';
        objectsOfAdditionalAddress.Company__c = acc.Id;
        objectsOfAdditionalAddress.City__c = 'NY';
        objectsOfAdditionalAddress.State__c = 'CA';
        objectsOfAdditionalAddress.County__c = 'Nebraska';
		
        AdditionalAddressList.add(objectsOfAdditionalAddress);
		
        Test.startTest();
            List<Additional_Address__c> listOfAdditionalAddress = AdditionalAddressDAO.insertAdditionalAddress(AdditionalAddressList);
        Test.stopTest();
        
        System.assertEquals(1, listOfAdditionalAddress.size(), 'Additional Address record is not inserted');
    }
    @isTest
    private static void testInsertAdditionalAddressIfListIsNotAccepted()
    {
        List<Additional_Address__c> AdditionalAddressList = new List<Additional_Address__c>();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
		
        Test.startTest();
            List<Additional_Address__c> listOfAdditionalAddress = AdditionalAddressDAO.insertAdditionalAddress(AdditionalAddressList);
        Test.stopTest();
        
        System.assertEquals(0, listOfAdditionalAddress.size(),'Additional Address record is inserted');
    }
}