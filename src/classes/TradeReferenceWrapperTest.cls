@isTest
private class TradeReferenceWrapperTest {
    
    @testSetup
    private static void testSetupData()
    {
        TestUtil.insertBusinessApplicationWrapper();
    } 
    
    public static Account getCompany()
    {
        return [SELECT Id, Company_Code__c FROM Account WHERE Name='Test Business Account' LIMIT 1];
    }
    
    @isTest
    private static void testTradeReferenceWrapperIfDataExist()
    {
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);        
        Test.startTest();
        TradeReferenceWrapper result = new TradeReferenceWrapper(accountRecord);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', result.companyName);
    }
    
    @isTest
    private static void testTradeReferenceWrapperIfDataNotExist()
    {
        Account accountRecord = new Account();
        
        Test.startTest();
        TradeReferenceWrapper result = new TradeReferenceWrapper(accountRecord);
        Test.stopTest();
        
        System.assertEquals(null, result.companyName);
    }
    @isTest
    public static void testTradeReferenceWrapperIfWrapperAndBooleanIsAccepted()
    {
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);      
        TradeReferenceWrapper tradeWrapperInfo = new TradeReferenceWrapper(accountRecord);
        Test.startTest();
            TradeReferenceWrapper result = new TradeReferenceWrapper(tradeWrapperInfo, true);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', result.companyName);    
    }
    
    @isTest
    public static void testTradeReferenceWrapperIfWrapperAndOpportunityAndCountIsAccepted()
    {
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);      
        TradeReferenceWrapper tradeWrapperInfo = new TradeReferenceWrapper(accountRecord);
        Account accountInfo = TestUtil.createAccountData();
        Test.startTest();
            TradeReferenceWrapper result = new TradeReferenceWrapper(tradeWrapperInfo, accountInfo, 2);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', result.dealTradeInfo.Name);    
    }
    
    @isTest
    public static void testContactInformationWrapperIfDataIsAccepted()
    {
        Contact contactrecord = TestUtil.createContactData();
        
        Test.startTest();
            TradeReferenceWrapper.ContactInformationWrapper result = new TradeReferenceWrapper.ContactInformationWrapper(contactrecord);
        Test.stopTest();
        
        System.assertEquals('FirstName 1', result.firstName);        
    }
    
    @isTest
    public static void testDefaultContactInformationWrapper()
    {        
        Test.startTest();
            TradeReferenceWrapper.ContactInformationWrapper result = new TradeReferenceWrapper.ContactInformationWrapper();
        Test.stopTest();
        
        System.assertEquals('', result.firstName);        
    }
}