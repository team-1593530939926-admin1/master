@isTest
private class BrokerApplicationFormControllerTest
{ 
    @testSetup
    private static void testSetupData()
    { 
        CVF_Equipment_Description__c equipmentDescriptionInstance = new CVF_Equipment_Description__c(Name = 'Software - Install', Description_Label__c = 'Software - Install');
        insert equipmentDescriptionInstance;
        
        Country_State_Mapping__c countryStateMappingInstance1 = new Country_State_Mapping__c(Name = 'California', State_Code__c = 'CA', State_Label__c = 'California', Country_Name__c = 'United States');
        Country_State_Mapping__c countryStateMappingInstance2 = new Country_State_Mapping__c(Name = 'Florida', State_Code__c = 'FL', State_Label__c = 'Florida', Country_Name__c = 'United States');
        List<Country_State_Mapping__c> listOfcountryStateMapping = new List<Country_State_Mapping__c>{countryStateMappingInstance1, countryStateMappingInstance2};
        insert listOfcountryStateMapping;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        
        TestUtil.createCVFCustomSettingsFileExtension();
        TestUtil.insertBusinessApplicationWrapper();
    }   
    
    private static List<ContentVersion> getContentVersion() {
        List<ContentVersion> listOfContentVersion = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Title = 'Test' LIMIT 1];  
        return listOfContentVersion;
    }
    
    private static List<ContentDocument> getContentDocuments() {
        List<ContentDocument> listOfContentDocuments = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        return listOfContentDocuments;
    }
    
    private static Account getCompany()
    {
        return [SELECT Id, Company_Code__c FROM Account Where Name = 'Test Business Account']; 
    }
    
    private static Contact getContact() {
        return [SELECT Id, Name FROM Contact LIMIT 1];
    }
    
    private static Opportunity getDeal()
    {
        return [SELECT Id, Application_Id__c, AccountId, Is_Automation_Completed__c FROM Opportunity LIMIT 1];
    }  
    
    public static CVF_Equipment_Description__c generateEquipmentDescriptionCustomSetting() {
        CVF_Equipment_Description__c equipmentDescriptionInstance = new CVF_Equipment_Description__c(Name = 'Software - Install', Description_Label__c = 'Software - Install');
        insert equipmentDescriptionInstance;
        
        return equipmentDescriptionInstance;
    }
    
    @isTest
    private static void testReturnRecordRelatedToDeal()
    {
        Opportunity opportunityRecord = getDeal();
        Test.startTest();
            Boolean resultOfStatus = BrokerApplicationFormController.returnRecordRelatedToDeal(opportunityRecord.Id);
        Test.stopTest();        
        System.assertEquals(false, resultOfStatus);
    }   
    
    @isTest
    private static void testupdateOpportunity()
    {
        Opportunity opportunityRecord = getDeal();
        Test.startTest();
            BrokerApplicationFormController.updateOpportunity(opportunityRecord.Id);
        Test.stopTest();
        
        Opportunity opportunityRecord1 = getDeal();
        
        System.assertEquals(true, opportunityRecord1.Is_Automation_Completed__c);
    }
    
    @isTest
    private static void testGetLeaseTermOptionValues()
    {
        Test.startTest();
            List<BrokerApplicationFormController.PicklistWrapper> listOfPicklistWrapper =   BrokerApplicationFormController.getLeaseTermOptionValues();
        Test.stopTest();
        
        System.assertEquals(true, listOfPicklistWrapper.size() > 0);
    }
    
    @isTest
    private static void getFileExtensionValues() {
        Test.startTest();
            Map<String, String> mapOfFileExtensions = BrokerApplicationFormController.getFileExtensionValues();
        Test.stopTest();
        
        System.assertEquals(1, mapOfFileExtensions.size());
    }
    
    @isTest
    private static void getCompanyRelatedDataByAddress() {
        Test.startTest();
            BusinessApplicationWrapper businessApplicationWrapperInstance = BrokerApplicationFormController.getCompanyRelatedDataByAddress('mc', 'Deep Driver', 'California', 'CA', 'United States', '52443');
        Test.stopTest();
        
        System.assertEquals(null, businessApplicationWrapperInstance);
    }
    
    @isTest
    private static void getTradeInformationByCompanyCode() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            TradeReferenceWrapper tradeReferenceWrapperInstance = BrokerApplicationFormController.getTradeInformationByCompanyCode(accountInstance.Id, '1');
        Test.stopTest();
        
        System.assertEquals(1, tradeReferenceWrapperInstance.rowId, 'Should get Value.');
        System.assertEquals(2, tradeReferenceWrapperInstance.count, 'Should get Value.');
    }
    
    @isTest
    private static void getGuarantorDetailsBasedOnSearchAccount() {
        Account accountInstance = getCompany();
        
        Test.startTest();
            GuarantorWrapper guarantorWrapperInstance = BrokerApplicationFormController.getGuarantorDetailsBasedOnSearch('Account', accountInstance.Id, '1', '1');
        Test.stopTest();
        
        System.assertEquals(1, guarantorWrapperInstance.rowId, 'Should get Value.');
        System.assertEquals(2, guarantorWrapperInstance.count, 'Should get Value.');
    }
    
    @isTest
    private static void getGuarantorDetailsBasedOnSearchContact() {
        Contact contactInstance = getContact();
        
        Test.startTest();
            GuarantorWrapper guarantorWrapperInstance = BrokerApplicationFormController.getGuarantorDetailsBasedOnSearch('Contact', contactInstance.Id, '1', '1');
        Test.stopTest();
        
        System.assertEquals(1, guarantorWrapperInstance.rowId, 'Should get Value.');
        System.assertEquals(2, guarantorWrapperInstance.count, 'Should get Value.');
    }
    
    @isTest
    private static void getDescriptionOptionValues() {
        //CVF_Equipment_Description__c equipmentDescriptionInstance = generateEquipmentDescriptionCustomSetting();
        
        Test.startTest();
            List<BrokerApplicationFormController.PicklistWrapper> listOfPicklistWrapper = BrokerApplicationFormController.getDescriptionOptionValues();
        Test.stopTest();
        
        System.assertEquals(1, listOfPicklistWrapper.size());
    }
    
    @isTest
    private static void getCountryAndStateValues() {
        
        Test.startTest();
            Map<String, List<BrokerApplicationFormController.PicklistWrapper>> mapOfPicklistWrapper = BrokerApplicationFormController.getCountryAndStateValues();
        Test.stopTest();
        
        System.assertEquals(1, mapOfPicklistWrapper.size());
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesExist()
    {
        TestUtil.createCVFCustomSettingsCountryAndStateData();
        
        Test.startTest();
            //Map<String, List<String>> result = BrokerApplicationFormController.getCountryAndStateValues();
        Test.stopTest();
        
        //System.assertEquals(1, result.values().size());        
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesNotExist()
    {    
        TestUtil.createCVFCustomSettingsCountryAndStateData();
        List<Country_State_Mapping__c> listOfOptions = [SELECT Name, State_Code__c FROM Country_State_Mapping__c];
        delete listOfOptions;
        
        Test.startTest();
            //Map<String, List<String>> result = BrokerApplicationFormController.getCountryAndStateValues();
        Test.stopTest();
        
        //System.assertEquals(0, result.values().size());        
    }    
    
    @isTest
    private static void testGetCurrentCompanyRecordTypeIdIdIfIdExist()
    {
        Test.startTest();
            RecordTypeWrappers wrapper = BrokerApplicationFormController.getCurrentCompanyRecordTypeId();
        Test.stopTest();
        System.assertEquals(false, String.isEmpty(wrapper.recordTypeId), 'RecordType Id does not exist');               
    }  
    
    @isTest
    private static void testGetCurrentDealRecordTypeIdIfIdExist()
    {
        Test.startTest();
            RecordTypeWrappers wrapper = BrokerApplicationFormController.getCurrentDealRecordTypeId();
        Test.stopTest();
        System.assertEquals(false, String.isEmpty(wrapper.recordTypeId), 'RecordType Id does not exist');               
    }  
    
    @isTest
    private static void testGetGuarantorRecordTypeIdIfIdExist()
    {
        Test.startTest();
            RecordTypeWrappers wrapper = BrokerApplicationFormController.getGuarantorRecordTypeId();
        Test.stopTest();
        System.assertEquals(false, String.isEmpty(wrapper.recordTypeId), 'RecordType Id does not exist');               
    }

    @isTest
    private static void testSubmitBrokerApplicationIfRecordValuesAccepted()
    {
        Account accountRecord = getCompany();
        Opportunity opportunityRecord = getDeal(); 
        List<ContentDocument> listOfContentDocuments = getContentDocuments();
        
        BusinessApplicationWrapper businessWrapper = DealDataTableController.getSubmittedApplicationOnView(opportunityRecord.Application_Id__c, accountRecord.Company_Code__c);
        
        businessWrapper.companyInformationWrapper = businessWrapper.companyWrapper;
        businessWrapper.partnerInformationWrapper = businessWrapper.partnerWrapper;
        businessWrapper.tradeInformationWrapper = businessWrapper.tradeWrapper;
        businessWrapper.bankInformationWrapper = businessWrapper.bankWrapper;
        businessWrapper.equipmentInformationWrapper = businessWrapper.equipmentWrapper;
        businessWrapper.guarantorInformationWrapper = businessWrapper.guarantorWrapper;
        
        businessWrapper.companyInformationWrapper.fullLegalCompanyName = 'testDominos';
        businessWrapper.companyInformationWrapper.city = 'testNY';
        businessWrapper.companyInformationWrapper.county = 'USA';
        businessWrapper.companyInformationWrapper.state = 'NY';
        businessWrapper.companyInformationWrapper.zip = '856743893';
        
        if(!businessWrapper.partnerInformationWrapper.isEmpty())
        {
        businessWrapper.partnerInformationWrapper[0].firstName = 'testYonik';
        businessWrapper.partnerInformationWrapper[0].lastName = 'testTyler';
        businessWrapper.partnerInformationWrapper[0].mailingStreet = 'street12Test';
        businessWrapper.partnerInformationWrapper[0].mailingCountry = 'USA';
        businessWrapper.partnerInformationWrapper[0].mailingState = 'CA';
        businessWrapper.partnerInformationWrapper[0].mailingCity = 'mailCity';
        businessWrapper.partnerInformationWrapper[0].mailingPostalCode = '74489856';
        }

        if(!businessWrapper.tradeInformationWrapper.isEmpty())
        {
        businessWrapper.tradeInformationWrapper[0].companyName = 'testBajaj';
        businessWrapper.tradeInformationWrapper[0].addressLine1 = 'testAddress';
        businessWrapper.tradeInformationWrapper[0].stateTrade = 'NY';
        businessWrapper.tradeInformationWrapper[0].countryTrade = 'USA';
        businessWrapper.tradeInformationWrapper[0].zipTrade = '39865567';
        }
        
        businessWrapper.fileDetailData = new List<String>{listOfContentDocuments.get(0).Id};
        
        String recordValues = JSON.serialize(businessWrapper);
        
        Test.startTest();
            BrokerApplicationFormController.ResultData result = BrokerApplicationFormController.submitBrokerApplication(recordValues);
        Test.stopTest();
        
        System.assertNotEquals(null, result.companyId, 'Records not inserted.');
        System.assertNotEquals(null, result.dealId, 'Records not inserted.');
        System.assertEquals(true, result.isPaynetApplicable, 'Records not inserted.');
    }
    
    @isTest
    private static void testSubmitBrokerApplicationThrowsException()
    {
        Account accountRecord = getCompany();
        Opportunity opportunityRecord = getDeal(); 
        
        BusinessApplicationWrapper businessWrapper = DealDataTableController.getSubmittedApplicationOnView(opportunityRecord.Application_Id__c, accountRecord.Company_Code__c);
        
        businessWrapper.companyInformationWrapper = businessWrapper.companyWrapper;
        businessWrapper.partnerInformationWrapper = businessWrapper.partnerWrapper;
        businessWrapper.tradeInformationWrapper = businessWrapper.tradeWrapper;
        businessWrapper.bankInformationWrapper = businessWrapper.bankWrapper;
        businessWrapper.equipmentInformationWrapper = businessWrapper.equipmentWrapper;
        businessWrapper.guarantorInformationWrapper = businessWrapper.guarantorWrapper;
        
        businessWrapper.companyInformationWrapper.fullLegalCompanyName = 'testDominos';
        businessWrapper.companyInformationWrapper.city = 'testNY';
        businessWrapper.companyInformationWrapper.county = 'USA';
        businessWrapper.companyInformationWrapper.state = 'NY';
        businessWrapper.companyInformationWrapper.zip = '856743893';
        
        if(!businessWrapper.partnerInformationWrapper.isEmpty())
        {
        businessWrapper.partnerInformationWrapper[0].firstName = 'testYonik';
        businessWrapper.partnerInformationWrapper[0].lastName = 'testTyler';
        businessWrapper.partnerInformationWrapper[0].mailingStreet = 'street12Test';
        businessWrapper.partnerInformationWrapper[0].mailingCountry = 'USA';
        businessWrapper.partnerInformationWrapper[0].mailingState = 'CA';
        businessWrapper.partnerInformationWrapper[0].mailingCity = 'mailCity';
        businessWrapper.partnerInformationWrapper[0].mailingPostalCode = '74489856';
        }

        if(!businessWrapper.tradeInformationWrapper.isEmpty())
        {
        businessWrapper.tradeInformationWrapper[0].companyName = 'testBajaj';
        businessWrapper.tradeInformationWrapper[0].addressLine1 = 'testAddress';
        businessWrapper.tradeInformationWrapper[0].stateTrade = 'NY';
        businessWrapper.tradeInformationWrapper[0].countryTrade = 'USA';
        businessWrapper.tradeInformationWrapper[0].zipTrade = '39865567';
        }
        
        
        String recordValues = JSON.serialize(businessWrapper);
        BrokerApplicationFormController.ResultData result;
        try {
            
            Test.startTest();
                result = BrokerApplicationFormController.submitBrokerApplication(recordValues);
            Test.stopTest();
            
            System.assert(false, 'Should throw Exception.');
        } catch(System.Exception ex) {
            System.assertEquals('Script-thrown exception', ex.getMessage());
        }
        
    }
    
    @isTest
    private static void testSubmitBrokerApplicationIfRecordValuesNotAccepted()
    { 
        String recordValues = '';
        
        Test.startTest();
            BrokerApplicationFormController.ResultData result = BrokerApplicationFormController.submitBrokerApplication(recordValues);
        Test.stopTest();
        
        System.assertEquals(null, result, 'Result should not be Present.');
        
    } 
    
    @isTest
    private static void testReviewBrokerApplicationIfRecordValuesAccepted()
    {
 
        Account accountRecord = getCompany();
        Opportunity opportunityRecord = getDeal(); 
        BusinessApplicationWrapper businessWrapper = DealDataTableController.getSubmittedApplicationOnView(opportunityRecord.Application_Id__c, accountRecord.Company_Code__c);
        
        businessWrapper.companyInformationWrapper = businessWrapper.companyWrapper;
        businessWrapper.partnerInformationWrapper = businessWrapper.partnerWrapper;
        businessWrapper.tradeInformationWrapper = businessWrapper.tradeWrapper;
        businessWrapper.bankInformationWrapper = businessWrapper.bankWrapper;
        businessWrapper.equipmentInformationWrapper = businessWrapper.equipmentWrapper;
        businessWrapper.guarantorInformationWrapper = businessWrapper.guarantorWrapper;
        
        String recordValues = JSON.serialize(businessWrapper);
        
        Test.startTest();
            BusinessApplicationWrapper result = BrokerApplicationFormController.reviewBrokerApplication(recordValues);
        Test.stopTest();
        System.assertEquals('Test Business Account', result.companyWrapper.fullLegalCompanyName, 'Records not visible.');
    }    
    
    @isTest
    private static void testReviewBrokerApplicationIfRecordValuesNotAccepted()
    {
 
        
        String recordValues = null;
        
        Test.startTest();
            BusinessApplicationWrapper result = BrokerApplicationFormController.reviewBrokerApplication(recordValues);
        Test.stopTest();
        System.assertEquals(null, result, 'Records visible.');
    } 
    
    @isTest
    private static void testGetInformationByCompanyCodeIfCompanyCodeExist()
    {
        Account accountRecord = getCompany();
        
        Test.startTest();
            BusinessApplicationWrapper result = BrokerApplicationFormController.getInformationByCompanyCode(accountRecord.Id);
        Test.stopTest();
        
        System.assertEquals('Test Business Account', result.CompanyInformationWrapper.fullLegalCompanyName);               
    }  

    @isTest
    private static void testGetInformationByCompanyCodeIfCompanyCodeNotExist()
    {

        Test.startTest();
            BusinessApplicationWrapper result = BrokerApplicationFormController.getInformationByCompanyCode('0015C00000XPfEhQAR');
        Test.stopTest();
        
        System.assertEquals(null, result.CompanyInformationWrapper.fullLegalCompanyName);               
    }    
    
}