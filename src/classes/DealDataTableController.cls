public without sharing class DealDataTableController {
    
    private static final Map<String, String> MAP_OF_DEAL_FIELDS = new Map<String, String>
    { 
        'Account.Company_Code__c' => CustomLabelConstants.DEAL_COMPANY_CODE, 
        'Application_Id__c'=> CustomLabelConstants.DEAL_APLLICATION_ID, 
        'Account.Name'=> CustomLabelConstants.DEAL_COMPANY_NAME, 
        'LeaseInfo_EquipmentDescription__c' => CustomLabelConstants.DEAL_EQUIPMENT_INFORMATION, 
        'CreatedDate'=> CustomLabelConstants.DEAL_SUBMITTED_DATE, 
        'Deal_Stage__c'=> CustomLabelConstants.DEAL_STAGE
     };
    private static final String DEFAULT_SORT_ORDER = 'CreatedDate';
    private static final String PAGE_SIZE = '50';
    private static final String OFFSET = '0';
    private static final String SORT_DIRECTION = 'DESC';
    private static List<String> PAGE_SIZE_OPTIONS = new List<String> {'10', '20', '50'};
    private static final OpportunityDAO.IOpportunityDAO oportunityDAOInstance = new OpportunityDAO();
    private static final ContentVersionDAO.IContentVersionDAO contentVersionDAOInstance = new ContentVersionDAO(); 
    private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    
    private static final Map<String, String> TABLE_ACTION = new Map<String, String> 
    {
      'View' => CustomLabelConstants.DEAL_VIEW
    };        
    private static final Set<String> SET_OF_DEAL_STAGES = new Set<String>{'Closed Won','Closed Lost'};    
    private static List<String> checkFLSForObjectAndFields(String sObjectName, List<String> listOfFields) 
    {
      return null;
    }
    
    @AuraEnabled (cacheable=true)            
    public static DealDataTableWrappper getListOfDealRecord()
    {
        try
        {
            DealDataTableWrappper wrapper = new DealDataTableWrappper();
            wrapper.dataTableColumn = MAP_OF_DEAL_FIELDS;
            wrapper.dataTableActionColumn = TABLE_ACTION;
            wrapper.defaultPageSize = PAGE_SIZE;
            wrapper.defaultSortField = DEFAULT_SORT_ORDER;
            wrapper.defaultSortDirection = SORT_DIRECTION;
            wrapper.pageSizeOptions = getSelectOptionsByListOfData(PAGE_SIZE_OPTIONS);
            wrapper.defaultOffset = OFFSET;
            wrapper.data = oportunityDAOInstance.getListOfOpportunityByCurrentUser(null,new List<String>(MAP_OF_DEAL_FIELDS.keySet()),PAGE_SIZE,OFFSET,DEFAULT_SORT_ORDER, SORT_DIRECTION,null,null);
            return wrapper;
        }
        catch(Exception exceptionObj)
        {
            throw new AuraHandledException(exceptionObj.getMessage());  
        }
    }
    
    @AuraEnabled (cacheable=true)            
    public static DealDataTableWrappper getListOfSearchRecord()
    {
        try
        {
            DealDataTableWrappper wrapper = new DealDataTableWrappper();
            wrapper.dataTableColumn = MAP_OF_DEAL_FIELDS;
            wrapper.dataTableActionColumn = TABLE_ACTION;
            wrapper.defaultPageSize = PAGE_SIZE;
            wrapper.defaultSortField = DEFAULT_SORT_ORDER;
            wrapper.defaultSortDirection = SORT_DIRECTION;
            wrapper.pageSizeOptions = getSelectOptionsByListOfData(PAGE_SIZE_OPTIONS);
            wrapper.defaultOffset = OFFSET;
            wrapper.data = null;
            return wrapper;
        }
        catch(Exception exceptionObj)
        {
            throw new AuraHandledException(exceptionObj.getMessage());  
        }
    }
    
    @AuraEnabled (cacheable=true)            
    public static DealDataTableWrappper getListOfPendingDealRecord()
    {
        try
        {
            DealDataTableWrappper wrapper = new DealDataTableWrappper();
            wrapper.dataTableColumn = MAP_OF_DEAL_FIELDS;
            wrapper.dataTableActionColumn = TABLE_ACTION;
            wrapper.defaultPageSize = PAGE_SIZE;
            wrapper.defaultSortField = DEFAULT_SORT_ORDER;
            wrapper.defaultSortDirection = SORT_DIRECTION;
            wrapper.pageSizeOptions = getSelectOptionsByListOfData(PAGE_SIZE_OPTIONS);
            wrapper.defaultOffset = OFFSET;
            wrapper.data = oportunityDAOInstance.getListOfOpportunityByStageName(null, new Set<String>(SET_OF_DEAL_STAGES),new List<String>(MAP_OF_DEAL_FIELDS.keySet()),PAGE_SIZE,OFFSET,DEFAULT_SORT_ORDER, SORT_DIRECTION,null,null);
            return wrapper;
        }
        catch(Exception exceptionObj)
        {
            throw new AuraHandledException(exceptionObj.getMessage());  
        }
    }
    
    @AuraEnabled (cacheable=true)            
    public static List<Opportunity> getListOfDealRecordBySearchKey(String searchKey, String pageSize, String offset, String sortOrderField, String sortDirection, String fromDate ,String toDate)
    {
        List<Opportunity> listOfOpportunity = new  List<Opportunity>();
        try
        {
           return oportunityDAOInstance.getListOfOpportunityByCurrentUser(searchKey,new List<String>(MAP_OF_DEAL_FIELDS.keySet()),pageSize,offset,sortOrderField,sortDirection,fromDate,toDate);
        }
        catch(Exception exceptionObj) 
        {
            throw new AuraHandledException(exceptionObj.getMessage());  
        }
        
    }
    
    @AuraEnabled (cacheable=true)            
    public static List<Opportunity> getListOfPendingDealRecordBySearchKey(String searchKey, String pageSize, String offset, String sortOrderField, String sortDirection,String fromDate ,String toDate)
    {
        List<Opportunity> listOfOpportunity = new  List<Opportunity>();
        try
        {
           return oportunityDAOInstance.getListOfOpportunityByStageName(searchKey, new Set<String>(SET_OF_DEAL_STAGES),new List<String>(MAP_OF_DEAL_FIELDS.keySet()),pageSize,offset,sortOrderField,sortDirection,fromDate,toDate);
        }
        catch(Exception exceptionObj)
        {
            throw new AuraHandledException(exceptionObj.getMessage());  
        }
        
    }
    
    @testVisible
    private static List<PicklistWrapper> getSelectOptionsByListOfData(List<String> optionData)
    {
        List<PicklistWrapper> listOfOptions = new List<PicklistWrapper>();
        for (String key : optionData) {
            listOfOptions.add(new PicklistWrapper(key, key));
        }
        return listOfOptions;
    }    
    
    
    public class DealDataTableWrappper
    {
        @AuraEnabled
        public Map<String, String> dataTableColumn{get; set;}
        @AuraEnabled
        public Map<String, String> dataTableActionColumn{get; set;}
        @AuraEnabled
        public List<Opportunity> data{get; set;}
        @AuraEnabled
        public String  defaultPageSize{get; set;}
        @AuraEnabled
        public String defaultSortField{get;set;}
        @AuraEnabled
        public String defaultSortDirection{get;set;}
        @AuraEnabled
        public List<PicklistWrapper> pageSizeOptions{get; set;}
        @AuraEnabled
        public String defaultOffset{get; set;}
        @AuraEnabled
        public List<PicklistWrapper> searchOptions{get; set;}
        
    }
    
    public class PicklistWrapper
    {
        @AuraEnabled
        public String label{get; set;}
        @AuraEnabled
        public String value{get; set;}
        
        public PicklistWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
        
    }
    

    @AuraEnabled(cacheable=true)
    public static BusinessApplicationWrapper getSubmittedApplicationOnView(String dealApplicationId, String dealCompanyCode)
    {
        Map<sObject, sObject> mapOfSObject = new Map<sObject, sObject>();
        Set<Id> setOfAccountId = new Set<Id>();
        Set<Id> setOfContactId = new Set<Id>();
        List<Account> listOfCorporateGuarantor = new List<Account>();
        List<Contact> listOfPersonalGuarantor = new List<Contact>();
        List<ContentVersion> listOfDocuments = new List<ContentVersion>();
        // Get All Data For Wrapper
        Opportunity dealRecord = Opportunities.getOpportunityDetailsByApplicationId(dealApplicationId);
        Account companyRecord = Accounts.getAccountRecordFromCompanyCode(dealCompanyCode);
    
        List<Account> listOfTrade = Accounts.getAccountRecordFromApplicationId(dealCompanyCode);
        List<Relationship__c> listOfRelationship = CvfRelationships.getListOfRelationship(dealApplicationId);
        
        for(Relationship__c rRecord : listOfRelationship){
            if(!String.isEmpty(rRecord.Business__c)){
                setOfAccountId.add(rRecord.Business__c);
            }
            if(!String.isEmpty(rRecord.Person__c)){
                setOfContactId.add(rRecord.Person__c);
            }
        }
        if(!setOfAccountId.isEmpty())
        {
             listOfCorporateGuarantor = Accounts.getListOfCorporateGuarantors(setOfAccountId);
        }
        if(!setOfContactId.isEmpty())
        {
            listOfPersonalGuarantor = Contacts.getListOfPersonalGuarantor(setOfContactId);
            
        }
        
        //Initialize Wrappers
        BusinessApplicationWrapper instanceOfBusinessWrapper = new BusinessApplicationWrapper();        
        List<PartnerDetailsWrapper> listOfPartnerWrapper = new List<PartnerDetailsWrapper>(); 
        List<TradeReferenceWrapper> listOfTradeReferenceWrapper = new List<TradeReferenceWrapper>();
        List<BankDetailsWrapper> listOfBankDetailsWrapper = new List<BankDetailsWrapper>();
        List<GuarantorWrapper> listOfGuarantorWrapper = new List<GuarantorWrapper>();
        EquipmentInformationWrapper equipmentInformationWrapperInstance;
        
        // Assign Data In Instance
        if(companyRecord != null)
        {
            PartnerDetailsWrapper partnerDetailWrapperInstance;
            for(Contact contactRecord : companyRecord.Partners__r)
            {
                partnerDetailWrapperInstance = new PartnerDetailsWrapper(contactRecord);
                /*if(!partnerDetailWrapperInstance.isCreatedByMe){
                    partnerDetailWrapperInstance.dOB = partnerDetailWrapperInstance.dOB != null ? CVFSerciceUtil.emcryptedData(partnerDetailWrapperInstance.dOB,'DOB') : null;
                    partnerDetailWrapperInstance.socialSecurity = partnerDetailWrapperInstance.socialSecurity != null ? CVFSerciceUtil.emcryptedData(partnerDetailWrapperInstance.socialSecurity,'SSN') : null;
                    partnerDetailWrapperInstance.mailingStreet = partnerDetailWrapperInstance.mailingStreet != null ? CVFSerciceUtil.emcryptedData(partnerDetailWrapperInstance.mailingStreet,'Street') : null;
                }*/
                listOfPartnerWrapper.add(partnerDetailWrapperInstance);
            }
            
            for(Bank_Reference__c bankRecord : companyRecord.Bank_Details__r)
            {
            listOfBankDetailsWrapper.add(new BankDetailsWrapper(bankRecord));
            }
        }
            
        if(dealRecord != null)
        {
            equipmentInformationWrapperInstance = new EquipmentInformationWrapper(dealRecord);
        }
         
        for(Account accountRecord : listOfTrade){
            listOfTradeReferenceWrapper.add(new TradeReferenceWrapper(accountRecord));
        }
        GuarantorWrapper guarantorWrapperInstance;
        for(Relationship__c guarantorRelationship : listOfRelationship){            
            for(Contact guarantorRecord : listOfPersonalGuarantor){
                if(!String.isEmpty(guarantorRelationship.Person__c) && guarantorRelationship.Person__c == guarantorRecord.Id){
                    guarantorWrapperInstance = new GuarantorWrapper(guarantorRecord, guarantorRelationship);
                    /*if(!guarantorWrapperInstance.isCreatedByMe){
                        guarantorWrapperInstance.dOB = guarantorWrapperInstance.dOB != null ? CVFSerciceUtil.emcryptedData(guarantorWrapperInstance.dOB,'DOB') : null;
                        guarantorWrapperInstance.socialSecurity = guarantorWrapperInstance.socialSecurity != null ? CVFSerciceUtil.emcryptedData(guarantorWrapperInstance.socialSecurity,'SSN') : null;
                        guarantorWrapperInstance.mailingStreetGurantor = guarantorWrapperInstance.mailingStreetGurantor != null ? CVFSerciceUtil.emcryptedData(guarantorWrapperInstance.mailingStreetGurantor,'Street') : null;
                    }*/
                    listOfGuarantorWrapper.add(guarantorWrapperInstance);
                }                
            } 
            for(Account guarantorRecord : listOfCorporateGuarantor){
                if(!String.isEmpty(guarantorRelationship.Business__c) && guarantorRelationship.Business__c == guarantorRecord.Id){
                    listOfGuarantorWrapper.add(new GuarantorWrapper(guarantorRecord, guarantorRelationship));
                }                
            }
        } 
        
        
        instanceOfBusinessWrapper.companyWrapper = Accounts.getAccountRecordByCompanyCode(dealCompanyCode); 
        instanceOfBusinessWrapper.partnerWrapper = listOfPartnerWrapper;
        instanceOfBusinessWrapper.tradeWrapper = listOfTradeReferenceWrapper;
        instanceOfBusinessWrapper.bankWrapper = listOfBankDetailsWrapper;
        instanceOfBusinessWrapper.equipmentWrapper = equipmentInformationWrapperInstance;
        instanceOfBusinessWrapper.guarantorWrapper = listOfGuarantorWrapper;
        instanceOfBusinessWrapper.listOfCreditDocuments = new List<ContentVersion>();
        instanceOfBusinessWrapper.listOfDocuments = new List<ContentVersion>();
        if(dealRecord != null){
            for(ContentVersion contentVersionRecord:contentVersionDAOInstance.getListOfDocumentsOpportunityId(dealRecord.Id)){
                if(contentVersionRecord.Is_Credit_Condition__c){
                    instanceOfBusinessWrapper.listOfCreditDocuments.add(contentVersionRecord);
                }
                else{
                    instanceOfBusinessWrapper.listOfDocuments.add(contentVersionRecord);
                }
            }
        }else{
            instanceOfBusinessWrapper.listOfDocuments = new List<ContentVersion>();
        }        
        return instanceOfBusinessWrapper;
    }  
}