public with sharing class FCSApiCallout {
   
    private static final Integer TIME_OUT = 120000;
    private static final String CONTENT_TYPE_HEADER = 'Content-Type';
    private static final String ACCEPT_HEADER = 'Accept';
 
    public static HttpResponse callout(FCSRequestWrapper requestWrapper) {
        HttpRequest request = new HttpRequest();
        request.setMethod(requestWrapper.methodType); 
        request.setEndpoint(requestWrapper.endPointURL);
        
        request.setHeader(ACCEPT_HEADER, FCSAPIConstant.CONTENT_TYPE_JSON);
        request.setHeader(CONTENT_TYPE_HEADER, requestWrapper.contentType == null ? FCSAPIConstant.CONTENT_TYPE_JSON : requestWrapper.contentType);
        request.setTimeout(TIME_OUT);

        if (!String.isBlank(requestWrapper.body)) {
            request.setBody(requestWrapper.body);
        }

        HttpResponse response = new HttpResponse();
        Http httpCall = new Http();
        response = httpCall.send(request);
        return response;
    }

    public class FCSRequestWrapper {
        
        public String endPointURL{get; set;}
        public String contentType{get; set;}
        public String body{get; set;}
        public String methodType{get; set;}

        public FCSRequestWrapper(String endPointURL, String methodType, String contentType, String body) {
            this.endPointURL = endPointURL;
            this.contentType = contentType;
            this.body = body;
            this.methodType = methodType;
        }
    }
}