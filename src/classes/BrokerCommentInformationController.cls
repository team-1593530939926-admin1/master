public without sharing class BrokerCommentInformationController {
    private static final OpportunityDAO.IOpportunityDAO opportunityDAOInstance = new OpportunityDAO();
    @AuraEnabled
    public static String addingBrokerComment(BrokerComment brokerInput){
        string success;
        try{
         Opportunity opportunityRecord = opportunityDAOInstance.getOpportunityByIdforAddingBrokerComment(brokerInput);  
            if(opportunityRecord != null){
                success = 'true';
            }
        }
        
         catch(System.DmlException de)
        {
            TransactionLogHandler.doHandleException(de , 'BrokerCommentInformationController');
            throw new AuraHandledException(de.getMessage());
        }
        catch(System.Exception ex)
        {
            TransactionLogHandler.doHandleException(ex , 'BrokerCommentInformationController');
            throw new AuraHandledException(ex.getMessage());  
        }  
        return success;
    }
     @AuraEnabled
    public static BrokerComment checkForBrokerComment(BrokerComment brokerInput){
        BrokerComment brokerDetails = new BrokerComment();
        try{
          Opportunity opportunityRecord = opportunityDAOInstance.getOpportunityByIdforCheckingBrokerComment(brokerInput);  
            system.debug('Opportunity'+opportunityRecord);
            if(opportunityRecord.Broker_Comment__c == null){
                brokerDetails.dealCode = opportunityRecord.Application_Id__c;
                brokerDetails.companyCode = opportunityRecord.Account.Company_Code__c;
                brokerDetails.companyName = opportunityRecord.Account.Name;
                brokerDetails.dealName = opportunityRecord.Name;
                brokerDetails.equipmentDescription = opportunityRecord.LeaseInfo_EquipmentDescription__c;
                if(opportunityRecord.Amount != null){
                     brokerDetails.Amount = string.valueOf(opportunityRecord.Amount);
                }
               
            }
        }
        catch(System.DmlException de)
        {
            TransactionLogHandler.doHandleException(de , 'BrokerCommentInformationController');
            throw new AuraHandledException(de.getMessage());
        }
        catch(System.Exception ex)
        {
            TransactionLogHandler.doHandleException(ex , 'BrokerCommentInformationController');
            throw new AuraHandledException(ex.getMessage());  
        }  
         return brokerDetails;
    }
    
    public class BrokerComment{
    @AuraEnabled
    public String comment{get;set;}
    @AuraEnabled
    public String applicationId{get;set;}
    @AuraEnabled
    public String companyCode{get;set;}
    @AuraEnabled
    public String dealCode{get;set;}
    @AuraEnabled
    public String companyName{get;set;}
    @AuraEnabled
    public String dealName{get;set;}
    @AuraEnabled
    public String equipmentDescription{get;set;}
    @AuraEnabled
    public String Amount{get;set;}
    
    }   
    
}