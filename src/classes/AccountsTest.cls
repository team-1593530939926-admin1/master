@isTest
private class AccountsTest
{
    
    @TestSetup 
    private static void setupTestData()
    {
        TestUtil.opportunityRelatedAccountData();
    } 
    
    private static List<Account> getAccount()
    {
        return [SELECT Id , Name, Company_Code__c, RecordTypeId, TamarackPI__Paynet_Id__c FROM Account]; 
    }
    
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c, Score__c, Tier__c FROM Opportunity];
    }
    
    public static Account getTradeReferenceAccount() {
        return [SELECT Id, Name, Trade_Reference__r.Company_Code__c FROM Account WHERE Name='Test tradeReference' LIMIT 1];
    }
    
    @isTest
    private static void runTamarackProcessAfterPaynetIdUpdate() {
        TestUtil.getBrokerDeal();
        List<Account> listOfAccounts = [SELECT Id , Name, Company_Code__c, RecordTypeId, TamarackPI__Paynet_Id__c FROM Account WHERE Name='Test Business Account Data'];
        Map<Id,Account> oldMapOfAccount = new Map<Id,Account>(listOfAccounts);
        
        Account accInstance = listOfAccounts.get(0);
        accInstance.TamarackPI__Paynet_Id__c = '226124';
        List<Account> listOfAccounts1 = new List<Account>{accInstance};
        update listOfAccounts1;
        
        Test.startTest();
            Accounts.runTamarackProcessAfterPaynetIdUpdate(listOfAccounts1, oldMapOfAccount);
        Test.stopTest();
        
        List<Opportunity> listOfOpportunity =  [SELECT Id, Application_Id__c, Score__c, Tier__c FROM Opportunity WHERE Name='Test Broker Opp Name'];
        
        System.assertEquals(400, listOfOpportunity.get(0).Score__c, 'Value should not be different.');
        System.assertEquals('N/A', listOfOpportunity.get(0).Tier__c, 'Value should not be different.');
    }
    
    @isTest
    private static void getAccountRecordByCompanyIdTest() {
        Account accountInstance = getAccount().get(0);
        
        Test.startTest();
            Account accountInstance1 = Accounts.getAccountRecordFromCompanyId(accountInstance.Id);
        Test.stopTest();

        System.assertEquals(accountInstance.Name, accountInstance1.Name, 'Id does not exist');
    }

    @isTest
    private static void getListOfCorporateGuarantorsTest() {
        Account accountInstance = getAccount().get(0);
        
        Test.startTest();
            List<Account> listOfAccounts = Accounts.getListOfCorporateGuarantors(new Set<Id>{accountInstance.Id});
        Test.stopTest();

        System.assertEquals(1, listOfAccounts.size(), 'Record does not exist.');
        System.assertEquals(accountInstance.Name, listOfAccounts.get(0).Name, 'Id does not exist');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdTest() {
        Account accountInstance = getAccount().get(0);
        
        Test.startTest();
            GuarantorWrapper guarantorWrapperInstance1 = Accounts.getGuarantorRecordFromGuarantorId(accountInstance.Id);
        Test.stopTest();

        System.assertEquals(accountInstance.Name, guarantorWrapperInstance1.companyName, 'Id does not exist');
    }
    
    @isTest
    private static void getGuarantorRecordFromGuarantorIdRecordNotFoundTest() {
        Account accountInstance = getAccount().get(0);
        
        Test.startTest();
            GuarantorWrapper guarantorWrapperInstance1 = Accounts.getGuarantorRecordFromGuarantorId('');
        Test.stopTest();

        System.assertEquals(null, guarantorWrapperInstance1, 'Record Not Found.');
    }
        
    @isTest
    private static void testGetBusinessRecordTypeIdIfIdExist()
    {
        Test.startTest();
        RecordTypeWrappers wrapper = Accounts.getBusinessRecordTypeId();
        Test.stopTest();
        System.assertEquals(false, String.isEmpty(wrapper.recordTypeId), 'RecordType Id does not exist');
    }
    
    @isTest
    private static void testGetAccountRecordByCompanyCodeIfCompanyCodeExist()
    {
        List<Account> listOfAccount = getAccount();        
        Account accountRecord = listOfAccount[0];
        CompanyInformationWrapper wrapperInstance = new CompanyInformationWrapper();
        
        Test.startTest();
            wrapperInstance = Accounts.getAccountRecordByCompanyCode(accountRecord.Company_Code__c);
        Test.stopTest();
        System.assertEquals('TestAccount', wrapperInstance.fullLegalCompanyName, 'Record does not exist.');
    }
   
    @isTest
    private static void testGetAccountRecordByCompanyCodeIfCompanyCodeNotExist()
    {
        CompanyInformationWrapper wrapperInstance = new CompanyInformationWrapper();
        Test.startTest();
            wrapperInstance = Accounts.getAccountRecordByCompanyCode('');
        Test.stopTest();
        System.assertEquals('Company Code does not exist', wrapperInstance.errorMessage, 'Record does exists.');
    }
    
    @isTest
    private static void testGetAccountRecordFromCompanyCodeIfCompanyCodeExist()
    {
        List<Account> listOfAccount = getAccount();        
        Account accountRecord = listOfAccount[0]; 
        
        Test.startTest();
            Account accountResult = Accounts.getAccountRecordFromCompanyCode(accountRecord.Company_Code__c);
        Test.stopTest();
        System.assertEquals('TestAccount', accountResult.Name, 'Record does not exist.');
    }
   
    @isTest
    private static void testGetAccountRecordFromCompanyCodeIfCompanyCodeNotExist()
    {        
        Test.startTest();
            Account accountResult = Accounts.getAccountRecordFromCompanyCode('');
        Test.stopTest();
        System.assertEquals(null, accountResult, 'Record does exists.'); 
    }
    
    @isTest
    private static void testGetAccountRecordFromApplicationId()
    {
        TestUtil.insertBusinessApplicationWrapper();
        Account accountRecord = getTradeReferenceAccount();
        //List<Opportunity> listOfOpportunity = getOpportunity();
        //Opportunity opportunityRecord = listOfOpportunity[0];
        Test.startTest();
            List<Account> listOfAccount = Accounts.getAccountRecordFromApplicationId(accountRecord.Trade_Reference__r.Company_Code__c);        
        Test.stopTest();
        System.assertEquals(1, listOfAccount.size(), 'Record does not exist.');
    }
    
/*   
    @isTest
    private static void testCompanyDataInputIfDataIsNotAccepted()
    {
        Account accountRecord = getAccount()[0];
        
        CompanyInformationWrapper companyWrapper = new CompanyInformationWrapper();
        
        companyWrapper.fullLegalCompanyName = 'testfullLegalCompanyName';
        companyWrapper.billingStreetAddress = 'testBilling';
        companyWrapper.city = 'testCity';
        companyWrapper.county  = 'testCounty';
        companyWrapper.state = 'UP';
        companyWrapper.zip = '12345';
        companyWrapper.phoneNumber = '1234567890';
        companyWrapper.fax = '54321';
        companyWrapper.federalTaxId = '54321';
        companyWrapper.natureOfBusiness = 'testNOB';
        companyWrapper.dba = 'terstDba';
        companyWrapper.companyType = 'Cooperative';
        companyWrapper.stateIncorporated = 'testStatee';
        companyWrapper.otherStreetAddress = 'testOtherStreet';
        companyWrapper.otherCity = 'testCity';
        companyWrapper.otherCounty ='TestCounty';
        companyWrapper.otherState = 'CA';
        companyWrapper.otherZip ='testZip';
        companyWrapper.contactFirstName ='testcontactFirstName';
        companyWrapper.contactLastName = 'testcontactLastName';
        companyWrapper.emailAddress= 'test@test.com';
        
        Test.startTest();
        try
        {
            Id result = Accounts.companyDataInput(companyWrapper);
        }
        catch(Exception exceptionObj)
        {              
        }
        Test.stopTest();
        
        List<Account> listOfAccount = getAccount();   
        
        System.assertEquals(1, listOfAccount.size() , 'Account record is inserted.');        
    }
*/    
    
    /*
    @isTest
    private static void testGetAccountRecordFromCompanyCodeFromDAOIfCompanyCodeExist()
    {
        CompanyInformationWrapper companyInfoInstance = new CompanyInformationWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '0000';
        objectOfAccount.Phone = '654321';
        objectOfAccount.Fax = '987654321';
        objectOfAccount.Federal_Tax_ID__c = '11223344';
        objectOfAccount.Industry = 'Machines';
        objectOfAccount.DBA__c = 'ABC';
        objectOfAccount.Entity_Type__c = 'ABC';
        objectOfAccount.State_Incorporated__c = 'ABC';
        
        insert objectOfAccount;
        
        List<Account> listOfAccount = [Select Company_Code__c From Account Where Name='Test Name'];
        String cCode;
        
        for(Account a : listOfAccount)
        {
            cCode = a.Company_Code__c;    
        } 
        
        Test.startTest();
            companyInfoInstance = Accounts.getAccountRecordFromCompanyCodeFromDAO(cCode);
        Test.stopTest();
        
        System.assertEquals('USA', companyInfoInstance.county, 'Company code do not exist');    
    }   
    
    @isTest
    private static void testGetAccountRecordFromCompanyCodeFromDAOIfCompanyCodeNotExist()
    {
        CompanyInformationWrapper companyInfoInstance = new CompanyInformationWrapper();
        
        String cCode = 'COMP-00218';
        
        Test.startTest();
        companyInfoInstance = Accounts.getAccountRecordFromCompanyCodeFromDAO(cCode);
        Test.stopTest();
        System.assertEquals(null, companyInfoInstance.county, 'Company code exists');
    } */

    /*
    @isTest
    private static void testUpdateWraperInfoBysObjectRecordIfRecordsAreFound()
    {
        CompanyInformationWrapper companyInfoInstance = new CompanyInformationWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
        
        objectOfAccount.Name = 'TestAccountName';
        objectOfAccount.BillingCity = 'Noida';
        objectOfAccount.BillingCountry = 'India';
        objectOfAccount.BillingState = 'NY';
        objectOfAccount.BillingStreet = 'sector-50';
        objectOfAccount.BillingPostalCode = '123456';
        objectOfAccount.Phone = '1234567890';
        objectOfAccount.Fax = '987651';
        objectOfAccount.Federal_Tax_ID__c = '11224';
        objectOfAccount.Industry = 'Machines';
        objectOfAccount.DBA__c = 'testDBA';
        objectOfAccount.Entity_Type__c = 'Corporation';
        objectOfAccount.State_Incorporated__c = 'testState';
        insert objectOfAccount;
        
        contactRecord.FirstName = 'testFirstName';
        contactRecord.LastName = 'testLastName';
        contactRecord.Email = 'testEmail@test.com';
        contactRecord.AccountId = objectOfAccount.Id;
        
        
        
        objectsOfAdditionalAddress.Street_Address_1__c = 'testStreetAddress';
        objectsOfAdditionalAddress.City__c = 'NYU';
        objectsOfAdditionalAddress.State__c = 'NY';
        objectsOfAdditionalAddress.County__c = 'India';
        objectsOfAdditionalAddress.Postal_Code__c = '23456';
        objectsOfAdditionalAddress.Company__c = objectOfAccount.Id;
        

        insert contactRecord;
        insert objectsOfAdditionalAddress;
        
        
        Test.startTest();
            companyInfoInstance = Accounts.updateWraperInfoBysObjectRecord(objectOfAccount);
        Test.stopTest();
        
        System.assertEquals('NY', companyInfoInstance.state, 'Records not found');
    }   
    @isTest
    private static void testUpdateWraperInfoBysObjectRecordIfRecordsAreNotFound()
    {
        CompanyInformationWrapper companyInfoInstance = new CompanyInformationWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'Noida';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'NY';
        objectOfAccount.BillingStreet = 'sector-50';
        objectOfAccount.BillingPostalCode = '0000';
        objectOfAccount.Phone = '';
        objectOfAccount.Fax = '987654321';
        objectOfAccount.Federal_Tax_ID__c = '11223344';
        objectOfAccount.Industry = 'Machines';
        objectOfAccount.DBA__c = 'ABC';
        objectOfAccount.Entity_Type__c = 'CORPORATION';
        objectOfAccount.State_Incorporated__c = 'ABC';
        insert objectOfAccount;
        
        contactRecord.FirstName = 'testFirstName';
        contactRecord.LastName = 'LastName';
        contactRecord.Email = 'abc@xyz.com';
        contactRecord.AccountId = objectOfAccount.Id;

        objectsOfAdditionalAddress.Street_Address_1__c = 'CAB Road';
        objectsOfAdditionalAddress.City__c = 'NYU';
        objectsOfAdditionalAddress.State__c = 'CA';
        objectsOfAdditionalAddress.County__c = 'Dallas';
        objectsOfAdditionalAddress.Postal_Code__c = '6789';
        objectsOfAdditionalAddress.Company__c = objectOfAccount.Id;
        
        
        insert contactRecord;
        insert objectsOfAdditionalAddress;
        
        Test.startTest();
            companyInfoInstance = Accounts.updateWraperInfoBysObjectRecord(objectOfAccount);
        Test.stopTest();
        
        System.assertEquals('', companyInfoInstance.phoneNumber, 'Records found');
    }  
    */
    
 
  /*  
    
    @isTest
    private static void testTradeDataInputReturningForDetailCreationIfListIsNotAccepted()
    {
        List<TradeReferenceWrapper> detailsOfTrade = new List<TradeReferenceWrapper>();
        TradeReferenceWrapper tradeWrapper = new TradeReferenceWrapper();
        
        Account accountRecord = getAccount()[0];
        
        tradeWrapper.cityTrade = 'NY';
        detailsOfTrade.add(tradeWrapper);       
          
        Test.startTest();
        try
        {
            Accounts.tradeDataInputReturningForTradeDetailCreation(detailsOfTrade, accountRecord.Id);
        }
        catch(Exception exceptionObj)
        {              
        }
        Test.stopTest();
        
        List<Contact> listOfContact = getContact();
        
        System.assertEquals(0, listOfContact.size(), 'Contact List is not empty.');
    }
    */
    
    /*
    @isTest
    private static void testGetTradeRecordFromCompanyCodeIfCompanyCodeExists()
    {
        TradeReferenceWrapper tradeInfoInstance = new TradeReferenceWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        Additional_Address__c objectsOfAdditionalAddress = new Additional_Address__c();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'NY';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'CA';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '0000';
        objectOfAccount.Phone = '654321';
        objectOfAccount.Fax = '987654321';
        objectOfAccount.Federal_Tax_ID__c = '11223344';
        objectOfAccount.Industry = 'Machines';
        objectOfAccount.DBA__c = 'ABC';
        objectOfAccount.Entity_Type__c = 'ABC';
        objectOfAccount.State_Incorporated__c = 'ABC';
        
        insert objectOfAccount;
        
        List<Account> listOfAccount = [Select Company_Code__c From Account Where Name='Test Name'];
        String cCode;
        
        for(Account a : listOfAccount)
        {
            cCode = a.Company_Code__c;    
        } 
        
        Test.startTest();
            tradeInfoInstance = Accounts.getTradeRecordCompanyCode(cCode);
        Test.stopTest();
        
        System.assertEquals('USA', tradeInfoInstance.countryTrade, 'Company Code does not exist');
    }
    
    @isTest
    private static void testGetTradeRecordFromCompanyCodeIfCompanyCodeNotExists()
    {
        TradeReferenceWrapper tradeInfoInstance = new TradeReferenceWrapper();
        
        String cCode = 'COMP-00218';
        
        Test.startTest();
            tradeInfoInstance = Accounts.getTradeRecordCompanyCode(cCode);
        Test.stopTest();
        
        System.assertEquals(null, tradeInfoInstance.countryTrade, 'Company Code do exist');
    }
    */
    
    /*
    @isTest
    private static void testUpdateWrapperInfoByTradeRecordIfRecordsAreFound()
    {
        TradeReferenceWrapper tradeInfoInstance = new TradeReferenceWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'Noida';
        objectOfAccount.BillingCountry = 'USA';
        objectOfAccount.BillingState = 'NY';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '0000';
        objectOfAccount.Phone = '654321';
        objectOfAccount.Fax = '987654321';
        
        insert objectOfAccount;
        
        contactRecord.FirstName = 'FirstName';
        contactRecord.LastName = 'LastName';
        contactRecord.AccountId = objectOfAccount.Id;
        
        insert contactRecord;
        
        Test.startTest();
            tradeInfoInstance = Accounts.updateWrapperInfoByTradeRecord(objectOfAccount);
        Test.stopTest();
        
        System.assertEquals('USA', tradeInfoInstance.countryTrade, 'Records not found');
    }
    
    @isTest
    private static void testUpdateWrapperInfoByTradeRecordIfRecordsAreNotFound()
    {
        TradeReferenceWrapper tradeInfoInstance = new TradeReferenceWrapper();
        Account objectOfAccount = new Account();
        Contact contactRecord = new Contact();
        
        objectOfAccount.Name = 'Test Name';
        objectOfAccount.BillingCity = 'Noida';
        objectOfAccount.BillingCountry = '';
        objectOfAccount.BillingState = 'NY';
        objectOfAccount.BillingStreet = 'MG Marg';
        objectOfAccount.BillingPostalCode = '0000';
        objectOfAccount.Phone = '654321';
        objectOfAccount.Fax = '987654321';
        
        insert objectOfAccount;
        
        contactRecord.FirstName = 'FirstName';
        contactRecord.LastName = 'LastName';
        contactRecord.AccountId = objectOfAccount.Id;
        
        insert contactRecord;
        
        Test.startTest();
            tradeInfoInstance = Accounts.updateWrapperInfoByTradeRecord(objectOfAccount);
        Test.stopTest();
        
        System.assertEquals('', tradeInfoInstance.countryTrade, 'Records Found');
    }  
    */
    
}