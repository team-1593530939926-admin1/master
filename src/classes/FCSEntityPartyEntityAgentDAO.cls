public class FCSEntityPartyEntityAgentDAO implements IFCSEntityPartyEntityAgentDAO {
    
    public interface IFCSEntityPartyEntityAgentDAO {
        List<FCS_Entity_Party_Agent__c> getEntityPartyEntityAgentById(Set<Id> setOfPartyAgentIds);
        List<FCS_Entity_Party_Agent__c> getEntityPartyEntityAgentByInformationId(Set<Id> setOfinformationId);
        void saveEntityPartyEntityAgent(List<FCS_Entity_Party_Agent__c> entityInfoList);
    }

    public static List<FCS_Entity_Party_Agent__c> getEntityPartyEntityAgentById(Set<Id> setOfPartyAgentIds) {
        return [ SELECT Id, Name, Name__c, Title__c, Type__c, Entity_Information__c, RecordTypeId, RecordType.Name
            FROM FCS_Entity_Party_Agent__c 
            WHERE Id = : setOfPartyAgentIds ];
    }

    public static List<FCS_Entity_Party_Agent__c> getEntityPartyEntityAgentByInformationId(Set<Id> setOfinformationId) {
        return [ SELECT Id, Name, Name__c, Title__c, Type__c, Entity_Information__c, RecordTypeId, RecordType.Name
            FROM FCS_Entity_Party_Agent__c 
            WHERE Entity_Information__c = : setOfinformationId ];
    }

    public static void saveEntityPartyEntityAgent(List<FCS_Entity_Party_Agent__c> entityPartyEntityAgentList) {
        insert entityPartyEntityAgentList;
    }

}