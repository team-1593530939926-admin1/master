public class BusinessApplicationWrapper {    
    
    @AuraEnabled
    public CompanyInformationWrapper companyWrapper{get;set;}
    @AuraEnabled
    public List<PartnerDetailsWrapper> partnerWrapper{get;set;}
    @AuraEnabled
    public List<TradeReferenceWrapper> tradeWrapper{get;set;}
    @AuraEnabled
    public List<BankDetailsWrapper> bankWrapper{get;set;}
    @AuraEnabled
    public EquipmentInformationWrapper equipmentWrapper{get;set;}
    @AuraEnabled
    public List<GuarantorWrapper> guarantorWrapper{get;set;}
    @AuraEnabled
    public List<ContentVersion> listOfDocuments {get;set;}
    @AuraEnabled
    public List<ContentVersion> listOfCreditDocuments {get;set;}
      
    @AuraEnabled
    public CompanyInformationWrapper companyInformationWrapper{get;set;}
    @AuraEnabled
    public List<PartnerDetailsWrapper> partnerInformationWrapper{get;set;}
    @AuraEnabled
    public List<TradeReferenceWrapper> tradeInformationWrapper{get;set;}
    @AuraEnabled
    public List<BankDetailsWrapper> bankInformationWrapper{get;set;}
    @AuraEnabled
    public EquipmentInformationWrapper equipmentInformationWrapper{get;set;}
    @AuraEnabled
    public List<GuarantorWrapper> guarantorInformationWrapper{get;set;}
    @AuraEnabled
    public FCSBusinessDetails entityInformation{get;set;}
    @AuraEnabled 
    public List<String> fileDetailData {get; set;} 
}