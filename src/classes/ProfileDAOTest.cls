@isTest
private class ProfileDAOTest 
{
    @isTest
    private static void testGetProfileIfSuccessfull()
    {
        String profileName = 'Crestmark Broker Profile';
		
        Test.startTest();
        List<Profile> p = ProfileDAO.getProfileByName(profileName);
        Test.stopTest();
		
        System.assertEquals('Crestmark Broker Profile', p[0].Name);
    }
    
    @isTest
    private static void testGetProfileIfUnsuccessfull()
    {
        String profileName = 'Rajat Crestmark Broker Profile';
		
        Test.startTest();
        List<Profile> p = ProfileDAO.getProfileByName(profileName);
        Test.stopTest();
		
        System.assertEquals(0, p.size(), 'Profile found with existing name');
    }
}