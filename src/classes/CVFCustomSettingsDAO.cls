public with sharing class CVFCustomSettingsDAO implements ICVFCustomSettingsDAO {
    
    public Interface ICVFCustomSettingsDAO {
      
        List<Country_State_Mapping__c> getCountryAndStateValues(); 
        List<CVF_Equipment_Description__c> getDescriptionOptionValues();  
        List<CVF_File_Extensions__c> getFilExtensionValues();
        List<CVF_Broker_Role__c> getCVFBrokerRoles();

    }
    
    public static List<CVF_Broker_Role__c> getCVFBrokerRoles() {
        List<CVF_Broker_Role__c> listOfCVFBrokerRoles = [SELECT Id, Name, Role_Id__c, Role_Label__c FROM CVF_Broker_Role__c];
        return listOfCVFBrokerRoles;
    }
    
    public static List<Country_State_Mapping__c> getCountryAndStateValues()
    {
        List<Country_State_Mapping__c> countryList = [SELECT Name, State_Code__c ,State_Label__c ,Country_Name__c FROM Country_State_Mapping__c ORDER BY State_Code__c ASC]; 
        return countryList;
    } 

     
     public static List<CVF_Equipment_Description__c> getDescriptionOptionValues()
     {
         List<CVF_Equipment_Description__c> descriptionList = [SELECT Name, Equipment_Category__c, Description_Label__c FROM CVF_Equipment_Description__c ORDER BY Description_Label__c ASC]; 
         return descriptionList;
     }
    
    public static List<CVF_File_Extensions__c> getFilExtensionValues()
    {
        List<CVF_File_Extensions__c> fileExtensionList = [SELECT Name, File_Extension__c FROM CVF_File_Extensions__c];
        return fileExtensionList;
    }

    
}