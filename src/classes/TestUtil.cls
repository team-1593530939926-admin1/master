@isTest
public class TestUtil{
    
    private static final Id RELATIONSHIP_INDIVIDUAL_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Relationship__c', 'Individual');
    private static final Id RELATIONSHIP_CORPORATE_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Relationship__c', 'Corporation');
    private static final Id OPPORTUNITY_CVF_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');

    public static List<sObject> createSOBjectData()
    {
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Accnt';
        accountRecord.billingCity='Tst';
        accountRecord.BillingCountry='tst';
        accountRecord.BillingState='NY';
        accountRecord.BillingStreet='tst';
        accountRecord.BillingPostalCode='11';
        
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Shubham';
        contactRecord.LastName = 'Lal';
        contactRecord.Email = 'shubham@lal.com';
        contactRecord.MailingStreet = 'street123';
        contactRecord.MailingCity = 'New York';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingPostalCode = '12345';
        contactRecord.MailingCountry = 'USA';
        
        List<sObject> listOfSObject = new List<sObject>();
        listOfSObject.add(accountRecord);
        listOfSObject.add(contactRecord);
        
        return listOfSObject;   
    }
    
    public static void createCVFCustomSettingsCountryAndStateData()
    {
        Country_State_Mapping__c countryList = new Country_State_Mapping__c();
        countryList.Name = 'USA';
        countryList.State_Code__c = 'NY';
        insert countryList;
    }
    
    public static void createCVFCustomSettingsEquipmentDescription()
    {
        CVF_Equipment_Description__c equipmentDecriptionList = new CVF_Equipment_Description__c();
        equipmentDecriptionList.Name = 'USA';
        equipmentDecriptionList.Description_Label__c = 'NY';
        insert equipmentDecriptionList;
    }
    
    public static void createCVFCustomSettingsFileExtension()
    {
        CVF_File_Extensions__c fileExtension = new CVF_File_Extensions__c();
        fileExtension.Name = 'USA';
        fileExtension.File_Extension__c = 'NY';
        insert fileExtension;
    }
    
    public static List<Account> createAccountData(Integer count, Boolean isInsert)
    {
        List<Account> listOfAccount = new List<Account>();
        for (integer i = 0; i < count; i++) {
            Account acc = new Account();
            acc.Name = 'MC - '+ count;
            acc.Source__c = 'CEF';
            acc.Location__c = 'Healthcare FS';
            acc.billingCity='DEEP DRIVER';
            acc.BillingCountry='United States';
            acc.BillingState='CA';
            acc.BillingStreet='202 S ST';
            acc.BillingPostalCode='12345';
            listOfAccount.add(acc);
        }
        if (isInsert) {
            insert listOfAccount;
        }
        
        return listOfAccount;
    }
    
    public static List<Lead> createLeadData(Integer count, Boolean isInsert) {
        List<Lead> listOfLead = new List<Lead>();
        for (integer i = 0; i < count; i++) {
            Lead leadRecord = new Lead();
            leadRecord.Status = 'Open';
            leadRecord.LastName = 'Jhonson - ' + count + ' - ' + System.now();
            leadRecord.Company = 'MC - '+count;
            leadRecord.Street = '202 S ST';
            leadRecord.PostalCode = '12345';
            leadRecord.City = 'DEEP DRIVER';
            leadRecord.State = 'CA';
            leadRecord.Country = 'United States';
            leadRecord.Source__c = 'CEF';
            leadRecord.Location__c = 'Healthcare FS';
            listOfLead.add(leadRecord);
        }
        
        if (isInsert) {
            insert listOfLead;
        }
        
        return listOfLead;
    }
    
    public static Lead createLeadData() {
            Lead leadRecord = new Lead();
            leadRecord.Status = 'Open';
            leadRecord.LastName = 'Jhonson';
            leadRecord.Company = 'MCD';
            leadRecord.Street = '202 S ST';
            leadRecord.PostalCode = '12345';
            leadRecord.City = 'DEEP DRIVER';
            leadRecord.State = 'CA';
            leadRecord.Country = 'United States';
            leadRecord.Source__c = 'CEF';
            leadRecord.Location__c = 'Healthcare FS';
            insert leadRecord;
            return leadRecord;
    }
    
    public static Opportunity opportunityRelatedAccountData() {  //each obj different method
        Account acct = new Account(Name='TestAccount',billingCity='Test',BillingCountry='test',
                                   BillingState='NY',BillingStreet='test',BillingPostalCode='111');//Company_Code__c='CMP000001'
        insert acct;
        // For each account just inserted, add opportunity
        
        Opportunity opps = new Opportunity(Name='Test',
                                           StageName='Pre Qual',
                                           LeaseInfo_EquipmentDescription__c='Test',
                                           closeDate=Date.newInstance(2020,11,20),
                                           //Broker_Comment__c='brokerComment',
                                           Score__c=400,
                                           Amount = 5000,
                                           Credit_Condition__c = 'Provide Bank Statements;Provide Proof of Ownership',
                                           RecordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID,
                                           //CreatedBy.Profile.Name = 'Crestmark Broker Profile',
                                           AccountId=acct.Id); 
        insert opps;
        return opps;
    }
    
    public static list<Opportunity> opportunityRelatedAccountList() { 
        list<Opportunity> oppList = new list <Opportunity>();
        opportunity opps=opportunityRelatedAccountData();
        oppList.add(opps);
        return oppList;
    }

    public static Opportunity createOpportunityData() {
        Opportunity opps = new Opportunity(Name='Test',
                                           StageName='Pre Qual',LeaseInfo_EquipmentDescription__c='Test',
                                           closeDate=Date.newInstance(2020,11,20)
                                           );
        insert opps;
        return opps;
    }
    
    
    public static List<Relationship__c> createRelationshipObjectData()
    {
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Accnt 3';
        accountRecord.billingCity='Tst 3';
        accountRecord.BillingCountry='tst 3';
        accountRecord.BillingState='NY';
        accountRecord.BillingStreet='tst';
        accountRecord.BillingPostalCode='112233';
        insert accountRecord;
        Opportunity opportunityRecord = new Opportunity(Name='Test 3',Company_Name__c = 'testSupplier 3',
                                           StageName='Pre Qual 3',LeaseInfo_EquipmentDescription__c='Test 3',
                                           closeDate=Date.newInstance(2020,11,21), RecordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID,
                                           AccountId=accountRecord.Id);
        insert opportunityRecord;        
        List<Relationship__c> listOfRelationships = new List<Relationship__c>(); 
        Relationship__c relationshipInstance = new Relationship__c();
      //  relationshipInstance.Business__c = accountRecord.Id;
      //  relationshipInstance.Person__c = contactRecord.Id;
        relationshipInstance.Related_Opportunity__c = opportunityRecord.Id; 
        relationshipInstance.Percent_of_Ownership__c = 50;
        relationshipInstance.Relationship_Type__c = 'Principal 3';
      //  relationshipInstance.Related_Opportunity__c = opportunityRecord.Id;
        listOfRelationships.add(relationshipInstance);
        return listOfRelationships;
    }
    public static Account createAccountData()
    {
        Account acc = new Account();
        acc.Name = 'Test Accnt 1';
        acc.billingCity='Tst 1';
        acc.BillingCountry='USA';
        acc.BillingState='NY';
        acc.BillingStreet='tst 1';
        acc.BillingPostalCode='1111';
        insert acc;
        return acc;
    }
    
    public static Contact createContactData()
    {
        Account acc = createAccountData();
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'FirstName 1';
        contactRecord.LastName = 'LastName 1';
        contactRecord.AccountId = acc.Id;
        contactRecord.MailingCity = 'testCity';
        contactRecord.MailingCountry = 'USA';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingStreet = 'testMailStreet';
        contactRecord.MailingPostalCode = '111';  
        insert contactRecord;
        return contactRecord;
    }
    
    /*public static Guarantor_Address__c createGuarentorAddress() {
        Guarantor_Address__c guarentorAddressInstance = new Guarantor_Address__c();
        guarentorAddressInstance.City__c = 'guarentor city';
        guarentorAddressInstance.County__c = 'guarentor country';
        guarentorAddressInstance.Postal_Code__c = '123422';
        return null;
    }*/
    
    public static Contact createBrokerContact()
    {
       Id brokerProfileId = '00e5C000000Qj4LQAS';
        Account accountRecord = new Account();
        accountRecord.Name = 'brokerAccount';
        accountRecord.billingCity='New York';
        accountRecord.BillingCountry='USA';
        accountRecord.BillingState='NY';
        accountRecord.BillingStreet='street123';
        accountRecord.BillingPostalCode='123456';
        accountRecord.Role__c = 'Vendor';
        accountRecord.RecordType.Name = 'Business';
        insert accountRecord;
        
             
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Shubham';
        contactRecord.LastName = 'Lal';
        contactRecord.Email = 'shubham@lal.com';
        contactRecord.MobilePhone = '123456789';
        contactRecord.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Crestmark Contact').getRecordTypeId();
        contactRecord.Relationship_Type__c = 'Broker';
        contactRecord.MailingStreet = 'street123';
        contactRecord.MailingCity = 'New York';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingPostalCode = '12345';
        contactRecord.MailingCountry = 'USA';
        contactRecord.Is_CVF_Broker__c = true ;
        contactRecord.AccountId = accountRecord.Id;
        insert contactRecord;
        return contactRecord;
    }
        
    public static Account getCompanyInformationWrapperData()
    {
        Account companyRecord = new Account();
        
        companyRecord.Name = 'UnitechAccount';
        companyRecord.billingCity='ghaziabad';
        companyRecord.BillingCountry='USA';
        companyRecord.BillingState='NY';
        companyRecord.BillingStreet='123 blockE';
        companyRecord.BillingPostalCode='122331';
        return companyRecord;
    }
    
    public static Contact getPartnerDetailWrapperData()
    {
        Contact partnerRecord = new Contact();
        partnerRecord.FirstName = 'partnerFirstName';
        partnerRecord.LastName = 'partnerLastName';
        partnerRecord.HomePhone = '1234567890';
        return partnerRecord;
    }
    
    public static Account getTradeReferenceWrapperData()
    {
        Account tradeRecord = new Account();
        tradeRecord.Name = 'tradeAccount';
        tradeRecord.billingCity='ghaziabad';
        tradeRecord.BillingCountry='USA';
        tradeRecord.BillingState='NY';
        tradeRecord.BillingStreet='123 blockE';
        tradeRecord.BillingPostalCode='122331';
        return tradeRecord;        
    }
    
    public static Bank_Reference__c getBankDetailsWrapperData()
    {
        Bank_Reference__c bankRecord = new Bank_Reference__c();
        bankRecord.Bank_Name__c = 'sbi bank';
        bankRecord.Address_Line_1__c = 'sector-36'; 
        bankRecord.City__c = 'Noida';   
        bankRecord.State__c = 'NY';   
        bankRecord.Zip__c = '12323';  
        bankRecord.Bank_Contact_Person__c = 'Shubham Lal';
        return bankRecord;
    }
    
    public static Opportunity getEquipmentInformationWrapperData()
    {
        Opportunity equipmentRecord = new Opportunity();
        equipmentRecord.Company_Name__c = 'testSupplierName';      
        equipmentRecord.Third_Party_Name__c = 'testThirdParty';
        return equipmentRecord;
    }      
    
    public static Communication_Address__c getCommunicationAddressWrapperData()
        {
            Communication_Address__c addressRecord = new Communication_Address__c();
        
            addressRecord.Street_Address_1__c = '123 Street';
            addressRecord.City__c = 'New York';
            addressRecord.County__c = 'NY';
            addressRecord.State__c = 'NY';
            addressRecord.Postal_Code__c = '1234566';
            
            return addressRecord;
        }
    
    public static Additional_Address__c getAdditionalAddressWrapperData()
        {
            Additional_Address__c addressRecord = new Additional_Address__c();
        
            addressRecord.Street_Address_1__c = '123 Street';
            addressRecord.City__c = 'New York';
            addressRecord.County__c = 'NY';
            addressRecord.State__c = 'NY';
            addressRecord.Postal_Code__c = '1234566';
            
            return addressRecord;
        }
    
    private static final ProfileDAO.IProfileDAO profileDAOInstance = new ProfileDAO();
    private static final String BROKER_PROFILE_NAME = 'Crestmark Broker Profile';
    private static final String LANGUAGE_LOCALE_KEY = 'en_US';
    private static final String EMAIL_ENCODING_KEY = 'UTF-8';
    private static final String TIME_ZONE_KEY = 'America/New_York';

    public static Opportunity getBrokerDeal() {
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Business Account Data';
        accountRecord.billingCity='BusinessCity';
        accountRecord.BillingCountry='United States';
        accountRecord.BillingState='CA';
        accountRecord.BillingStreet='Business Street';
        accountRecord.BillingPostalCode='112239';
        insert accountRecord;
        //Account accountRecord1 = getAccount();
        
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'testFirstName Business';
        contactRecord.MiddleName = 'testMiddlieName'; 
        contactRecord.LastName = 'testLastName';  
        contactRecord.MobilePhone = '1234123400';
        contactRecord.MailingCity = 'testCity';
        contactRecord.MailingCountry = 'USA';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingStreet = 'testMailStreet';
        contactRecord.MailingPostalCode = '111';        
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Guarantor_Type__c = 'Individual';
        contactRecord.Email = 'john@acme.com';
        insert contactRecord;
        
        Id brokerProfileId = profileDAOInstance.getProfileByName(BROKER_PROFILE_NAME)[0].Id;
        User userRecord = new User();
        userRecord.FirstName = contactRecord.FirstName;
        userRecord.LastName = contactRecord.LastName;
        userRecord.MiddleName = contactRecord.MiddleName;
        userRecord.Email = contactRecord.Email;
        userRecord.UserName = contactRecord.Email + '.broker';
        userRecord.ContactId = contactRecord.Id;
        userRecord.MobilePhone = contactRecord.MobilePhone;
        userRecord.CompanyName = accountRecord.Name;
        userRecord.CommunityNickname = contactRecord.LastName;
        userRecord.Alias = contactRecord.FirstName.substring(0,1) + contactRecord.LastName.substring(0,3);
        userRecord.TimeZoneSidKey = TIME_ZONE_KEY;
		userRecord.LocaleSidKey = LANGUAGE_LOCALE_KEY;
		userRecord.EmailEncodingKey = EMAIL_ENCODING_KEY;
        userRecord.ProfileId = brokerProfileId;
        userRecord.LanguageLocaleKey = LANGUAGE_LOCALE_KEY;
        userRecord.Street = contactRecord.MailingStreet;
        userRecord.City = contactRecord.MailingCity;
        userRecord.State = contactRecord.MailingState;
        userRecord.PostalCode = contactRecord.MailingPostalCode;
        userRecord.Country = contactRecord.MailingCountry;
        insert userRecord;
        Opportunity opportunityRecord = new Opportunity(Name='Test Broker Opp Name',Company_Name__c = 'testSupplier',
                                           StageName='Pre Qual',LeaseInfo_EquipmentDescription__c='Test',
                                           Score__c=400,
                                           Amount = 5000,
                                           Credit_Condition__c = 'Provide Bank Statements;Provide Proof of Ownership',
                                           createdById = userRecord.Id,
                                           closeDate=Date.newInstance(2020,11,20), RecordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID,
                                           AccountId=accountRecord.Id);
        insert opportunityRecord;
        return opportunityRecord;
    }
    
    public static void insertBusinessApplicationWrapper()
    {
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Business Account';
        accountRecord.billingCity='BusinessCity';
        accountRecord.BillingCountry='United States';
        accountRecord.BillingState='CA';
        accountRecord.BillingStreet='Business Street';
        accountRecord.BillingPostalCode='112239';
        insert accountRecord;
        //Account accountRecord1 = getAccount();
        
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'testFirstName Business';
        contactRecord.LastName = 'testLastName';        
        contactRecord.MailingCity = 'testCity';
        contactRecord.MailingCountry = 'USA';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingStreet = 'testMailStreet';
        contactRecord.MailingPostalCode = '111';        
        contactRecord.AccountId = accountRecord.Id;
        contactRecord.Guarantor_Type__c = 'Individual';
        insert contactRecord;
        //Contact contactRecord1 = getContact();
        
        Additional_Address__c additionalAddressRecord = new Additional_Address__c();
        additionalAddressRecord.Street_Address_1__c = 'Street 123';
        additionalAddressRecord.City__c = 'New York';
        additionalAddressRecord.County__c = 'testCounty';
        additionalAddressRecord.State__c = 'NY';
        additionalAddressRecord.Postal_Code__c = '12324';
        additionalAddressRecord.Company__c = accountRecord.Id;
        insert additionalAddressRecord;
        
        Opportunity opportunityRecord = new Opportunity(Name='Test',Company_Name__c = 'testSupplier',
                                           StageName='Pre Qual',LeaseInfo_EquipmentDescription__c='Test',
                                           closeDate=Date.newInstance(2020,11,20), RecordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID,
                                           AccountId=accountRecord.Id);
        insert opportunityRecord;

        Account tradeReferenceRecord = new Account();
        tradeReferenceRecord.Name = 'Test tradeReference';
        tradeReferenceRecord.billingCity='TsttradeReference';
        tradeReferenceRecord.BillingCountry='tsttradeReference';
        tradeReferenceRecord.BillingState='NY';
        tradeReferenceRecord.BillingStreet='tsttradeReference';
        tradeReferenceRecord.BillingPostalCode='112233';
        tradeReferenceRecord.Trade_Reference__c = accountRecord.Id;
        insert tradeReferenceRecord;
        
        //insert contact of Trade reference (lookup of trade)
        Contact tradeContactRecord = new Contact();
        tradeContactRecord.FirstName = 'testFirstName';
        tradeContactRecord.LastName = 'testLastName';
        tradeContactRecord.Email = 'test@test.com';
        tradeContactRecord.MailingCity = 'testCity';
        tradeContactRecord.MailingCountry = 'USA';
        tradeContactRecord.MailingState = 'NY';
        tradeContactRecord.MailingStreet = 'testMailStreet';
        tradeContactRecord.MailingPostalCode = '111'; 
        tradeContactRecord.AccountId = tradeReferenceRecord.Id;
        insert tradeContactRecord;
        
        //insert contact as a partner and lookup of opportunity.
        Contact partnerContactRecord = new Contact();
        partnerContactRecord.FirstName = 'FirstName Contact';
        partnerContactRecord.LastName = 'LastName';
        partnerContactRecord.Email = 'business@test.com';
        partnerContactRecord.MailingCity = 'ContactCity';
        partnerContactRecord.MailingCountry = 'USA';
        partnerContactRecord.MailingState = 'CA';
        partnerContactRecord.MailingStreet = 'MailingStreet Contact';
        partnerContactRecord.MailingPostalCode = '111668';         
        partnerContactRecord.Partners__c = accountRecord.Id;
        insert partnerContactRecord;
        
        //insert bankDeatils lookup with opport.
        Bank_Reference__c bankInformationRecord = new Bank_Reference__c();
        bankInformationRecord.Account_Number__c = '34677323';
        bankInformationRecord.Address_Line_1__c = 'sector-62';
        bankInformationRecord.Bank_Contact_Person__c = 'Shubham Lal';
        bankInformationRecord.Bank_Name__c = 'sbi bank';
        bankInformationRecord.City__c = 'Noida';
        bankInformationRecord.Country__c = 'USA';
        bankInformationRecord.Phone_Number__c = '1234567890';
        bankInformationRecord.State__c = 'NY';
        bankInformationRecord.Zip__c = '2345678'; 
        bankInformationRecord.Bank_Details__c = accountRecord.Id;
        insert bankInformationRecord;
        
        //insert communication address lookup with opp.
        Communication_Address__c communicationAddressRecord = new Communication_Address__c();
        communicationAddressRecord.Street_Address_1__c = 'Street 123';
        communicationAddressRecord.City__c = 'New York';
        communicationAddressRecord.County__c = 'testCounty';
        communicationAddressRecord.State__c = 'NY';
        communicationAddressRecord.Postal_Code__c = '12324';
        communicationAddressRecord.Opportunity__c = opportunityRecord.Id;
        insert communicationAddressRecord;
        
 
        Relationship__c relationshipInstance = new Relationship__c();

        relationshipInstance.Person__c = contactRecord.Id;
        relationshipInstance.Related_Opportunity__c = opportunityRecord.Id; 
        relationshipInstance.Percent_of_Ownership__c = 50;
        relationshipInstance.Relationship_Type__c = 'Individual';
        relationshipInstance.RecordTypeId = RELATIONSHIP_INDIVIDUAL_RECORD_TYPE_ID;
        insert relationshipInstance;
        
        Relationship__c relationshipInstance1 = new Relationship__c();

        relationshipInstance1.Business__c = accountRecord.Id;
        relationshipInstance1.Related_Opportunity__c = opportunityRecord.Id; 
        relationshipInstance1.Percent_of_Ownership__c = 50;
        relationshipInstance1.Relationship_Type__c = 'Business Guarantee';
        relationshipInstance1.RecordTypeId = RELATIONSHIP_CORPORATE_RECORD_TYPE_ID;
        insert relationshipInstance1;
    }
    
    
	
    public static void createRelationshipRelatedOpportunityData()
    {
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Accnt';
        accountRecord.billingCity='Tst';
        accountRecord.BillingCountry='tst';
        accountRecord.BillingState='NY';
        accountRecord.BillingStreet='tst';
        accountRecord.BillingPostalCode='11';
        insert accountRecord;
        
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Shubham';
        contactRecord.LastName = 'Lal';
        contactRecord.Email = 'shubham@lal.com';
        contactRecord.MailingStreet = 'street123';
        contactRecord.MailingCity = 'New York';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingPostalCode = '12345';
        contactRecord.MailingCountry = 'USA';
        insert contactRecord;
        
        Opportunity opportunityRecord = new Opportunity();
        opportunityRecord.Name='Test';
        opportunityRecord.StageName='Pre Qual';
        opportunityRecord.LeaseInfo_EquipmentDescription__c='Test';
        opportunityRecord.closeDate=Date.newInstance(2020,11,20);
        insert opportunityRecord;
        
        Relationship__c relationshipInstance = new Relationship__c();
        relationshipInstance.Business__c = accountRecord.Id;
        relationshipInstance.Person__c = contactRecord.Id;
        relationshipInstance.Related_Opportunity__c = opportunityRecord.Id; 
        relationshipInstance.Percent_of_Ownership__c = 50;
        relationshipInstance.Relationship_Type__c = 'Principal';
        relationshipInstance.Related_Opportunity__c = opportunityRecord.Id;
        insert relationshipInstance;
        
    }
	
    public static void CreatePartnerUserAccount()
    {
        // For Partner Account Running User with Role is Required.
        User ValidUser = [select Id ,UserRoleId from user where IsActive=true and profile.name='System Administrator' and UserRole.Name != Null Limit 1];
        System.RunAs(ValidUser)
        {
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                Approval_Last_Credit_Review_Date__c= Date.newInstance(2016, 12, 9),
                Approval_Referral__c='Approved',
                billingCity='New York',
                BillingCountry='USA',
                BillingState='NY',
                Phone='1234567890',
                BillingStreet='street123',
                BillingPostalCode='123456',
                Role__c = 'Broker',
                Location__c = 'Bloomfield'
                
            );
            Database.insert(portalAccount1);
            portalAccount1.isPartner = true;
            update portalAccount1;
            
            String ss = String.valueOf(System.now());
            ss = ss.replace('-','');
            ss = ss.replace(':','');
            ss = ss.replace(' ','');
            //Create contact
            Contact contact1 = new Contact(
                RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Crestmark Contact').getRecordTypeId(),
                FirstName = 'Test',
                Lastname = 'McTesty',
                MailingCity = 'testCity',
                MailingCountry = 'USA',
                MailingState = 'NY',
                MailingStreet = 'testMailStreet',
                MailingPostalCode = '111',
                Relationship_Type__c = 'Broker',
                Is_CVF_Broker__c = true ,
                AccountId = portalAccount1.Id,
                Email = 'test' + ss + 'test@test.com'
            );
            Database.insert(contact1);
            
        }
    }
    
     public static contentversion getContentVersionData()
    {
        ContentVersion content=new Contentversion();
        content.title='ABC';
        content.PathOnClient ='test';
        content.isBrokerUploaded__c = true;
        Blob b=Blob.valueOf('Unit Test Attachment Body');
        content.versiondata=EncodingUtil.base64Decode('Unit Test Attachment Body');
        insert content;
        return content;
    }      
    
    
    
}