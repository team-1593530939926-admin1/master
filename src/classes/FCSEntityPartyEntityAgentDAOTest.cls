@isTest
private class FCSEntityPartyEntityAgentDAOTest {

    private static final String INFORMATION_ID = 'E1234'; 
    private static final String SHOULD_GET_RESULT_ASSERT_MESSAGE = 'Should get result here.';

    @TestSetup
    static void testSetup() {
        List<FCS_Entity_Information__c> listOfEntityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1, true);
        FCS_Entity_Party_Agent__c partyAgent = new FCS_Entity_Party_Agent__c(Name__c = 'xyz', Title__c = 'CEO', Type__c = 'type1');
        List<FCS_Entity_Party_Agent__c> listOfPartyAgent = FCSTestDataFactory.generateFcsEntityPartyAgent(listOfEntityInformation.get(0).Id, 1, false);
        listOfPartyAgent.get(0).Name__c = 'xyz';
        listOfPartyAgent.get(0).Title__c = 'CEO';
        listOfPartyAgent.get(0).Type__c = 'type1';
        insert listOfPartyAgent;
    }

    private static FCS_Entity_Information__c getEntityInformationRecord() {
        return [SELECT Id FROM FCS_Entity_Information__c LIMIT 1];
    }

    private static FCS_Entity_Party_Agent__c getPartyAgentRecord() {
        return [SELECT Id FROM FCS_Entity_Party_Agent__c LIMIT 1];
    }

    @isTest
    private static void getEntityPartyEntityAgentByInformationIdTest() {
        FCS_Entity_Information__c entityInformationRecord = getEntityInformationRecord();
        Test.startTest();
            List<FCS_Entity_Party_Agent__c> partyAgentList = FCSEntityPartyEntityAgentDAO.getEntityPartyEntityAgentByInformationId(new Set<Id> {entityInformationRecord.Id});
        Test.stopTest();

        System.assertEquals(2, partyAgentList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('xyz', partyAgentList.get(0).Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('CEO', partyAgentList.get(0).Title__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('type1', partyAgentList.get(0).Type__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, partyAgentList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void getEntityPartyEntityAgentByIdTest() {
        FCS_Entity_Party_Agent__c partyEntityRecord = getPartyAgentRecord();
        Test.startTest();
            List<FCS_Entity_Party_Agent__c> partyAgentList = FCSEntityPartyEntityAgentDAO.getEntityPartyEntityAgentById(new Set<Id> {partyEntityRecord.Id});
        Test.stopTest();

        System.assertEquals(1, partyAgentList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('xyz', partyAgentList.get(0).Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('CEO', partyAgentList.get(0).Title__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('type1', partyAgentList.get(0).Type__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, partyAgentList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void saveEntityPartyEntityAgentTest() {
        FCS_Entity_Information__c entityInformationRecord = getEntityInformationRecord();
        List<FCS_Entity_Party_Agent__c> listOfPartyAgent = FCSTestDataFactory.generateFcsEntityPartyAgent(entityInformationRecord.Id, 1, false);
        listOfPartyAgent.get(0).Name__c = 'TEST 1';
        listOfPartyAgent.get(0).Title__c = 'CTO';
        listOfPartyAgent.get(0).Type__c = 'PERSONAL';

        Test.startTest();
            FCSEntityPartyEntityAgentDAO.saveEntityPartyEntityAgent(listOfPartyAgent);
        Test.stopTest();

        List<FCS_Entity_Party_Agent__c> listOfResult =
         [SELECT Id, Name__c, Title__c, Type__c, Entity_Information__c 
                FROM FCS_Entity_Party_Agent__c WHERE Name__c = 'TEST 1'];

        System.assertEquals(1, listOfResult.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Name__c, listOfPartyAgent.get(0).Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Title__c, listOfPartyAgent.get(0).Title__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Type__c, listOfPartyAgent.get(0).Type__c, SHOULD_GET_RESULT_ASSERT_MESSAGE); 
    }
}