public class Accounts  {
    
     @TestVisible
     private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    
    @TestVisible
     private static final ContactDAO.IContactDAO contactDAOInstance = new ContactDAO();
    @TestVisible
     private static final OpportunityDAO.IOpportunityDAO opportunityDAOInstance = new OpportunityDAO();
    
    @TestVisible
     private static final AdditionalAddressDAO.IAdditionalAddressDAO additionalAddressDAOInstance = new AdditionalAddressDAO();
    
     private static final Id ACCOUNT_BUSINESS_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Account', 'Business');
    
    //getBusinessRecordTypeId
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getBusinessRecordTypeId()
    {
        RecordTypeWrappers wrapper = new RecordTypeWrappers();
        wrapper.recordTypeId = ACCOUNT_BUSINESS_RECORD_TYPE_ID;
        System.debug('wrapper from domain   ' + wrapper);
        return wrapper;
    }
    
     public static CompanyInformationWrapper getAccountRecordByCompanyCode(String companyCode)
     {
        
         Account accountRecord =  accountDAOInstance.getAccountRecordFromCompanyCode(companyCode);
         if (accountRecord != null) {
             return new CompanyInformationWrapper(accountRecord); 
         }
         return new CompanyInformationWrapper('Company Code does not exist');   
      }
    
  /*   public static CompanyInformationWrapper getCompanyRecordByCompanyId(String companyId)
     {
        
         Account accountRecord =  accountDAOInstance.getCompanyRecordFromCompanyId(companyId);
         if (accountRecord != null) {
             return new CompanyInformationWrapper(accountRecord); 
         }
         return new CompanyInformationWrapper('Company Code does not exist');   
      }    */
    
      public static Account getAccountRecordFromCompanyCode(String companyCode)
      {
          return accountDAOInstance.getAccountRecordFromCompanyCode(companyCode);
      }
    
     public static Account getAccountRecordFromCompanyId(String companyId)
      {
          return accountDAOInstance.getAccountRecordByCompanyId(companyId);
      }    
    
      public static List<Account> getAccountRecordFromApplicationId(String applicationId)
      {
          return accountDAOInstance.getAccountRecordFromApplicationId(applicationId);          
      }
    
   /*   public static List<Account> getTradeAccountRecordFromCompanyId(String companyId)
      {
          return accountDAOInstance.getTradeAccountRecordFromCompanyId(companyId);          
      }    */
    
      public static List<Account> getListOfCorporateGuarantors(Set<Id> setOfAccountId)
      {
          return accountDAOInstance.getListOfCorporateGuarantors(setOfAccountId);
      }
    
    
    
   /*  @AuraEnabled
        public static CompanyInformationWrapper getAccountRecordFromCompanyCodeFromDAO(String companyCode)
        {
        
        Account accountRecord =  accountDAOInstance.getAccountRecordFromCompanyCode(companyCode);
        if (accountRecord != null) {
            return updateWraperInfoBysObjectRecord(accountRecord);  
        }
        return new CompanyInformationWrapper('Company Code does not exist');   
    }
    
 
    @TestVisible
    private static CompanyInformationWrapper updateWraperInfoBysObjectRecord(Account accountRecord)
    {
        CompanyInformationWrapper wrapper = new CompanyInformationWrapper();
        
        wrapper.companyCode = accountRecord.Company_Code__c;        
        wrapper.fullLegalCompanyName = accountRecord.Name;
        wrapper.billingStreetAddress = accountRecord.BillingStreet;
        wrapper.city = accountRecord.BillingCity;
        wrapper.county = accountRecord.BillingCountry;
        wrapper.state = accountRecord.BillingState;
        wrapper.zip = accountRecord.BillingPostalCode;
        wrapper.phoneNumber = accountRecord.Phone;
        wrapper.fax = accountRecord.Fax;
        wrapper.federalTaxId = accountRecord.Federal_Tax_ID__c;
        wrapper.natureOfBusiness = accountRecord.Industry;
        wrapper.dba = accountRecord.DBA__c;
        wrapper.companyType = accountRecord.Entity_Type__c;
        wrapper.stateIncorporated = accountRecord.State_Incorporated__c;
        
        if(!accountRecord.Contacts.isEmpty()) {
            wrapper.contactFirstName = accountRecord.Contacts.get(0).FirstName;
        wrapper.contactLastName = accountRecord.Contacts.get(0).LastName;
        wrapper.emailAddress = accountRecord.Contacts.get(0).Email;
        }
        
        if(!accountRecord.Addresses__r.isEmpty())
        {
        wrapper.otherStreetAddress = accountRecord.Addresses__r.get(0).Street_Address_1__c;
        wrapper.otherCity = accountRecord.Addresses__r.get(0).City__c;
        wrapper.otherCounty = accountRecord.Addresses__r.get(0).County__c;
        wrapper.otherState = accountRecord.Addresses__r.get(0).State__c;
        wrapper.otherZip = accountRecord.Addresses__r.get(0).Postal_Code__c; 
        }
        System.debug('from acoountss  ' + wrapper);
        return wrapper;
    }
    

    public static TradeReferenceWrapper getTradeRecordCompanyCode( String companyCode)
    {
        Account accountRecord = accountDAOInstance.getAccountRecordFromCompanyCode(companyCode);
        if(accountRecord != null)
        {
            return updateWrapperInfoByTradeRecord(accountRecord);
        }
        return new TradeReferenceWrapper();
    }
    
    @TestVisible
    private static TradeReferenceWrapper updateWrapperInfoByTradeRecord(Account accountRecord)
    {
        TradeReferenceWrapper wrapper = new TradeReferenceWrapper();
        
        wrapper.companyName = accountRecord.Name;
        wrapper.addressLine1 = accountRecord.BillingStreet;
        wrapper.cityTrade = accountRecord.BillingCity;
        wrapper.stateTrade = accountRecord.BillingState;
        wrapper.zipTrade = accountRecord.BillingPostalCode;
        wrapper.countryTrade = accountRecord.BillingCountry; 
        wrapper.phoneNumberTrade = accountRecord.Phone;
        wrapper.faxTrade = accountRecord.Fax;
        if( !accountRecord.Contacts.isEmpty())
        {
            wrapper.firstNameTrade = accountRecord.Contacts.get(0).FirstName;
            wrapper.lastNameTrade = accountRecord.Contacts.get(0).LastName;
        }

        return wrapper;   
    } */
    
     public static GuarantorWrapper getGuarantorRecordFromGuarantorId(String companyCode)
     {
        
         Account accountRecord =  AccountDAO.getGuarantorRecordFromGuarantorId(companyCode);
         if (accountRecord != null) {
             return new GuarantorWrapper(accountRecord, null); 
         } 
         //return new GuarantorWrapper('Guarantor does not exist');   
         return null;
      }

    public static void runTamarackProcessAfterPaynetIdUpdate(List<Account> listOfAccounts , Map<Id,Account> oldMapOfAccount){
        Set<String> setOfIds = new Set<String>();
        for(Account accountRecord:listOfAccounts){
            if(accountRecord.RecordTypeId == ACCOUNT_BUSINESS_RECORD_TYPE_ID 
               && oldMapOfAccount.containskey(accountRecord.Id) 
               && accountRecord.TamarackPI__Paynet_Id__c != oldMapOfAccount.get(accountRecord.Id).TamarackPI__Paynet_Id__c 
               && String.isEmpty(oldMapOfAccount.get(accountRecord.Id).TamarackPI__Paynet_Id__c))
            {
                setOfIds.add(accountRecord.Id);
            }
        }
        if(!setOfIds.isEmpty()){
            List<Opportunity> listOfOpportunityRecords = opportunityDAOInstance.getOpportunityByAccountId(setOfIds);
            if(!listOfOpportunityRecords.isEmpty()){
                TC_StartServiceAutomation.start(listOfOpportunityRecords[0].Id);
            }
        }
    }    
}