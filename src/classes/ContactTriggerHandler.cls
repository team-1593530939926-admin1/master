public class ContactTriggerHandler implements TriggerInterface {

    private static List<Contact> listOfContacts;    
    private static Map<Id, Contact> oldContactMap = new Map<Id, Contact>();

    public void beforeInsert(List<sObject> newObjList) {
        
    }

    public void afterInsert(List<sObject> newObjList) {
        listOfContacts = (List<Contact>)newObjList;
        Contacts.createPartnerUserForBrokerContact(listOfContacts, oldContactMap);
    }

    public void beforeUpdate(List<sObject> newObjList, Map<Id, sObject> oldObjMap) {
    }

    public void afterUpdate(List<sObject> newObjList,  Map<Id, sObject> oldObjMap) {
        List<Contact> oldContactList = (List<Contact>) oldObjMap.values();
        oldContactMap = new Map<Id, Contact> (oldContactList);
        listOfContacts = (List<Contact>)newObjList;
        Contacts.createPartnerUserForBrokerContact(listOfContacts, oldContactMap);
        Contacts.updateUserRole(listOfContacts, oldContactMap);
    }

    public void beforeDelete(Map<Id, sObject> oldObjMap) {}

    public void afterDelete(Map<Id, sObject> oldObjMap) {}

    public void afterUnDelete(List<sObject> newObjList) {}
}