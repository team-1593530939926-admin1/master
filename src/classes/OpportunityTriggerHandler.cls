public without sharing class OpportunityTriggerHandler implements TriggerInterface {
    private static List<Opportunity> listOfOpportunities;    
    private static Map<Id, Opportunity> oldOpportunitiesMap = new Map<Id, Opportunity>();
    
    public void beforeInsert(List<sObject> listOfNewSObject)
    {
        // before insert logic
    }
    
    public void afterInsert(List<sObject> listOfNewSObject)
    {
        // after insert logic
    }
    
    public void beforeUpdate(List<sObject> listOfNewSObject, Map<Id, sObject> oldObjMap)
    {
        // before update logic
         List<Opportunity> oldOpportunityList = (List<Opportunity>) oldObjMap.values();
        oldOpportunitiesMap = new Map<Id, Opportunity> (oldOpportunityList);
        listOfOpportunities = (List<Opportunity>)listOfNewSObject;
        Opportunities.updateOpportunityStatus(listOfOpportunities, oldOpportunitiesMap);
    }
    
    public void afterUpdate(List<sObject> listOfNewSObject,  Map<Id, sObject> oldObjMap)
    {
        
    }
    
    public void beforeDelete(Map<Id, sObject> oldObjMap)
    {
        // before delete logic
    }
    
    public void afterDelete(Map<Id, sObject> oldObjMap)
    {
        // after delete logic
    }
    
    public void afterUnDelete(List<sObject> newObjList)
    {
        // after undelete logic
    }
}