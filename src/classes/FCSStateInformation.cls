public class FCSStateInformation {
    
    @AuraEnabled
    public String stateName {get; set;}
    @AuraEnabled
    public String stateCode {get; set;}
    @AuraEnabled
    public String indexDate {get; set;}
    @AuraEnabled
    public String indexOrigin {get; set;}
    @AuraEnabled
    public Boolean allowEntityIDSearch {get; set;}
    
    public FCSStateInformation() {
        
    }
}