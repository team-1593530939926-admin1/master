public class CvfRelationships 
{
    private static final RelationshipDAO.IRelationshipDAO relationshipDAOINstance = new RelationshipDAO();
    
    public static List<Relationship__c> getListOfRelationship(String dealApplicationId)
    {
        return relationshipDAOINstance.getListOfRelationship(dealApplicationId);
    }
}