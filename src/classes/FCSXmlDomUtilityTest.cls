@isTest(isParallel = true)
private class FCSXmlDomUtilityTest {

    private static final String agentXml = '<Agent><Names><EntityName>JOHN FREDDY CHAVERRA</EntityName><Address><Address1>1710 W HILLCREST DR #206</Address1><City>NEWBURY PARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>91320</PostalCode><CountryOrParish /><Country /></Address></Names><Names><EntityName>Rakesh</EntityName><Address><AddressType Type="Officer_Address" /><Address1>RABDAI </Address1><Address2 /><City>DHANI</City><StateOrProvince>MA</StateOrProvince><PostalCode>00110</PostalCode><CountryOrParish /><Country /></Address></Names></Agent>';
    private static final String partiesXml = '<Parties><PartiesType Type="Officers" /><Title Type="CEO" /><Names><EntityName>CHAVERRA, CLAUDIA ALEJANDRA</EntityName><Address><AddressType Type="Officer_Address" /><Address1>1710 W HILLCREST DR #206 </Address1><Address2 /><City>NEWBURY PARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>91320</PostalCode><CountryOrParish /><Country /></Address></Names><Names><EntityName>Rakesh</EntityName><Address><AddressType Type="Officer_Address" /><Address1>RABDAI </Address1><Address2 /><City>DHANI</City><StateOrProvince>MA</StateOrProvince><PostalCode>00110</PostalCode><CountryOrParish /><Country /></Address></Names></Parties>';
    private static final String entityTypeXml = '<EntityType Type="Corporation - Articles of Incorporation"><Domicile><State>CA</State><Country>United States</Country></Domicile><Names><EntityName>PURLUX SKIN CARE, INC.</EntityName><Address><AddressType Type="Filing_Address" /><Name>PURLUX SKIN CARE, INC.</Name><Address1>207 W LOS ANGELES AVE #108 </Address1><Address2 /><City>MOORPARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>93021</PostalCode><CountryOrParish /><Country>United States</Country></Address></Names><EntityID>C4212644</EntityID><EntityStanding Standing="Active" /></EntityType>';
    private static final String entityInfoXml = '<EntityInfo><Dates Type="DateOfIncorporation"><Value>Nov 13, 2018</Value></Dates><Dates Type="FilingDate"><Value>Nov 28, 2018</Value></Dates>'+entityTypeXml+partiesXml+agentXml+'</EntityInfo>';
    private static final String recordXml = '<Record><SeqNumber>1</SeqNumber>'+entityInfoXml+'</Record>';
    private static final String xmlString = '<CorporateResults><XmlData><ProcessInfoRequestResult><XMLVersion/><Header><PacketNum>C4212644</PacketNum><Test Value="No" /></Header>'+recordXml+'</ProcessInfoRequestResult></XmlData></CorporateResults>';

    @isTest
    private static void parseFCSBusinessDetailXmlResponseTest() {

        Test.startTest();
        FCSXmlDomUtility.ProcessInfoRequestResult xmlResponseWrapper = FCSXmlDomUtility.parseFCSBusinessDetailXmlResponse(xmlString);
        Test.stopTest();

        System.assertEquals('1', xmlResponseWrapper.recordInfo.sequenceNumber);
    }

    @isTest
    private static void parseXMLTest() {
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(xmlString);
        Dom.XmlNode node = xmlDoc.getRootElement();
        FCSXmlDomUtility.ProcessInfoRequestResult xmlResponseWrapper = new FCSXmlDomUtility.ProcessInfoRequestResult();
        
        Test.startTest();
        FCSXmlDomUtility.parseXML(node, xmlResponseWrapper);
        Test.stopTest();
        
        System.assertEquals('C4212644', xmlResponseWrapper.headerInfo.packetNumber);
    }

    @isTest
    private static void parseHeaderInfoTest() {
        String headerXml = '<Header>'
                                +'<PacketNum>C1905389</PacketNum>'
                            +'</Header>';
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(headerXml);
        Dom.XmlNode headerXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Header headerInfoObject = new FCSXmlDomUtility.Header();

        Test.startTest();
        FCSXmlDomUtility.parseHeaderInfo(headerXmlNode, headerInfoObject);
        Test.stopTest();

        System.assertEquals('C1905389', headerInfoObject.packetNumber);
    }

    @isTest
    private static void parseRecordInfoTest() {

        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(recordXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Record recordInfo = new FCSXmlDomUtility.Record();
        
        Test.startTest();
        FCSXmlDomUtility.parseRecordInfo (recordXmlNode,  recordInfo);
        
        Test.stopTest();
        System.assertEquals('1', recordInfo.sequenceNumber);
    }

    @isTest
    private static void parseEntityInfoTest() {

        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(entityInfoXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.EntityInfo entityInfoDetails = new FCSXmlDomUtility.EntityInfo();
        
        Test.startTest();
        FCSXmlDomUtility.parseEntityInfo (recordXmlNode,  entityInfoDetails);
        Test.stopTest();
        
        System.assertEquals(2, entityInfoDetails.listOfDates.size());
    }

    @isTest
    private static void parseAgentInfoTest() {

        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(agentXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Agent agentInfoObject = new FCSXmlDomUtility.Agent();
        
        Test.startTest();
        FCSXmlDomUtility.parseAgentInfo(recordXmlNode, agentInfoObject);
        Test.stopTest();
        
        System.assertEquals(2, agentInfoObject.listOfAgentsName.size());
    }

    @isTest
    private static void parsePartyInfoTest() {

        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(partiesXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Parties partyInfoObject = new FCSXmlDomUtility.Parties();
        
        Test.startTest();
        FCSXmlDomUtility.parsePartyInfo(recordXmlNode, partyInfoObject);
        Test.stopTest();
        
        System.assertEquals('Officers', partyInfoObject.partiesType);
    }

    @isTest
    private static void parseDateInfoTest() {
        String datesXml = '<Dates Type=\"DateOfIncorporation\">'
                            +'<Value>Dec 26, 1996</Value>'
                        +'</Dates>';
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(datesXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Dates dateInfoObject = new FCSXmlDomUtility.Dates();
        
        Test.startTest();
        FCSXmlDomUtility.parseDateInfo(recordXmlNode, dateInfoObject);
        Test.stopTest();
        
        System.assertEquals('DateOfIncorporation', dateInfoObject.dateType);
    }

    @isTest
    private static void parseEntityTypeTest() {

        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(entityTypeXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.EntityType entityTypeInfoObject = new FCSXmlDomUtility.EntityType();
        
        Test.startTest();
        FCSXmlDomUtility.parseEntityType(recordXmlNode, entityTypeInfoObject);
        Test.stopTest();
        
        System.assertEquals('C4212644', entityTypeInfoObject.entityId);
    }

    @isTest
    private static void parseDomicileInfoTest() {
        String domicileInfoXml = '<Domicile>'
                                    +'<State>CA</State>'
                                    +'<Country>United States</Country>'
                                +'</Domicile>';
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(domicileInfoXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Domicile domicileInfoObject = new FCSXmlDomUtility.Domicile();
        
        Test.startTest();
        FCSXmlDomUtility.parseDomicileInfo(recordXmlNode, domicileInfoObject);
        Test.stopTest();
        
        System.assertEquals('CA', domicileInfoObject.state);
    }

    @isTest
    private static void parseNamesInfoTest() {
        String namesXml = '<Names>'
                                +'<EntityName>IAN MCDOANLD</EntityName>'
                                +'<Address>'
                                    +'<Address1>839 E GRAND AVE</Address1>'
                                    +'<City>ENCINITAS</City>'
                                    +'<StateOrProvince>CA</StateOrProvince>'
                                    +'<PostalCode>92024</PostalCode>'
                                    +'<CountryOrParish />'
                                    +'<Country />'
                                +'</Address>'
                            +'</Names>';
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(namesXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Names namesInfoObject = new FCSXmlDomUtility.Names();
        
        Test.startTest();
        FCSXmlDomUtility.parseNamesInfo(recordXmlNode, namesInfoObject);
        Test.stopTest();
        
        System.assertEquals('IAN MCDOANLD', namesInfoObject.entityName);
    }

    @isTest
    private static void parseAddressInfoTest() {
        String addressXml = '<Address>'
                                +'<Address1>839 E GRAND AVE</Address1>'
                                +'<City>ENCINITAS</City>'
                                +'<StateOrProvince>CA</StateOrProvince>'
                                +'<PostalCode>92024</PostalCode>'
                                +'<CountryOrParish />'
                                +'<Country />'
                            +'</Address>';
        Dom.Document xmlDoc = new Dom.Document();
        xmlDoc.load(addressXml);
        Dom.XmlNode recordXmlNode = xmlDoc.getRootElement();
        FCSXmlDomUtility.Address addressInfoObject = new FCSXmlDomUtility.Address();
        
        Test.startTest();
        FCSXmlDomUtility.parseAddressInfo(recordXmlNode, addressInfoObject);
        Test.stopTest();
        
        System.assertEquals('ENCINITAS', addressInfoObject.city);
    }
}