public class PartnerDetailsWrapper {
    @AuraEnabled
    public String existingId{get;set;} 
    @AuraEnabled
    public Boolean isCreatedByMe{get;set;}
    @AuraEnabled
    public String maskedDob{get;set;}
    @AuraEnabled
    public String maskedSocialSecurity{get;set;}
    @AuraEnabled
    public String maskedMailingStreet{get;set;} 
    @auraEnabled
    public string maskedFirstName {get;set;}
    @auraEnabled
    public string maskedMiddleName {get;set;}
    @auraEnabled
    public string maskedLastName {get;set;}
    @auraEnabled
    public string maskedHomePhone {get;set;}    
    @AuraEnabled
    public Boolean isDisable{get;set;}    
    @AuraEnabled
    public Integer rowId{get;set;}
    @AuraEnabled
    public Integer count{get;set;}
    @auraEnabled
    public string firstName {get;set;}
    @auraEnabled
    public string middleName {get;set;}
    @auraEnabled
    public string lastName {get;set;}
    @auraEnabled
    public string mailingStreet {get;set;}
    @auraEnabled
    public string mailingCountry {get;set;}
    @auraEnabled
    public string mailingCity {get;set;}
    @auraEnabled
    public string mailingState {get;set;}
    @auraEnabled
    public string mailingPostalCode {get;set;}
    @auraEnabled
    public string homePhone {get;set;}
    @auraEnabled
    public string socialSecurity {get;set;}
    @auraEnabled
    public String dOB {get;set;}
    @auraEnabled
    public string ownershipPercentage {get;set;}
    @auraEnabled
    public string partnerId {get;set;}
    public Contact partnerInformation{get;set;}
    private static final Id BUSINESS_CONTACT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Contact', 'Crestmark Contact');
    private static final Id LOGIN_USER_ID = UserInfo.getUserId();
    public PartnerDetailsWrapper(){

    }

    public PartnerDetailsWrapper(Integer count){
        this.rowId = count;
        this.count = count + 1;
    }
    
    public PartnerDetailsWrapper(PartnerDetailsWrapper partnerInfoWrapper, Boolean isReview){
        this.homePhone = partnerInfoWrapper.homePhone;
        this.firstName = partnerInfoWrapper.firstName;
        this.middleName = partnerInfoWrapper.middleName;
        this.lastName = partnerInfoWrapper.lastName;
        this.socialSecurity = partnerInfoWrapper.socialSecurity;
        this.dOB = partnerInfoWrapper.dOB;
        this.mailingStreet = partnerInfoWrapper.mailingStreet;
        this.mailingCity = partnerInfoWrapper.mailingCity;
        this.mailingState = partnerInfoWrapper.mailingState;
        this.mailingPostalCode = partnerInfoWrapper.mailingPostalCode;
        this.mailingCountry = partnerInfoWrapper.mailingCountry;
        this.ownershipPercentage = partnerInfoWrapper.ownershipPercentage;
        this.existingId  = partnerInfoWrapper.existingId;
        this.isCreatedByMe  = partnerInfoWrapper.isCreatedByMe;
        this.maskedDob = this.dOB != null ? CVFSerciceUtil.emcryptedData(this.dOB,'DOB') : null;
        this.maskedSocialSecurity = this.socialSecurity != null ? CVFSerciceUtil.emcryptedData(this.socialSecurity,'SSN') : null;
        this.maskedMailingStreet = this.mailingStreet != null ? CVFSerciceUtil.emcryptedData(this.mailingStreet,'Street') : null; 
        this.maskedFirstName = this.firstName != null ? CVFSerciceUtil.emcryptedData(this.firstName,'Full') : null; 
        this.maskedMiddleName = this.middleName != null ? CVFSerciceUtil.emcryptedData(this.middleName,'Full') : null; 
        this.maskedLastName = this.lastName != null ? CVFSerciceUtil.emcryptedData(this.lastName,'Full') : null; 
        this.maskedHomePhone = this.homePhone != null ? CVFSerciceUtil.emcryptedData(this.homePhone,'Full') : null; 
    }
    
    public PartnerDetailsWrapper(PartnerDetailsWrapper partnerInfoWrapper, Account companyInfo){
        Contact contactRecord = new Contact(Partners__r = new Account(External_System_Id__c = companyInfo.External_System_Id__c));
        contactRecord.HomePhone =partnerInfoWrapper.homePhone;
        contactRecord.Account = new Account(External_System_Id__c = companyInfo.External_System_Id__c);
        contactRecord.FirstName = partnerInfoWrapper.firstName;
        contactRecord.MiddleName = partnerInfoWrapper.middleName;
        contactRecord.LastName = partnerInfoWrapper.lastName;
        contactRecord.SSN__c = partnerInfoWrapper.socialSecurity;
        contactRecord.Relationship_Type__c = 'Partner';
        contactRecord.RecordTypeId = BUSINESS_CONTACT_RECORD_TYPE_ID;
        if(!String.isEmpty(partnerInfoWrapper.dOB)){
            contactRecord.Birthdate = date.valueof(partnerInfoWrapper.dOB);
        }
        contactRecord.MailingStreet = partnerInfoWrapper.mailingStreet;
        contactRecord.MailingCity = partnerInfoWrapper.mailingCity;
        contactRecord.MailingState = partnerInfoWrapper.mailingState;
        contactRecord.MailingPostalCode = partnerInfoWrapper.mailingPostalCode;
        contactRecord.MailingCountry = partnerInfoWrapper.mailingCountry;
        if(!String.isEmpty(partnerInfoWrapper.ownershipPercentage)){
            contactRecord.Ownership__c = Decimal.valueof(partnerInfoWrapper.ownershipPercentage);
        }
        if(!String.isEmpty(partnerInfoWrapper.existingId)){
            contactRecord.Id = partnerInfoWrapper.existingId;
            this.existingId = partnerInfoWrapper.existingId;
        }
        this.partnerInformation = contactRecord;
    }
    
    public PartnerDetailsWrapper(Contact contactRecord){
        this.firstName = contactRecord.FirstName;
        this.middleName = contactRecord.MiddleName;
        this.lastName = contactRecord.LastName;
        this.mailingStreet = contactRecord.MailingStreet;
        this.mailingCountry = contactRecord.MailingCountry;
        this.mailingCity = contactRecord.MailingCity;
        this.mailingState = contactRecord.MailingState;
        this.mailingPostalCode = contactRecord.MailingPostalCode;
        this.homePhone = contactRecord.HomePhone;
        this.socialSecurity = contactRecord.SSN__c;
        this.dOB = contactRecord.Birthdate != null ? String.valueOf(contactRecord.Birthdate) : null;
        this.ownershipPercentage = contactRecord.Ownership__c != null ? String.valueOf(contactRecord.Ownership__c): null;
        this.partnerId = contactRecord.Id;
        this.existingId = contactRecord.Id;
        this.isCreatedByMe  = true;
        if(contactRecord.CreatedById == LOGIN_USER_ID){
            this.isCreatedByMe = true;
        }else{
            this.isCreatedByMe = false;
        }
        this.maskedDob = this.dOB != null ? CVFSerciceUtil.emcryptedData(this.dOB,'DOB') : null;
        this.maskedSocialSecurity = this.socialSecurity != null ? CVFSerciceUtil.emcryptedData(this.socialSecurity,'SSN') : null;
        this.maskedMailingStreet = this.mailingStreet != null ? CVFSerciceUtil.emcryptedData(this.mailingStreet,'Street') : null;
        this.maskedFirstName = this.firstName != null ? CVFSerciceUtil.emcryptedData(this.firstName,'Full') : null; 
        this.maskedMiddleName = this.middleName != null ? CVFSerciceUtil.emcryptedData(this.middleName,'Full') : null; 
        this.maskedLastName = this.lastName != null ? CVFSerciceUtil.emcryptedData(this.lastName,'Full') : null; 
        this.maskedHomePhone = this.homePhone != null ? CVFSerciceUtil.emcryptedData(this.homePhone,'Full') : null;       
        
    }   
    
}