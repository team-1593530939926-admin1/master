@isTest
private class BankDetailsWrapperTest {
    
    @isTest
    private static void testBankDetailsWrapperIfDataExist()
    {
        Bank_Reference__c bankRecord = TestUtil.getBankDetailsWrapperData();
        Test.startTest();
            BankDetailsWrapper result = new BankDetailsWrapper(bankRecord);
        Test.stopTest();
        System.assertEquals('sbi bank', result.BankName);
    }
    
    @isTest
    private static void testBankDetailsWrapperIfDataNotExist() 
    {
        Bank_Reference__c bankRecord = new Bank_Reference__c();
        Test.startTest();
            BankDetailsWrapper result = new BankDetailsWrapper(bankRecord);
        Test.stopTest();
        System.assertEquals(null, result.BankName);
    }
    
    @isTest
    private static void testDefaultBankDetailsWrapperConstructor()
    {
        Test.startTest();
            BankDetailsWrapper result = new BankDetailsWrapper();
        Test.stopTest();
        System.assertEquals(null, result.BankName);
    }
    
    @isTest
    private static void testBankDetailsWrapperIfWrapperAndBooleanIsAccepted()
    {
        Bank_Reference__c bankRecord = TestUtil.getBankDetailsWrapperData();
        BankDetailsWrapper bankRecordWrapper = new BankDetailsWrapper(bankRecord);
        Test.startTest();
            BankDetailsWrapper result = new BankDetailsWrapper(bankRecordWrapper, true);
        Test.stopTest();
        System.assertEquals('sbi bank', result.BankName);        
    }
    
    @isTest
    private static void testBankDetailsWrapperIfWrapperAndOpportunityIsAccepted()
    {
        Bank_Reference__c bankRecord = TestUtil.getBankDetailsWrapperData();
        BankDetailsWrapper bankRecordWrapper = new BankDetailsWrapper(bankRecord);
        Account accountInfo = TestUtil.createAccountData();
        
        Test.startTest();
            BankDetailsWrapper result = new BankDetailsWrapper(bankRecordWrapper, accountInfo);
        Test.stopTest();
        System.assertEquals('sbi bank', result.dealBankDetails.Bank_Name__c);        
    }
}