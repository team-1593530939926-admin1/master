public with sharing class SecurityUtil
{

    public static List<String> getReadPermissionOfFieldsByObjectName( String objectName, List<String> listOfFields)
    {
        List<String> listOfFieldsToReturn = new List<String>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> objectFields = objectType.getDescribe().fields.getMap();

        for(String fieldName : listOfFields)
        {
            if (objectFields.get(fieldName).getDescribe().isAccessible())
            {
                listOfFieldsToReturn.add(fieldName);

            }       
        }
        return listOfFieldsToReturn; 
    } 

    public static List<String> getEditablePermissionOfFieldsByObjectName( String objectName, List<String> listOfFields)
    {
        List<String> listOfFieldsToReturn = new List<String>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> objectFields = objectType.getDescribe().fields.getMap();

        for(String fieldName : listOfFields)
        {
            if (objectFields.get(fieldName).getDescribe().isCreateable())
            {
                listOfFieldsToReturn.add(fieldName);

            }       
        }
        return listOfFieldsToReturn; 
    }   
    
    public static Boolean getUpdatePermissionOfObject( String objectName)
    {
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        return objectType.getDescribe().isUpdateable();

    }

    public static Boolean getCreatePermissionOfObject( String objectName)
    {
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        return objectType.getDescribe().isCreateable();

    }
    public static Boolean getDeletePermissionOfObject( String objectName)
    {
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        return objectType.getDescribe().isDeletable();

    }
    public static Boolean getReadPermissionOfObject( String objectName)
    {
        SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
        return objectType.getDescribe().isAccessible();

    }

}