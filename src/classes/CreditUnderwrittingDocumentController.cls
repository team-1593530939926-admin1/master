public class CreditUnderwrittingDocumentController {
    private static final OpportunityDAO.IOpportunityDAO opportunityDAOInstance = new OpportunityDAO();
    private static final ContentVersionDAO.IContentVersionDAO contentVersionDAOInstance = new ContentVersionDAO();
    @AuraEnabled (cacheable=true)
    public static List<CreditDocumentList> returnCreditConditionForOpportunity (String opportunityId){
        Opportunity opportunityRecord = opportunityDAOInstance.getOpportunityCreditConditions (opportunityId);
        List<CreditDocumentList> creditList = new List<CreditDocumentList>();
        CreditDocumentList credValue;
        List<String> lstAlpha = opportunityRecord.Credit_Condition__c.split(';');
        List<String> lstOfOtherCreditConditions = new List<String>();
        if(!String.isEmpty(opportunityRecord.Other_Credit_Condition__c)){
            lstOfOtherCreditConditions = opportunityRecord.Other_Credit_Condition__c.split(';|\\\n');
        }        
        system.debug('lstOfOtherCreditConditions'+lstOfOtherCreditConditions);
        String CompanyName = opportunityRecord.Account.Name;
        String dealName = opportunityRecord.Name;
        String equipmentDescription = opportunityRecord.LeaseInfo_EquipmentDescription__c;
        String amount;
         if(opportunityRecord.Amount != null){
                     amount = string.valueOf(opportunityRecord.Amount);
                }
        Integer rowId = 0;
        for(string credValues:lstAlpha){
            credValue = new CreditDocumentList();
            credValue.opportunityId = opportunityId;
            credValue.companyName = CompanyName;
            credValue.dealName = dealName;
            credValue.Amount = amount;
            credValue.equipmentDescription = equipmentDescription;
            credValue.rowId = string.valueOf(rowId);            
            if(credValues != 'Other'){
                credValue.creditConditions = credValues;
                creditList.add(credValue);
            }
            else{
                for(string otherCondition:lstOfOtherCreditConditions){
                    if(!String.isEmpty(otherCondition.trim())){
                        credValue = new CreditDocumentList();
                        credValue.opportunityId = opportunityId;
                        credValue.companyName = CompanyName;
                        credValue.dealName = dealName;
                        credValue.Amount = amount;
                        credValue.equipmentDescription = equipmentDescription;
                        credValue.rowId = string.valueOf(rowId);
                        credValue.creditConditions = otherCondition;
                        system.debug(credValue.creditConditions);
                        creditList.add(credValue);
                        rowId = rowId + 1;
                    }

                }
            }
            rowId = rowId + 1;
            
            
        }
        system.debug('creditList'+creditList);
        return creditList;
    }
    @AuraEnabled
    public static String handleSubmitOfDocuments(List<String> fileDetailData , String opportunityId){
        String status;
        List<ContentVersion> listOfDocuments = new List<ContentVersion>();
            List<ContentDocumentLink> listOfContentDocumentLink = new List<ContentDocumentLink>();
            ContentDocumentLink cDocLink;
        CreditDocumentList credDocIdList = new CreditDocumentList();
            credDocIdList.fileDetailData = fileDetailData;
            if(!credDocIdList.fileDetailData.isEmpty())
            {
                listOfDocuments = contentVersionDAOInstance.getListOfContentVersion(credDocIdList.fileDetailData); 
                for(ContentVersion contentDocumentRecord : listOfDocuments )
                {
                    contentDocumentRecord.isBrokerUploaded__c = false;
                    contentDocumentRecord.Is_Credit_Condition__c = true;
                    cDocLink = new ContentDocumentLink();
                    cDocLink.ContentDocumentId = contentDocumentRecord.ContentDocumentId;//Add ContentDocumentId
                    cDocLink.LinkedEntityId = opportunityId;
                    cDocLink.ShareType = 'V';
                    cDocLink.Visibility = 'AllUsers';
                    listOfContentDocumentLink.add(cDocLink);
                }
            }
            
            if(!listOfDocuments.isEmpty())
            { 
               // update listOfDocuments;
               // insert listOfContentDocumentLink;
               contentVersionDAOInstance.updateContentVersions(listOfDocuments);
               contentVersionDAOInstance.insertListOfContentVersion(listOfContentDocumentLink);
                Opportunity opportunityToUpdate = new Opportunity(Id = opportunityId , Is_Credit_Condition_FullFilled__c = true
                                                                  , Credit_Condition_Submission_Date__c = system.now());
                opportunityDAOInstance.updateOpportunityRecord(opportunityToUpdate);
                //update opportunityToUpdate;
                status = 'success';
            }
        return status;
    }
    
    public class CreditDocumentList{
        @AuraEnabled
        public String dealName{get;set;}
        @AuraEnabled
        public String companyName{get;set;}
        @AuraEnabled
        public String equipmentDescription{get;set;}
        @AuraEnabled
        public String Amount{get;set;}
         @AuraEnabled
        public String creditConditions{get;set;}
        @AuraEnabled
        public String opportunityId{get;set;}
        @AuraEnabled
        public String rowId{get;set;}
        @AuraEnabled 
        public List<String> fileDetailData {get; set;} 
    }
}