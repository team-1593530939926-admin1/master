public class FCSEntityVerificationController {
    
    private static FCSApiService.IFCSApiService fcsApiServiceInstance = new FCSApiService();
    
    @AuraEnabled
    public static FCSBusinessDetails validateEntityByBusinessNameAndStateCode(String businessName, String stateCode) {
        FCSBusinessDetails businessDetailWrapper = new FCSBusinessDetails();
        
        if(String.isBlank(businessName) || String.isBlank(stateCode)) {
            
            String exceptionMessage = System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null;
            if(String.isBlank(stateCode)) {
                exceptionMessage = System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null;
            }
            
            AuraHandledException auraException = new AuraHandledException(exceptionMessage);
            auraException.setMessage(exceptionMessage);
            throw auraException;
        }
        
        HttpResponse businessInfoResponse = fcsApiServiceInstance.searchBusinessByStateAndBusinessName(stateCode, businessName, null, null);
        if(businessInfoResponse.getStatusCode() == FCSAPIConstant.STATUS_CODE_SUCCESS) {
            
            FCSBusinessInformation businessInfoWrapper = (FCSBusinessInformation) JSON.deserialize(businessInfoResponse.getBody(),
                                                                                                   FCSBusinessInformation.class);
            
            if(businessInfoWrapper.header != null && String.isNotBlank(businessInfoWrapper.header.stateCode)) {
                String transactionId = businessInfoWrapper.header.TransactionID;
                
                if(!businessInfoWrapper.results.isEmpty()) {
                    FCSBusinessInformation.Results businessInfoResult = businessInfoWrapper.results.get(0);
                    String entityId = businessInfoResult.entityID;
                    String additionalSearch = businessInfoResult.additionalSearchParameters;
                    String passThrough = businessInfoWrapper.header.passThrough;
                    HttpResponse businessDetailsResponse = fcsApiServiceInstance.getBusinessDetailByEntityAndTransactionId(
                        transactionId, businessInfoResult.EntityID, additionalSearch, passThrough);
                    
                    if(businessDetailsResponse.getStatusCode() == FCSAPIConstant.STATUS_CODE_SUCCESS) {
                        
                        businessDetailWrapper = (FCSBusinessDetails) JSON.deserialize(
                            businessDetailsResponse.getBody(), FCSBusinessDetails.class);
                        System.debug('businessDetailsResponse.getBody(): '+businessDetailsResponse.getBody());
                        businessDetailWrapper.processXmlResponseToWrapper();
                    }
                    else {
                        fcsApiServiceInstance.handleFCSErrorResponseAura(businessDetailsResponse);
                    }
                } else {
                    AuraHandledException customAuraException = new AuraHandledException(System.Label.Text_No_Result_Found);
                    customAuraException.setMessage(System.Label.Text_No_Result_Found);
                    throw customAuraException;
                }
            } else {
                AuraHandledException auraException = new AuraHandledException(System.Label.Text_Record_Not_Found_For_Selected_Request+' : '+System.Label.Text_Business_Name+' = ' + businessName + ' and '+System.Label.Text_State_Code+' = ' + stateCode);
                auraException.setMessage(System.Label.Text_Record_Not_Found_For_Selected_Request+' : '+System.Label.Text_Business_Name+' = ' + businessName + ' and '+System.Label.Text_State_Code+' = ' + stateCode);
                throw auraException;
            }//Text_Record_Not_Found_With_Business_Name_And_State_Code
        }
        else {
            fcsApiServiceInstance.handleFCSErrorResponseAura(businessInfoResponse);
        }
        return  businessDetailWrapper;
    }
    
    @AuraEnabled
    public static List<FCSStateInformation> searchStateInfoForEntityValidation(String stateCode) {
        HttpResponse stateInfoResponse = fcsApiServiceInstance.getStateInfo(stateCode);
        List<FCSStateInformation> listOfStateInfo;
        if(stateInfoResponse.getStatusCode() == FCSAPIConstant.STATUS_CODE_SUCCESS) {
            
            listOfStateInfo = (List<FCSStateInformation>) JSON.deserialize(stateInfoResponse.getBody(), List<FCSStateInformation>.class);
            
            if (listOfStateInfo.isEmpty()) {
                System.debug(LoggingLevel.INFO, 'No result found!!');
                AuraHandledException customAuraException = new AuraHandledException(System.Label.Text_No_Result_Found);
                customAuraException.setMessage(System.Label.Text_No_Result_Found);
                throw customAuraException;
            }
        } else {
            fcsApiServiceInstance.handleFCSErrorResponseAura(stateInfoResponse);
        }
        return listOfStateInfo;
    }
    
    @AuraEnabled
    public static FCSBusinessInformation getBusinessInfoByBusinessNameAndStateCode(String businessName, String stateCode) {
        FCSBusinessDetails businessDetailWrapper = new FCSBusinessDetails();
        
        if(String.isBlank(businessName) || String.isBlank(stateCode)) {
            
            String exceptionMessage = System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null;
            if(String.isBlank(stateCode)) {
                exceptionMessage = System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null;
            }
            
            AuraHandledException auraException = new AuraHandledException(exceptionMessage);
            auraException.setMessage(exceptionMessage);
            throw auraException;
        }
        
        
        HttpResponse businessInfoResponse = fcsApiServiceInstance.searchBusinessByStateAndBusinessName(stateCode, businessName, null, null);
        FCSBusinessInformation businessInfoWrapper;
        if(businessInfoResponse.getStatusCode() == FCSAPIConstant.STATUS_CODE_SUCCESS) {
            
            businessInfoWrapper = (FCSBusinessInformation) JSON.deserialize(businessInfoResponse.getBody(),
                                                                            FCSBusinessInformation.class);
            
            if(businessInfoWrapper.header == null || String.isBlank(businessInfoWrapper.header.stateCode)) {
                AuraHandledException auraException = new AuraHandledException(System.Label.Text_Record_Not_Found_For_Selected_Request+' : '+System.Label.Text_Business_Name+' = ' + businessName + ' and '+System.Label.Text_State_Code+' = ' + stateCode);
                auraException.setMessage(System.Label.Text_Record_Not_Found_For_Selected_Request+' : '+System.Label.Text_Business_Name+' = ' + businessName + ' and '+System.Label.Text_State_Code+' = ' + stateCode);
                throw auraException;				
            }
            if(businessInfoWrapper.results.isEmpty()) {
                AuraHandledException customAuraException = new AuraHandledException(System.Label.Text_No_Result_Found);
                customAuraException.setMessage(System.Label.Text_No_Result_Found);
                throw customAuraException;
            }
        } else {
            fcsApiServiceInstance.handleFCSErrorResponseAura(businessInfoResponse);
        }
        return businessInfoWrapper;
    }   
    
    @AuraEnabled
    public static FCSBusinessDetails getBusinessDetailsByTransactionAndEntityId(String transactionId, String entityId, String additionalSearch, String passThrough) {
        
        if(String.isBlank(transactionId) || String.isBlank(entityId)) {
            
            String exceptionMessage = System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null;
            if(String.isBlank(entityId)) {
                exceptionMessage = System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null;
            }
            
            AuraHandledException auraException = new AuraHandledException(exceptionMessage);
            auraException.setMessage(exceptionMessage);
            throw auraException;
        }
        
        
        FCSBusinessDetails businessDetailWrapper = new FCSBusinessDetails();
        HttpResponse businessDetailsResponse = fcsApiServiceInstance.getBusinessDetailByEntityAndTransactionId(
            transactionId, entityId, additionalSearch, passThrough);
        
        if(businessDetailsResponse.getStatusCode() == FCSAPIConstant.STATUS_CODE_SUCCESS) {
            
            businessDetailWrapper = (FCSBusinessDetails) JSON.deserialize(
                businessDetailsResponse.getBody(), FCSBusinessDetails.class);
            businessDetailWrapper.processXmlResponseToWrapper();
        }
        else {
            fcsApiServiceInstance.handleFCSErrorResponseAura(businessDetailsResponse);
        }
        return businessDetailWrapper;
    }
    
    @AuraEnabled
    public static void saveEntityVerificationDetails(String recordId, String businessDetailWrapperString) {
        System.debug('businessDetailWrapper>> ' + businessDetailWrapperString);
        if (String.isBlank(businessDetailWrapperString) || String.isBlank(recordId)) {
            String exceptionMessage = System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null;
            if (String.isBlank(businessDetailWrapperString)) {
                exceptionMessage = System.Label.Text_Verification_Details_Can_Not_Be_Empty;
            }
            
            AuraHandledException auraException = new AuraHandledException(exceptionMessage);
            auraException.setMessage(exceptionMessage);
            throw auraException;
        }
        
        FCSBusinessDetails businessDetailWrapper = (FCSBusinessDetails) JSON.deserialize(businessDetailWrapperString, FCSBusinessDetails.class);
        List<FCSEntityVerifications.FCSEntityVerificationWrapper> listOfFCSEntityVerificationWrapper = 
            FCSEntityVerifications.generateEntityObjectFromWrapper(businessDetailWrapper);
        if (!listOfFCSEntityVerificationWrapper.isEmpty()) {
            System.debug('listOfFCSEntityVerificationWrapper>> ' + listOfFCSEntityVerificationWrapper);
            FCSEntityVerifications.saveEntityVerificationInformation(listOfFCSEntityVerificationWrapper, recordId);
        }
    }
    
    @AuraEnabled
    public static Boolean isNeedToFCSAPICall(String recordId) {
        if (String.isNotBlank(recordId)) {
            return !FCSEntityVerifications.isSFEntityDataAvailableInSFForThisMonthRange(Id.valueOf(recordId));
        }
        return true;
    }
    
    @AuraEnabled
    public static FCSEntityVerificationSF getEntityVerificationDetailByCompanyIdSF(String recordId) {
        FCSEntityVerificationSF entityVerificationSF = new FCSEntityVerificationSF();
        
        if (String.isNotBlank(recordId)) {
            FCSEntityVerifications.FCSEntityVerificationWrapper verificationDetailBySF;
            
            if (Id.valueOf(recordId).getSobjectType() == Account.getSObjectType()) {
                verificationDetailBySF = FCSEntityVerifications.getEntityInformationWithInMonthByCompany(recordId); 
            }
            
            if (Id.valueOf(recordId).getSobjectType() == Lead.getSObjectType()) {
                verificationDetailBySF = FCSEntityVerifications.getEntityInformationWithInMonthByLead(recordId); 
            }
            
            
            if (verificationDetailBySF != null) {
                entityVerificationSF.entityInformation =  verificationDetailBySF.entityInformation;
                entityVerificationSF.listOfEntityCommunicationAddress = verificationDetailBySF.listOfEntityCommunicationAddress;
                
                List<FCSPartySF> listOfEntityParties = new List<FCSPartySF>();
                List<FCSAgentSF> listOfAgentSF = new List<FcsAgentSF>();
                
                for (FCSEntityVerifications.FCSPartyAgentWrapper eachPartyAgent : verificationDetailBySF.listOfEntityPartyOrAgent) {
                    FCS_Entity_Party_Agent__c partyOrAgent = eachPartyAgent.entityPartyOrAgent;
                    if (partyOrAgent.RecordType.Name == 'Agent') {
                        listOfAgentSF.add( new FCSAgentSF(partyOrAgent, eachPartyAgent.listOfPartyOrAgentCommunicationAddress));
                    }
                    
                    if (partyOrAgent.RecordType.Name == 'Party') {
                        listOfEntityParties.add( new FCSPartySF(partyOrAgent, eachPartyAgent.listOfPartyOrAgentCommunicationAddress));
                    }
                }
                
                entityVerificationSF.listOfAgentSF = listOfAgentSF;
                entityVerificationSF.listOfPartySF = listOfEntityParties;
            }
        }
        return entityVerificationSF;
    }
    
    public class FCSEntityVerificationSF {
        @AuraEnabled
        public FCS_Entity_Information__c entityInformation {get;set;}
        @AuraEnabled
        public List<Communication_Address__c> listOfEntityCommunicationAddress {get;set;}
        @AuraEnabled
        public List<FCSPartySF> listOfPartySF {get;set;}
        @AuraEnabled
        public List<FCSAgentSF> listOfAgentSF {get;set;}
        
        public FCSEntityVerificationSF() {}
        
        public FCSEntityVerificationSF(FCS_Entity_Information__c entityInformation, 
                                       List<Communication_Address__c> listOfEntityCommunicationAddress, List<FCSPartySF> listOfPartySF, 
                                       List<FCSAgentSF> listOfAgentSF) {
                                           this.entityInformation = entityInformation;
                                           this.listOfEntityCommunicationAddress = listOfEntityCommunicationAddress;
                                           this.listOfPartySF = listOfPartySF;
                                           this.listOfAgentSF = listOfAgentSF;
                                       }
    }
    
    public class FCSPartySF {
        @AuraEnabled
        public FCS_Entity_Party_Agent__c entityParty {get;set;}
        @AuraEnabled
        public List<Communication_Address__c> listOfPartyAddress {get;set;}
        
        public FCSPartySF() {}
        
        public FCSPartySF(FCS_Entity_Party_Agent__c entityParty, List<Communication_Address__c> listOfPartyAddress) {
            this.entityParty = entityParty;
            this.listOfPartyAddress = listOfPartyAddress;
        }
        
    }
    
    public class FCSAgentSF {
        @AuraEnabled
        public FCS_Entity_Party_Agent__c entityAgent {get;set;}
        @AuraEnabled
        public List<Communication_Address__c> listOfAgentAddress {get;set;}
        
        public FCSAgentSF() {}
        
        public FCSAgentSF(FCS_Entity_Party_Agent__c entityAgent, List<Communication_Address__c> listOfAgentAddress) {
            this.entityAgent = entityAgent;
            this.listOfAgentAddress = listOfAgentAddress;
        }
    }
}