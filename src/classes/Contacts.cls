public class Contacts {
    private static final Id BUSINESS_CONTACT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Contact', 'Crestmark Contact');
    private static final ContactDAO.IContactDAO contactDAOInstance = new ContactDAO();
    private static final String RELATIONSHIP_TYPE_BROKER = 'Broker';
    private static final String REFERRAL_TYPE_APPROVED = 'Approved';
    private static final String ROLE_TYPE_BROKER = 'Broker';
    private static final String ROLE_TYPE_VENDOR = 'Vendor';
    private static final String ACCOUNT_RECORDTYPE_BUSINESS = 'Business';
    private static final String BROKER_PROFILE_NAME = 'Crestmark Broker Profile';
    private static final String BROKER_PERMISSIONSET_NAME = 'Broker_Crestmark_Permission_Set';
    private static final String FCS_API_PERMISSIONSET_NAME = 'FCSApiPermissionSet';
    private static final String LANGUAGE_LOCALE_KEY = 'en_US';
    private static final String EMAIL_ENCODING_KEY = 'UTF-8';
    private static final String TIME_ZONE_KEY = 'America/New_York';
    private static final String CONTACT_RECORDTYPEID_BUSINESS_CONTACT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Crestmark Contact').getRecordTypeId();
    private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    private static final ProfileDAO.IProfileDAO profileDAOInstance = new ProfileDAO();
    private static final UserDAO.IUserDAO userDAOInstance = new UserDAO();
    private static final PermissionSetDAO.IPermissionSetDAO permissionSetDAOInstance = new PermissionSetDAO();
    private static final CVFCustomSettingsDAO.ICVFCustomSettingsDAO cVFCustomSettingsDAOInstance = new CVFCustomSettingsDAO();

    public static Boolean createPartnerUserForBrokerContact(List<Contact> listOfContact, Map<Id, Contact> mapOfOldContacts){
        List<User> listOfUser = new List<User>();
        List<User> listOfExistingUser = new List<User>();
        List<User> listOfUserToUpdate = new List<User>();
        Id brokerProfileId;
        User userRecord;
        List<Id> listOfUserIdNeedToActive = new List<Id>();
        List<Id> listOfUserIdNeedToInActive = new List<Id>();
        Set<Id> setOfAccountIds = new Set<Id>();
        Set<Id> setOfContactIds = new Set<Id>();
        Set<Id> setOfInActiveContactIds = new Set<Id>();
        Set<Id> setOfActiveContactIds = new Set<Id>();
        Set<Id> setOfPrimaryContactIds = new Set<Id>();
        Map<Id, Account> mapOfAccount = new Map<Id,Account>();
        Map<Id, User> mapOfUser = new Map<Id, User>();
        Map<Id, User> mapOfExistingUser = new Map<Id, User>();
        Map<String, Id> mapOfCVFBrokerRoles = new Map <String, Id>();
        Map<Id, Contact> mapOfContact = new Map<Id, Contact>(listOfContact);
        List<Contact> listOfbrokerContact = new List<Contact>();
        List<UserRole> listOfUserRoles = new List<UserRole>();
        Map<String, UserRole> mapOfUserRoles = new Map<String, UserRole>();
        
        for(Contact contactRecord : listOfContact){
            if((contactRecord.Relationship_Type__c == RELATIONSHIP_TYPE_BROKER
                && contactRecord.RecordTypeId == CONTACT_RECORDTYPEID_BUSINESS_CONTACT
                && mapOfOldContacts.isEmpty()) || (mapOfOldContacts.containskey(contactRecord.Id)
                && ((contactRecord.Relationship_Type__c == RELATIONSHIP_TYPE_BROKER
                && contactRecord.Relationship_Type__c != mapOfOldContacts.get(contactRecord.Id).Relationship_Type__c) 
                || (contactRecord.Is_CVF_Broker__c != mapOfOldContacts.get(contactRecord.Id).Is_CVF_Broker__c)))){
                setOfAccountIds.add(contactRecord.AccountId);
                setOfContactIds.add(contactRecord.Id);
            }
        }        
        if(!setOfAccountIds.isEmpty()){
            List<Account> listOfAccounts = accountDAOInstance.getListOfAccounts(setOfAccountIds);
            listOfExistingUser = [SELECT Id,Name,IsActive,ContactId From User Where ContactId In : setOfContactIds];
            listOfUserRoles = [SELECT Name,Id, PortalAccountId FROM UserRole Where PortalAccountId In: setOfAccountIds];
            if(!listOfUserRoles.isEmpty()){
                for(UserRole userRoleRecord : listOfUserRoles){
                    if(userRoleRecord.Name.contains('Partner User')){
                        mapOfUserRoles.put(userRoleRecord.PortalAccountId + 'PartnerUser', userRoleRecord);
                    }else if(userRoleRecord.Name.contains('Partner Manager')){
                        mapOfUserRoles.put(userRoleRecord.PortalAccountId + 'PartnerManager', userRoleRecord);
                    }                    
                }
            }
            for(Account accountRecord : listOfAccounts){
                if(accountRecord.isPartner && accountRecord.Approval_Referral__c == REFERRAL_TYPE_APPROVED
                   && (accountRecord.Role__c.contains(ROLE_TYPE_BROKER) || accountRecord.Role__c.contains(ROLE_TYPE_VENDOR))
                   && accountRecord.RecordType.Name == ACCOUNT_RECORDTYPE_BUSINESS){
                       mapOfAccount.put(accountRecord.Id, accountRecord);
                   }
            }
            if(!mapOfAccount.isEmpty()){
                brokerProfileId = profileDAOInstance.getProfileByName(BROKER_PROFILE_NAME)[0].Id;
            }
            if(!listOfExistingUser.isEmpty()){
                for(User existingUserRecord : listOfExistingUser){
                    mapOfUser.put(existingUserRecord.ContactId, existingUserRecord);
                }
            } 
        }
        for(Contact contactRecord : listOfContact){
            if((!mapOfUser.containsKey(contactRecord.Id) 
                && setOfContactIds.contains(contactRecord.Id))
                && mapOfAccount.containskey(contactRecord.AccountId)
                && contactRecord.Is_CVF_Broker__c == true){
                   userRecord = new User();
                   userRecord.FirstName = contactRecord.FirstName;
                   userRecord.LastName = contactRecord.LastName;
                   userRecord.MiddleName = contactRecord.MiddleName;
                   userRecord.Email = contactRecord.Email;
                   userRecord.UserName = contactRecord.Email + '.brokerrr';
                   System.debug('userRecord.UserName : ' + userRecord.UserName);
                   userRecord.ContactId = contactRecord.Id;
                   userRecord.MobilePhone = contactRecord.MobilePhone;
                   userRecord.CompanyName = mapOfAccount.get(contactRecord.AccountId).Name;
                   userRecord.CommunityNickname = contactRecord.LastName;
                   userRecord.Alias = contactRecord.FirstName.substring(0,1) + contactRecord.LastName.substring(0,3);
                   userRecord.TimeZoneSidKey = TIME_ZONE_KEY;
                   userRecord.LocaleSidKey = LANGUAGE_LOCALE_KEY;
                   userRecord.EmailEncodingKey = EMAIL_ENCODING_KEY;
                   userRecord.ProfileId = brokerProfileId;
                   userRecord.LanguageLocaleKey = LANGUAGE_LOCALE_KEY;
                   userRecord.Street = contactRecord.MailingStreet;
                   userRecord.City = contactRecord.MailingCity;
                   userRecord.State = contactRecord.MailingState;
                   userRecord.PostalCode = contactRecord.MailingPostalCode;
                   userRecord.Country = contactRecord.MailingCountry;
                   if (contactRecord.Primary_Contact__c == true 
                        && mapOfUserRoles.containskey(contactRecord.AccountId + 'PartnerManager')) {
                        userRecord.UserRoleId = mapOfUserRoles.get(contactRecord.AccountId + 'PartnerManager').Id;
                   } else if (contactRecord.Primary_Contact__c == false 
                        && mapOfUserRoles.containskey(contactRecord.AccountId + 'PartnerUser')) {
                        userRecord.UserRoleId = mapOfUserRoles.get(contactRecord.AccountId + 'PartnerUser').Id;
                   }
                   listOfUser.add(userRecord);
               }
               else if(mapOfAccount.containskey(contactRecord.AccountId) 
                   && mapOfUser.containsKey(contactRecord.Id)
                   && contactRecord.Is_CVF_Broker__c == false
                   && mapOfUser.get(contactRecord.Id).IsActive == true){
                   listOfUserIdNeedToInActive.add(mapOfUser.get(contactRecord.Id).Id);
               }
        }
        
        if(!listOfUserIdNeedToInActive.isEmpty()){
            updateUserToInActive(listOfUserIdNeedToInActive);
        }
        
        if(!listOfUser.isEmpty()){
            String listOfUserJSON = JSON.serialize(listOfUser);
            insertUserFutureMethod(listOfUserJSON);
            return true;
        }else{
            return false;
        }
    }   

    @future
    private static void insertUserFutureMethod(String listOfUserJSON) {
        List<User> listOfUser = (List<User>)JSON.deserialize(listOfUserJSON, List<User>.class);
        if(!listOfUser.isEmpty()){
            insert listOfUser;
        }        
    }
    
    @future
    private static void updateUserFutureMethod(String listOfUserJSON) {
        List<User> listOfUser = (List<User>)JSON.deserialize(listOfUserJSON, List<User>.class);
        if(!listOfUser.isEmpty()){
            update listOfUser;
        }        
    }

     
    public static void AssignPermissionSetToUsers (Set<Id> setOfpermissionSetIds, Set<Id> setOfUserIds) {
        List<PermissionSetAssignment> listOfPermissionSetAssignment = new List<PermissionSetAssignment>();
        for(Id insertedUserRecordId : setOfUserIds){
            for (Id permissionSetId : setOfpermissionSetIds) {
                listOfPermissionSetAssignment.add(new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = insertedUserRecordId));
            }
        }
        permissionSetDAOInstance.inserPermissionSetAssignment(listOfPermissionSetAssignment);
    }
    
    @future
    public static void updateUserToInActive(List<Id> listOfUserIdNeedToInActive) {
        List<User> listOfExistingUsers = [Select Id, IsActive From User Where Id In: listOfUserIdNeedToInActive];
        for(User userRecord : listOfExistingUsers){
            userRecord.IsActive = false;
        }
        update listOfExistingUsers;
    }
    
    @InvocableMethod(label='Update CVF Contact')
    public static void updateContactToActive(List<Contact> contactId) {
        Set<Id> setOfIds = new Set<Id>();
        for(Contact contactRecord : contactId){
            setOfIds.add(contactRecord.Id);
        }
        updateContact(setOfIds);
    } 
    
    @future
    public static void updateContact(Set<Id> contactIds) {
        List<Contact> listOfContacts = new List<Contact>();
        for(Id contactRecordId : contactIds){
            listOfContacts.add(new Contact(Id = contactRecordId, Is_CVF_Broker__c = true));
        }
        update listOfContacts;
    } 
    
    public static List<Contact> getListOfPersonalGuarantor(Set<Id> setOfContactId){
        return contactDAOInstance.getListOfPersonalGuarantor(setOfContactId);
    }
    
    
    public static GuarantorWrapper getGuarantorRecordFromGuarantorId(String companyCode){        
        Contact contactRecord =  ContactDAO.getGuarantorRecordFromGuarantorId(companyCode);
        if (contactRecord != null) {
            return new GuarantorWrapper(contactRecord, null); 
        }  
        return null;
    }
    
    public static void updateUserRole(List<Contact> listOfContact, Map<Id, Contact> mapOfOldContacts){
        Set<Id> setOfContactIds = new Set<Id>();
        Set<Id> setOfAccountIds = new Set<Id>();
        Map<Id, User> mapOfUser = new Map<Id, User>();
        List<User> listOfUserToUpdate = new List<User>();
        List<UserRole> listOfUserRoles = new List<UserRole>();
        Map<String, UserRole> mapOfUserRoles = new Map<String, UserRole>();
        for(Contact contactRecord : listOfContact){
            if(contactRecord.Relationship_Type__c == RELATIONSHIP_TYPE_BROKER
               && contactRecord.RecordTypeId == CONTACT_RECORDTYPEID_BUSINESS_CONTACT
               && contactRecord.Primary_Contact__c != mapOfOldContacts.get(contactRecord.Id).Primary_Contact__c){
               setOfContactIds.add(contactRecord.Id);
               setOfAccountIds.add(contactRecord.AccountId);
            }            
        }
        List<User> listOfExistingUser = [SELECT Id,Name,IsActive,ContactId From User Where ContactId In : setOfContactIds];
        if(!listOfExistingUser.isEmpty()){
            for(User existingUserRecord : listOfExistingUser){
                if(existingUserRecord.IsActive){
                    mapOfUser.put(existingUserRecord.ContactId, existingUserRecord);
                }                
            }
        } 
        listOfUserRoles = [SELECT Name,Id, PortalAccountId FROM UserRole Where PortalAccountId In: setOfAccountIds];
        if(!listOfUserRoles.isEmpty()){
            for(UserRole userRoleRecord : listOfUserRoles){
                if(userRoleRecord.Name.contains('Partner User')){
                    mapOfUserRoles.put(userRoleRecord.PortalAccountId + 'PartnerUser', userRoleRecord);
                }else if(userRoleRecord.Name.contains('Partner Manager')){
                    mapOfUserRoles.put(userRoleRecord.PortalAccountId + 'PartnerManager', userRoleRecord);
                }                    
            }
        }
        for(Contact contactRecord : listOfContact){
            if(mapOfUser.containskey(contactRecord.Id)){
                if(contactRecord.Primary_Contact__c == true && mapOfUserRoles.containskey(contactRecord.AccountId + 'PartnerManager')){
                    listOfUserToUpdate.add(new User(Id = mapOfUser.get(contactRecord.Id).Id, UserRoleId = mapOfUserRoles.get(contactRecord.AccountId + 'PartnerManager').Id));
                }else if(contactRecord.Primary_Contact__c == false && mapOfUserRoles.containskey(contactRecord.AccountId + 'PartnerUser')){
                    listOfUserToUpdate.add(new User(Id = mapOfUser.get(contactRecord.Id).Id, UserRoleId = mapOfUserRoles.get(contactRecord.AccountId + 'PartnerUser').Id));
                }
            }            
        }
        if(!listOfUserToUpdate.isEmpty()){
            String listOfUserJSON = JSON.serialize(listOfUserToUpdate);
            updateUserFutureMethod(listOfUserJSON);
        }
    }
}