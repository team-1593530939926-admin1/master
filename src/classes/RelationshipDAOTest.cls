@isTest
private class RelationshipDAOTest 
{
    @testSetup
    private static void testSetupData()
    {
        TestUtil.createRelationshipRelatedOpportunityData();
    }
    
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity];
    }
    
    @isTest
    private static void testGetListOfRelationshipIfApplicationIdIsAccepted()
    {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Opportunity opportunityRecord = listOfOpportunity[0];
        
        Test.startTest();
             List<Relationship__c> listOfrelationships = RelationshipDAO.getListOfRelationship(opportunityRecord.Application_Id__c);
        Test.stopTest(); 
        System.assertEquals(1, listOfrelationships.size());
    }
    
    @isTest
    private static void testGetListOfRelationshipIfApplicationIdIsNotAccepted()
    {
        Test.startTest();
             List<Relationship__c> listOfrelationships = RelationshipDAO.getListOfRelationship('');
        Test.stopTest();  
        System.assertEquals(0, listOfrelationships.size());
    }
    
    @isTest
    private static void insertRelationshipsTestIfListIsAccepted() {

        List<Relationship__c> relationshipList = TestUtil.createRelationshipObjectData();
        Test.startTest();
             List<Relationship__c> listOfRelationships = RelationshipDAO.insertRelationships(relationshipList);
        Test.stopTest(); 
        System.assertEquals(1, listOfRelationships.size(), 'Records not inserted.');
    }
    
    @isTest
    private static void insertRelationshipsTestIfListIsNotAccepted() { 

        List<Relationship__c> relationshipList = new List<Relationship__c>();
        Test.startTest();
             List<Relationship__c> listOfRelationships = RelationshipDAO.insertRelationships(relationshipList);
        Test.stopTest(); 
        System.assertEquals(0, listOfRelationships.size(), 'Records inserted.');
    }    
}