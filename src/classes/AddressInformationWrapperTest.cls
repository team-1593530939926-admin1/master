@isTest
private class AddressInformationWrapperTest {
    
    @isTest
    private static void testAddressInformationWrapperIfCommunicationAddressExists()
    {
        Communication_Address__c addressRecord = TestUtil.getCommunicationAddressWrapperData();
               
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper(addressRecord);
        Test.stopTest();
        
        System.assertEquals('New York', result.otherCity);
    }
    
    @isTest
    private static void testAddressInformationWrapperIfCommunicationAddressNotExists()
    {
        Communication_Address__c addressRecord = new Communication_Address__c();
               
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper(addressRecord);
        Test.stopTest();
        
        System.assertEquals(null , result.otherCity);
    }

    @isTest
    private static void testAddressInformationWrapperIfAdditionalAddressExists()
    {
        Additional_Address__c addressRecord = TestUtil.getAdditionalAddressWrapperData();
               
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper(addressRecord);
        Test.stopTest();
        
        System.assertEquals('New York', result.otherCity);
    }
    
    @isTest
    private static void testAddressInformationWrapperIsReview()
    {
        Additional_Address__c addressRecord = TestUtil.getAdditionalAddressWrapperData();
               
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper(addressRecord);
        	AddressInformationWrapper result1 = new AddressInformationWrapper(result, true);
        Test.stopTest();
        
        System.assertEquals('New York', result1.otherCity);
    }
    
    @isTest
    private static void testAddressInformationWrapperIfAdditionalAddressNotExists()
    {
        Additional_Address__c addressRecord = new Additional_Address__c();
               
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper(addressRecord);
        Test.stopTest();
        
        System.assertEquals(null, result.otherCity);
    }
    
    @isTest
    private static void testDefaultAddressInformationWrapper()
    {
        Additional_Address__c addressRecord = new Additional_Address__c();
        
        Test.startTest();
            AddressInformationWrapper result = new AddressInformationWrapper();
        Test.stopTest();
        System.assertEquals(null, result.otherCity);
    }

}