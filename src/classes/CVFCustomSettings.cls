public class CVFCustomSettings {
    
    
    private static final CVFCustomSettingsDAO.ICVFCustomSettingsDAO cvfCustomSettingsDAOInstance = new CVFCustomSettingsDAO();

  
    public static  List<Country_State_Mapping__c> getCountryAndStateValues()
    {
        List<Country_State_Mapping__c> stateCountryList = cvfCustomSettingsDAOInstance.getCountryAndStateValues();
        return stateCountryList ;

    }

    
    public static  List<CVF_Equipment_Description__c> getDescriptionOptionValues()
    {
        List<CVF_Equipment_Description__c> descriptionOptionList = cvfCustomSettingsDAOInstance.getDescriptionOptionValues();
        return descriptionOptionList ;

    }
    
    public static List<CVF_File_Extensions__c> getFilExtensionValues()
    {
        List<CVF_File_Extensions__c> fileExtensionList = cvfCustomSettingsDAOInstance.getFilExtensionValues();
        return fileExtensionList;
    }
    
     public static Map<String, String> getMapOfEquipmentValues()
    {
       Map<String, String> mapOfEquipmentDiscription = new Map<String, String>();
       List<CVF_Equipment_Description__c> descriptionOptionList = cvfCustomSettingsDAOInstance.getDescriptionOptionValues();
        For(CVF_Equipment_Description__c EquipmentValue:descriptionOptionList){
            mapOfEquipmentDiscription.put(EquipmentValue.Description_Label__c,EquipmentValue.Equipment_Category__c);
        }
        return mapOfEquipmentDiscription;
    }
        
}