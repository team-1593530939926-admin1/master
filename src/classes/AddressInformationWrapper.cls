public class AddressInformationWrapper{
    @AuraEnabled
    public String otherStreetAddress{get;set;}
    @AuraEnabled
    public String otherCity{get;set;}
    @AuraEnabled
    public String otherCounty{get;set;}
    @AuraEnabled
    public String otherState{get;set;}
    @AuraEnabled
    public String otherZip{get;set;}  
    
    public AddressInformationWrapper(){
        this.otherStreetAddress = null;
        this.otherCity = null;
        this.otherCounty = null;
        this.otherState = null;
        this.otherZip = null;
    }
    public AddressInformationWrapper(AddressInformationWrapper addressInfoWrapper , Boolean isReview){
        this.otherStreetAddress =addressInfoWrapper.otherStreetAddress;
        this.otherCity = addressInfoWrapper.otherCity;
        this.otherCounty = addressInfoWrapper.otherCounty;
        this.otherState = addressInfoWrapper.otherState;
        this.otherZip = addressInfoWrapper.otherZip;
    }
    
    public AddressInformationWrapper(Communication_Address__c addressRecord){
        this.otherStreetAddress = addressRecord.Street_Address_1__c;
        this.otherCity = addressRecord.City__c;
        this.otherCounty = addressRecord.County__c;
        this.otherState = addressRecord.State__c;
        this.otherZip = addressRecord.Postal_Code__c;
    }
    
    public AddressInformationWrapper(Additional_Address__c addressRecord){
        this.otherStreetAddress = addressRecord.Street_Address_1__c;
        this.otherCity = addressRecord.City__c;
        this.otherCounty = addressRecord.County__c;
        this.otherState = addressRecord.State__c;
        this.otherZip = addressRecord.Postal_Code__c;
    }
}