@isTest(isParallel=true)
private class FCSEntityVerificationControllerTest {
    
    private static final Integer STATUS_CODE_SUCCESS = 200; 
    private static final Integer STATUS_CODE_BAD_REQUEST = 400;
    private static final Integer STATUS_CODE_INTERNAL_ERROR = 500;
    private static final Integer STATUS_CODE_REQUEST_ENTITY_TOO_LARGE = 413;

    private static final String CONTENT_TYPE_JSON = 'application/JSON';
    private static final String STATE_CODE = 'CA';
    private static final String BUSINESS_NAME = 'XYZ';
    private static final String REFERENCE = 'Ams';
    private static final String PASS_THROUGH = 'ACK';
    private static final String TRANSACTION_ID = '1801675';
    private static final String ENTITY_ID = 'C1905389';

    private static final String SHOULD_GET_RESPONSE_ASSERT_MESSAGE = 'Should get Response.';
    private static final String SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE = 'Should get Header Response.';
    private static final String VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE = 'Value can not be null';
    private static final String VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE = 'Value must be available';
    private static final String SIZE_MUST_BE_ONE_ASSERT_MESSAGE = 'Size must be 1';

    private static final String EMPTY_STATE_CODE_ASSERT_MESSAGE = 'Must Throw Custom Exception when state code is not provided.';
    private static final String EMPTY_BUSINESS_NAME_ASSERT_MESSAGE = 'Must Throw Custom Exception when Business Name is not provided.';
    private static final String THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE = 'Exception should be return here.';
    private static final String FAILURE_REASON_FOR_ERROR_RESPONSE = 'Failure Reason.';
    private static final String MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE = 'Exception must throw when error response occur.';
    private static final String RECORD_NOT_FOUND = 'Record not found for selected request : Business Name = ' + BUSINESS_NAME + ' and State Code = ' + STATE_CODE;
    private static final String NO_RESULTS_FOUND = 'No result found!!';
    private static final String VERIFICATION_DETAILS_CAN_NOT_BE_EMPTY = 'Verification details can not be empty.';

    private static final String BODY_ERROR_RESPONSE = '{"Message":"Failure Reason."}';
    
    private static final String BUSINESS_INFO_BODY = '{"Header":{"TransactionID":1801675,"OrderDate":"2020-08-10T12:44:32","StateCode":"CA","SearchString":"McDonald","IndexDate":"2020-08-10T12:44:00","NumberOfHits":2,"PassThrough":"CrestMark FCS API Request"},' + 
        '"Results":[{"CompanyName":"MCDONALD, MADSEN & ANDRUS DENTAL CORPORATION","EntityID":"C1905389","EntityStanding":"Active","EntityType":"Corporation - Articles of Incorporation","NameType":"","State":"CA","AdditionalSearchParameters":""},{"CompanyName":"MCDONALD\'S BEEFHEART COMPANY, INC.","EntityID":"C1059381","EntityStanding":"Active",' + 
        '"EntityType":"Corporation - Articles of Incorporation","NameType":"","State":"CA","AdditionalSearchParameters":""}]}';

    private static final String BUSINESS_INFO_BODY_EMPTY_RESULTS = '{"Header":{"TransactionID":1801675,"OrderDate":"2020-08-10T12:44:32","StateCode":"CA","SearchString":"McDonald","IndexDate":"2020-08-10T12:44:00","NumberOfHits":2,"PassThrough":"CrestMark FCS API Request"},' + 
        '"Results":[]}';
    
    private static final String BUSINESS_INFO_BODY_EMPTY_HEADER = '{"Header":{},' + 
        '"Results":[{"CompanyName":"MCDONALD, MADSEN & ANDRUS DENTAL CORPORATION","EntityID":"C1905389","EntityStanding":"Active","EntityType":"Corporation - Articles of Incorporation","NameType":"","State":"CA","AdditionalSearchParameters":""},{"CompanyName":"MCDONALD\'S BEEFHEART COMPANY, INC.","EntityID":"C1059381","EntityStanding":"Active",' + 
        '"EntityType":"Corporation - Articles of Incorporation","NameType":"","State":"CA","AdditionalSearchParameters":""}]}';

    private static final String BUSINESS_DETAILS_BODY = '{"Header":{"TransactionID":1802358,"OrderDate":"2020-08-27T02:30:37","StateCode":"CA","SearchString":"201832410141","IndexDate":"2020-08-27T02:30:00","NumberOfHits":1,"PassThrough":"CrestMark FCS API Request"},"Results":[{"Results":"<CorporateResults><XmlData><ProcessInfoRequestResult><XMLVersion /><Header ><PacketNum>201832410141</PacketNum><Test  /></Header><Record ><SeqNumber>1</SeqNumber><EntityInfo><Dates ><Value /></Dates><Dates ><Value>Nov 19, 2018</Value></Dates><EntityType><Domicile><State>CA</State><Country>United States</Country></Domicile><Names><EntityName>WAMTO, LLC</EntityName><Address><AddressType/><Name>WAMTO, LLC</Name><Address1>716 CORPORATE CENTER DR</Address1><Address2 /><City>POMONA</City><StateOrProvince>CA</StateOrProvince><PostalCode>91768</PostalCode><CountryOrParish /><Country>United States</Country></Address></Names><EntityID>201832410141</EntityID><EntityStanding /></EntityType><Parties><PartiesType /><Title/><Names><EntityName>AMIR SIDDIQI</EntityName><Address><AddressType /><Address1>716 CORPORATE CENTER DR</Address1><Address2/><City>POMONA</City><StateOrProvince>CA</StateOrProvince><PostalCode>91768</PostalCode><CountryOrParish /><Country /></Address></Names></Parties><Agent><Names><EntityName>DAVID N BUFFINGTON</EntityName><Address><Address1>716 CORPORATE CENTER DR</Address1><City>POMONA</City><StateOrProvince>CA</StateOrProvince><PostalCode>91768</PostalCode><CountryOrParish /><Country /></Address></Names></Agent></EntityInfo></Record></ProcessInfoRequestResult></XmlData></CorporateResults>"}]}';

    private static final String STATE_INFO_BODY = '[{"StateName":"California","StateCode":"CA","IndexDate":"2005-01-01T00:00:00","IndexOrigin":"Official index data obtained directly from the state","AllowEntityIDSearch":true}]';
    private static final String STATE_INFO_EMPTY_BODY = '[]';

    @TestSetup
    static void testSetup(){
        TestUtil.createAccountData(1, true);
        TestUtil.createLeadData();
    }

    private static FCS_Entity_Information__c getEntityInformation() {
        FCS_Entity_Information__c fcsEntityInfo = [SELECT Id, Entity_Id__c, Entity_Name__c,
            Domicile_Country__c, Domicile_State__c, Company__c, Lead__c FROM FCS_Entity_Information__c LIMIT 1];
        return fcsEntityInfo;
    }

    private static Account getCompany() {
        return [SELECT Id, Name FROM Account LIMIT 1];
    }

    private static List<Lead> getLeads() {
        return [SELECT Id, Status, LastName, Company, State, Source__c, Location__c, Street, PostalCode, City, Country, IsConverted FROM Lead LIMIT 1];
    }

    @isTest
    private static void saveEntityVerificationDetailsTest() {
        FCSBusinessDetails fcsBusinessDetailsInstance = (FCSBusinessDetails)JSON.deserialize(BUSINESS_DETAILS_BODY, FCSBusinessDetails.class);
        fcsBusinessDetailsInstance.processXmlResponseToWrapper();
        String businessDetailsBodyParam = JSON.serialize(fcsBusinessDetailsInstance);

        Test.startTest();
        	FCSEntityVerificationController.saveEntityVerificationDetails(getCompany().Id, businessDetailsBodyParam);
        Test.stopTest();

        FCS_Entity_Information__c fcsEntityInfo1 = getEntityInformation();

        System.assertEquals('201832410141', fcsEntityInfo1.Entity_Id__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals('WAMTO, LLC', fcsEntityInfo1.Entity_Name__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals('United States', fcsEntityInfo1.Domicile_Country__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals('CA', fcsEntityInfo1.Domicile_State__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityInfo1, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void saveEntityVerificationDetailsEmptyCompanyIdTest() {        
        try {

            Test.startTest();
                FCSEntityVerificationController.saveEntityVerificationDetails('', BUSINESS_DETAILS_BODY);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void saveEntityVerificationDetailsEmptyWrapperStringTest() {        
        try {

            Test.startTest();
                FCSEntityVerificationController.saveEntityVerificationDetails(getCompany().Id, '');
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(VERIFICATION_DETAILS_CAN_NOT_BE_EMPTY, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void isNeedToFCSAPICall() {
        Account company = getCompany();
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        fcsEntityInfo.Company__c = company.Id;
        insert fcsEntityInfo;

        Test.startTest();
            Boolean isNeedToCall = FCSEntityVerificationController.isNeedToFCSAPICall(company.Id);
        Test.stopTest();

        System.assertEquals(false, isNeedToCall, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(true, isNeedToCall, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void isNeedToFCSAPICallForEmptyRecordId() {

        Test.startTest();
            Boolean isNeedToCall = FCSEntityVerificationController.isNeedToFCSAPICall(null);
        Test.stopTest();

        System.assertEquals(true, isNeedToCall, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(false, isNeedToCall, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    private static FCS_Entity_Information__c insertEntityVerificationObjectWithItsRelatedRecords(String recordId) {
        List<FCS_Entity_Information__c> listOfFcsEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        FCS_Entity_Information__c fcsEntityInfo = listOfFcsEntityInfo.get(0);
        if (String.isNotBlank(recordId)) {

			if (Id.valueOf(recordId).getSobjectType() == Account.getSObjectType()) {
                fcsEntityInfo.Company__c = recordId;
                insert fcsEntityInfo;
			}

			if (Id.valueOf(recordId).getSobjectType() == Lead.getSObjectType()) {
                fcsEntityInfo.Lead__c = recordId;
                insert fcsEntityInfo;
            }
            fcsEntityInfo = getEntityInformation();
            List<Communication_Address__c> listOfAddressForfcsEntityInfo = FCSTestDataFactory.generateCommunicationAddress(1, false);
            Communication_Address__c addr = listOfAddressForfcsEntityInfo.get(0);
            addr.Entity_Information__c = fcsEntityInfo.Id;
            insert addr;
            FCS_Entity_Party_Agent__c fcsEntityParty = FCSTestDataFactory.generateFCSPartyInfo();
            fcsEntityParty.Entity_Information__c = fcsEntityInfo.Id;
            insert fcsEntityParty;

            FCS_Entity_Party_Agent__c fcsEntityAgent = FCSTestDataFactory.generateFCSAgentInfo();
            fcsEntityAgent.Entity_Information__c = fcsEntityInfo.Id;
            insert fcsEntityAgent;
        }
        return fcsEntityInfo;
    }

    @isTest
    private static void getEntityVerificationDetailByCompanyIdSF() {
        Account company = getCompany();
        FCS_Entity_Information__c fcsEntityInfo = insertEntityVerificationObjectWithItsRelatedRecords(company.Id);

        Test.startTest();
            FCSEntityVerificationController.FCSEntityVerificationSF fcsEntityVerificationSF= FCSEntityVerificationController.getEntityVerificationDetailByCompanyIdSF(company.Id);
        Test.stopTest();

        System.assertEquals(1, fcsEntityVerificationSF.listOfAgentSF.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(1, fcsEntityVerificationSF.listOfPartySF.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(1, fcsEntityVerificationSF.listOfEntityCommunicationAddress.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Company__c, fcsEntityVerificationSF.entityInformation.Company__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityVerificationSF.entityInformation.Entity_Name__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityVerificationSF.entityInformation.Domicile_Country__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityVerificationSF.entityInformation.Domicile_State__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationSF.entityInformation, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void getEntityVerificationDetailByLeadIdSF() {
        Lead lead = getLeads().get(0);
        FCS_Entity_Information__c fcsEntityInfo = insertEntityVerificationObjectWithItsRelatedRecords(lead.Id);

        Test.startTest();
            FCSEntityVerificationController.FCSEntityVerificationSF fcsEntityVerificationSF= FCSEntityVerificationController.getEntityVerificationDetailByCompanyIdSF(lead.Id);
        Test.stopTest();

        System.assertEquals(1, fcsEntityVerificationSF.listOfAgentSF.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(1, fcsEntityVerificationSF.listOfPartySF.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(1, fcsEntityVerificationSF.listOfEntityCommunicationAddress.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Company__c, fcsEntityVerificationSF.entityInformation.Company__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Entity_Name__c, fcsEntityVerificationSF.entityInformation.Entity_Name__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_Country__c, fcsEntityVerificationSF.entityInformation.Domicile_Country__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(fcsEntityInfo.Domicile_State__c, fcsEntityVerificationSF.entityInformation.Domicile_State__c, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsEntityVerificationSF.entityInformation, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void validateEntityByBusinessNameAndStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));
        
        Test.startTest();
        FCSBusinessDetails fcsBusinessDetails = FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
        Test.stopTest();

        System.assertEquals(1802358, fcsBusinessDetails.header.transactionID, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals('CA', fcsBusinessDetails.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals('', fcsBusinessDetails.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetails.header, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void validateEntityByBusinessNameAndStateCodeEmptyHeaderInBusinessInfoTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY_EMPTY_HEADER, STATUS_CODE_BAD_REQUEST, BODY_ERROR_RESPONSE, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(RECORD_NOT_FOUND, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void validateEntityByBusinessNameAndStateCodeEmptyResultsInBusinessInfoTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY_EMPTY_RESULTS, STATUS_CODE_BAD_REQUEST, BODY_ERROR_RESPONSE, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(NO_RESULTS_FOUND, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }
    
    @isTest
    private static void validateEntityByBusinessNameAndStateCodeErrorResponseInBusinessDetailsTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_BAD_REQUEST, BODY_ERROR_RESPONSE, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(FAILURE_REASON_FOR_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void validateEntityByBusinessNameAndStateCodeErrorResponseInBusinessInfoTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_INTERNAL_ERROR, BODY_ERROR_RESPONSE, 0, '', CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
			System.assertEquals(BODY_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void validateEntityByBusinessNameAndStateCodeEmptyStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode(BUSINESS_NAME, '');
            Test.stopTest();

            system.assert(false, EMPTY_STATE_CODE_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void validateEntityByBusinessNameAndStateCodeEmptyBusinessNameTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.validateEntityByBusinessNameAndStateCode('', STATE_CODE);
            Test.stopTest();

            System.assert(false, EMPTY_BUSINESS_NAME_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void searchStateInfoForEntityValidationTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(STATUS_CODE_SUCCESS, CONTENT_TYPE_JSON, STATE_INFO_BODY ));        
        
        Test.startTest();
        List<FCSStateInformation> fcsStateInformation = FCSEntityVerificationController.searchStateInfoForEntityValidation(STATE_CODE);
        Test.stopTest();

        System.assertEquals(1, fcsStateInformation.size(), SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals('California', fcsStateInformation.get(0).stateName, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals('CA', fcsStateInformation.get(0).stateCode, SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(0, fcsStateInformation.size(), SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void searchStateInfoForEntityValidationEmptyResultTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(STATUS_CODE_SUCCESS, CONTENT_TYPE_JSON, STATE_INFO_EMPTY_BODY ));        
        try {

            Test.startTest();
                FCSEntityVerificationController.searchStateInfoForEntityValidation(STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(NO_RESULTS_FOUND, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void searchStateInfoForEntityValidationErrorResponseTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(STATUS_CODE_INTERNAL_ERROR, CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE ));        
        try {

            Test.startTest();
                FCSEntityVerificationController.searchStateInfoForEntityValidation(STATE_CODE);
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
			System.assertEquals(BODY_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeEmptyStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode(BUSINESS_NAME, '');
            Test.stopTest();

            system.assert(false, EMPTY_STATE_CODE_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeEmptyBusinessNameTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode('', STATE_CODE);
            Test.stopTest();

            System.assert(false, EMPTY_BUSINESS_NAME_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException){
            System.assertEquals(System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));
        
        Test.startTest();
        FCSBusinessInformation fcsBusinessInfo = FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
        Test.stopTest();

        System.assertEquals('1801675', fcsBusinessInfo.header.transactionID, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals('CA', fcsBusinessInfo.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals('', fcsBusinessInfo.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInfo.header, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeErrorResponseTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(STATUS_CODE_INTERNAL_ERROR, CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE ));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
			System.assertEquals(BODY_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeEmptyHeaderInBusinessInfoTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY_EMPTY_HEADER, STATUS_CODE_BAD_REQUEST, BODY_ERROR_RESPONSE, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(RECORD_NOT_FOUND, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }

    @isTest
    private static void getBusinessInfoByBusinessNameAndStateCodeEmptyResultsInBusinessInfoTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY_EMPTY_RESULTS, STATUS_CODE_BAD_REQUEST, BODY_ERROR_RESPONSE, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode(BUSINESS_NAME, STATE_CODE);
            Test.stopTest();

            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(NO_RESULTS_FOUND, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);            
        }
    }
    
    @isTest
    private static void getBusinessDetailsByTransactionAndEntityIdTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));        

        Test.startTest();
        FCSBusinessDetails fcsBusinessDetails = FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId(TRANSACTION_ID, ENTITY_ID, null, null);
        Test.stopTest();

        System.assertEquals(1802358, fcsBusinessDetails.header.transactionID, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals('CA', fcsBusinessDetails.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals('', fcsBusinessDetails.header.StateCode, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetails.header, SHOULD_GET_HEADER_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void getBusinessDetailsByTransactionAndEntityIdEmptyEntityIdTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId(TRANSACTION_ID, '', null, null);
            Test.stopTest();

            system.assert(false, EMPTY_STATE_CODE_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }
    
    @isTest
    private static void getBusinessDetailsByTransactionAndEntityIdEmptyTransactionIdTest() {
        Test.setMock(HttpCalloutMock.class, new FCSMultipleApiCalloutsMock(STATUS_CODE_SUCCESS, BUSINESS_INFO_BODY, STATUS_CODE_SUCCESS, BUSINESS_DETAILS_BODY, CONTENT_TYPE_JSON));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId('', ENTITY_ID, null, null);
            Test.stopTest();

            System.assert(false, EMPTY_BUSINESS_NAME_ASSERT_MESSAGE);
        } catch(AuraHandledException actualException) {
            System.assertEquals(System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void getBusinessDetailsByTransactionAndEntityIdErrorResponseTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(STATUS_CODE_INTERNAL_ERROR, CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE ));        
        try {

            Test.startTest();
                FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId(TRANSACTION_ID, ENTITY_ID, null, null);
            Test.stopTest();
            
            System.assert(false, MUST_SHOW_ASSERT_MESSAGE_WHEN_NOT_HANDLE_ERROR_RESPONSE);
        } catch(AuraHandledException actualException) {
			System.assertEquals(BODY_ERROR_RESPONSE, actualException.getMessage(), THERE_MUST_BE_EXCEPTION_ASSERT_MESSAGE);
        }
    }

    @isTest
    private static void fcsStateInformationTest() {
        
        Test.startTest();
        	List<FCSStateInformation> listOfWrapperObj = (List<FCSStateInformation>) JSON.deserialize(STATE_INFO_BODY, List<FCSStateInformation>.class);
        Test.stopTest();
        
        System.assertEquals(1, listOfWrapperObj.size(), SIZE_MUST_BE_ONE_ASSERT_MESSAGE);
        System.assertNotEquals(null, listOfWrapperObj.get(0).stateName, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
        System.assertNotEquals(null, listOfWrapperObj.get(0).stateCode, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
        System.assertNotEquals(null, listOfWrapperObj.get(0).indexDate, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
        System.assertNotEquals(null, listOfWrapperObj.get(0).indexOrigin, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
        System.assertNotEquals(null, listOfWrapperObj.get(0).allowEntityIDSearch, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
    }
    
    @isTest
    private static void fcsBusinessInformationTest() {
        
        Test.startTest();
        	FCSBusinessInformation fcsBusinessInformationObj = (FCSBusinessInformation) JSON.deserialize(BUSINESS_INFO_BODY, FCSBusinessInformation.class);
        Test.stopTest();
        
        System.assertNotEquals(null, fcsBusinessInformationObj.header, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.transactionID, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);	
    	System.assertNotEquals(null, fcsBusinessInformationObj.header.orderDate, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.stateCode, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.searchString, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.indexDate, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.numberOfHits, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.header.passThrough, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).companyName, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).entityID, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).entityType, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).entityStanding, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).nameType, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).state, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).additionalSearchParameters, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessInformationObj.results.get(0).isSelected, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(false, fcsBusinessInformationObj.results.get(0).isSelected, VALUE_MUST_BE_AVAILABLE_ASSERT_MESSAGE);
    }
    
    @isTest
    private static void fcsBusinessDetailsTest() {
        
        Test.startTest();
        	FCSBusinessDetails fcsBusinessDetailObj = (FCSBusinessDetails) JSON.deserialize(BUSINESS_DETAILS_BODY, FCSBusinessDetails.class);
        Test.stopTest();
        
        System.assertNotEquals(null, fcsBusinessDetailObj.header, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.results, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.transactionID, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);	
    	System.assertNotEquals(null, fcsBusinessDetailObj.header.orderDate, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.stateCode, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.searchString, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.indexDate, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.numberOfHits, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.header.passThrough, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, fcsBusinessDetailObj.results.get(0).results, VALUE_CAN_NOT_BE_NULL_ASSERT_MESSAGE); 
    }
}