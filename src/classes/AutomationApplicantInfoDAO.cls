public without sharing class AutomationApplicantInfoDAO implements IAutomationApplicantInfoDAO {
    
    public  interface IAutomationApplicantInfoDAO
    {
        List<Automation_Applicant_Info__c> getRecordRelatedToDeal(String dealId, String lastStage);
        List<Opportunity> getOpportunityByAccountId(Set<String> setOfIds);
    }
    
    public static List<Automation_Applicant_Info__c> getRecordRelatedToDeal (String dealId , String lastStage){
        String status;
        List<Automation_Applicant_Info__c> listOfAutomationApplicant = [SELECT Name , Id FROM Automation_Applicant_Info__c WHERE
                                                                        Credit_App__c =:dealId AND Last_Stage__c =:lastStage];
        return listOfAutomationApplicant;
    }
    
     public static List<Opportunity> getOpportunityByAccountId(Set<String> setOfIds)
    {
        return [SELECT Id ,Name,(Select Id From Automation_Applicant_Infos__r Where Last_Stage__c = 'Complete') FROM Opportunity 
                WHERE AccountId IN :setOfIds  AND RecordType.DeveloperName = 'CVF' Order By CreatedDate desc Limit 1 ];
    }//AND createdBy.Profile.Name = 'Crestmark Broker Profile'

}