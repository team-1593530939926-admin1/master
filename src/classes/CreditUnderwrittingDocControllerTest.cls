@isTest
private class CreditUnderwrittingDocControllerTest {
     
    @TestSetup
    static void testSetup(){
        TestUtil.opportunityRelatedAccountData();
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
    }

    private static List<ContentVersion> getContentVersion() {
        List<ContentVersion> listOfContentVersion = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Title = 'Test' LIMIT 1];  
        return listOfContentVersion;
    }
    
    private static List<ContentDocument> getContentDocuments() {
        List<ContentDocument> listOfContentDocuments = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        return listOfContentDocuments;
    }

    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Name, Account.Name, LeaseInfo_EquipmentDescription__c, LeaseInfo_SoftwareAmount__c, Amount, Application_Id__c, StageName, Score__c, Tier__c, Broker_Comment__c, Credit_Condition__c FROM Opportunity];
    }

    @isTest
    private static void returnCreditConditionForOpportunity() {
        List<Opportunity> listOfOpportunity = getOpportunity();

        Test.startTest();
            List<CreditUnderwrittingDocumentController.CreditDocumentList> listOfCreditDocumentList = CreditUnderwrittingDocumentController.returnCreditConditionForOpportunity(listOfOpportunity.get(0).Id);
        Test.stopTest();

        System.assertEquals(2, listOfCreditDocumentList.size(), 'List should not be null.');
    }

    @isTest
    private static void handleSubmitOfDocuments() {
        List<ContentDocument> listOfContentDocuments = getContentDocuments();
        List<Opportunity> listOfOpportunity = getOpportunity();

        Test.startTest();
            String status = CreditUnderwrittingDocumentController.handleSubmitOfDocuments(new List<String>{listOfContentDocuments.get(0).Id}, listOfOpportunity.get(0).Id);
        Test.stopTest();

        System.assertEquals('success', status, 'Should not get error.');
    }

}