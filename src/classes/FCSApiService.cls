public with sharing class FCSApiService implements FCSApiService.IFCSApiService {

    public interface IFCSApiService {
        HttpResponse getStateInfo(String stateCode);
        HttpResponse searchBusinessByStateAndBusinessName(String stateCode, String businessName, String reference, String passThrough);
        HttpResponse getBusinessDetailByEntityAndTransactionId(String transactionId, String entityId, String additionalSearchParameters, String passThrough);
        HttpResponse searchEntityIdWithStateCode(String stateCode, String entityId, String reference, String passThrough);
        void handleFCSErrorResponse(HttpResponse res);
        void handleFCSErrorResponseAura(HttpResponse res);
    }
    
    public static HttpResponse getStateInfo(String stateCode) {
        String endPointUrl = FCSAPIConstant.GET_STATE_INFO;

        if (!String.isBlank(stateCode)) {
            endPointUrl = endPointUrl + '/' + stateCode;
        }
        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_GET, FCSAPIConstant.CONTENT_TYPE_JSON, null);
        return FCSApiCallout.callout(requestWrapper);
        
    }

    public static HttpResponse searchBusinessByStateAndBusinessName(String stateCode, String businessName, String reference, String passThrough) {
        String endPointUrl = FCSAPIConstant.SEARCH_BUSINESS_NAME;

        if (String.isBlank(stateCode)) {
            throw new FCSAPIException(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null);
        }

        if (String.isBlank(businessName)) {
            throw new FCSAPIException(System.Label.Error_Business_Name_Can_Not_Be_Empty_Or_Null);
        }

        Map<String, String> params = new Map<String, String>();
        params.put(FCSAPIConstant.STATE_CODE_REQUEST_PARAM_KEY, stateCode);
        params.put(FCSAPIConstant.SEARCH_NAME_REQUEST_PARAM_KEY, businessName);

        if (String.isBlank(reference)) {
            //reference = FCSUtil.generateRandomKey(FCSAPIConstant.REFERENCE_DEFAULT_VALUE + String.valueof(DateTime.now().getTime()));
            reference = FCSAPIConstant.REFERENCE_DEFAULT_VALUE;
        }

        if (String.isBlank(passThrough)) {
            passThrough = FCSUtil.generateRandomKey(FCSAPIConstant.PASS_THROUGH_DEFAULT_VALUE + String.valueof(DateTime.now().getTime()));
        }
        params.put(FCSAPIConstant.REFERENCE_REQUEST_PARAM_KEY, reference);
        params.put(FCSAPIConstant.PASS_THROUGH_REQUEST_PARAM_KEY, passThrough);

        String requestBody = JSON.serialize(params);

        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_POST, FCSAPIConstant.CONTENT_TYPE_JSON, requestBody);

        return FCSApiCallout.callout(requestWrapper);
    }

    public static HttpResponse getBusinessDetailByEntityAndTransactionId(String transactionId, String entityId, String additionalSearchParameters, String passThrough) {
        String endPointUrl = FCSAPIConstant.GET_BUSINESS_DETAILS;
        if (String.isBlank(transactionId)) {
            throw new FCSAPIException(System.Label.Error_Transaction_Id_Can_Not_Be_Empty_Or_Null);
        }

        if (String.isBlank(entityId)) {
             throw new FCSAPIException(System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null);
        }

        Map<String, String> params = new Map<String, String>();
        params.put(FCSAPIConstant.TRANSACTION_ID_REQUEST_PARAM_KEY, transactionId);
        params.put(FCSAPIConstant.ENTITY_ID_REQUEST_PARAM_KEY, entityId);

        if (String.isBlank(additionalSearchParameters)) {
            additionalSearchParameters = FCSAPIConstant.ADDITIONAL_SEARCH_PARAMETERS_DEFAULT_VALUE;
        }

        if (String.isBlank(passThrough)) {
            passThrough = FCSUtil.generateRandomKey(FCSAPIConstant.PASS_THROUGH_DEFAULT_VALUE + String.valueof(DateTime.now().getTime()));
        }
        params.put(FCSAPIConstant.ADDITIONAL_SEARCH_PARAMETERS_REQUEST_PARAM_KEY, additionalSearchParameters);
        params.put(FCSAPIConstant.PASS_THROUGH_REQUEST_PARAM_KEY, passThrough);

       String requestBody = JSON.serialize(params);

       FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_POST, FCSAPIConstant.CONTENT_TYPE_JSON, requestBody);

        return FCSApiCallout.callout(requestWrapper);
    }

    public static HttpResponse searchEntityIdWithStateCode(String stateCode, String entityId, String reference, String passThrough) {
        String endPointUrl = FCSAPIConstant.SEARCH_ENTITY_ID;

         if (String.isBlank(stateCode)) {
             throw new FCSAPIException(System.Label.Error_State_Code_Can_Not_Be_Empty_Or_Null);
         }

         if (String.isBlank(entityId)) {
            throw new FCSAPIException(System.Label.Error_Entity_Id_Can_Not_Be_Empty_Or_Null);
       }

         Map<String, String> params = new Map<String, String>();
         params.put(FCSAPIConstant.STATE_CODE_REQUEST_PARAM_KEY, stateCode);
         params.put(FCSAPIConstant.ENTITY_ID_REQUEST_PARAM_KEY, entityId);

         if (String.isBlank(reference)) {
             //reference = FCSUtil.generateRandomKey(FCSAPIConstant.REFERENCE_DEFAULT_VALUE + String.valueof(DateTime.now().getTime()));
             reference = FCSAPIConstant.REFERENCE_DEFAULT_VALUE;
         }

         if (String.isBlank(passThrough)) {
             passThrough = FCSUtil.generateRandomKey(FCSAPIConstant.PASS_THROUGH_DEFAULT_VALUE + String.valueof(DateTime.now().getTime()));
         }
         params.put(FCSAPIConstant.REFERENCE_REQUEST_PARAM_KEY, reference);
         params.put(FCSAPIConstant.PASS_THROUGH_REQUEST_PARAM_KEY, passThrough);

        String requestBody = JSON.serialize(params);

        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_POST, FCSAPIConstant.CONTENT_TYPE_JSON, requestBody);

        return FCSApiCallout.callout(requestWrapper);
    }

    public static void handleFCSErrorResponse(HttpResponse res) {
        if (res.getStatusCode() == FCSAPIConstant.STATUS_CODE_BAD_REQUEST || res.getStatusCode() == FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE) {
            ErrorResponse errorResponse = (ErrorResponse)JSON.deserialize(res.getBody(), ErrorResponse.class);
            throw new FCSAPIException(errorResponse.Message);
        }
        throw new FCSAPIException(res.getBody());
    }
    
    public static void handleFCSErrorResponseAura(HttpResponse res) {
        if (res.getStatusCode() == FCSAPIConstant.STATUS_CODE_BAD_REQUEST || res.getStatusCode() == FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE) {
            ErrorResponse errorResponse = (ErrorResponse)JSON.deserialize(res.getBody(), ErrorResponse.class);
            AuraHandledException auraExc = new AuraHandledException(errorResponse.Message);
            auraExc.setMessage(errorResponse.Message);
            throw auraExc;
        }
        AuraHandledException auraExc = new AuraHandledException(res.getBody());
        auraExc.setMessage(res.getBody());
        throw auraExc;
    }

    public class ErrorResponse {
        private String Message {get; set;}
    }

    public class FCSAPIException extends Exception {}
}