public with sharing class AdditionalAddressDAO implements IAdditionalAddressDAO 
{
    public Interface IAdditionalAddressDAO
    {
        List<Additional_Address__c> insertAdditionalAddress( List<Additional_Address__c> listOfAdditionalAddress);
    }
    public static List<Additional_Address__c> insertAdditionalAddress( List<Additional_Address__c> listOfAdditionalAddress)
    {
        insert listOfAdditionalAddress; 
        System.debug('from AdditionalDAO  '+ listOfAdditionalAddress);
        return listOfAdditionalAddress;
    }
}