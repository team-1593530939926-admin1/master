public with sharing class CustomLookupController {
    
    private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    private static final ContactDAO.IContactDAO contactDAOInstance = new ContactDAO();
    
    @AuraEnabled(cacheable=true)
    public static List<sObject> search(String searchTerm, string myObject, String filter) {
        if(myObject == 'Account'){
             return accountDAOInstance.search(searchTerm, myObject, filter, 'Business');
        }
       if(myObject == 'contact'){
             return contactDAO.search(searchTerm, myObject, filter, 'Crestmark Contact');//class is withSharing 
        }
       return null;
    }
}