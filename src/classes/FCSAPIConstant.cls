public class FCSAPIConstant {
    
    private static final String VERSION_NUMBER = 'V1';
    private static final String END_POINT_CALLOUT = 'callout:FCS_Auth/Search/';
    public static final String REFERENCE_DEFAULT_VALUE = 'CrestMark';
    public static final String PASS_THROUGH_DEFAULT_VALUE = 'CrestMark FCS API Request';
    public static final String ADDITIONAL_SEARCH_PARAMETERS_DEFAULT_VALUE = '';

    public static final String GET_STATE_INFO = END_POINT_CALLOUT + VERSION_NUMBER + '/BESearch/GetStateInfo';
    public static final String SEARCH_BUSINESS_NAME = END_POINT_CALLOUT + VERSION_NUMBER + '/BESearch/SearchBusinessName';
    public static final String GET_BUSINESS_DETAILS = END_POINT_CALLOUT + VERSION_NUMBER + '/BESearch/GetBusinessDetails';
    public static final String SEARCH_ENTITY_ID = END_POINT_CALLOUT + VERSION_NUMBER + '/BESearch/SearchEntityID';

    public static final String METHOD_TYPE_POST = 'POST';
    public static final String METHOD_TYPE_GET = 'GET';
    public static final String CONTENT_TYPE_HEADER = 'Content-Type';
    public static final String CONTENT_TYPE_JSON = 'application/JSON';

    public static final Integer STATUS_CODE_SUCCESS = 200;
    public static final Integer STATUS_CODE_BAD_REQUEST = 400;
    public static final Integer STATUS_CODE_INTERNAL_ERROR = 500;
    public static final Integer STATUS_CODE_REQUEST_ENTITY_TOO_LARGE = 413;

    public static final String STATE_CODE_REQUEST_PARAM_KEY = 'StateCode';
    public static final String SEARCH_NAME_REQUEST_PARAM_KEY = 'SearchName';
    public static final String REFERENCE_REQUEST_PARAM_KEY = 'Reference';
    public static final String PASS_THROUGH_REQUEST_PARAM_KEY = 'PassThrough';
    public static final String TRANSACTION_ID_REQUEST_PARAM_KEY = 'TransactionID';
    public static final String ENTITY_ID_REQUEST_PARAM_KEY = 'EntityID';
    public static final String ADDITIONAL_SEARCH_PARAMETERS_REQUEST_PARAM_KEY = 'AdditionalSearchParameters';
}