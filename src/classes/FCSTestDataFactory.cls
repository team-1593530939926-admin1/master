@isTest
public class FCSTestDataFactory {

    private static final String agentXml = '<Agent><Names><EntityName>JOHN FREDDY CHAVERRA</EntityName><Address><Address1>1710 W HILLCREST DR #206</Address1><City>NEWBURY PARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>91320</PostalCode><CountryOrParish /><Country /></Address></Names><Names><EntityName>Rakesh</EntityName><Address><AddressType Type="Officer_Address" /><Address1>RABDAI </Address1><Address2 /><City>DHANI</City><StateOrProvince>MA</StateOrProvince><PostalCode>00110</PostalCode><CountryOrParish /><Country /></Address></Names></Agent>';
    private static final String partiesXml = '<Parties><PartiesType Type="Officers" /><Title Type="CEO" /><Names><EntityName>CHAVERRA, CLAUDIA ALEJANDRA</EntityName><Address><AddressType Type="Officer_Address" /><Address1>1710 W HILLCREST DR #206 </Address1><Address2 /><City>NEWBURY PARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>91320</PostalCode><CountryOrParish /><Country /></Address></Names><Names><EntityName>Rakesh</EntityName><Address><AddressType Type="Officer_Address" /><Address1>RABDAI </Address1><Address2 /><City>DHANI</City><StateOrProvince>MA</StateOrProvince><PostalCode>00110</PostalCode><CountryOrParish /><Country /></Address></Names></Parties>';
    private static final String entityTypeXml = '<EntityType Type="Corporation - Articles of Incorporation"><Domicile><State>CA</State><Country>United States</Country></Domicile><Names><EntityName>PURLUX SKIN CARE, INC.</EntityName><Address><AddressType Type="Filing_Address" /><Name>PURLUX SKIN CARE, INC.</Name><Address1>207 W LOS ANGELES AVE #108 </Address1><Address2 /><City>MOORPARK</City><StateOrProvince>CA</StateOrProvince><PostalCode>93021</PostalCode><CountryOrParish /><Country>United States</Country></Address></Names><EntityID>C4212644</EntityID><EntityStanding Standing="Active" /></EntityType>';
    private static final String entityInfoXml = '<EntityInfo><Dates Type="DateOfIncorporation"><Value>Nov 13, 2018</Value></Dates><Dates Type="FilingDate"><Value>Nov 28, 2018</Value></Dates>'+entityTypeXml+partiesXml+agentXml+'</EntityInfo>';
    private static final String recordXml = '<Record><SeqNumber>1</SeqNumber>'+entityInfoXml+'</Record>';
    public static final String xmlString = '<CorporateResults><XmlData><ProcessInfoRequestResult><XMLVersion/><Header><PacketNum>C4212644</PacketNum><Test Value="No" /></Header>'+recordXml+'</ProcessInfoRequestResult></XmlData></CorporateResults>';

    public static final String PARTY_RECORD_TYPE = Schema.SObjectType.FCS_Entity_Party_Agent__c.getRecordTypeInfosByName().get('Party').getRecordTypeId();
    public static final String AGENT_RECORD_TYPE = Schema.SObjectType.FCS_Entity_Party_Agent__c.getRecordTypeInfosByName().get('Agent').getRecordTypeId();

    public static FCSBusinessDetails generateFCSBusinessDetailsWrapper() {
        FCSBusinessDetails fcsBusinessDetails = new FCSBusinessDetails();
        fcsBusinessDetails.header = new FCSBusinessDetails.Header();
        fcsBusinessDetails.header.transactionID = 1801505;
        fcsBusinessDetails.header.stateCode = 'CA';
        fcsBusinessDetails.header.searchString = 'mcdonald';
        fcsBusinessDetails.header.passThrough = 'ACK';
        fcsBusinessDetails.header.orderDate = '2020-08-06T03:38:07';
        fcsBusinessDetails.header.numberOfHits = 2;
        fcsBusinessDetails.header.indexDate = '2020-08-06T03:38:00';
        FCSBusinessDetails.Results results = new FCSBusinessDetails.Results();
        results.results = xmlString;
        fcsBusinessDetails.results = new List<FCSBusinessDetails.Results>();
        fcsBusinessDetails.results.add(results);
        fcsBusinessDetails.processXmlResponseToWrapper();
        return fcsBusinessDetails;
    }

    public static List<FCSXmlDomUtility.Address> generateAddressWrapper() {
        FCSXmlDomUtility.Address address = new FCSXmlDomUtility.Address();
        address.addressType = 'Filing_Address';
        address.name = 'MCDONALD';
        address.address1 = 'MARGARITA';
        address.address2 = '';
        address.addressType = 'Officer_Address';
        address.city = 'ENCINITAS';
        address.country = 'United States';
        address.countryOrParish = '';
        address.postalCode = '92024';
        address.stateOrProvince = 'CA'; 
        return new List<FCSXmlDomUtility.Address>{address};
    }

    public static FCSXmlDomUtility.EntityInfo generateFCSEntityInfoWrapper() {
        FCSXmlDomUtility.EntityInfo entityInfo = new FCSXmlDomUtility.EntityInfo();
        entityInfo.listOfDates = new List<FCSXmlDomUtility.Dates>();
        FCSXmlDomUtility.Dates date1 = new FCSXmlDomUtility.Dates();
        date1.dateType = 'DateOfIncorporation';
        date1.dateValue = 'Dec 26, 1996';
        entityInfo.listOfDates.add(date1);
        FCSXmlDomUtility.Dates date2 = new FCSXmlDomUtility.Dates();
        date2.dateType = 'FilingDate';
        date2.dateValue = 'Nov 27, 2018';
        entityInfo.listOfDates.add(date2);

        FCSXmlDomUtility.EntityType entityType = new FCSXmlDomUtility.EntityType();
        entityType.entityId = 'C1905389';
        entityType.entityStanding = 'Active';
        entityType.entityType = 'Corporation - Articles of Incorporation';
        entityType.listOfEntityName = new List<FCSXmlDomUtility.Names>();
        FCSXmlDomUtility.Names name = new FCSXmlDomUtility.Names();
        name.entityName = 'MCDONALD';
        name.listOfAddress = new List<FCSXmlDomUtility.Address>();

        FCSXmlDomUtility.Address address = new FCSXmlDomUtility.Address();
        address.addressType = 'Filing_Address';
        address.name = 'MCDONALD';
        address.address1 = 'MARGARITA';
        address.address2 = '';
        address.addressType = 'Officer_Address';
        address.city = 'ENCINITAS';
        address.country = 'United States';
        address.countryOrParish = '';
        address.postalCode = '92024';
        address.stateOrProvince = 'CA'; 
        name.listOfAddress.add(address);
        entityType.listOfEntityName.add(name);
        entityType.domicileDetails = new FCSXmlDomUtility.Domicile();
        entityType.domicileDetails.country = 'United States';
        entityType.domicileDetails.state = 'CA';

        entityInfo.entityType = entityType;

        entityInfo.listOfParties = generateFCSPartiesWrapper();
        entityInfo.listOfAgents = generateFCSAgentsWrapper();

        return entityInfo;
    }

    public static List<FCSXmlDomUtility.Parties> generateFCSPartiesWrapper() {
        FCSXmlDomUtility.Parties party = new FCSXmlDomUtility.Parties();
        party.partiesType = 'Officers';
        party.title = 'CEO';
        party.listOfPartiesName = new List<FCSXmlDomUtility.Names>();

        FCSXmlDomUtility.Names name = new FCSXmlDomUtility.Names();
        name.entityName = 'MCDONALD';
        name.listOfAddress = new List<FCSXmlDomUtility.Address>();

        FCSXmlDomUtility.Address address = new FCSXmlDomUtility.Address();
        address.address1 = 'MARGARITA';
        address.address2 = '';
        address.addressType = 'Officer_Address';
        address.city = 'ENCINITAS';
        address.country = '';
        address.countryOrParish = '';
        address.postalCode = '92024';
        address.stateOrProvince = 'CA'; 
        name.listOfAddress.add(address);

        party.listOfPartiesName.add(name);
        return new List<FCSXmlDomUtility.Parties>{party};
    }

    public static List<FCSXmlDomUtility.Agent> generateFCSAgentsWrapper() {
        FCSXmlDomUtility.Agent agent = new FCSXmlDomUtility.Agent();
        agent.listOfAgentsName = new List<FCSXmlDomUtility.Names>();

        FCSXmlDomUtility.Names name = new FCSXmlDomUtility.Names();
        name.entityName = 'MCDONALD';
        name.listOfAddress = new List<FCSXmlDomUtility.Address>();

        FCSXmlDomUtility.Address address = new FCSXmlDomUtility.Address();
        address.address1 = 'MARGARITA';
        address.city = 'ENCINITAS';
        address.country = '';
        address.countryOrParish = '';
        address.postalCode = '92024';
        address.stateOrProvince = 'CA'; 
        name.listOfAddress.add(address);

        agent.listOfAgentsName.add(name);
        return new List<FCSXmlDomUtility.Agent>{agent};
    }
     
    public static List<FCS_Entity_Information__c> generateFcsEntityInfoRecord(Integer count, Boolean isInsert) {
        List<FCS_Entity_Information__c> listOfFCSEntityInfo = new List<FCS_Entity_Information__c>();

        for (integer i = 1; i <= count; i++) {
            FCS_Entity_Information__c entityInformation = new FCS_Entity_Information__c(
                Entity_Id__c = 'TEST000' + i, 
                Entity_Name__c = 'Test Record',
                Domicile_Country__c = 'USA', 
                Domicile_State__c = 'CA'
            );
            listOfFCSEntityInfo.add(entityInformation);
        }

        if (isInsert) {
            insert listOfFCSEntityInfo;
        }

        return listOfFCSEntityInfo;
    }

    public static FCS_Entity_Party_Agent__c generateFCSPartyInfo() {
        FCS_Entity_Party_Agent__c partyAgentInformation1 = new FCS_Entity_Party_Agent__c(
            Name__c = 'TEST1', 
            Title__c = 'CEO1', 
            Type__c = 'type1',
            RecordTypeId = PARTY_RECORD_TYPE
        );
        return partyAgentInformation1;
    }

    public static FCS_Entity_Party_Agent__c generateFCSAgentInfo() {
        FCS_Entity_Party_Agent__c partyAgentInformation2 = new FCS_Entity_Party_Agent__c(
            Name__c = 'TEST2', 
            RecordTypeId = AGENT_RECORD_TYPE
        );
        return partyAgentInformation2;
    }

    public static List<FCS_Entity_Party_Agent__c> generateFcsEntityPartyAgent(Id entityInformationId, Integer count, Boolean isInsert) {
        List<FCS_Entity_Party_Agent__c> listOfPartyAgents = new List<FCS_Entity_Party_Agent__c>();

        for (integer i = 1; i <= count; i++) {
            FCS_Entity_Party_Agent__c partyAgentInformation1 = new FCS_Entity_Party_Agent__c(
               Name__c = 'TEST1', 
               Title__c = 'CEO1', 
               Type__c = 'type1',
               RecordTypeId = PARTY_RECORD_TYPE,
               Entity_Information__c = entityInformationId
            );
            FCS_Entity_Party_Agent__c partyAgentInformation2 = new FCS_Entity_Party_Agent__c(
               Name__c = 'TEST2', 
               RecordTypeId = AGENT_RECORD_TYPE,
               Entity_Information__c = entityInformationId
            );
            listOfPartyAgents.add(partyAgentInformation1);
            listOfPartyAgents.add(partyAgentInformation2);
        }
        if (isInsert) {
            insert listOfPartyAgents;
        }
        return listOfPartyAgents;
    }

    public static List<Communication_Address__c> generateCommunicationAddressForPartyAgent(Id partyAgentId, Integer count, Boolean isInsert) {
        List<Communication_Address__c> listOfCommunicationAddress = new List<Communication_Address__c>();
        for (integer i = 1; i <= count; i++) {
            Communication_Address__c address = new Communication_Address__c(
                City__c = 'Paris', 
                Country_Parish__c = 'CA',
                County__c = 'USA', 
                Postal_Code__c = '544121',
                State__c = 'CA',
                Street_Address_1__c = 'address1',
                Entity_Party_Agent__c = partyAgentId,
                Street_Address_2__c = 'address2'
            );
            listOfCommunicationAddress.add(address);
        }
        if (isInsert) {
            insert listOfCommunicationAddress;
        }
        return listOfCommunicationAddress;
    }

    public static List<Communication_Address__c> generateCommunicationAddressForEntity(Id entityId, Integer count, Boolean isInsert) {
        List<Communication_Address__c> listOfCommunicationAddress = new List<Communication_Address__c>();
        for (integer i = 1; i <= count; i++) {
            Communication_Address__c address = new Communication_Address__c(
                City__c = 'Paris', 
                Country_Parish__c = 'CA',
                County__c = 'USA', 
                Postal_Code__c = '544121',
                State__c = 'CA',
                Street_Address_1__c = 'address1',
                Entity_Information__c = entityId,
                Street_Address_2__c = 'address2'
            );
            listOfCommunicationAddress.add(address);
        }
        if (isInsert) {
            insert listOfCommunicationAddress;
        }
        return listOfCommunicationAddress;
    }
    
    public static List<Communication_Address__c> generateCommunicationAddress(Integer count, Boolean isInsert) {
        List<Communication_Address__c> listOfCommunicationAddress = new List<Communication_Address__c>();

        for (integer i = 1; i <= count; i++) {
            Communication_Address__c address = new Communication_Address__c(
                City__c = 'Paris', 
                Country_Parish__c = 'CA',
                County__c = 'USA', 
                Postal_Code__c = '544121',
                State__c = 'CA',
                Street_Address_1__c = 'address1',
                Street_Address_2__c = 'address2'
            );
            listOfCommunicationAddress.add(address);
        }
        if (isInsert) {
            insert listOfCommunicationAddress;
        }
        return listOfCommunicationAddress;
    }
}