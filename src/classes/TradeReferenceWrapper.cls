public class TradeReferenceWrapper {
    @AuraEnabled
    public Boolean isClearButtonVisible{get;set;}    
    @AuraEnabled
    public Boolean isDeleteRequire{get;set;}     
    @AuraEnabled
    public String existingId{get;set;}    
    @AuraEnabled
    public Boolean isDisable{get;set;}    
    @AuraEnabled
    public Integer rowId{get;set;}
    @AuraEnabled
    public Integer count{get;set;}
    @AuraEnabled
    public string companyName{get;set;}
    @AuraEnabled
    public string addressLine1{get;set;}
    @AuraEnabled
    public string cityTrade {get;set;}
    @AuraEnabled
    public string stateTrade{get;set;}
    @AuraEnabled
    public string countryTrade{get;set;}
    @AuraEnabled
    public string zipTrade{get;set;}
    @AuraEnabled
    public string phoneNumberTrade{get;set;}
    @AuraEnabled
    public string faxTrade{get;set;}
    @AuraEnabled
    public String firstNameTrade{get;set;}
    @AuraEnabled
    public String lastNameTrade{get;set;}
    @AuraEnabled
    public String tradeId{get;set;}
    @AuraEnabled
    public String fullSearchName{get;set;}
    @AuraEnabled
    public ContactInformationWrapper contactInfo{get;set;}
    public Account dealTradeInfo {get;set;}
    public Contact dealTradeContactInfo {get;set;}
    private static final Id BUSINESS_ACCOUNT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Account', 'Business');

    public TradeReferenceWrapper(Integer count){
        this.rowId = count;
        this.count = count + 1;
    }
    
    public TradeReferenceWrapper(TradeReferenceWrapper tradeWrapperInfo, Account accountInfo, Integer count){
        Account tradeInfo = new Account();  
        tradeInfo.Trade_Reference__c = accountInfo.Id;
        tradeInfo.Name = tradeWrapperInfo.companyName;
        tradeInfo.BillingStreet = tradeWrapperInfo.addressLine1;
        tradeInfo.Fax = tradeWrapperInfo.faxTrade;
        tradeInfo.BillingCity = tradeWrapperInfo.cityTrade;
        tradeInfo.BillingState = tradeWrapperInfo.stateTrade;
        tradeInfo.BillingPostalCode = tradeWrapperInfo.zipTrade;
        tradeInfo.BillingCountry = tradeWrapperInfo.countryTrade;
        tradeInfo.RecordTypeId = BUSINESS_ACCOUNT_RECORD_TYPE_ID;
        tradeInfo.Phone = tradeWrapperInfo.phoneNumberTrade;
        if(!String.isEmpty(tradeWrapperInfo.existingId)){
            tradeInfo.Id = tradeWrapperInfo.existingId;
            this.existingId = tradeWrapperInfo.existingId;
        }
        this.dealTradeInfo = tradeInfo;
        if(!String.isEmpty(tradeWrapperInfo.lastNameTrade) && String.isEmpty(tradeWrapperInfo.existingId)){
            tradeInfo.External_System_Id__c = 'Trade-' + String.valueOf(count) + ' : ' + accountInfo.External_System_Id__c;
            Contact contactInstance = new Contact(Account = new Account(External_System_Id__c = tradeInfo.External_System_Id__c));
            contactInstance.FirstName = tradeWrapperInfo.firstNameTrade;
            contactInstance.LastName = tradeWrapperInfo.lastNameTrade;         
            this.dealTradeContactInfo = contactInstance;
        }
    }
    
    
    public TradeReferenceWrapper(){}
    
    public TradeReferenceWrapper(Account accountRecord){
        this.companyName = accountRecord.Name; 
        this.addressLine1 = accountRecord.BillingStreet;   
        this.cityTrade = accountRecord.BillingCity;
        this.stateTrade = accountRecord.BillingState;    
        this.countryTrade = accountRecord.BillingCountry;    
        this.zipTrade = accountRecord.BillingPostalCode;
        this.phoneNumberTrade = accountRecord.Phone;    
        this.faxTrade = accountRecord.Fax;
        this.tradeId = accountRecord.Id;
        this.existingId = accountRecord.Id;
        this.fullSearchName = accountRecord.Name;/////
        if(!accountRecord.Contacts.isEmpty()){
            this.firstNameTrade = accountRecord.Contacts.get(0).FirstName;
            this.lastNameTrade = accountRecord.Contacts.get(0).LastName;
        }else{
            this.firstNameTrade = '';
            this.lastNameTrade = '';
        }       
    } 
    @testVisible
    public TradeReferenceWrapper(TradeReferenceWrapper tradeWrapperInfo, Boolean isVisible ){
        this.companyName = tradeWrapperInfo.companyName; 
        this.addressLine1 = tradeWrapperInfo.addressLine1;   
        this.cityTrade = tradeWrapperInfo.cityTrade;
        this.stateTrade = tradeWrapperInfo.stateTrade;    
        this.countryTrade = tradeWrapperInfo.countryTrade;    
        this.zipTrade = tradeWrapperInfo.zipTrade;
        this.phoneNumberTrade = tradeWrapperInfo.phoneNumberTrade;    
        this.faxTrade = tradeWrapperInfo.faxTrade;
        this.tradeId = tradeWrapperInfo.tradeId;
        this.firstNameTrade = tradeWrapperInfo.firstNameTrade;
        this.lastNameTrade = tradeWrapperInfo.lastNameTrade;
        this.existingId = tradeWrapperInfo.existingId;
    } 
    
    
    public class ContactInformationWrapper{
        @AuraEnabled
        public String firstName{get;set;}
        @AuraEnabled
        public String lastName{get;set;}      
        
        public ContactInformationWrapper(Contact contactRecord){
            this.firstName = contactRecord.FirstName;
            this.lastName = contactRecord.LastName;
        }
        public ContactInformationWrapper(){
            this.firstName = '';
            this.lastName = '';
        }
    }    
}