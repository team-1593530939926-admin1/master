@isTest
private class FCSEntityInformationDAOTest {
    
    private static final String ENTITY_ID = 'E1234';
    private static final String SHOULD_GET_RESULT_ASSERT_MESSAGE = 'Should get result here.';

    @TestSetup
    static void testSetup() {
        TestUtil.createAccountData(1, true);
        Lead leadRecord = TestUtil.createLeadData();
        Account acc = getCompany();
        List<FCS_Entity_Information__c> listOfEntityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        listOfEntityInformation.get(0).Entity_Id__c = ENTITY_ID;
        listOfEntityInformation.get(0).Entity_Name__c = 'mcdonald';
        listOfEntityInformation.get(0).Company__c = acc.Id;
        listOfEntityInformation.get(0).Lead__c = leadRecord.Id;
        insert listOfEntityInformation;
    }

    private static Account getCompany() {
        return [SELECT Id, Name FROM Account LIMIT 1];
    }

    private static Lead getLead() {
        return [SELECT LastName, Status, Company, Source__c, Location__c FROM Lead LIMIT 1];
    }

    private static List<FCS_Entity_Information__c> getEntityInformation(String entityId) {
        List<FCS_Entity_Information__c> listOfResult = [SELECT Id, Entity_Id__c, Entity_Name__c, Domicile_Country__c, Company__c, Lead__c 
            FROM FCS_Entity_Information__c WHERE Entity_Id__c = :entityId ];
        return listOfResult;
    }

    @isTest
    private static void getEntityByEntityIdTest() {

        Test.startTest();
            List<FCS_Entity_Information__c> entityInformationList = FCSEntityInformationDAO.getEntityByEntityId(new Set<String> {ENTITY_ID});
        Test.stopTest();

        System.assertEquals(1, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_ID, entityInformationList.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('mcdonald', entityInformationList.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('USA', entityInformationList.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }
    
    @isTest
    private static void getEntityByIdTest() {
        FCS_Entity_Information__c record = getEntityInformation(ENTITY_ID).get(0);
        
        Test.startTest();
            List<FCS_Entity_Information__c> entityInformationList = FCSEntityInformationDAO.getEntityById(new Set<Id> {record.Id});
        Test.stopTest();

        System.assertEquals(1, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_ID, entityInformationList.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('mcdonald', entityInformationList.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('USA', entityInformationList.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void getLatestCurrentMonthEntityByCompanyIdTest() {
        FCS_Entity_Information__c entityInfo = getEntityInformation(ENTITY_ID).get(0);

        Test.startTest();
            List<FCS_Entity_Information__c> entityInformationList = FCSEntityInformationDAO.getLatestCurrentMonthEntityByCompanyId(new Set<Id>{entityInfo.Company__c});
        Test.stopTest();

        System.assertEquals(1, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_ID, entityInformationList.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('mcdonald', entityInformationList.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('USA', entityInformationList.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void getLatestCurrentMonthEntityByLeadIdTest() {
        FCS_Entity_Information__c entityInfo = getEntityInformation(ENTITY_ID).get(0);

        Test.startTest();
            List<FCS_Entity_Information__c> entityInformationList = FCSEntityInformationDAO.getLatestCurrentMonthEntityByLeadId(new Set<Id>{entityInfo.Lead__c});
        Test.stopTest();

        System.assertEquals(1, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_ID, entityInformationList.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('mcdonald', entityInformationList.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('USA', entityInformationList.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void saveEntityTest() {
        
        List<FCS_Entity_Information__c> listOfEntityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        listOfEntityInformation.get(0).Entity_Id__c = 'E5678';
        listOfEntityInformation.get(0).Entity_Name__c = 'MC Mechanics';
        listOfEntityInformation.get(0).Domicile_State__c = 'GA';

        Test.startTest();
            FCSEntityInformationDAO.saveEntity(listOfEntityInformation);
        Test.stopTest();

        List<FCS_Entity_Information__c> listOfResult = getEntityInformation('E5678');

        System.assertEquals(listOfResult.size(), 1, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Entity_Id__c, listOfEntityInformation.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Entity_Name__c, listOfEntityInformation.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfResult.get(0).Domicile_Country__c, listOfEntityInformation.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, listOfEntityInformation.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);        
    }

    @isTest
    private static void getEntityByLeadIdTest() {
        FCS_Entity_Information__c entityInfo = getEntityInformation(ENTITY_ID).get(0);

        Test.startTest();
            List<FCS_Entity_Information__c> entityInformationList = FCSEntityInformationDAO.getEntityByLeadId(new Set<Id>{entityInfo.Lead__c});
        Test.stopTest();

        System.assertEquals(1, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(ENTITY_ID, entityInformationList.get(0).Entity_Id__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('mcdonald', entityInformationList.get(0).Entity_Name__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals('USA', entityInformationList.get(0).Domicile_Country__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertNotEquals(0, entityInformationList.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }
}