public with sharing class FCSEntityVerifications {

    @TestVisible private static final String PARTIES_OR_AGENTS = 'PARTIES_OR_AGENTS';
    @TestVisible private static final String COMMUNICATION_ADDRESSES = 'COMMUNICATION_ADDRESSES';

    @TestVisible private static final String PARTY_RECORD_TYPE = Schema.SObjectType.FCS_Entity_Party_Agent__c.getRecordTypeInfosByName().get('Party').getRecordTypeId();
    @TestVisible private static final String AGENT_RECORD_TYPE = Schema.SObjectType.FCS_Entity_Party_Agent__c.getRecordTypeInfosByName().get('Agent').getRecordTypeId();

    
    @TestVisible private static final FCSEntityInformationDAO.IFCSEntityInformationDAO fcsEntityInformationDaoInstance = new FCSEntityInformationDAO();
    @TestVisible private static final FCSEntityPartyEntityAgentDAO.IFCSEntityPartyEntityAgentDAO fcsEntityPartyEntityAgentDaoInstance = new FCSEntityPartyEntityAgentDAO();
    @TestVisible private static final FCSCommunicationAddressDAO.IFCSCommunicationAddressDAO fcsCommunicationAddressDaoInstance = new FCSCommunicationAddressDAO();
    
    
    public static List<FCSEntityVerificationWrapper> generateEntityObjectFromWrapper(FCSBusinessDetails businessDetailResponseWrapper) {
        List<FCSEntityVerificationWrapper> fcsEntityVerificationSObjectList = new List<FCSEntityVerificationWrapper>();

        List<FCSXmlDomUtility.ProcessInfoRequestResult> listOfXmlResponseWrapper = businessDetailResponseWrapper.listOfResultWrapper;
        
        if (!listOfXmlResponseWrapper.isEmpty()) {
            for (FCSXmlDomUtility.ProcessInfoRequestResult eachProcessInfoResult : listOfXmlResponseWrapper) {
                if (eachProcessInfoResult.recordInfo != null) {
                    FCSXmlDomUtility.EntityInfo eachEntityInfo = eachProcessInfoResult.recordInfo.entityInfo;
                    if (eachProcessInfoResult.recordInfo.entityInfo != null) {
                        FCSXmlDomUtility.EntityType entityTypeRecord = eachEntityInfo.entityType;

                        FCS_Entity_Information__c fcsEntityInfoObject = new FCS_Entity_Information__c();
                        List<Communication_Address__c> listOfEntityCommunicationAddressInfo = new List<Communication_Address__c>();

                        fcsEntityInfoObject.Type__c = entityTypeRecord.entityType;
                        fcsEntityInfoObject.Entity_Id__c = entityTypeRecord.entityId;
                        fcsEntityInfoObject.Entity_Standing__c = entityTypeRecord.entityStanding;

                        FCSXmlDomUtility.Domicile entityDomicileInfo = entityTypeRecord.domicileDetails;
                        if (entityDomicileInfo != null) {
                            fcsEntityInfoObject.Domicile_Country__c = entityDomicileInfo.country;
                            fcsEntityInfoObject.Domicile_State__c = entityDomicileInfo.state;
                        }

                        for (FCSXmlDomUtility.Names eachNameRecord : entityTypeRecord.listOfEntityName) {
                            fcsEntityInfoObject.Entity_Name__c = eachNameRecord.entityName;

                            if (eachNameRecord.listOfAddress != null && !eachNameRecord.listOfAddress.isEmpty()) {
                                listOfEntityCommunicationAddressInfo.addAll(generateAddressSObjectFromResponse(eachNameRecord.listOfAddress));
                            }
                        }
                        
                        if (eachEntityInfo.listOfDates != null && !eachEntityInfo.listOfDates.isEmpty()) {
                            for (FCSXmlDomUtility.Dates eachDate : eachEntityInfo.listOfDates) {
                                if( 'DateOfIncorporation'.equalsIgnoreCase(eachDate.dateType)) {
                                    if (String.isNotBlank(eachDate.dateType)) {
                                        fcsEntityInfoObject.Date_Of_Incorporation__c = FCSUtil.convertFCSDateInSFDate(eachDate.dateValue);
                                    }
                                } else if ('FilingDate'.equalsIgnoreCase(eachDate.dateType)) {
                                    if (String.isNotBlank(eachDate.dateType)) {
                                        fcsEntityInfoObject.Date_of_Filing__c = FCSUtil.convertFCSDateInSFDate(eachDate.dateValue);
                                    }
                                }
                            }
                        }
                        
                        FCSEntityVerificationWrapper fcsEntityVerificationWrapperObject = new FCSEntityVerificationWrapper(fcsEntityInfoObject, listOfEntityCommunicationAddressInfo, null);
                        
                        List<FCSPartyAgentWrapper> listOfEntityPartyOrAgent = new List<FCSPartyAgentWrapper>();
                        
                        if (eachEntityInfo.listOfParties != null && !eachEntityInfo.listOfParties.isEmpty()) {
                            listOfEntityPartyOrAgent.addAll(generateEntityPartySObjectFromResponse (eachEntityInfo.listOfParties));
                        }

                        if (eachEntityInfo.listOfAgents != null && !eachEntityInfo.listOfAgents.isEmpty()) {
                            listOfEntityPartyOrAgent.addAll(generateEntityAgentSObjectFromResponse (eachEntityInfo.listOfAgents));
                        }
                        
                        fcsEntityVerificationWrapperObject.listOfEntityPartyOrAgent = listOfEntityPartyOrAgent;
                        fcsEntityVerificationSObjectList.add(fcsEntityVerificationWrapperObject);
                    }
                }
                
            }
            
        }
        return fcsEntityVerificationSObjectList;
    }

    @TestVisible
    private static List<Communication_Address__c> generateAddressSObjectFromResponse(List<FCSXmlDomUtility.Address> listOfAddress) {
        List<Communication_Address__c> listOfEntityCommunicationAddress = new List<Communication_Address__c>();
        
        if (listOfAddress != null && !listOfAddress.isEmpty()) {
            for (FCSXmlDomUtility.Address eachAddress: listOfAddress) {
                Communication_Address__c communicationAddress = new Communication_Address__c();
                communicationAddress.City__c = eachAddress.city;
                communicationAddress.Country_Parish__c = eachAddress.countryOrParish;
                communicationAddress.County__c = eachAddress.country;
                communicationAddress.Postal_Code__c = eachAddress.postalCode;
                communicationAddress.Street_Address_1__c = eachAddress.address1;
                communicationAddress.Street_Address_2__c = eachAddress.address2; 
                communicationAddress.State__c = eachAddress.stateOrProvince;
                communicationAddress.Address_Type__c = eachAddress.addressType;
                listOfEntityCommunicationAddress.add(communicationAddress);
            }
        }
        return listOfEntityCommunicationAddress;
    }

    @TestVisible
    private static List<FCSPartyAgentWrapper> generateEntityPartySObjectFromResponse(List<FCSXmlDomUtility.Parties> listOfParties) {

         List<FCSPartyAgentWrapper> listOfPartiesWrapper = new List<FCSPartyAgentWrapper>();

         if (listOfParties != null && !listOfParties.isEmpty()) {
             for (FCSXmlDomUtility.Parties eachParty : listOfParties) {
                    FCS_Entity_Party_Agent__c partyInformationRecord = new FCS_Entity_Party_Agent__c();
                    FCSPartyAgentWrapper partySObjectWrapper = new FCSPartyAgentWrapper();
                    
                    
                    partyInformationRecord.Type__c = eachParty.partiesType;
                    partyInformationRecord.Title__c = eachParty.title;
                    partyInformationRecord.RecordTypeId = PARTY_RECORD_TYPE;
                    
                   

                    List<Communication_Address__c> listOfPartyAddress = new List<Communication_Address__c>();
                    if (eachParty.listOfPartiesName != null && !eachParty.listOfPartiesName.isEmpty()) {
                        for (FCSXmlDomUtility.Names eachPartyName : eachParty.listOfPartiesName) {
                             partyInformationRecord.Name__c = eachPartyName.entityName;
                              
                             if (eachPartyName.listOfAddress != null && !eachPartyName.listOfAddress.isEmpty()) {
                                listOfPartyAddress.addAll(generateAddressSObjectFromResponse(eachPartyName.listOfAddress));
                             }
                        }
                    }
                    partySObjectWrapper.entityPartyOrAgent = partyInformationRecord;
                    partySObjectWrapper.listOfPartyOrAgentCommunicationAddress = listOfPartyAddress;
                    listOfPartiesWrapper.add(partySObjectWrapper);
             }
         }
         return listOfPartiesWrapper;
    }

    @TestVisible
    private static List<FCSPartyAgentWrapper> generateEntityAgentSObjectFromResponse(List<FCSXmlDomUtility.Agent> listOfAgent) {

         List<FCSPartyAgentWrapper> listOfAgentsWrapper = new List<FCSPartyAgentWrapper>();

         if (listOfAgent != null && !listOfAgent.isEmpty()) {
             for (FCSXmlDomUtility.Agent eachPartyOrAgentRecord : listOfAgent) {
                    FCS_Entity_Party_Agent__c agentInformationWrapper = new FCS_Entity_Party_Agent__c();
                    FCSPartyAgentWrapper agentSObjectWrapper = new FCSPartyAgentWrapper();
                    
                    agentInformationWrapper.RecordTypeId = AGENT_RECORD_TYPE;
                   
                    List<Communication_Address__c> listOfAgentAddress = new List<Communication_Address__c>();
                    
                    if (eachPartyOrAgentRecord.listOfAgentsName != null &&  !eachPartyOrAgentRecord.listOfAgentsName.isEmpty()) {
                        for (FCSXmlDomUtility.Names eachAgentName : eachPartyOrAgentRecord.listOfAgentsName) {
                             agentInformationWrapper.Name__c = eachAgentName.entityName;
                              
                             if (eachAgentName.listOfAddress != null && !eachAgentName.listOfAddress.isEmpty()) {
                                listOfAgentAddress.addAll(generateAddressSObjectFromResponse(eachAgentName.listOfAddress));
                             }
                        }
                    }
                    agentSObjectWrapper.entityPartyOrAgent = agentInformationWrapper;
                    agentSObjectWrapper.listOfPartyOrAgentCommunicationAddress = listOfAgentAddress;
                    listOfAgentsWrapper.add(agentSObjectWrapper);
             }
         }
         return listOfAgentsWrapper;
    }


    public static void saveEntityVerificationInformation ( List<FCSEntityVerificationWrapper> listOfEntityVerificationWrapper, String recordId) {
        Savepoint sp = Database.setSavepoint();
        try {
            if (listOfEntityVerificationWrapper != null && !listOfEntityVerificationWrapper.isEmpty()) {
                List<FCS_Entity_Information__c> listOfEntityVerificationRecord = new List<FCS_Entity_Information__c>();

                Map<String, FCS_Entity_Information__c> mapOfEntityIdVsEntityVerificationRecord = new Map<String, FCS_Entity_Information__c>();
                for (FCSEntityVerificationWrapper eachFCSEntityVerification: listOfEntityVerificationWrapper) {
                    FCS_Entity_Information__c entityInformation = eachFCSEntityVerification.entityInformation;
                    if (entityInformation != null) {
                        if (Id.valueOf(recordId).getSobjectType() == Account.getSObjectType()) {
                            entityInformation.Company__c = recordId;
                        }

                        else if (Id.valueOf(recordId).getSobjectType() == Lead.getSObjectType()) {
                             entityInformation.Lead__c = recordId;
                        }
                        
                        mapOfEntityIdVsEntityVerificationRecord.put(entityInformation.Entity_Id__c, entityInformation);
                    }
                }

                if (!mapOfEntityIdVsEntityVerificationRecord.keySet().isEmpty()) {
                    insert mapOfEntityIdVsEntityVerificationRecord.values();
                }
                
                List<Communication_Address__c> listOfCommunicationAddresses = new List<Communication_Address__c>();
                List<FCSPartyAgentWrapper> listOfPartyAgentInfo = new List<FCSPartyAgentWrapper>();
                List<FCS_Entity_Party_Agent__c> listOfPartyOrAgentWrapper = new List<FCS_Entity_Party_Agent__c>();

                for (FCSEntityVerificationWrapper eachFCSEntityVerification: listOfEntityVerificationWrapper) {
                    FCS_Entity_Information__c entityInformation = eachFCSEntityVerification.entityInformation;

                    if (entityInformation != null) {
                        FCS_Entity_Information__c savedEntityInformation = mapOfEntityIdVsEntityVerificationRecord.get(entityInformation.Entity_Id__c);

                        if (eachFCSEntityVerification.listOfEntityCommunicationAddress != null && !eachFCSEntityVerification.listOfEntityCommunicationAddress.isEmpty()) {
                            for (Communication_Address__c eachCommunicationAdd : eachFCSEntityVerification.listOfEntityCommunicationAddress) {
                                eachCommunicationAdd.Entity_Information__c = savedEntityInformation.Id;
                            }
                            listOfCommunicationAddresses.addAll(eachFCSEntityVerification.listOfEntityCommunicationAddress);
                        }

                        if (eachFCSEntityVerification.listOfEntityPartyOrAgent != null && !eachFCSEntityVerification.listOfEntityPartyOrAgent.isEmpty()) {
                            for (FCSPartyAgentWrapper eachPartyOrAgentInfo : eachFCSEntityVerification.listOfEntityPartyOrAgent) {
                                eachPartyOrAgentInfo.entityPartyOrAgent.Entity_Information__c = savedEntityInformation.Id;
                                listOfPartyAgentInfo.add(eachPartyOrAgentInfo);
                                listOfPartyOrAgentWrapper.add(eachPartyOrAgentInfo.entityPartyOrAgent);
                            }
                        } 
                    }
                }

                if (!listOfPartyOrAgentWrapper.isEmpty()) {
                    insert listOfPartyOrAgentWrapper;
                }

                for (integer i = 0; i < listOfPartyOrAgentWrapper.size() ; i++) {
                    FCS_Entity_Party_Agent__c savedPartyRecord = listOfPartyOrAgentWrapper.get(i);
                    FCSPartyAgentWrapper fcsPartyAgentInfo = listOfPartyAgentInfo.get(i);
                    for (Communication_Address__c eachCommunicationAddress: fcsPartyAgentInfo.listOfPartyOrAgentCommunicationAddress) {
                        eachCommunicationAddress.Entity_Party_Agent__c = savedPartyRecord.Id;
                    }

                    listOfCommunicationAddresses.addAll(fcsPartyAgentInfo.listOfPartyOrAgentCommunicationAddress);
                }

                insert listOfCommunicationAddresses;
            }
        } catch(Exception ex) {
            Database.rollback(sp);
        }
    }

    public static FCSEntityVerificationWrapper getEntityInformationWithInMonthByCompany(String companyId) {
        
        FCSEntityVerificationWrapper objectOfFCSEntityVerficationWrapper = null;

        if (!String.isBlank(companyId)) {

            List<FCS_Entity_Information__c> listOfFCSEntityInformation = 
                fcsEntityInformationDaoInstance.getLatestCurrentMonthEntityByCompanyId(new Set<Id> {Id.valueOf(companyId)});

              objectOfFCSEntityVerficationWrapper = generateWrapperObjectByEntityRecords(listOfFCSEntityInformation);
        }
        return objectOfFCSEntityVerficationWrapper;
    }

    public static FCSEntityVerificationWrapper getEntityInformationWithInMonthByLead(String leadId) {
        
        FCSEntityVerificationWrapper objectOfFCSEntityVerficationWrapper = null;

        if (!String.isBlank(leadId)) {

            List<FCS_Entity_Information__c> listOfFCSEntityInformation = 
                fcsEntityInformationDaoInstance.getLatestCurrentMonthEntityByLeadId(new Set<Id> {Id.valueOf(leadId)});

            objectOfFCSEntityVerficationWrapper = generateWrapperObjectByEntityRecords(listOfFCSEntityInformation);
        }
        return objectOfFCSEntityVerficationWrapper;
    }

    private static FCSEntityVerificationWrapper generateWrapperObjectByEntityRecords (List<FCS_Entity_Information__c> listOfFCSEntityInformation) {
        FCSEntityVerificationWrapper objectOfFCSEntityVerficationWrapper = null;

        if (!listOfFCSEntityInformation.isEmpty()) {
                
                FCS_Entity_Information__c entityInformationObject = listOfFCSEntityInformation.get(0);
                
                List<Communication_Address__c> listOfEntityCommunicationAddress = 
                    fcsCommunicationAddressDaoInstance.getCommunicationAddressByEntityInformationId(new Set<Id> {entityInformationObject.Id});

                List<FCS_Entity_Party_Agent__c> listOfPartiesOrAgents = 
                    fcsEntityPartyEntityAgentDaoInstance.getEntityPartyEntityAgentByInformationId(new Set<Id> {entityInformationObject.Id});

                objectOfFCSEntityVerficationWrapper = 
                    new FCSEntityVerificationWrapper(entityInformationObject, listOfEntityCommunicationAddress, null);

                objectOfFCSEntityVerficationWrapper.listOfEntityPartyOrAgent = getPartiesAddressByPartiesId(listOfPartiesOrAgents);
            }

            return objectOfFCSEntityVerficationWrapper;
    }
    
    @TestVisible 
    private static List<FCSPartyAgentWrapper> getPartiesAddressByPartiesId(List<FCS_Entity_Party_Agent__c> listOfPartiesOrAgents) {
        List<FCSPartyAgentWrapper> listOfPartiesAddressWrapper = new List<FCSPartyAgentWrapper>();

        Set<Id> setOfPartiesAgentsIds = new Set<Id>();
        if (!listOfPartiesOrAgents.isEmpty()) {
            for (FCS_Entity_Party_Agent__c eachPartyAgent : listOfPartiesOrAgents) {
                FCSPartyAgentWrapper partyAgentWrapper = new FCSPartyAgentWrapper();
                partyAgentWrapper.entityPartyOrAgent = eachPartyAgent;
                setOfPartiesAgentsIds.add(eachPartyAgent.Id);
                listOfPartiesAddressWrapper.add(partyAgentWrapper);
            }

            List<Communication_Address__c> listOfEntityPartyAgentAddresses = 
                fcsCommunicationAddressDaoInstance.getCommunicationAddressByPartyAgentId(setOfPartiesAgentsIds);

            for (FCSPartyAgentWrapper eachWrapper : listOfPartiesAddressWrapper) {
                List<Communication_Address__c> listOfEachPartyAddress = eachWrapper.listOfPartyOrAgentCommunicationAddress;
                
                if (listOfEachPartyAddress == null) {
                    listOfEachPartyAddress = new List<Communication_Address__c>();
                }

                for (Communication_Address__c eachAddress : listOfEntityPartyAgentAddresses) {
                    if (eachAddress.Entity_Party_Agent__c  != null && 
                        eachAddress.Entity_Party_Agent__c == eachWrapper.entityPartyOrAgent.Id) {
                            listOfEachPartyAddress.add(eachAddress);
                    }
                }
                eachWrapper.listOfPartyOrAgentCommunicationAddress = listOfEachPartyAddress;
            }
        }
        return listOfPartiesAddressWrapper;
    }

     public static Boolean isSFEntityDataAvailableInSFForThisMonthRange(Id recordId) {
         if (recordId.getSobjectType() == Account.getSObjectType()) {
             List<FCS_Entity_Information__c> listOfFCSEntity = fcsEntityInformationDaoInstance.getLatestCurrentMonthEntityByCompanyId(new Set<Id> {recordId});
            return listOfFCSEntity.size() > 0;
         }

         if (recordId.getSobjectType() == Lead.getSObjectType()) {
             List<FCS_Entity_Information__c> listOfFCSEntity = fcsEntityInformationDaoInstance.getLatestCurrentMonthEntityByLeadId(new Set<Id> {recordId});
            return listOfFCSEntity.size() > 0;
         }
         return false;
     }

     
    
     public class FCSEntityVerificationWrapper {
         @AuraEnabled
         public FCS_Entity_Information__c entityInformation {get;set;}
         @AuraEnabled
         public List<Communication_Address__c> listOfEntityCommunicationAddress {get;set;}
         @AuraEnabled
         public List<FCSPartyAgentWrapper> listOfEntityPartyOrAgent {get;set;}

         public FCSEntityVerificationWrapper(FCS_Entity_Information__c entityInformation, 
            List<Communication_Address__c> listOfEntityCommunicationAddress, List<FCSPartyAgentWrapper> listOfEntityPartyOrAgent) {
                this.entityInformation = entityInformation;
                this.listOfEntityCommunicationAddress = listOfEntityCommunicationAddress;
                this.listOfEntityPartyOrAgent = listOfEntityPartyOrAgent;
         }
     }

     public class FCSPartyAgentWrapper {
         @AuraEnabled
         public FCS_Entity_Party_Agent__c entityPartyOrAgent {get;set;}
         @AuraEnabled
         public List<Communication_Address__c> listOfPartyOrAgentCommunicationAddress {get;set;}
     }
}