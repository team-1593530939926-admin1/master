@isTest(isParallel=true)
private class FCSApiCalloutTest {

    private static final String SHOULD_GET_RESPONSE_ASSERT_MESSAGE = 'Should get proper Response.';
    private static final String SUCCESS_STATUS_CODE_ASSERT_MESSAGE = 'Status Code Should be '+FCSAPIConstant.STATUS_CODE_SUCCESS+'.';
    private static final String BAD_REQUEST_STATUS_CODE_ASSERT_MESSAGE = 'Status Code Should be '+FCSAPIConstant.STATUS_CODE_BAD_REQUEST+'.';
    private static final String ENTITY_SIZE_TOO_LARGE_STATUS_CODE_ASSERT_MESSAGE = 'Status Code Should be '+FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE+'.';
    private static final String INTERNAL_ERROR_RESPONSE_STATUS_CODE_ASSERT_MESSAGE = 'Status Code Should be '+FCSAPIConstant.STATUS_CODE_INTERNAL_ERROR+'.';
    private static final String SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE = 'There must be error response.';
    private static final String FAILURE_REASON_FOR_ERROR_RESPONSE = 'Failure Reason.';

    private static final String BODY_ERROR_RESPONSE = '{"Message":"'+FAILURE_REASON_FOR_ERROR_RESPONSE+'"}';

    private static final String STATE_INFO_BODY = '[{"StateCode" : "CA"}]';

    @isTest
    private static void calloutSuccessTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_SUCCESS, FCSAPIConstant.CONTENT_TYPE_JSON, STATE_INFO_BODY));
        String endPointUrl = FCSAPIConstant.GET_STATE_INFO;
        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_GET, FCSAPIConstant.CONTENT_TYPE_JSON, null);
        
        Test.startTest();
        HttpResponse actualRes = FCSApiCallout.callout(requestWrapper);
        Test.stopTest();
        
        System.assertEquals(STATE_INFO_BODY, actualRes.getBody(), SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals(FCSAPIConstant.STATUS_CODE_SUCCESS, actualRes.getStatusCode(), SUCCESS_STATUS_CODE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), SHOULD_GET_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void calloutBadRequestErrorResponseTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_BAD_REQUEST, FCSAPIConstant.CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE));
        String endPointUrl = FCSAPIConstant.GET_STATE_INFO;
        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_GET, FCSAPIConstant.CONTENT_TYPE_JSON, null);
        
        Test.startTest();
        HttpResponse actualRes = FCSApiCallout.callout(requestWrapper);
        Test.stopTest();
        
        System.assertEquals(BODY_ERROR_RESPONSE, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals(FCSAPIConstant.STATUS_CODE_BAD_REQUEST, actualRes.getStatusCode(), BAD_REQUEST_STATUS_CODE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void calloutRequestEntityTooLargeTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE, FCSAPIConstant.CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE));
        String endPointUrl = FCSAPIConstant.GET_STATE_INFO;
        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_GET, FCSAPIConstant.CONTENT_TYPE_JSON, null);
        
        Test.startTest();
        HttpResponse actualRes = FCSApiCallout.callout(requestWrapper);
        Test.stopTest();
        
        System.assertEquals(BODY_ERROR_RESPONSE, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals(FCSAPIConstant.STATUS_CODE_REQUEST_ENTITY_TOO_LARGE, actualRes.getStatusCode(), ENTITY_SIZE_TOO_LARGE_STATUS_CODE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
    }

    @isTest
    private static void calloutInternalErrorResponseTest() {
        Test.setMock(HttpCalloutMock.class, new FCSApiCalloutMock(FCSAPIConstant.STATUS_CODE_INTERNAL_ERROR, FCSAPIConstant.CONTENT_TYPE_JSON, BODY_ERROR_RESPONSE));
        String endPointUrl = FCSAPIConstant.GET_STATE_INFO;
        FCSApiCallout.FCSRequestWrapper requestWrapper = new FCSApiCallout.FCSRequestWrapper(
            endPointUrl, FCSAPIConstant.METHOD_TYPE_GET, FCSAPIConstant.CONTENT_TYPE_JSON, null);
        
        Test.startTest();
        HttpResponse actualRes = FCSApiCallout.callout(requestWrapper);
        Test.stopTest();
        
        System.assertEquals(BODY_ERROR_RESPONSE, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
        System.assertEquals(FCSAPIConstant.STATUS_CODE_INTERNAL_ERROR, actualRes.getStatusCode(), INTERNAL_ERROR_RESPONSE_STATUS_CODE_ASSERT_MESSAGE);
        System.assertNotEquals(null, actualRes.getBody(), SHOULD_GET_ERROR_RESPONSE_ASSERT_MESSAGE);
    }
}