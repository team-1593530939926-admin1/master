public class EquipmentInformationWrapper
{    
    @AuraEnabled
    public String dealName{get;set;}
    @AuraEnabled
    public String supplierName{get;set;}
    @AuraEnabled
    public String contactName{get;set;}
    @AuraEnabled
    public String leaseTermMonths{get;set;}
    @AuraEnabled
    public String estimatedEquipmentCost{get;set;}
    @AuraEnabled
    public String equipmentDescription{get;set;}
     @AuraEnabled
    public String equipmentLocation{get;set;}
    @AuraEnabled
    public String endOfLeasePurchaseOption{get;set;}        
    @AuraEnabled
    public String totalCashPrice{get;set;}
    @AuraEnabled
    public String otherText{get;set;}
    @AuraEnabled
    public String otherStreetAddress{get;set;}
    @AuraEnabled
    public String otherCity{get;set;}
    @AuraEnabled
    public String otherCounty{get;set;}
    @AuraEnabled
    public String otherState{get;set;}
    @AuraEnabled
    public String otherZip{get;set;}
    @AuraEnabled
    public String companyId{get;set;}
    @AuraEnabled
    public String recordTypeId{get;set;}
    @AuraEnabled
    public String comment{get;set;}
    @AuraEnabled
    public String documentationType{get;set;}
    @AuraEnabled
    public String note{get;set;}
    @AuraEnabled
    public AddressInformationWrapper addressInfo{get;set;}
    public Opportunity dealInformation {get;set;}
    public Communication_Address__c otherDealAddress{get; set;}
    public static RecordTypeDAO.IRecordTypeDAO recordTypeDAOInstance = new RecordTypeDAO();
    public static UserDAO.IUserDAO userDAOInstance = new UserDAO();
    
    public EquipmentInformationWrapper(EquipmentInformationWrapper equipmentInfo, Account companyInfo){
        Opportunity opportunityRecord = new Opportunity(); //Account = new Account(External_System_Id__c = companyInfo.External_System_Id__c)
        Datetime dt = System.now();
        String companyNameValue = companyInfo.Name;
        if(companyInfo.Name.length() > 96){
            companyNameValue = companyNameValue.substring(0,96);
        }
        opportunityRecord.Name = companyNameValue + ' - ' + dt.month() + '/' + dt.day() + '/' + dt.year() +  ' - ' + dt.hour() + ':' + dt.minute();
        opportunityRecord.StageName = 'Prospect';
        opportunityRecord.CloseDate = System.today().addDays(30);
        opportunityRecord.Company_Name__c = equipmentInfo.supplierName;
        opportunityRecord.Third_Party_Name__c = equipmentInfo.contactName;
        opportunityRecord.Lease_Term_Request_1__c = equipmentInfo.leaseTermMonths;
        opportunityRecord.LeaseInfo_DealBackground__c = equipmentInfo.note;
        opportunityRecord.Documentation_Type__c = equipmentInfo.documentationType;
        if(!String.isEmpty(equipmentInfo.estimatedEquipmentCost)){
            opportunityRecord.Amount = decimal.valueOf(equipmentInfo.estimatedEquipmentCost);            
        }            
        opportunityRecord.LeaseInfo_EquipmentDescription__c = equipmentInfo.equipmentDescription;
        opportunityRecord.Lease_Type_Request_1__c = equipmentInfo.endOfLeasePurchaseOption;        
        if(!String.isEmpty(equipmentInfo.totalCashPrice)){
            opportunityRecord.Total_Cash_Price__c = decimal.valueOf(equipmentInfo.totalCashPrice);
        }
        Map<String, String> mapOfEquipmentDiscription = CVFCustomSettings.getMapOfEquipmentValues();
        if(mapOfEquipmentDiscription.containskey(equipmentInfo.equipmentDescription)) {
          opportunityRecord.LeaseInfo_EquipmentCategory__c =   mapOfEquipmentDiscription.get(equipmentInfo.equipmentDescription);
        }
        opportunityRecord.Lease_Type_Request_6__c = equipmentInfo.otherText;
        User contactRefererId = userDAOInstance.getContactByUserId();
        If(!String.isEmpty(contactRefererId.contactId)){
            opportunityRecord.Referrer__c = Id.valueOf(contactRefererId.contactId);
            opportunityRecord.Referral_Type__c = 'Broker';
        }
        opportunityRecord.External_System_Id__c = opportunityRecord.Name;
        opportunityRecord.RecordTypeId = recordTypeDAOInstance.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');
        
        Communication_Address__c otherAddressInstance = new Communication_Address__c(Opportunity__r = new Opportunity(External_System_Id__c = opportunityRecord.Name));        
        otherAddressInstance.Street_Address_1__c = equipmentInfo.otherStreetAddress;
        otherAddressInstance.City__c = equipmentInfo.otherCity;
        otherAddressInstance.State__c = equipmentInfo.otherState;
        otherAddressInstance.County__c = equipmentInfo.otherCounty;
        otherAddressInstance.Postal_Code__c = equipmentInfo.otherZip;
        
        this.dealInformation = opportunityRecord;
        this.otherDealAddress = otherAddressInstance;
        
        
    }
    public EquipmentInformationWrapper(){}
    
    public EquipmentInformationWrapper(EquipmentInformationWrapper equipmentInformation , Boolean isReview){
        this.dealName = equipmentInformation.dealName;
        this.documentationType = equipmentInformation.documentationType;
        this.supplierName = equipmentInformation.supplierName;      
        this.contactName = equipmentInformation.contactName;       
        this.leaseTermMonths = equipmentInformation.leaseTermMonths;       
        this.estimatedEquipmentCost = equipmentInformation.estimatedEquipmentCost;      
        this.equipmentDescription = equipmentInformation.equipmentDescription;      
        this.endOfLeasePurchaseOption = equipmentInformation.endOfLeasePurchaseOption;    
        this.totalCashPrice = equipmentInformation.totalCashPrice;       
        this.otherText = equipmentInformation.otherText;    
        this.recordTypeId = equipmentInformation.recordTypeId;
        this.otherCity = equipmentInformation.otherCity;
        this.otherCounty = equipmentInformation.otherCounty;
        this.otherState = equipmentInformation.otherState;
        this.otherStreetAddress = equipmentInformation.otherStreetAddress;
        this.otherZip = equipmentInformation.otherZip; 
        this.equipmentLocation = equipmentInformation.equipmentLocation;
        this.note = equipmentInformation.note;
    }  
    
    public EquipmentInformationWrapper(Opportunity opportunityRecord){
        this.dealName = opportunityRecord.Name;    
        this.documentationType = opportunityRecord.Documentation_Type__c;
        this.supplierName = opportunityRecord.Company_Name__c;      
        this.contactName = opportunityRecord.Third_Party_Name__c;       
        this.leaseTermMonths = opportunityRecord.Lease_Term_Request_1__c;       
        this.estimatedEquipmentCost = opportunityRecord.Amount != null ? String.valueOf(opportunityRecord.Amount) : null;      
        this.equipmentDescription = opportunityRecord.LeaseInfo_EquipmentDescription__c;      
        this.endOfLeasePurchaseOption = opportunityRecord.Lease_Type_Request_1__c;    
        this.totalCashPrice = opportunityRecord.Total_Cash_Price__c != null ? String.valueOf(opportunityRecord.Total_Cash_Price__c) : null;       
        this.otherText = opportunityRecord.Lease_Type_Request_6__c;
        this.note = opportunityRecord.LeaseInfo_DealBackground__c;
        this.recordTypeId = opportunityRecord.RecordTypeId;
        this.comment = opportunityRecord.Broker_Comment__c; 
        if(!opportunityRecord.Communication_Addresses__r.isEmpty()){
            this.otherCity = opportunityRecord.Communication_Addresses__r.get(0).City__c;
            this.otherCounty = opportunityRecord.Communication_Addresses__r.get(0).County__c;
            this.otherState = opportunityRecord.Communication_Addresses__r.get(0).State__c;
            this.otherStreetAddress = opportunityRecord.Communication_Addresses__r.get(0).Street_Address_1__c;
            this.otherZip = opportunityRecord.Communication_Addresses__r.get(0).Postal_Code__c;
        }else{
            this.otherCity = '';
            this.otherCounty = '';
            this.otherState = '';
            this.otherStreetAddress = '';
            this.otherZip = '';
        }
    }  
    
    
    
}