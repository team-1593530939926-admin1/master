public class FCSBusinessDetails { 
    
    @AuraEnabled
    public Header header {get; set;}
    @AuraEnabled
    public List<Results> results {get; set;}
    @AuraEnabled
    public List<FCSXmlDomUtility.ProcessInfoRequestResult> listOfResultWrapper {get; set;}
    
    public FCSBusinessDetails() {}
    
    public class Header {
        
        @AuraEnabled
        public Integer transactionID {get; set;}
        @AuraEnabled
        public String orderDate {get; set;}
        @AuraEnabled
        public String stateCode {get; set;}
        @AuraEnabled
        public String searchString {get; set;}
        @AuraEnabled
        public String indexDate {get; set;}
        @AuraEnabled
        public Integer numberOfHits {get; set;}
        @AuraEnabled
        public String passThrough {get; set;}
        
        public Header() { }
    }
    
    public class Results {
        
        @AuraEnabled
        public String results {get; set;}
               
        public Results() {}
    }

    public void processXmlResponseToWrapper () {
        if (this.listOfResultWrapper == null) {
            this.listOfResultWrapper = new List<FCSXmlDomUtility.ProcessInfoRequestResult>();
        }
        if (this.results != null && !this.results.isEmpty()) {
            for (Results eachResultRecord: this.results) {
                if (String.isNotBlank(eachResultRecord.results)) {
                    this.listOfResultWrapper.add(FCSXmlDomUtility.parseFCSBusinessDetailXmlResponse(eachResultRecord.results));
                }
            }
        }
    }
}