@isTest
private class ContentVersionDAOTest {
    
    @TestSetup
    static void testSetup() { 
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
    }

    private static List<ContentVersion> getContentVersion() {
    	List<ContentVersion> listOfContentVersion = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Title = 'Test' LIMIT 1];  
        return listOfContentVersion;
    }
    
    private static List<ContentDocument> getContentDocuments() {
        List<ContentDocument> listOfContentDocuments = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        return listOfContentDocuments;
    }

    /*private static List<ContentDocumentLink> getContentDocumentLink() {
        List<ContentDocumentLink> listOfCDLink = [Select Id, ContentDocumentId, LinkedEntityId From ContentDocumentLink];
        return listOfCDLink;
    }*/
    
    @isTest
    private static void insertListOfContentVersionTest() {
        List<ContentVersion> listOfContentVersion = getContentVersion();
        Opportunity opportunityInstance = TestUtil.createOpportunityData();
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = opportunityInstance.Id;
        cdl.ContentDocumentId = listOfContentVersion.get(0).ContentDocumentId;
        cdl.shareType = 'V';
        
        Test.startTest();
            List<ContentDocumentLink> listOfContentDocumentLink = ContentVersionDAO.insertListOfContentVersion(new List<ContentDocumentLink>{cdl});
        Test.stopTest();
        
        System.assertEquals(1, listOfContentDocumentLink.size(), 'List should not be empty or null');
    }
    
    @isTest
    private static void getListOfContentVersionTest() {
        List<ContentDocument> listOfContentDocuments = getContentDocuments();

        Test.startTest();
            List<ContentVersion> listOfContentDocuments1 = ContentVersionDAO.getListOfContentVersion(new List<String>{listOfContentDocuments.get(0).Id});
        Test.stopTest();

        System.assertEquals(listOfContentDocuments.size(), listOfContentDocuments1.size(), 'List should not be empty or null');
    }

    @isTest
    private static void getListOfDocumentsOpportunityIdTest() {
        List<ContentVersion> listOfContentVersion = getContentVersion();
        Opportunity opportunityInstance = TestUtil.createOpportunityData();
        
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = opportunityInstance.Id;
        cdl.ContentDocumentId = listOfContentVersion.get(0).ContentDocumentId;
        cdl.shareType = 'V';
        Insert cdl;
        
        Test.startTest();
        	List<ContentVersion> listOfContentVersion1 = ContentVersionDAO.getListOfDocumentsOpportunityId(opportunityInstance.Id);
        Test.stopTest();
        
        System.assertEquals(1, listOfContentVersion1.size(), 'List should not be empty or null');
    }

    @isTest
    private static void updateContentVersionsTest() {
        List<ContentVersion> listOfContentVersion = getContentVersion();
        listOfContentVersion.get(0).Title = 'Test1';
        
        Test.startTest();
        	List<ContentVersion> listOfContentVersion1= ContentVersionDAO.updateContentVersions(listOfContentVersion);
        Test.stopTest();
        
        System.assertEquals(listOfContentVersion.get(0).Title, listOfContentVersion1.get(0).Title, 'Value should be same.');
    }
}