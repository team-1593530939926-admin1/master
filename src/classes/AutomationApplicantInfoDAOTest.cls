@istest
public class AutomationApplicantInfoDAOTest {
      @testSetup
    private static void testSetupData(){ 
        TestUtil.opportunityRelatedAccountData();
    }
    
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c, StageName, Score__c, Tier__c, Broker_Comment__c FROM Opportunity];
    }
    
     private static List<Account> getAccount()
    {
        return [SELECT Id, Name FROM Account];
    }
    
  @isTest
    private static void testGetOpportunityByIdIfExist()
    {
        List<Account> accountList = getAccount();
        Account accountRecord = accountList[0];
        set<String> setIds = new set<String>();
        setIds.add(accountList[0].Id);
        Test.startTest();
            List<Opportunity> listOfOpportunity = AutomationApplicantInfoDAO.getOpportunityByAccountId(setIds);
        Test.stopTest();
        System.assertEquals(1, listOfOpportunity.size(), 'Record not found.');            
    }    

    @isTest
    private static void testGetOpportunityByIdIfNotExist()
    {       
        set<String> setIds = new set<String>();
        setIds.add('0065C000005jsOpQAJ');
        Test.startTest();
            List<Opportunity> listOfOpportunity = AutomationApplicantInfoDAO.getOpportunityByAccountId(setIds);
        Test.stopTest();
        System.assertEquals(0, listOfOpportunity.size(), 'Record found.');            
    }
    
    @isTest
    private static void testGetRecordRelatedToDeal()
    {
        List<Opportunity> opportunityList = getOpportunity();
        Opportunity opprtunityRecord = opportunityList[0];
        Test.startTest();
            List<Automation_Applicant_Info__c> listOfAutomation = AutomationApplicantInfoDAO.getRecordRelatedToDeal(opportunityList[0].Id,'Complete');
        Test.stopTest();
        System.assertEquals(0, listOfAutomation.size(), 'Record not found.');            
    }    
    
    
}