@isTest
private class CVFCustomSettingsTest {
    
     @testSetup
    private static void testSetupData()
    {
        TestUtil.createCVFCustomSettingsCountryAndStateData();
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesExist()
    {        
        Test.startTest();
            List<Country_State_Mapping__c> result = CVFCustomSettings.getCountryAndStateValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size());        
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesNotExist()
    {       
        List<Country_State_Mapping__c> listOfOptions = [SELECT Name, State_Code__c FROM Country_State_Mapping__c];
        delete listOfOptions;
        
        Test.startTest();
            List<Country_State_Mapping__c> result = CVFCustomSettings.getCountryAndStateValues();
        Test.stopTest();
        
        System.assertEquals(0, result.size());        
    }
    
    @isTest
    private static void testGetDescriptionOptionValues()
    {        
        CVF_Equipment_Description__c testRecord = new CVF_Equipment_Description__c();
        testRecord.Description_Label__c = 'test';
        testRecord.Name = 'test';
        testRecord.Equipment_Category__c = 'test1';
        insert testRecord;
        Test.startTest();
            List<CVF_Equipment_Description__c> result = CVFCustomSettings.getDescriptionOptionValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size());        
    }
    
     @isTest
    private static void testGetMapOfEquipmentValues()
    {        
        CVF_Equipment_Description__c testRecord = new CVF_Equipment_Description__c();
        testRecord.Description_Label__c = 'test';
        testRecord.Name = 'test';
        testRecord.Equipment_Category__c = 'test1';
        insert testRecord;
        Test.startTest();
            Map<String,String> result = CVFCustomSettings.getMapOfEquipmentValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size());        
    }

}