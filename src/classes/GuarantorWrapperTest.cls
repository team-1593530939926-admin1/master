@isTest
private class GuarantorWrapperTest {
    
    private static final Id OPPORTUNITY_CVF_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');
    
    @testSetup
    private static void testSetupData()
    {
        TestUtil.insertBusinessApplicationWrapper();
    }
    
    public static Opportunity getDeal()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity LIMIT 1];
    }
    
    private static GuarantorWrapper getGuarantorWrapper() {
        GuarantorWrapper GuarantorWrappertest = new GuarantorWrapper();
        
        GuarantorWrappertest.homePhoneGuarantor = '12345678';
        GuarantorWrappertest.firstNameGuarantor = 'Firstname';
        GuarantorWrappertest.middleNameGuarantor = 'Middlename';
        GuarantorWrappertest.lastNameGuarantor = 'Lastname';
        GuarantorWrappertest.socialSecurity = '385-15-2084';
        GuarantorWrappertest.contigent = '100';
        GuarantorWrappertest.dOB = String.valueOf(System.today());
        GuarantorWrappertest.mailingStreetGurantor = 'sector72';
        GuarantorWrappertest.mailingCityGuarantor = 'Noida';
        GuarantorWrappertest.mailingStateGuarantor = 'NY';
        GuarantorWrappertest.mailingCountryGuarantor = 'India';
        GuarantorWrappertest.mailingPostalCodeGuarantor = '201307';
        GuarantorWrappertest.faxGuarantor = '100';
        return GuarantorWrappertest;
    }
    
    @isTest
    private static void testDefaultGuarantorWrapperConstructor()
    {
        
        Test.startTest();
            GuarantorWrapper result = new GuarantorWrapper();
        Test.stopTest();
        
        System.assertEquals(null, result.firstNameGuarantor);
    }
    
    @isTest
    private static void testDefaultGuarantorWrapperConstructorIfListisInserted()
    {
        GuarantorWrapper GuarantorWrappertest = getGuarantorWrapper();
        
        Test.startTest();
            GuarantorWrapper result1 = new GuarantorWrapper(GuarantorWrappertest , true);
            GuarantorWrapper result2 = new GuarantorWrapper(GuarantorWrappertest , 'EXT1234', true, '1');
            GuarantorWrapper result3 = new GuarantorWrapper(GuarantorWrappertest , 'EXT1234', false, '1');
        Test.stopTest();
     
        System.assertEquals(GuarantorWrappertest.firstNameGuarantor, result1.firstNameGuarantor);
    }    
    
    @isTest
    private static void guarantorWrapperConstructorWithParameterContactAndRelationshipObjectTest() {
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'testFirstName';
        contactRecord.LastName = 'testLastName';
        contactRecord.Email = 'test@test.com';
        contactRecord.MailingCity = 'testCity';
        contactRecord.MailingCountry = 'USA';
        contactRecord.MailingState = 'NY';
        contactRecord.MailingStreet = 'testMailStreet';
        contactRecord.MailingPostalCode = '111'; 
        
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Accnt';
        accountRecord.billingCity='Tst';
        accountRecord.BillingCountry='tst';
        accountRecord.BillingState='NY';
        accountRecord.BillingStreet='tst';
        accountRecord.BillingPostalCode='11';
        
        Opportunity opportunityRecord = new Opportunity(Name='Test',Company_Name__c = 'testSupplier',
                                           StageName='Pre Qual',LeaseInfo_EquipmentDescription__c='Test',
                                           closeDate=Date.newInstance(2020,11,20), RecordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID,
                                           AccountId=accountRecord.Id);
        
        Relationship__c relationshipInstance = new Relationship__c();
        relationshipInstance.Business__c = accountRecord.Id;
        relationshipInstance.Person__c = contactRecord.Id;
        relationshipInstance.Related_Opportunity__c = opportunityRecord.Id; 
        relationshipInstance.Percent_of_Ownership__c = 50;
        relationshipInstance.Relationship_Type__c = 'Principal';
        
        Test.startTest();
            GuarantorWrapper result1 = new GuarantorWrapper(contactRecord , relationshipInstance);
            GuarantorWrapper result2 = new GuarantorWrapper(accountRecord , relationshipInstance);
        Test.stopTest();
        
        System.assertEquals(accountRecord.billingCity, result2.mailingCityGuarantor);
        System.assertEquals(contactRecord.LastName, result1.lastNameGuarantor);
        System.assertEquals(String.valueOf(relationshipInstance.Percent_of_Ownership__c), result1.contigent);
    }
}