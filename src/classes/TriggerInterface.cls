public interface TriggerInterface{    
    void beforeInsert(List<sObject> newObjList);
    void afterInsert(List<sObject> newObjList);
    void beforeUpdate(List<sObject> newObjList, Map<Id, sObject> oldObjMap);
    void afterUpdate(List<sObject> newObjList,  Map<Id, sObject> oldObjMap);
    void beforeDelete(Map<Id, sObject> oldObjMap);
    void afterDelete(Map<Id, sObject> oldObjMap);
    void afterUnDelete(List<sObject> newObjList);
}