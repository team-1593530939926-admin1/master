@isTest 
private class AccountTriggerHandlerTest {
    private static AccountTriggerHandler AccountTriggerHandlerInstance = new AccountTriggerHandler();
    private static List<sObject> listOfNewSObject = new List<sObject>();
    private static Map<Id, sObject> mapOfOldObject = new Map<Id, sObject>();

    @TestSetup
    static void testSetup() {
        TestUtil.createAccountData();
    }

    private static List<Account> getAccountRecord()
    {
        return [Select Id, Name, RecordTypeId, TamarackPI__Paynet_Id__c, billingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode From Account Limit 1];
    }

    // This method is used to test AccountTriggerHandler Apex Class afterInsert and afterUpdate methods.

    @isTest
    private static void testAfterInsertAndAfterUpdate()
    {
        List<User> listOfValidUser1 = [select Id ,UserRoleId from user];

        Test.startTest();
            TestUtil.CreatePartnerUserAccount();
        Test.stopTest();
        
        List<User> listOfValidUser2 = [select Id ,UserRoleId from user];
        
		System.assertEquals(listOfValidUser1.size()+1, listOfValidUser2.size());
    }
    
    @isTest 
    private static void beforeInsertTest() {

        Test.startTest();
            accountTriggerHandlerInstance.beforeInsert(listOfNewSObject);
        Test.stopTest();
    }

    @isTest
    private static void afterInsertTest() {
        Account accountInstance = getAccountRecord().get(0);

        Test.startTest();
            accountTriggerHandlerInstance.afterInsert(new List<Account>{accountInstance});
        Test.stopTest();
    }

    @isTest
    private static void beforeUpdateTest() {

        Test.startTest();
            accountTriggerHandlerInstance.beforeUpdate(listOfNewSObject, mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUpdateTest() {
        Account accountInstance = getAccountRecord().get(0);
        List<Account> listOfAccount = new List<Account>{AccountInstance};
        Map<Id,Account> mapOfAccountVsId = new Map<Id,Account>(listOfAccount);

        Test.startTest();
            accountTriggerHandlerInstance.afterUpdate(listOfAccount, mapOfAccountVsId);
        Test.stopTest();
    }

    @isTest
    private static void beforeDeleteTest() {

        Test.startTest();
            accountTriggerHandlerInstance.beforeDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterDeleteTest() {

        Test.startTest();
            accountTriggerHandlerInstance.afterDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUnDeleteTest() {

        Test.startTest();
            accountTriggerHandlerInstance.afterUnDelete(listOfNewSObject);
        Test.stopTest();
    }
}