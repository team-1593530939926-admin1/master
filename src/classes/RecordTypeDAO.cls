public with sharing class RecordTypeDAO implements IRecordTypeDAO
{
    public interface IRecordTypeDAO
    {
        Id recordTypeIdByObjectNameAndRecordTypeName(string objAPIName, string recTypeName);
    }
    
    public static Id recordTypeIdByObjectNameAndRecordTypeName(string objAPIName, string recTypeName)
    {
        List<RecordType> listOfRecordType = [SELECT Id FROM RecordType WHERE sObjectType =: objAPIName AND Name =: recTypeName];
        if(!listOfRecordType.isEmpty())
            return listOfRecordType[0].Id;
        else
            return null;
    }    
}