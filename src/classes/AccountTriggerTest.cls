@isTest
private class AccountTriggerTest {

    private static List<Account> getAccount()
    {
        return [Select Id, Name, billingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode From Account Limit 1];
    }
    @isTest
    private static void testAccountTriggerBeforeInsert(){
        Test.startTest();
            TestUtil.createAccountData();
        Test.stopTest();
        
        System.assertEquals(1, getAccount().size(), 'Account Not Inserted.');
    }
    @isTest
    private static void testAccountTriggerBeforeUpdate(){
        TestUtil.createAccountData();
        Account updateRecord = getAccount()[0];
        updateRecord.Name = 'TestName';
        Test.startTest();
            update updateRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getAccount()[0].Name, 'Account Not Updated.');
    }
    @isTest
    private static void testAccountTriggerBeforeDelete(){
        TestUtil.createAccountData();
        Account deleteRecord = getAccount()[0];
        deleteRecord.Name = 'TestName';
        Test.startTest();
            delete deleteRecord;
        Test.stopTest();
        
        System.assertEquals(0, getAccount().size(), 'Account Not Deleted.');
    }
    @isTest
    private static void testAccountTriggerBeforeUnDelete(){
        TestUtil.createAccountData();        
        Account undeleteRecord = getAccount()[0];
		undeleteRecord.Name = 'TestName';
        delete undeleteRecord;
        undelete undeleteRecord;
        Test.startTest();
        	update undeleteRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getAccount()[0].Name, 'Account Not Deleted.');
    }
}