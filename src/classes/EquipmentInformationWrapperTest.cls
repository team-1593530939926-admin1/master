@isTest
private class EquipmentInformationWrapperTest {
    
    @testSetup
    private static void testSetupData()
    {
        TestUtil.insertBusinessApplicationWrapper(); 
    }
    
    public static Opportunity getDeal()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity LIMIT 1]; 
    }
    
     public static Account getCompany()
    {
        return [SELECT Id, Name, External_System_Id__c FROM Account LIMIT 1];
    }
    
    @isTest
    private static void testEquipmentInformationWrapperIfDataExist()
    {
        List<Opportunity> opportunityRecord = OpportunityDAO.getOpportunityDetailsByApplicationId(getDeal().Application_Id__c);
        Test.startTest();
            EquipmentInformationWrapper result = new EquipmentInformationWrapper(opportunityRecord.get(0));
        Test.stopTest();
        System.assertEquals('testSupplier', result.supplierName);
    }
    
    @isTest
    private static void testEquipmentInformationWrapperIfDataNotExist()
    {
        Opportunity opportunityRecord = new Opportunity();
        Test.startTest();
            EquipmentInformationWrapper result = new EquipmentInformationWrapper(opportunityRecord);
        Test.stopTest();
        System.assertEquals(null, result.supplierName);
    }
    
    @isTest
    private static void testDefaultEquipmentInformationWrapperConstructor()
    {
        Test.startTest();
            EquipmentInformationWrapper result = new EquipmentInformationWrapper();
        Test.stopTest();
        System.assertEquals(null, result.supplierName);
    }
    @isTest
    private static void testEquipmentInformationWrapperIfWrapperAndBooleanAccepted()
    {
        List<Opportunity> opportunityRecord = OpportunityDAO.getOpportunityDetailsByApplicationId(getDeal().Application_Id__c);
        EquipmentInformationWrapper equipmentInformation = new EquipmentInformationWrapper(opportunityRecord.get(0));
        Test.startTest();
            EquipmentInformationWrapper result = new EquipmentInformationWrapper(equipmentInformation , true);
        Test.stopTest();
        System.assertEquals('testSupplier', result.supplierName);       
    }
    
    @isTest
    private static void testEquipmentInformationWrapperIfWrapperAndAccountAccepted()
    {
        List<Opportunity> opportunityRecord = OpportunityDAO.getOpportunityDetailsByApplicationId(getDeal().Application_Id__c);
        EquipmentInformationWrapper equipmentInformation = new EquipmentInformationWrapper(opportunityRecord.get(0));
        Account companyInfo = getCompany();
        Test.startTest();
            EquipmentInformationWrapper result = new EquipmentInformationWrapper(equipmentInformation , companyInfo);
        Test.stopTest();
        System.assertEquals('testSupplier', result.dealInformation.Company_Name__c);       
    }
}