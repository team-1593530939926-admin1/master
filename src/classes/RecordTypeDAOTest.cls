@isTest
public class RecordTypeDAOTest
{
    @isTest
    private static void testRecordTypeIdByObjectNameAndRecordTypeNameIfRecordTypeExists()
    {
        Test.startTest();
            Id recordTypeId = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');
        Test.stopTest();
        
        System.assertEquals(false, String.isEmpty(recordTypeId), 'RecordType does not exist');       
     }
    
    @isTest
    private static void testRecordTypeIdByObjectNameAndRecordTypeNameIfRecordTypeNotExists()
    {
        Test.startTest();
            Id recordTypeId = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'Test');
        Test.stopTest();
        
        System.assertEquals(true, String.isEmpty(recordTypeId), 'RecordType exist');   
     }
}