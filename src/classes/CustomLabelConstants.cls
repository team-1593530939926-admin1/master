public with sharing class CustomLabelConstants
{
    public static final String DEAL_COMPANY_CODE = System.Label.Text_Company_Code;
    public static final String DEAL_APLLICATION_ID = System.Label.Text_Application_Id;
    public static final String DEAL_COMPANY_NAME = System.Label.Text_Company_Name;
    public static final String DEAL_EQUIPMENT_INFORMATION = System.Label.Text_Equipment_Description;
    public static final String DEAL_SUBMITTED_DATE = System.Label.Text_Submitted_Date;
    public static final String DEAL_STAGE = System.Label.Text_Stage;
    public static final String DEAL_VIEW = System.Label.Text_View;
    public static final String DEAL_DESC = System.Label.Text_DESC;
    public static final String DEAL_20 = System.Label.Text_20;
    public static final String DEAL_0 = System.Label.Text_0;
    public static final String DEAL_50 = System.Label.Text_50;
    public static final String DEAL_Search_by_Company_Code = System.Label.Text_Search_by_Company_Code;
    public static final String DEAL_Search_by_Application_ID = System.Label.Text_Search_by_Application_ID;    
}