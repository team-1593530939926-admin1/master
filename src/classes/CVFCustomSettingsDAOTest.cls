@isTest
private class CVFCustomSettingsDAOTest {
   
    @testSetup
    private static void testSetupData()
    {
        TestUtil.createCVFCustomSettingsCountryAndStateData();
        TestUtil.createCVFCustomSettingsFileExtension();
        TestUtil.createCVFCustomSettingsEquipmentDescription();
    }
    
    @isTest
    private static void getDescriptionOptionValues() {
        Test.startTest();
            List<CVF_Equipment_Description__c> result = CVFCustomSettingsDAO.getDescriptionOptionValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size()); 
    }
    
    @isTest
    private static void getFilExtensionValues() {
        Test.startTest();
            List<CVF_File_Extensions__c> result = CVFCustomSettingsDAO.getFilExtensionValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size()); 
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesExist()
    {        
        Test.startTest();
            List<Country_State_Mapping__c> result = CVFCustomSettingsDAO.getCountryAndStateValues();
        Test.stopTest();
        
        System.assertEquals(1, result.size());       
    }
    
    @isTest
    private static void testGetCountryAndStateValuesIfValuesNotExist()
    {              
        List<Country_State_Mapping__c> listOfOptions = [SELECT Name, State_Code__c FROM Country_State_Mapping__c];
        delete listOfOptions;
        
        Test.startTest();
            List<Country_State_Mapping__c> result = CVFCustomSettingsDAO.getCountryAndStateValues();
        Test.stopTest();
        
        System.assertEquals(0, result.size());          
    }
    
}