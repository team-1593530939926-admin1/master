public with sharing class AccountDAO implements IAccountDAO {
    public Interface IAccountDAO {
        List<Account> getListOfAccounts(Set<Id> setOfAccountIds);
        Account getAccountRecordFromCompanyCode(String companyCode);
        List<Account> insertAccounts( List<Account> listOfAccounts);
        List<Account> getAccountRecordFromApplicationId(String companyCode);
        List<sObject> search(String searchTerm, string myObject, String filter, String recordTypeName);
        List<Account> getListOfCorporateGuarantors(Set<Id> setOfAccountId);
        Account getAccountRecordByCompanyId(String companyId);
        Account getExistingCopmanyDetails(Account accountRecord);
        //  List<Account> getTradeAccountRecordFromCompanyId(String companyId);
        //  Account getAccountRecordFromCompanyId(String companyId);
        //  Account getCompanyRecordFromCompanyId(String companyId);
        
    }
    
    public static List<Account> getListOfAccounts(Set<Id> setOfAccountIds)
    {
        return [Select Id, Name, Date_Of_Incorporation__c, CreatedById, Approval_Referral__c,Role__c,IsPartner,RecordType.Name From Account Where Id In : setOfAccountIds];
    }
    
    
    
    public static List<Account> insertAccounts(List<Account> listOfAccounts)
    {
        insert listOfAccounts;
        return listOfAccounts;
    }
    
    public static List<Account> upsertAccounts(List<Account> listOfAccounts)
    {
        upsert listOfAccounts;
        return listOfAccounts;
    }    
    
    public static Account getAccountRecordFromCompanyCode(String companyCode)
    {
        if(companyCode!=''){system.debug('h');}
        List<Account> accList = [Select Id, Company_Code__c, Name,CreatedById, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
                                 Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,Date_of_Incorporation__c,
                                 (Select Id, FirstName, LastName,CreatedById, Email From Contacts),
                                 (SELECT Id, FirstName,CreatedById, MiddleName, LastName, MailingStreet, MailingCountry, MailingCity, MailingState, MailingPostalCode,HomePhone, SSN__c, Birthdate, Ownership__c FROM Partners__r) ,
                                 (SELECT Id, Bank_Name__c,CreatedById, Address_Line_1__c, City__c, State__c, Zip__c, Bank_Contact_Person__c, Phone_Number__c, Account_Number__c, Country__c FROM Bank_Details__r),
                                 (Select Id, Street_Address_1__c,CreatedById, City__c, State__c, County__c,Postal_Code__c From Addresses__r ),
                                 (SELECT Id,CreatedById, Date_Of_Incorporation__c,Name From Entity_Informations__r WHERE CreatedDate = LAST_N_DAYS:30) 
                                 From Account Where Company_Code__c = :companyCode OR Id =: companyCode]; 
        if(!accList.isEmpty()){
            return accList[0];
        }else{
            return null;
        }
        
    }
    
    /*   public static Account getCompanyRecordFromCompanyId(String companyId)
{
if(companyId!=''){system.debug('companyId h');}
List<Account> accList = [Select Id, Company_Code__c, Name, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,
(Select Id, FirstName, LastName, Email From Contacts),
(SELECT Id, FirstName, MiddleName, LastName, MailingStreet, MailingCountry, MailingCity, MailingState, MailingPostalCode,HomePhone, SSN__c, Birthdate, Ownership__c FROM Partners__r) ,
(SELECT Id, Bank_Name__c, Address_Line_1__c, City__c, State__c, Zip__c, Bank_Contact_Person__c, Phone_Number__c, Account_Number__c, Country__c FROM Bank_Details__r),
(Select Id, Street_Address_1__c, City__c, State__c, County__c,Postal_Code__c From Addresses__r )
From Account Where Id = :companyId]; 
if(!accList.isEmpty()){
return accList[0];
}else{
return null;
}

}    */
    
    /*  public static Account getAccountRecordFromCompanyId(String companyId)
{
if(companyId!=''){system.debug('comapny id h');}
List<Account> accList = [Select Id, Company_Code__c, Name, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,
(Select Id, FirstName, LastName, Email From Contacts),
(SELECT Id, FirstName, MiddleName, LastName, MailingStreet, MailingCountry, MailingCity, MailingState, MailingPostalCode,HomePhone, SSN__c, Birthdate, Ownership__c FROM Partners__r) ,
(SELECT Id, Bank_Name__c, Address_Line_1__c, City__c, State__c, Zip__c, Bank_Contact_Person__c, Phone_Number__c, Account_Number__c, Country__c FROM Bank_Details__r),
(Select Id, Street_Address_1__c, City__c, State__c, County__c,Postal_Code__c From Addresses__r )
From Account Where Id = :companyId]; 
if(!accList.isEmpty()){
return accList[0];
}else{
return null;
}

} */
    
    /*  public static List<Account> getTradeAccountRecordFromCompanyId(String companyId)
{
return [Select Id, Company_Code__c, Name, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,
(Select Id, FirstName, LastName, Email From Contacts),
(Select Id, Street_Address_1__c, City__c, State__c, County__c,Postal_Code__c From Addresses__r )
From Account Where Trade_Reference__r.Id = :companyId];         
}    */
    
    public static Account getAccountRecordByCompanyId(String companyId)
    {
        if(companyId!=''){system.debug('h');}
        List<Account> accList = [Select Id, CORE_Account__c, CEF_Account__c, Company_Code__c,CreatedById, Name, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
                                 Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,Date_of_Incorporation__c,
                                 (Select Id,CreatedById, FirstName, LastName, Email From Contacts),
                                 (Select Id,CreatedById, Street_Address_1__c, City__c, State__c, County__c,Postal_Code__c From Addresses__r )
                                 From Account Where Id = :companyId]; 
        if(!accList.isEmpty()){
            return accList[0];
        }else{
            return null;
        }
        
    }
    
    public static List<Account> getAccountRecordFromApplicationId(String companyCode)
    {
        return [Select Id, Company_Code__c,CreatedById, Name, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
                Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,Date_of_Incorporation__c,
                (Select Id, FirstName,CreatedById, LastName, Email From Contacts),
                (Select Id, Street_Address_1__c,CreatedById, City__c, State__c, County__c,Postal_Code__c From Addresses__r )
                From Account Where Trade_Reference__r.Company_Code__c = :companyCode OR Trade_Reference__r.Id = :companyCode];         
    }
    
    @AuraEnabled(cacheable=true)
    public static List<sObject> search(String searchTerm, string myObject, String filter, String recordTypeName)
    {
        String myQuery = null;
        if(filter != null && filter != ''){
            myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\' AND '+filter+'And RecordType.Name= :recordTypeName'+' LIMIT  5';
        }
        else {
            if(searchTerm == null || searchTerm == ''){
                myQuery = 'Select Id, Name from '+myObject+' Where LastViewedDate != NULL And RecordType.Name= :recordTypeName ORDER BY LastViewedDate DESC LIMIT  5';
            }
            else {
                myQuery = 'Select Id, Name from '+myObject+' Where Name Like  \'%' + searchTerm + '%\''+' And RecordType.Name= :recordTypeName LIMIT 5';
            }
        }
        List<sObject> lookUpList = database.query(myQuery);
        return lookUpList;
    }
    
    public static List<Account> getListOfCorporateGuarantors(Set<Id> setOfAccountId)
    {
        return [Select Id,Fax,Name,Federal_Tax_ID__c,CreatedById,Industry,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,Phone,RecordType.DeveloperName,RecordTypeId,(Select Id,Fax,FirstName,MiddleName,LastName,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Phone,SSN__c,Nature_of_Business__c,Email,Primary_Guarantor__c,Guarantor_Type__c,Birthdate From Contacts) From Account Where Id In : setOfAccountId];
    }
    
    public static Account getGuarantorRecordFromGuarantorId(String companyCode)
    {
        if(companyCode!=''){system.debug('h');}
        List<Account> accList = [Select Id, Company_Code__c,CreatedById, Name, RecordTypeId, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode, Phone, Fax,
                                 Federal_Tax_ID__c, Industry, DBA__c, Entity_Type__c, State_Incorporated__c,
                                 (Select Id, FirstName,CreatedById, MiddleName, LastName, RecordTypeId, Email From Contacts)
                                 From Account Where Company_Code__c = :companyCode OR Id =: companyCode]; 
        if(!accList.isEmpty()){
            return accList[0];
        }else{
            return null;
        }
        
    }
    
    public static Account getExistingCopmanyDetails(Account accountRecord)
    {
        list<FCS_Entity_Information__c> listOfEntityInformations = new list<FCS_Entity_Information__c>();
        if(accountRecord == null){
            return null;
        }
        List<Account> accList = [SELECT Id,(SELECT Id, CreatedById, Name from Entity_Informations__r WHERE CreatedDate = LAST_N_DAYS:30)
                                 FROM Account 
                                 WHERE Name =: accountRecord.Name 
                                 AND BillingStreet =: accountRecord.BillingStreet 
                                 AND BillingCity =: accountRecord.BillingCity 
                                 AND BillingState =: accountRecord.BillingState
                                 AND BillingCountry =: accountRecord.BillingCountry 
                                 AND BillingPostalCode =: accountRecord.BillingPostalCode];
       
        
        if(!accList.isEmpty()){
            return accList[0];
        }else{ 
            return null;
        }
        
    }
}