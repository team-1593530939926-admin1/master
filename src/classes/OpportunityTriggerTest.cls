@isTest
private class OpportunityTriggerTest {
    
    private static List<Opportunity> getOpportunity()
    {
        return [Select Id, Name, StageName, LeaseInfo_EquipmentDescription__c From Opportunity Limit 1];
    }
    @isTest
    private static void testOpportunityTriggerBeforeInsert(){
        Test.startTest();
            TestUtil.createOpportunityData();
        Test.stopTest();
        
        System.assertEquals(1, getOpportunity().size(), 'Opportunity Not Inserted.');
    }
    @isTest
    private static void testOpportunityTriggerBeforeUpdate(){
        TestUtil.createOpportunityData();
        Opportunity updateRecord = getOpportunity()[0];
        updateRecord.Name = 'TestName';
        Test.startTest();
            update updateRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getOpportunity()[0].Name, 'Opportunity Not Updated.');
    }
    @isTest
    private static void testOpportunityTriggerBeforeDelete(){
        TestUtil.createOpportunityData();
        Opportunity deleteRecord = getOpportunity()[0];
        deleteRecord.Name = 'TestName';
        Test.startTest();
            delete deleteRecord;
        Test.stopTest();
        
        System.assertEquals(0, getOpportunity().size(), 'Opportunity Not Deleted.');
    }
    @isTest
    private static void testOpportunityTriggerBeforeUnDelete(){
        TestUtil.createOpportunityData();        
        Opportunity undeleteRecord = getOpportunity()[0];
		undeleteRecord.Name = 'TestName';
        delete undeleteRecord;
        undelete undeleteRecord;
        Test.startTest();
        	update undeleteRecord;
        Test.stopTest();
        
        System.assertEquals('TestName', getOpportunity()[0].Name, 'Opportunity Not Deleted.');
    }
}