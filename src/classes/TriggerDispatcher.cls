public without sharing class TriggerDispatcher {
    
    // static map of handlername, times run() was invoked
    private static Map<String, LoopCount> loopCountMap;
    private static Set<String> bypassedHandlers;
    private static String objectHandlerName;
    
    // the current context of the trigger, overridable in tests
    @TestVisible
    private TriggerContext context;
    
    @TestVisible
    private static TriggerInterface getHandlerInstance() {
        return (TriggerInterface)Type.forName(getHandlerName()).newInstance();
    }
    
    // the current context of the trigger, overridable in tests
    @TestVisible
    private Boolean isTriggerExecuting;
    
    // static initialization
    static {
        loopCountMap = new Map<String, LoopCount>();
        bypassedHandlers = new Set<String>();
    }
    
    // constructor
    public TriggerDispatcher() {
        this.setTriggerContext();
    }
    
    /***************************************
* public instance methods
***************************************/
    
    // main method that will be called during execution
    public void run(String handlerName) {
        objectHandlerName = handlerName;
        if(!validateRun()) {
            return;
        }
        if(validateDisable()){
            return;
        }
        addToLoopCount();
        // dispatch to the correct handler method
        switch on this.context {
            when BEFORE_INSERT {
                beforeInsert(Trigger.new);
            }
            when BEFORE_UPDATE {
                beforeUpdate(Trigger.new, Trigger.oldMap);
            }
            when BEFORE_DELETE {
                beforeDelete(Trigger.oldMap);
            }
            when AFTER_INSERT {
                afterInsert(Trigger.new);
            }
            when AFTER_UPDATE {
                afterUpdate(Trigger.new, Trigger.oldMap);
            }
            when AFTER_DELETE {
                afterDelete(Trigger.oldMap);
            }
            when AFTER_UNDELETE {
                afterUndelete(Trigger.new);
            }
        }
    }
    
    public void setMaxLoopCount(Integer max) {
        String handlerName = getHandlerName();
        if(!TriggerDispatcher.loopCountMap.containsKey(handlerName)) {
            TriggerDispatcher.loopCountMap.put(handlerName, new LoopCount(max));
        } else {
            TriggerDispatcher.loopCountMap.get(handlerName).setMax(max);
        }
    }
    
    public void clearMaxLoopCount() {
        this.setMaxLoopCount(-1);
    }
    
    /***************************************
* public static methods
***************************************/
    
    public static void bypass(String handlerName) {
        TriggerDispatcher.bypassedHandlers.add(handlerName);
    }
    
    public static void clearBypass(String handlerName) {
        TriggerDispatcher.bypassedHandlers.remove(handlerName);
    }
    
    public static Boolean isBypassed(String handlerName) {
        return TriggerDispatcher.bypassedHandlers.contains(handlerName);
    }
    
    public static void clearAllBypasses() {
        TriggerDispatcher.bypassedHandlers.clear();
    }
    
    /***************************************
* private instancemethods
***************************************/
    
    @TestVisible
    private void setTriggerContext() {
        this.setTriggerContext(null, false);
    }
    
    @TestVisible
    private void setTriggerContext(String ctx, Boolean testMode) {
        if(!Trigger.isExecuting && !testMode) {
            this.isTriggerExecuting = false;
   
            return;
        } else {
            this.isTriggerExecuting = true;
         
        }
        
        if((Trigger.isExecuting && Trigger.isBefore && Trigger.isInsert) ||
           (ctx != null && ctx == 'before insert')) {
               this.context = TriggerContext.BEFORE_INSERT;
           } else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isUpdate) ||
                     (ctx != null && ctx == 'before update')){
                         this.context = TriggerContext.BEFORE_UPDATE;
                     } else if((Trigger.isExecuting && Trigger.isBefore && Trigger.isDelete) ||
                               (ctx != null && ctx == 'before delete')) {
                                   this.context = TriggerContext.BEFORE_DELETE;
                               } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isInsert) ||
                                         (ctx != null && ctx == 'after insert')) {
                                             this.context = TriggerContext.AFTER_INSERT;
                                         } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUpdate) ||
                                                   (ctx != null && ctx == 'after update')) {
                                                       this.context = TriggerContext.AFTER_UPDATE;
                                                   } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isDelete) ||
                                                             (ctx != null && ctx == 'after delete')) {
                                                                 this.context = TriggerContext.AFTER_DELETE;
                                                             } else if((Trigger.isExecuting && Trigger.isAfter && Trigger.isUndelete) ||
                                                                       (ctx != null && ctx == 'after undelete')) {
                                                                           this.context = TriggerContext.AFTER_UNDELETE;
                                                                       }
    }
    
    // increment the loop count
    @TestVisible
    private void addToLoopCount() {
        String handlerName = getHandlerName();
        if(TriggerDispatcher.loopCountMap.containsKey(handlerName)) {
            Boolean exceeded = TriggerDispatcher.loopCountMap.get(handlerName).increment();
            if(exceeded) {
                Integer max = TriggerDispatcher.loopCountMap.get(handlerName).max;
                throw new TriggerHandlerException('Maximum loop count of ' + String.valueOf(max) + ' reached in ' + handlerName);
            }
        }
    }
    
    // make sure this trigger should continue to run
    @TestVisible
    private Boolean validateRun() {
        if(!this.isTriggerExecuting || this.context == null) {
            throw new TriggerHandlerException('Trigger handler called outside of Trigger execution');
        }
        return !TriggerDispatcher.bypassedHandlers.contains(getHandlerName());
    }
    
    @TestVisible
    private Boolean validateDisable() {
        Trigger_Setting__c triggerSetting = Trigger_Setting__c.getInstance();
        return triggerSetting.Disabled__c;
    }
    
    @TestVisible
    private static String getHandlerName() {
        //return String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
        return objectHandlerName;
    }
    
    /***************************************
* context methods
***************************************/
    
    // context-specific methods for override
    @TestVisible
    private static void beforeInsert(List<sObject> newObjList){
        getHandlerInstance().beforeInsert(newObjList);
    }
    @TestVisible
    private static void beforeUpdate(List<sObject> newObjList, Map<Id,sObject> oldObjMap){
        getHandlerInstance().beforeUpdate(newObjList, oldObjMap);
    }
    @TestVisible
    private static void beforeDelete(Map<Id,sObject> oldObjMap){
        getHandlerInstance().beforeDelete(oldObjMap);
    }
    @TestVisible
    private static void afterInsert(List<sObject> newObjList){
        getHandlerInstance().afterInsert(newObjList);
    }
    @TestVisible
    private static void afterUpdate(List<sObject> newObjList, Map<Id,sObject> oldObjMap){
        getHandlerInstance().afterUpdate(newObjList, oldObjMap);
    }
    @TestVisible
    private static void afterDelete(Map<Id,sObject> oldObjMap){
        getHandlerInstance().afterDelete(oldObjMap);
    }
    @TestVisible
    private static void afterUndelete(List<sObject> newObjList){
        getHandlerInstance().afterUndelete(newObjList);
    }
    
    /***************************************
* inner classes
***************************************/
    
    // inner class for managing the loop count per handler
    @TestVisible
    private class LoopCount {
        private Integer max;
        public Integer count;
        
        public LoopCount() {
            this.max = 5;
            this.count = 0;
        }
        
        public LoopCount(Integer max) {
            this.max = max;
            this.count = 0;
        }
        
        public Boolean increment() {
            this.count++;
            return this.exceeded();
        }
        
        public Boolean exceeded() {
            return this.max >= 0 && this.count > this.max;
        }
        
        public Integer getMax() {
            return this.max;
        }
        
        public Integer getCount() {
            return this.count;
        }
        
        public void setMax(Integer max) {
            this.max = max;
        }
    }
    
    // possible trigger contexts
    @TestVisible
    private enum TriggerContext {
        BEFORE_INSERT, BEFORE_UPDATE, BEFORE_DELETE,
            AFTER_INSERT, AFTER_UPDATE, AFTER_DELETE,
            AFTER_UNDELETE
            }
    
    // exception class
    public class TriggerHandlerException extends Exception {}
    
}