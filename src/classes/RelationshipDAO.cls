public with sharing class RelationshipDAO implements IRelationshipDAO
{    
    public interface IRelationshipDAO 
    { 
        List<Relationship__c> getListOfRelationship(String dealApplicationId);
        List<Relationship__c> insertRelationships(List<Relationship__c> listOfRelationships);
    }
    
    public static List<Relationship__c> getListOfRelationship(String dealApplicationId)
    {
        return [Select Id, Business__c, Person__c, Related_Opportunity__c, Percent_of_Ownership__c, Relationship_Type__c,RecordType.DeveloperName,RecordTypeId,Primary_Guarantor__c 
                From Relationship__c Where Related_Opportunity__r.Application_Id__c =: dealApplicationId];  
    }
    
    public static List<Relationship__c> insertRelationships(List<Relationship__c> listOfRelationships)
    {
        insert listOfRelationships;
        return listOfRelationships;
    }
}