@isTest
private class TriggerDispatcherTest {
    
    private static final String TRIGGER_CONTEXT_ERROR = 'Trigger handler called outside of Trigger execution';
    
    private static String lastMethodCalled;
    private static TriggerDispatcher handler;
    
    
    static {
        handler = new TriggerDispatcher();
        handler.isTriggerExecuting = true;
    }
    
    /***************************************
* unit tests
***************************************/
    
    @isTest
    static void testBeforeInsert() {
        
        Test.startTest();
        
        beforeInsertMode();
        handler.run('ContactTriggerHandler');
        TriggerDispatcherTest.lastMethodCalled = 'beforeInsert';
        
        Test.stopTest();
        
        System.assertEquals('beforeInsert', lastMethodCalled, 'last method should be beforeInsert');
    }
    
    @isTest
    static void testBeforeUpdate() {
        
        Test.startTest();
        
        beforeUpdateMode();
        handler.run('ContactTriggerHandler');
        TriggerDispatcherTest.lastMethodCalled = 'beforeUpdate';
        
        Test.stopTest();
        
        System.assertEquals('beforeUpdate', lastMethodCalled, 'last method should be beforeUpdate');
    }
    
    @isTest
    static void testBeforeDelete() {
        
        Test.startTest();
        
        beforeDeleteMode();
        handler.run('ContactTriggerHandler');
        TriggerDispatcherTest.lastMethodCalled = 'beforeDelete';
        
        Test.stopTest();
        
        System.assertEquals('beforeDelete', lastMethodCalled, 'last method should be beforeDelete');
    }
    
    @isTest
    static void testAfterInsert() {
        TestUtil.CreatePartnerUserAccount();
        List<contact> con = [select id,FirstName,LastName,MiddleName,Email,MobilePhone,MailingStreet,MailingCity,
                             MailingState,MailingPostalCode,MailingCountry,Relationship_Type__c,recordtypeId,Is_CVF_Broker__c, Primary_Contact__c, AccountId from contact limit 1];
        
        Test.startTest();
        
        TriggerDispatcher.afterInsert(con);
        
        Test.stopTest();
    }
    
    @isTest
    static void testAfterDelete() {
        
        Test.startTest();
        
        afterDeleteMode();
        handler.run('ContactTriggerHandler');
        TriggerDispatcherTest.lastMethodCalled = 'afterDelete';
        
        Test.stopTest();
        
        System.assertEquals('afterDelete', lastMethodCalled, 'last method should be afterDelete');
    }
    
    @isTest
    static void testAfterUndelete() {
        
        Test.startTest();
        
        afterUndeleteMode();
        handler.run('ContactTriggerHandler');
        TriggerDispatcherTest.lastMethodCalled = 'afterUndelete';
        
        Test.stopTest();
        
        System.assertEquals('afterUndelete', lastMethodCalled, 'last method should be afterUndelete');
    }
    
    @isTest 
    static void testNonTriggerContext() {
        
        try{
            handler.run('ContactTriggerHandler');
            
            System.assert(false, 'the handler ran but should have thrown');
        } 
        catch(TriggerDispatcher.TriggerHandlerException te) {
            
            System.assertEquals(TRIGGER_CONTEXT_ERROR, te.getMessage(), 'the exception message should match');
            
        } 
        catch(Exception e) 
        {
            System.assert(false, 'the exception thrown was not expected: ' + e.getTypeName() + ': ' + e.getMessage());
        }
    }
    
    
    
    // test bypass api
    
    @isTest
    static void testBypassAPI() {
        
        Boolean returnValue;
        TriggerDispatcher.LoopCount loopCountInstance;
        TriggerDispatcher.clearAllBypasses();
        new TriggerDispatcher().setMaxLoopCount(5);
        new TriggerDispatcher().clearMaxLoopCount();
        
        Test.startTest();
        
        TriggerDispatcher.bypass('TestHandler');
        TriggerDispatcher.clearBypass('TestHandler');
        
        Test.stopTest();
        
        System.assertEquals(false, TriggerDispatcher.isBypassed('TestHandler'), 'test handler should be bypassed');
        
    }
    @isTest
    static void testLoopCountClass() {
        
        Test.startTest();
        
        TriggerDispatcher.LoopCount lc = new TriggerDispatcher.LoopCount();
        

        
        System.assertEquals(5, lc.getMax(), 'max should be five on init');
        System.assertEquals(0, lc.getCount(), 'count should be zero on init');
        
        
        
        lc.increment();
        
        
        
        System.assertEquals(1, lc.getCount(), 'count should be 1');
        System.assertEquals(false, lc.exceeded(), 'should not be exceeded with count of 1');
        
        
        
        lc.increment();
        lc.increment();
        lc.increment();
        lc.increment();
        
       
        
        System.assertEquals(5, lc.getCount(), 'count should be 5');
        System.assertEquals(false, lc.exceeded(), 'should not be exceeded with count of 5');
        
        
        
        lc.increment();
        
        Test.stopTest();
        
        System.assertEquals(6, lc.getCount(), 'count should be 6');
        System.assertEquals(true, lc.exceeded(), 'should not be exceeded with count of 6');
    }
    
    private static void resetTest() {
        lastMethodCalled = null;
    }
    
    // These methods are used in other test class methods .They are different modes for testing.
    
    private static void beforeInsertMode() {
        handler.setTriggerContext('before insert', true);
    }
    
    private static void beforeUpdateMode() {
        handler.setTriggerContext('before update', true);
    }
    
    private static void beforeDeleteMode() {
        handler.setTriggerContext('before delete', true);
    }
    
    private static void afterInsertMode() {
        handler.setTriggerContext('after insert', true);
    }
    
    private static void afterUpdateMode() {
        handler.setTriggerContext('after update', true);
    }
    
    private static void afterDeleteMode() {
        handler.setTriggerContext('after delete', true);
    }
    
    private static void afterUndeleteMode() {
        handler.setTriggerContext('after undelete', true);
    }
    
}