@isTest(isParallel=true)
private class FCSUtilTest {
    
    @isTest
    private static void convertFCSDateInSFDateTest() {
        String inputDateString = 'Nov 20, 2001';

        Test.startTest();
            Date result = FCSUtil.convertFCSDateInSFDate(inputDateString);
        Test.stopTest();

        System.assertNotEquals(null, result, 'Date must be return.');
        System.assertEquals(20, result.day(), 'Day must be same as input string.');
        System.assertEquals(11, result.month(), 'Month must be same as input string.');
        System.assertEquals(2001, result.year(), 'Year must be same as input string.');
    }

    @isTest
    private static void convertFCSDateInSFDateWhenPassingDifferntPatternDateTest() {
        String inputDateString = 'November 20, 2001';

        Test.startTest();
            Date result = FCSUtil.convertFCSDateInSFDate(inputDateString);
        Test.stopTest();

        System.assertEquals(null, result, 'Method must return null instance as pattern not matched.'); 
    }

    @isTest
    private static void convertFCSDateInSFDateWhenPassingNullTest() {
        String inputDateString = null;

        Test.startTest();
            Date result = FCSUtil.convertFCSDateInSFDate(inputDateString);
        Test.stopTest();

        System.assertEquals(null, result, 'Method must return null instance as pattern not matched.'); 
    }

    @isTest
    private static void convertFCSDateInSFDateWhenPassingEmptyTest() {
        String inputDateString = '  ';

        Test.startTest();
            Date result = FCSUtil.convertFCSDateInSFDate(inputDateString);
        Test.stopTest();

        System.assertEquals(null, result, 'Method must return null instance as pattern not matched.'); 
    }

    @isTest
    private static void generateRandomKeyTest() {
        
        Test.startTest();
            String randomKey = FCSUtil.generateRandomKey('k1234');
        Test.stopTest();

        System.assertNotEquals(null, randomKey, 'Method must return some Random Key.');
    }
}