@isTest
private class BrokerCommentInformationControllerTest {
    
    private static final String TODAYS_DATE = Datetime.now().format('yyyy-MM-dd');
    
    @testSetup
    private static void testSetupData(){ 
        TestUtil.opportunityRelatedAccountData();
    }
        
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c, StageName, Score__c, Tier__c, Broker_Comment__c, AccountId FROM Opportunity];
    }

    @isTest
    private static void addingBrokerComment() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        BrokerCommentInformationController.BrokerComment brokerInput = new BrokerCommentInformationController.BrokerComment();
        brokerInput.applicationId = listOfOpportunity.get(0).Id;
        brokerInput.comment = 'BrokerComment1';

        Test.startTest();
            String status = BrokerCommentInformationController.addingBrokerComment(brokerInput);
        Test.stopTest();
        
        List<Opportunity> listOfOpportunity1 = getOpportunity();
        
        System.assertEquals('true', status, 'Value should not be different.');
    }

    //@isTest
    private static void addingBrokerCommentDmlException() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        BrokerCommentInformationController.BrokerComment brokerInput = new BrokerCommentInformationController.BrokerComment();
        brokerInput.applicationId = listOfOpportunity.get(0).AccountId;
        brokerInput.comment = '';

        try {

            Test.startTest();
                BrokerCommentInformationController.addingBrokerComment(brokerInput);
            Test.stopTest();

            System.assert(false, 'Should throw exception.');
        } catch (System.DmlException ex) {
            System.assertEquals('Script-thrown exception', ex.getMessage(), 'Should get Message.');
        }
    }

    @isTest
    private static void addingBrokerCommentException() {
        BrokerCommentInformationController.BrokerComment brokerInput = null;

        try {

            Test.startTest();
                BrokerCommentInformationController.addingBrokerComment(brokerInput);
            Test.stopTest();

            System.assert(false, 'Should throw exception.');
        } catch (System.Exception ex) {
            System.assertEquals('Script-thrown exception', ex.getMessage(), 'Should get Message.');
        }

    }

    @isTest
    private static void checkForBrokerComment() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        BrokerCommentInformationController.BrokerComment brokerInput = new BrokerCommentInformationController.BrokerComment();
        brokerInput.applicationId = listOfOpportunity.get(0).Id;
        
        Test.startTest();
            BrokerCommentInformationController.BrokerComment brokerDetails = BrokerCommentInformationController.checkForBrokerComment(brokerInput);
        Test.stopTest();
                
        System.assertEquals('Test', brokerDetails.dealName, 'Should not be different.');
    }

    @isTest
    private static void checkForBrokerCommentDmlException() {
        
    }

    @isTest
    private static void checkForBrokerCommentException() {
        BrokerCommentInformationController.BrokerComment brokerInput = null;
        
        try {

            Test.startTest();
                BrokerCommentInformationController.checkForBrokerComment(brokerInput);
            Test.stopTest();

            System.assert(false, 'Should throw exception.');
        } catch (System.Exception ex) {
            System.assertEquals('Script-thrown exception', ex.getMessage(), 'Should get Message.');
        }
    }
}