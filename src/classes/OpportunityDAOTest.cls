@istest 
private class OpportunityDAOTest
{    
    private static final String TODAYS_DATE = Datetime.now().format('yyyy-MM-dd');
     
    @testSetup
    private static void testSetupData(){ 
        TestUtil.opportunityRelatedAccountData();
    }
    
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c, StageName, Score__c, Tier__c, Broker_Comment__c FROM Opportunity];
    }
    
    @isTest
    private static void getOpportunityCreditConditions() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        
        Test.startTest();
            Opportunity opportunityInstance = OpportunityDAO.getOpportunityCreditConditions(listOfOpportunity.get(0).Id);
        Test.stopTest();
        
        System.assertEquals(listOfOpportunity.get(0).Id, opportunityInstance.Id, 'Id should be same.');
    }
    
    @isTest
    private static void getOpportunityByIdforAddingBrokerCommentTest() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        BrokerCommentInformationController.BrokerComment brokerInput = new BrokerCommentInformationController.BrokerComment();
        brokerInput.applicationId = listOfOpportunity.get(0).Id;
        brokerInput.comment = 'BrokerComment1';
        
        Test.startTest();
            OpportunityDAO.getOpportunityByIdforAddingBrokerComment(brokerInput);
        Test.stopTest();
        
        List<Opportunity> listOfOpportunity1 = getOpportunity();
        
        System.assertEquals('BrokerComment1', listOfOpportunity1.get(0).Broker_Comment__c, 'Value should not be different.');
        System.assertNotEquals('BrokerComment', listOfOpportunity1.get(0).Broker_Comment__c, 'Value should not be same.');
    }
    
    @isTest
    private static void getOpportunityByIdforCheckingBrokerComment() {
        List<Opportunity> listOfOpportunity = getOpportunity();
        BrokerCommentInformationController.BrokerComment brokerInput = new BrokerCommentInformationController.BrokerComment();
        brokerInput.applicationId = listOfOpportunity.get(0).Id;
        
        Test.startTest();
            Opportunity opportunityInstance = OpportunityDAO.getOpportunityByIdforCheckingBrokerComment(brokerInput);
        Test.stopTest();
                
        System.assertEquals(opportunityInstance.Id, listOfOpportunity.get(0).Id, 'Ids should not be different.');
    }
    
    /*@isTest
    private static void updateOpportunityRecordTest() {

        Test.startTest();
            OpportunityDAO.updateOpportunityRecord(getOpportunity());
        Test.stopTest();

        List<Opportunity> listOfOpportunity = getOpportunity();

        System.assertEquals('Pre Qual', listOfOpportunity.get(0).StageName, 'StageName should not be different.');
    }*/
    
    @isTest
    private static void updateOpportunityRecordTierEqualTo4Test() {
        Opportunity opportunityInstance = getOpportunity().get(0);
        opportunityInstance.Score__c = 672;
        
        Test.startTest();
            Opportunity opportunityInstance1 = OpportunityDAO.updateOpportunityRecord(opportunityInstance);
        Test.stopTest();

        System.assertEquals(opportunityInstance.Score__c, opportunityInstance1.Score__c, 'Value should not be different.');
    }

    @isTest
    private static void testGetListOfOpportunityByCurrentUserIfParametersAreValid()
    {
        
        List<String> LIST_OF_DEAL_FIELDS = new List<String>
        { 'Account.Company_Code__c', 'Application_Id__c', 'Account.Name', 'LeaseInfo_EquipmentDescription__c', 'CreatedDate', 'StageName' };
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getListOfOpportunityByCurrentUser('TestAccount',LIST_OF_DEAL_FIELDS,'2','0','Name','ASC',TODAYS_DATE, TODAYS_DATE);
        Test.stopTest();
        
        //System.assertEquals(1, listOfOpportunity.size(),'Opportunity list is empty.');          
    }   
    
    @isTest
    private static void testGetListOfOpportunityByCurrentUserIfParametersAreNotValid()
    {
        
        List<String> LIST_OF_DEAL_FIELDS = new List<String>
        { 'Account.Company_Code__c', 'Application_Id__c', 'Account.Name', 'LeaseInfo_EquipmentDescription__c', 'CreatedDate', 'StageName' };
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getListOfOpportunityByCurrentUser('bestCount',LIST_OF_DEAL_FIELDS,'2','0','Name','ASC', TODAYS_DATE, TODAYS_DATE);
        Test.stopTest();
        
        System.assertEquals(0, listOfOpportunity.size(),'Opportunity list is not empty.');          
    }   
    
    
    @isTest
    private static void testGetListOfOpportunityByStageNameIfParametersAreValid()
    {
        List<String> LIST_OF_DEAL_FIELDS = new List<String>
        { 'Account.Company_Code__c', 'Application_Id__c', 'Account.Name', 'LeaseInfo_EquipmentDescription__c', 'CreatedDate', 'StageName' };
        Set<String> SET_OF_DEAL_STAGES = new set<String>{'Closed Won','Closed Lost'};
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getListOfOpportunityByStageName(null,SET_OF_DEAL_STAGES,LIST_OF_DEAL_FIELDS,'2','0','Name','ASC', TODAYS_DATE, TODAYS_DATE);
        Test.stopTest();
        
        //System.assertEquals(1, listOfOpportunity.size(),'Opportunity list is empty.');          
    }  
    
    @isTest
    private static void testGetListOfOpportunityByStageNameIfParametersAreNotValid()
    {
        List<String> LIST_OF_DEAL_FIELDS = new List<String>
        { 'Account.Company_Code__c', 'Application_Id__c', 'Account.Name', 'LeaseInfo_EquipmentDescription__c', 'CreatedDate', 'StageName' };
        Set<String> SET_OF_DEAL_STAGES = new set<String>{'Pre Qual'};
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getListOfOpportunityByStageName(null,SET_OF_DEAL_STAGES,LIST_OF_DEAL_FIELDS,'2','0','Name','ASC', TODAYS_DATE, TODAYS_DATE);
        Test.stopTest();
        
        System.assertEquals(0, listOfOpportunity.size(),'Opportunity list is empty.');          
    }  
    
    @isTest
    private static void testInsertOpportunitiesIfListIsAccepted()
    {
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        Account objectOfAccount = new Account(Name='TestAccount2',billingCity='TestCity2',BillingCountry='testCountry',
                                  BillingState='CA',BillingStreet='testCountry',BillingPostalCode='222');//Company_Code__c='CMP000001'
        insert objectOfAccount;
        
        Opportunity objectOfOpportunity = new Opportunity(Name='TestOpp',
                                           StageName='Prospect',LeaseInfo_EquipmentDescription__c='TestDesc',
                                           closeDate=Date.newInstance(2020,11,20),
                                           AccountId=objectOfAccount.Id);
        opportunityList.add(objectOfOpportunity);
        
        Test.startTest();
        List<Opportunity> listOfOpportunity = OpportunityDAO.insertOpportunities(opportunityList);
        Test.stopTest();
        
        System.assertEquals(1, listOfOpportunity.size(),'Opportunity list empty.');        
    }
    
    @isTest
    private static void testInsertOpportunitiesIfListIsNotAccepted()
    {
        List<Opportunity> opportunityList = new List<Opportunity>();
        
        Test.startTest();
        List<Opportunity> listOfOpportunity = OpportunityDAO.insertOpportunities(opportunityList);
        Test.stopTest();
        
        System.assertEquals(0, listOfOpportunity.size(),'Opportunity list is not empty.');        
    }
    
    @isTest
    private static void testGetOpportunityDetailsByApplicationIdIfIdExists()
    {
        List<Opportunity> opportunityList = getOpportunity();
        Opportunity opportunityRecord = opportunityList[0];
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getOpportunityDetailsByApplicationId(opportunityRecord.Application_Id__c);
        Test.stopTest();
        System.assertEquals(1, listOfOpportunity.size(), 'Record does not exist.');        
    }
    
    @isTest
    private static void testGetOpportunityDetailsByApplicationIdIfIdNotExists()
    {        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getOpportunityDetailsByApplicationId('');
        Test.stopTest();
        System.assertEquals(0, listOfOpportunity.size(), 'Record do exist.');        
    }
    
    @isTest
    private static void testGetOpportunityByIdIfExist()
    {
        List<Opportunity> opportunityList = getOpportunity();
        Opportunity opprtunityRecord = opportunityList[0];
        
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getOpportunityById(opprtunityRecord.Id);
        Test.stopTest();
        System.assertEquals(1, listOfOpportunity.size(), 'Record not found.');            
    }    

    @isTest
    private static void testGetOpportunityByIdIfNotExist()
    {       
        Test.startTest();
            List<Opportunity> listOfOpportunity = OpportunityDAO.getOpportunityById('0065C000005jsOpQAJ');
        Test.stopTest();
        System.assertEquals(0, listOfOpportunity.size(), 'Record found.');            
    }
}