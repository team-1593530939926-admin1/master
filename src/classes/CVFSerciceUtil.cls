public class CVFSerciceUtil {
    
    public static String emcryptedData(String value, String fieldType){
        String result = '';
        if(fieldType == 'SSN' && value.length() >= 9){
            result = 'xxx-xx-' + value.substring(value.length() - 4, value.length());
        }
        if(fieldType == 'DOB' && !value.contains('-') && value.length() >= 9){
            result = 'xxx xx, ' + value.substring(value.length() - 5, value.length());
        }
        if(fieldType == 'DOB' && value.contains('-')){
            result = value.substring(0, 4) + '-xx-xx';
        }
        if(fieldType == 'Street'){
            for(Integer i = 0; i < value.length(); i++){
                if(i < 4){
                   result = result + value.substring(i, i + 1);
                }else{
                    result = result + 'x';
                }
            }
        }
        if(fieldType == 'Full'){
            for(Integer i = 0; i < value.length(); i++){
                result = result + 'x';
            }
        }
        return result;
    }
}