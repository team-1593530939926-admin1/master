@isTest
private class PartnerDetailsWrapperTest {
    
    @isTest
    private static void testPartnerDetailsWrappeIfDataExist()
    {
        Contact contactRecord = TestUtil.getPartnerDetailWrapperData();
 
        Test.startTest();
            PartnerDetailsWrapper result = new PartnerDetailsWrapper(contactRecord); 
        Test.stopTest();
        System.assertEquals('partnerFirstName', result.firstName);      
    }
    
    @isTest
    private static void testPartnerDetailsWrappeIfDataNotExist()
    {
        Contact contactRecord = new Contact();

        Test.startTest();
            PartnerDetailsWrapper result = new PartnerDetailsWrapper(contactRecord); 
        Test.stopTest();
        System.assertEquals(null, result.firstName);      
    }
    
    @istest
    private static void testPartnerDetailsWrapperIfWrapperAndBooleanIsAccepted()
    {
        Contact contactRecord = TestUtil.getPartnerDetailWrapperData();
        PartnerDetailsWrapper partnerInfoWrapper = new PartnerDetailsWrapper(contactRecord); 
        
        Test.startTest();
            PartnerDetailsWrapper result = new PartnerDetailsWrapper(partnerInfoWrapper, true);        
        Test.stopTest();
        System.assertEquals('partnerFirstName', result.firstName);      
    }
    
    @istest
    private static void testPartnerDetailsWrapperIfWrapperAndOpportunityIsAccepted()
    {
        Contact contactRecord = TestUtil.getPartnerDetailWrapperData();
        PartnerDetailsWrapper partnerInfoWrapper = new PartnerDetailsWrapper(contactRecord); 
        Account companyInfo = TestUtil.createAccountData();
        
        Test.startTest();
            PartnerDetailsWrapper result = new PartnerDetailsWrapper(partnerInfoWrapper, companyInfo);        
        Test.stopTest();
        System.assertEquals('partnerFirstName', result.partnerInformation.firstName);      
    }
    
    
}