@isTest
private class SObjectDAOTest {
    
    @isTest
    private static void testInsertSObjectRecordsIfListIsAccepted()
    {
        List<sObject> sObjectsList = TestUtil.createSOBjectData();
        
        Test.startTest();
            List<sObject> listOfSObjects = SObjectDAO.insertSObjectRecords(sObjectsList);
        Test.stopTest();
        System.assertEquals(2, listOfSObjects.size(), 'Records does not exist.');        
    }
    
    @isTest
    private static void testInsertSObjectRecordsIfListIsNotAccepted()
    {
        List<sObject> sObjectsList = new List<sObject>();
        
        Test.startTest();
            List<sObject> listOfSObjects = SObjectDAO.insertSObjectRecords(sObjectsList);
        Test.stopTest();
        System.assertEquals(0, listOfSObjects.size(), 'Records exist.');        
    }    

}