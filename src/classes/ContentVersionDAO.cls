public with sharing class ContentVersionDAO implements IContentVersionDAO
{
    
    public Interface IContentVersionDAO
    {
        List<ContentVersion> getListOfDocumentsOpportunityId(String opportunityId);
        List<ContentVersion> getListOfContentVersion(List<String> setOfDocumentIds);
        List<ContentVersion> updateContentVersions(List<ContentVersion> listOfContentVersion);
        List<ContentDocumentLink> insertListOfContentVersion(List<ContentDocumentLink> listOfContentVersion);
    }
    
    public static List<ContentVersion> getListOfDocumentsOpportunityId(String opportunityId)
    {
        List<ContentDocumentLink> listOfCDLink = [Select Id, ContentDocumentId From ContentDocumentLink Where LinkedEntityId =: opportunityId];
        Set<Id> setOfIds = new Set<Id>();
        for(ContentDocumentLink cdRecord : listOfCDLink){
            setOfIds.add(cdRecord.ContentDocumentId);
        }
        List<ContentVersion> listOfContentVersion = [SELECT Id, Title, Type__c, Description, ContentDocumentId, FileExtension, Is_Credit_Condition__c, FileType FROM ContentVersion Where ContentDocumentId In : setOfIds];               
        return listOfContentVersion;
    }
    
    public static List<ContentVersion> getListOfContentVersion(List<String> setOfDocumentIds)
    {
        return [SELECT Id, Title, Type__c, Description ,ContentDocumentId, FileExtension, FileType, Is_Credit_Condition__c
                FROM ContentVersion 
                WHERE ContentDocumentId In :setOfDocumentIds ];
    }
        
    public static List<ContentVersion> updateContentVersions(List<ContentVersion> listOfContentVersion)
    {
        update listOfContentVersion; 
        return listOfContentVersion;
    }
    
     public static List<ContentDocumentLink> insertListOfContentVersion(List<ContentDocumentLink> listOfContentVersion)
    {
    insert listOfContentVersion; 
    return listOfContentVersion;
    }
}