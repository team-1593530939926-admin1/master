public with sharing class SObjectDAO implements ISObjectDAO {
    
    public interface ISObjectDAO
    {
        List<sObject> insertSObjectRecords(List<sObject> listOfSObjects);
    }
    
    public static List<sObject> insertSObjectRecords(List<sObject> listOfSObjects)
    {
        insert listOfSObjects;
        return listOfSObjects;
    }
}