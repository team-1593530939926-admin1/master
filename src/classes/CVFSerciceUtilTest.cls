@isTest
private class CVFSerciceUtilTest {
    
    @isTest
    private static void testEmcryptedDataIfSSNisAccepted()
    {
        String value = '123456789';
        String fieldType = 'SSN';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals('xxx-xx-6789', result);
    }
    
    @isTest
    private static void testEmcryptedDataIfSSNisNotAccepted()
    {
        String value = '12345678';
        String fieldType = 'SSN';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals(true, String.isEmpty(result));
    }     
    
    @isTest
    private static void testEmcryptedDataIfDOBisAccepted()
    {
        String value = '2020-12-12';
        String fieldType = 'DOB';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals('2020-xx-xx', result);
    }  
    
    @isTest
    private static void testEmcryptedDataIfDOBisAcceptedWithoutDash()
    {
        String value = '2020 12 17 00:00:00';
        String fieldType = 'DOB';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals('xxx xx, 00:00', result);
    }    

    @isTest
    private static void testEmcryptedDataIfStreetIsAccepted()
    {
        String value = 'test street 123 ';
        String fieldType = 'Street';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals('testxxxxxxxxxxxx', result);
    }  
    
    @isTest
    private static void testEmcryptedDataIfFullValueIsAccepted()
    {
        String value = 'test street 123 ';
        String fieldType = 'Full';
        
        Test.startTest();
            String result = CVFSerciceUtil.emcryptedData(value, fieldType);            
        Test.stopTest();
        System.assertEquals('xxxxxxxxxxxxxxxx', result);
    }     
    
}