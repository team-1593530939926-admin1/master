@isTest
private class OpportunityTriggerHandlerTest {
    private static OpportunityTriggerHandler opportunityTriggerHandlerInstance = new OpportunityTriggerHandler();
    private static List<sObject> listOfNewSObject = new List<sObject>();
    private static Map<Id, sObject> mapOfOldObject = new Map<Id, sObject>();

    @TestSetup
    static void testSetup() {
        TestUtil.createOpportunityData();
    }

    private static List<Opportunity> getOpportunityRecord()
    {
        return [Select Id, Name, StageName, LeaseInfo_EquipmentDescription__c From Opportunity Limit 1];
    }

    // This method is used to test OpportunityTriggerHandler Apex Class afterInsert and afterUpdate methods.

    @isTest
    private static void testAfterInsertAndAfterUpdate()
    {
        List<User> listOfValidUser1 = [select Id ,UserRoleId from user];

        Test.startTest();
            TestUtil.CreatePartnerUserAccount();
        Test.stopTest();
        
        List<User> listOfValidUser2 = [select Id ,UserRoleId from user];
        
		System.assertEquals(listOfValidUser1.size()+1, listOfValidUser2.size());
    }
    
    @isTest 
    private static void beforeInsertTest() {

        Test.startTest();
            opportunityTriggerHandlerInstance.beforeInsert(listOfNewSObject);
        Test.stopTest();
    }

    @isTest
    private static void afterInsertTest() {
        Opportunity opportunityInstance = getOpportunityRecord().get(0);

        Test.startTest();
        opportunityTriggerHandlerInstance.afterInsert(new List<Opportunity>{opportunityInstance});
        Test.stopTest();
    }

    @isTest
    private static void beforeUpdateTest() {

        Test.startTest();
            opportunityTriggerHandlerInstance.beforeUpdate(listOfNewSObject, mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUpdateTest() {
        Opportunity opportunityInstance = getOpportunityRecord().get(0);
        List<Opportunity> listOfOpportunity = new List<Opportunity>{opportunityInstance};
        Map<Id,Opportunity> mapOfOpportunityVsId = new Map<Id,Opportunity>(listOfOpportunity);

        Test.startTest();
            opportunityTriggerHandlerInstance.afterUpdate(listOfOpportunity, mapOfOpportunityVsId);
        Test.stopTest();
    }

    @isTest
    private static void beforeDeleteTest() {

        Test.startTest();
            opportunityTriggerHandlerInstance.beforeDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterDeleteTest() {

        Test.startTest();
            opportunityTriggerHandlerInstance.afterDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUnDeleteTest() {

        Test.startTest();
            opportunityTriggerHandlerInstance.afterUnDelete(listOfNewSObject);
        Test.stopTest();
    }
}