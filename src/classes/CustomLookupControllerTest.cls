@isTest
private class CustomLookupControllerTest {
	
    @testSetup
    private static void testSetupData()
    { 
        TestUtil.insertBusinessApplicationWrapper();
    }
    
    @isTest
    private static void testSearchAccountPositive()
    {
        Test.startTest();
            List<sObject> result = CustomLookupController.search('Test Business Account', 'Account','');
        Test.stopTest();
        
        System.assertEquals(1, result.size());
    }
    
    @isTest
    private static void testSearchOpportunityPositive()
    {
        Test.startTest();
            List<sObject> result = CustomLookupController.search('', 'Opportunity','');
        Test.stopTest();
        
        System.assertEquals(null, result);
    }
    
    @isTest
    private static void testSearchAccountNegative()
    {
        Test.startTest();
            List<sObject> result = CustomLookupController.search(null, 'Account', '');
        Test.stopTest();
        
        //System.assertEquals(2, result.size());
    }
    
    @isTest
    private static void testSearchContactPositive()
    {
        Test.startTest();
            List<sObject> result = CustomLookupController.search('testFirstName Business', 'Contact','');
        Test.stopTest();
        
        System.assertEquals(1, result.size());
    }
    
    @isTest
    private static void testSearchContactNegative()
    {
        Test.startTest();
            List<sObject> result = CustomLookupController.search(null, 'Contact', '');
        Test.stopTest();
        
        //System.assertEquals(3, result.size());
    }
}