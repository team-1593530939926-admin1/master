public with sharing class Leads {
    
    private static FCSEntityInformationDAO.IFCSEntityInformationDAO fcsEntityInformationDAOInstance = new FCSEntityInformationDAO();

    public static void moveRelatedInfoToConvertedCompany(
        Map<Id, Lead> mapOfNewLeadVsId, Map<Id, Lead> mapOfOldLeadVsId) {

        if (!mapOfNewLeadVsId.values().isEmpty()) {
            Map<Id, Id> mapOfLeadIdVsCompanyId = new Map<Id, Id>();
            for (Id eachLeadId : mapOfNewLeadVsId.keySet()) {

                Lead newLeadRecord = mapOfNewLeadVsId.get(eachLeadId);
                Lead oldLeadRecord = mapOfOldLeadVsId.get(eachLeadId);

                if (oldLeadRecord.isConverted == false && newLeadRecord.isConverted == true)  {
                    Id companyId = mapOfLeadIdVsCompanyId.get(newLeadRecord.Id);
                    if (companyId == null) {
                        companyId = newLeadRecord.ConvertedAccountID;
                    }

                    mapOfLeadIdVsCompanyId.put(newLeadRecord.Id, companyId);
                }
            }

            if (!mapOfLeadIdVsCompanyId.keySet().isEmpty()) {
                linkLeadEntityVerificationWithCompany(mapOfLeadIdVsCompanyId);
            }
        }

    }

    @TestVisible
    private static void linkLeadEntityVerificationWithCompany(Map<Id, Id> mapOfLeadIdsVsCompanyIds) {
        if (!mapOfLeadIdsVsCompanyIds.keySet().isEmpty()) {
            List<FCS_Entity_Information__c> listOfEntityInformation = fcsEntityInformationDAOInstance.getEntityByLeadId(mapOfLeadIdsVsCompanyIds.keySet());

            if (!listOfEntityInformation.isEmpty()) {
                for (FCS_Entity_Information__c eachEntityInfo : listOfEntityInformation) {
                    Id companyId = mapOfLeadIdsVsCompanyIds.get(eachEntityInfo.Lead__c);
                    eachEntityInfo.Company__c = companyId;
                }

                update listOfEntityInformation;
            }
        }
    } 
}