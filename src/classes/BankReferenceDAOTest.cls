@isTest
private class BankReferenceDAOTest
{
    @isTest
    private static void testInsertBankReferenceIfListIsAccpted()
    {
        List<Bank_Reference__c> bankReferencesList = new List<Bank_Reference__c>();
        Bank_Reference__c objectOfBankReferences = new Bank_Reference__c();
        
        objectOfBankReferences.Name = 'Test Name';
        objectOfBankReferences.Bank_Name__c = 'XYZ';
        objectOfBankReferences.City__c = 'NY';
        objectOfBankReferences.State__c = 'CA';
        objectOfBankReferences.Country__c = 'USA';
        
        bankReferencesList.add(objectOfBankReferences);
        
        Test.startTest();
            List<Bank_Reference__c> listofBankDetails = BankReferenceDAO.insertBankReference(bankReferencesList);
        Test.stopTest();
        
        System.assertEquals(1, listofBankDetails.size(), 'Record is not created');
    }
    
    @isTest
    private static void testInsertBankReferenceIfListIsNotAccpted()
    {
        List<Bank_Reference__c> bankReferencesList = new List<Bank_Reference__c>();
        
        Test.startTest();
           List<Bank_Reference__c> listofBankDetails = BankReferenceDAO.insertBankReference(bankReferencesList);
        Test.stopTest();
        
        System.assertEquals(0, listofBankDetails.size(),'Record is created');
    }
    
    @isTest
    private static void getBankRecordByApplicationIdRecordFoundTest() {
        Bank_Reference__c bankReferenceInstance = TestUtil.getBankDetailsWrapperData();
        insert bankReferenceInstance;
        
        Test.startTest();
           List<Bank_Reference__c> listofBankDetails = BankReferenceDAO.getBankRecordByApplicationId('');
        Test.stopTest();
        
        System.assertEquals(1, listofBankDetails.size(), 'Record Not Found.');
        System.assertEquals(bankReferenceInstance.Id, listofBankDetails.get(0).Id, 'Record Not Found.');
        System.assertNotEquals(0, listofBankDetails.size(), 'Record Must be Present.');
    }
    
    @isTest
    private static void getBankRecordByApplicationIdRecordNotFoundTest() {
        Bank_Reference__c bankReferenceInstance = TestUtil.getBankDetailsWrapperData();
        
        Test.startTest();
           List<Bank_Reference__c> listofBankDetails = BankReferenceDAO.getBankRecordByApplicationId('');
        Test.stopTest();
        
        System.assertEquals(0, listofBankDetails.size(), 'Record is Created.');
    }
}