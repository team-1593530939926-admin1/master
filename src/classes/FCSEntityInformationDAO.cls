public class FCSEntityInformationDAO implements IFCSEntityInformationDAO {

    public interface IFCSEntityInformationDAO {
        List<FCS_Entity_Information__c> getEntityById(Set<Id> setOfIds);
        List<FCS_Entity_Information__c> getEntityByEntityId(Set<String> setOfEntityId);
        List<FCS_Entity_Information__c> getEntityByLeadId(Set<Id> setOfLeadIds);
        List<FCS_Entity_Information__c> getLatestCurrentMonthEntityByCompanyId(Set<Id> setOfCompanyId);
        List<FCS_Entity_Information__c> getLatestCurrentMonthEntityByLeadId(Set<Id> setOfLeadId);
        void saveEntity(List<FCS_Entity_Information__c> entityInfoList);
    }

    public static List<FCS_Entity_Information__c> getEntityById(Set<Id> setOfIds) {
        return [ SELECT Id, Entity_Id__c, Name, Entity_Name__c, Entity_Standing__c, Type__c,
         Date_of_Filing__c, Date_Of_Incorporation__c, Domicile_Country__c, Domicile_State__c, 
         Domicile_Type__c, Company__c, Lead__c, Time_In_Service__c 
         FROM FCS_Entity_Information__c 
         WHERE Id = :setOfIds ];
    }

    public static List<FCS_Entity_Information__c> getEntityByLeadId(Set<Id> setOfLeadIds) {
        return [ SELECT Id, Entity_Id__c, Name, Entity_Name__c, Entity_Standing__c, Type__c,
         Date_of_Filing__c, Date_Of_Incorporation__c, Domicile_Country__c, Domicile_State__c, 
         Domicile_Type__c, Company__c, Lead__c, Time_In_Service__c 
         FROM FCS_Entity_Information__c 
         WHERE Lead__c = :setOfLeadIds ];
    }

    public static List<FCS_Entity_Information__c> getEntityByEntityId(Set<String> setOfEntityId) {
        return [ SELECT Id, Entity_Id__c, Name, Entity_Name__c, Entity_Standing__c, Type__c,
         Date_of_Filing__c, Date_Of_Incorporation__c, Domicile_Country__c, Domicile_State__c, 
         Domicile_Type__c, Company__c, Lead__c, Time_In_Service__c  
         FROM FCS_Entity_Information__c 
         WHERE Entity_Id__c = :setOfEntityId];
    }

    public static List<FCS_Entity_Information__c> getLatestCurrentMonthEntityByCompanyId(Set<Id> setOfCompanyId) {
        return [ SELECT Id, Entity_Id__c, Name, Entity_Name__c, Entity_Standing__c, Type__c,
         Date_of_Filing__c, Date_Of_Incorporation__c, Domicile_Country__c, Domicile_State__c, 
         Domicile_Type__c, Company__c, Lead__c, Time_In_Service__c 
         FROM FCS_Entity_Information__c 
         WHERE Company__c = :setOfCompanyId AND CreatedDate = LAST_N_DAYS:30 ORDER BY CreatedDate DESC];
    }

    public static List<FCS_Entity_Information__c> getLatestCurrentMonthEntityByLeadId(Set<Id> setOfLeadId) {
        return [ SELECT Id, Entity_Id__c, Name, Entity_Name__c, Entity_Standing__c, Type__c,
         Date_of_Filing__c, Date_Of_Incorporation__c, Domicile_Country__c, Domicile_State__c, 
         Domicile_Type__c, Company__c, Lead__c, Time_In_Service__c 
         FROM FCS_Entity_Information__c 
         WHERE Lead__c = :setOfLeadId AND CreatedDate = LAST_N_DAYS:30 ORDER BY CreatedDate DESC];
    }

    public static void saveEntity(List<FCS_Entity_Information__c> entityInfoList) {
        insert entityInfoList;
    }
}