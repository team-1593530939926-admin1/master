@isTest
private class LeadsTest {

    private static final String ID_MUST_NOT_BE_NULL_ASSERT_MESSAGE = 'Id must not be null.';
    private static final String ID_MUST_BE_EQUAL_ASSERT_MESSAGE = 'Id must be equal.';
    private static final String ID_SHOULD_BE_PRESENT_ASSERT_MESSAGE = 'Id should be present.';

    @TestSetup
    static void testSetup() {
        TestUtil.createLeadData(1, true);
        TestUtil.createAccountData(1, true);
    }

    private static List<Account> getCompany() {
        return [SELECT Name, Source__c, Location__c, billingCity, BillingCountry, BillingState, BillingStreet, BillingPostalCode FROM Account LIMIT 1];
    }

    private static List<Lead> getLeads() {
        return [SELECT Status, LastName, Company, State, Source__c, Location__c, Street, PostalCode, City, Country, IsConverted FROM Lead LIMIT 1];
    }

    private static List<FCS_Entity_Information__c> getEntityInformation() {
        return [SELECT Id, Entity_Id__c, Entity_Name__c, Lead__c, Company__c FROM FCS_Entity_Information__c LIMIT 1];
    }
    
    @isTest
    private static void moveRelatedInfoToConvertedCompanyTest() {
        Lead lead = getLeads().get(0);
        FCS_Entity_Information__c entityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false).get(0);
        entityInformation.Lead__c = lead.Id;
        insert entityInformation;
        Map<Id, Lead> mapOfNewLeadVsId = new Map<Id, Lead>();
        Map<Id, Lead> mapOfOldLeadVsId = new Map<Id, Lead>();
        mapOfOldLeadVsId.put(lead.Id, lead);
        Account acc = getCompany().get(0);

        Database.LeadConvert lc = new database.LeadConvert();  
        lc.setLeadId( lead.Id ); 
        lc.setAccountId(acc.Id); 
        lc.setDoNotCreateOpportunity( true ); 
        LeadStatus convertStatus = [SELECT Id, MasterLabel, IsConverted FROM LeadStatus WHERE IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
          
        Database.LeadConvertResult lcr = Database.convertLead(lc); 
        
        Id convertedLeadId = lcr.getLeadId();
        Lead convertedLead = [SELECT Id, Status, LastName, Company, State, Source__c, Location__c, Street, PostalCode, City, Country, IsConverted, ConvertedAccountId FROM Lead WHERE Id = :convertedLeadId];
        mapOfNewLeadVsId.put(convertedLeadId, convertedLead);

        Test.startTest();
            Leads.moveRelatedInfoToConvertedCompany(mapOfNewLeadVsId, mapOfOldLeadVsId);
        Test.stopTest();

        FCS_Entity_Information__c entityInformation1 = getEntityInformation().get(0);
        System.assertEquals(convertedLeadId, entityInformation1.Lead__c, ID_MUST_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(lcr.getAccountId(), entityInformation1.Company__c, ID_MUST_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertEquals(entityInformation.Lead__c, entityInformation1.Lead__c, ID_MUST_BE_EQUAL_ASSERT_MESSAGE);
        System.assertNotEquals(null, entityInformation1.Company__c, ID_SHOULD_BE_PRESENT_ASSERT_MESSAGE);
    }

    @isTest
    private static void linkLeadEntityVerificationWithCompanyTest() {
        List<Lead> listOfLead = getLeads();
        List<Account> listOfCompany = getCompany();
        FCS_Entity_Information__c createEntityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false).get(0);
        createEntityInformation.Lead__c = listOfLead.get(0).Id;
        insert createEntityInformation;
        Map<Id, Id> mapOfLeadIdsVsCompanyIds = new Map<Id,Id>();
        mapOfLeadIdsVsCompanyIds.put(listOfLead.get(0).Id, listOfCompany.get(0).Id);

        Test.startTest();
            Leads.linkLeadEntityVerificationWithCompany(mapOfLeadIdsVsCompanyIds);
        Test.stopTest();

        FCS_Entity_Information__c entityInformation = getEntityInformation().get(0);
        System.assertEquals(listOfCompany.get(0).Id, entityInformation.Company__c, ID_MUST_NOT_BE_NULL_ASSERT_MESSAGE);
        System.assertNotEquals(null, entityInformation.Company__c, ID_SHOULD_BE_PRESENT_ASSERT_MESSAGE);
    }
}