public class UserTriggerHandler implements TriggerInterface {

    private static List<User> listOfUsers;    
    private static Map<Id, User> oldUserMap = new Map<Id, User>();
    private static final ProfileDAO.IProfileDAO profileDAOInstance = new ProfileDAO();
    private static final String BROKER_PROFILE_NAME = 'Crestmark Broker Profile';
    private static Id brokerProfileId = profileDAOInstance.getProfileByName(BROKER_PROFILE_NAME)[0].Id;
    private static final String BROKER_PERMISSIONSET_NAME = 'Broker_Crestmark_Permission_Set';
    private static final String FCS_API_PERMISSIONSET_NAME = 'FCSApiPermissionSet';
    private static final PermissionSetDAO.IPermissionSetDAO permissionSetDAOInstance = new PermissionSetDAO();

    public void beforeInsert(List<sObject> newObjList) {}

    public void afterInsert(List<sObject> newObjList) {
        Set<Id> setOfUserIDs = new Set<Id>();
        listOfUsers = (List<User>)newObjList;
        for(User userRecord : listOfUsers){
            if(userRecord.ProfileId == brokerProfileId){
                setOfUserIDs.add(userRecord.Id);
            }
        }
        if(!setOfUserIDs.isEmpty()){
            List<PermissionSet> listOfPermissionSet = permissionSetDAOInstance.getPermissionSetByName(new Set<String>{FCS_API_PERMISSIONSET_NAME, BROKER_PERMISSIONSET_NAME});
            Map<Id, PermissionSet> setOfPermissionSetIds = new Map<Id, PermissionSet>(listOfPermissionSet);
            if(!setOfPermissionSetIds.isEmpty()){
                Contacts.AssignPermissionSetToUsers(setOfPermissionSetIds.keySet(), setOfUserIDs);
            }
        }
    }

    public void beforeUpdate(List<sObject> newObjList, Map<Id, sObject> oldObjMap) {}

    public void afterUpdate(List<sObject> newObjList,  Map<Id, sObject> oldObjMap) {}

    public void beforeDelete(Map<Id, sObject> oldObjMap) {}

    public void afterDelete(Map<Id, sObject> oldObjMap) {}

    public void afterUnDelete(List<sObject> newObjList) {}
}