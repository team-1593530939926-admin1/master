@isTest
private class PermissionSetDAOTest {
    
    @TestSetup
    static void testSetup() {
        TestUtil.createContactData();
    }

    private static Contact getContactRecord() {
        return [SELECT Id, FirstName, LastName, Relationship_Type__c FROM Contact WHERE LastName = 'LastName 1' LIMIT 1];
    }

    private static UserRole getUserRoleRecord() {
        return [SELECT Id, Name FROM UserRole WHERE Name = 'System Administrator' LIMIT 1];
    }

    @isTest
    private static void testGetPermissionSetByNameIfSuccessful()
    {
        String permissionSetName = 'Broker_Crestmark_Permission_Set';
        
        Test.startTest();
            List<PermissionSet> listOfPermissionSet = PermissionSetDAO.getPermissionSetByName(new Set<String>{permissionSetName});
        Test.stopTest();
        
        System.assertEquals('Broker_Crestmark_Permission_Set', listOfPermissionSet.get(0).Name);
    }
    
    @isTest
    private static void testGetPermissionSetByNameIfUnsuccessful()
    {
        String permissionSetName = 'Crestmark_Permission_Set';
        
        Test.startTest();
            List<PermissionSet> listOfPermissionSet = PermissionSetDAO.getPermissionSetByName(new Set<String>{permissionSetName});
        Test.stopTest();
        
        System.assertEquals(null, listOfPermissionSet);
    }
    
    @isTest
    private static void testInserPermissionSetAssignmentIfSuccessful()
    {
        //UserRole userRole = new UserRole(Name = 'System Administrator');
        //insert userRole;
        //UserRole userRole1 = getUserRoleRecord();
        
        Contact contactforPortal = getContactRecord();
        
        Profile profileForUser = [SELECT Id FROM Profile WHERE Name='Crestmark Broker Profile' LIMIT 1];

        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profileForUser.Id,
                             ContactId = contactforPortal.Id, 
                             TimeZoneSidKey='America/Los_Angeles',
                             UserName='testUtilSecurity@abc.com');
        
        Test.startTest();            
            insert user;
        Test.stopTest();
    }  
}