global class CrestMarkBrokerLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global String startURL {get; set;}
    global Boolean desktopView {get; set;}
    global Boolean isMobile { get; set; }

    global CrestMarkBrokerLoginController () {
        startURL = Site.getPathPrefix() + '/s/';
        isMobile = UserInfo.getUiTheme() == 'Theme4t';
    }
    

    global PageReference doLogin() {
        PageReference pageRef;
        if(String.isBlank(username)) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info, System.Label.Error_Username_is_a_required));
            return null;
        }

        if(String.isBlank(password)){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Info, System.Label.Error_Password_is_a_required));
            return null;
        }
        else{
            pageRef = Site.login(username, password, Site.getPathPrefix() + '/s/');
        }
        return pageRef;
    }
    
}