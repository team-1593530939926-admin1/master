public class FCSBusinessInformation {
    
    @AuraEnabled
    public Header header {get; set;}
    @AuraEnabled
    public List<Results> results {get; set;}
    
    public FCSBusinessInformation() {
        
    }
    
    public class Header {
        
        @AuraEnabled
        public String transactionID {get; set;}
        @AuraEnabled
        public String orderDate {get; set;} 
        @AuraEnabled
        public String stateCode {get; set;}
        @AuraEnabled
        public String searchString {get; set;}
        @AuraEnabled
        public String indexDate {get; set;}
        @AuraEnabled
        public Integer numberOfHits {get; set;}
        @AuraEnabled
        public String passThrough {get; set;}
        
        public Header() {
            
        }
    }
    public class Results {
        
        @AuraEnabled
        public String companyName {get; set;}
        @AuraEnabled
        public String entityID {get; set;} 
        @AuraEnabled
        public String entityStanding {get; set;}
        @AuraEnabled
        public String entityType {get; set;}
        @AuraEnabled
        public String nameType {get; set;}
        @AuraEnabled
        public String state {get; set;}
        @AuraEnabled
        public String additionalSearchParameters {get; set;}
        @AuraEnabled
        public Boolean isSelected {
        get 
        {
            if (isSelected == null) {
                isSelected = false;
            }
            return isSelected;
        }
        set;}
        
        public Results() {
            this.isSelected = false;
        }
    }
}