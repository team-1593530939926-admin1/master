public with sharing class ContactDAO implements IContactDAO
{
    public interface IContactDAO 
	{
        List<Contact> insertContacts(List<Contact> listOfContacts );
        List<Contact> getContactDetailsByApplicationId(String applicationId);
        List<Contact> getListOfPersonalGuarantor(Set<Id> setOfContactId);
    }
 
    public static List<Contact> insertContacts(List<Contact> listOfContacts )
    {
        insert listOfContacts;
        return listOfContacts;
    }   
    
    public static List<Contact> getContactDetailsByApplicationId(String applicationId)
    {
        return [SELECT Id, Name, MailingCity,CreatedById, Company_Name__c, MailingCountry, BirthDate, Email, Fax, 
                FirstName, Guarantor_Type__c, LastName, MiddleName, Nature_of_Business__c, Phone, MailingPostalCode, 
                Primary_Guarantor__c, Salutation, SSN__c, MailingState, MailingStreet, Tax_ID__c,RecordTypeId               
                FROM Contact Where AccountId =: applicationId];
    }
    
    public static List<Contact> getListOfPersonalGuarantor(Set<Id> setOfContactId)
    {
        return [Select Id,Fax,FirstName,MiddleName,CreatedById,LastName,MailingStreet,MailingCity,MailingState,
                MailingPostalCode,MailingCountry,RecordType.DeveloperName,RecordTypeId,Phone,SSN__c,Nature_of_Business__c,
                Email,Primary_Guarantor__c,Guarantor_Type__c,Birthdate, (Select Id,Address_Type__c,City__c,County__c,
                Postal_Code__c,State__c,Street_Address_1__c From Guarantor_Address__r)
                From Contact Where Id in : setOfContactId];
    }
    
     @AuraEnabled(cacheable=true)
    public static List<sObject> search(String searchTerm, string myObject, String filter, String recordTypeName)
    {
        String myQuery = null;
        if(filter != null && filter != ''){
            myQuery = 'Select Id, Name from '+myObject+' Where  Name Like \'%' + searchTerm + '%\' AND '+filter+ 'And RecordType.Name= :recordTypeName' +' LIMIT 5';
        }
        else {
            if(searchTerm == null || searchTerm == ''){
                myQuery = 'Select Id, Name from '+myObject+' Where LastViewedDate != NULL And RecordType.Name= :recordTypeName ORDER BY LastViewedDate DESC LIMIT 5';
            }
            else {
                myQuery = 'Select Id, Name from '+myObject+' Where Name Like \'%' + searchTerm + '%\''+' And RecordType.Name= :recordTypeName LIMIT 5';
                System.debug('calling from else');
            }
        }
        List<sObject> lookUpList = database.query(myQuery);
        return lookUpList;
    }
    
    public static Contact getGuarantorRecordFromGuarantorId(String companyCode)
    {
        List<Contact> contactList = new List<Contact>();
        if(companyCode!=''){
        contactList = [Select Id,Fax,FirstName,MiddleName,CreatedById,LastName,MailingStreet,MailingCity,MailingState,
                                     MailingPostalCode,MailingCountry,RecordType.DeveloperName,RecordTypeId,Phone,SSN__c,Nature_of_Business__c,
                                     Email,Primary_Guarantor__c,Guarantor_Type__c,Birthdate, (Select Id,Address_Type__c,City__c,County__c,
                                                                                              Postal_Code__c,State__c,Street_Address_1__c From Guarantor_Address__r)
                                     From Contact Where Id =: companyCode]; 
            }
        if(!contactList.isEmpty()){
            return contactList[0];
        }else{
            return null;
        }
            
    }
    
    
}