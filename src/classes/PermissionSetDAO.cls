public with sharing class PermissionSetDAO implements IPermissionSetDAO {
    
    public Interface IPermissionSetDAO {
        List<PermissionSet> getPermissionSetByName(Set<String> listOfPermissionSetName);
        List<PermissionSetAssignment> inserPermissionSetAssignment(List<PermissionSetAssignment> listOfPermissionSetAssignment);
    }
    
    public static List<PermissionSet> getPermissionSetByName(Set<String> listOfPermissionSetName){
        List<PermissionSet> listOfPermissionSet = [Select Id, Name From PermissionSet Where Name IN : listOfPermissionSetName];
        
        if(!listOfPermissionSet.isEmpty())
        {
            return listOfPermissionSet;
        }
        else
        {            
            return null;
        }
    }
    
    public static List<PermissionSetAssignment> inserPermissionSetAssignment(List<PermissionSetAssignment> listOfPermissionSetAssignment){
        insert listOfPermissionSetAssignment;
        return listOfPermissionSetAssignment;        
    }

}