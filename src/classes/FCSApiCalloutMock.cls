@isTest
public class FCSApiCalloutMock implements HttpCalloutMock {
    
    private static final String CONTENT_TYPE_HEADER = 'Content-Type';
    private Integer code;
    private String contentType;
    private String body;
    private Boolean isException;
    private String exceptionMessage;

    public FCSApiCalloutMock(Integer code, String contentType, String body) { 
        this.code = code;
        this.contentType = contentType;
        this.body = body;
        this.isException = false;
    }

    public FCSApiCalloutMock(String exceptionMessage) {
        this.isException = true;
        this.exceptionMessage = exceptionMessage;
    }

    public HTTPResponse respond(HTTPRequest req) {
        if(isException) {
            CalloutException calloutExceptionInstance = (CalloutException)CalloutException.class.newInstance();
        	calloutExceptionInstance.setMessage(exceptionMessage);
        	throw calloutExceptionInstance;
        }
        HttpResponse res = new HttpResponse();
        res.setHeader(CONTENT_TYPE_HEADER, contentType);
        res.setBody(body);
        res.setStatusCode(code);
        return res;
    }
}