public class DealDocumentUploadTableController {
    
    // This method get all the attached files from the Deal.
    
    @AuraEnabled
    public static list<DealDocumentUploadWrapper> relatedFiles(list<Id> idParent){
        list<DealDocumentUploadWrapper> docUploadedList = new list<DealDocumentUploadWrapper>();
        
        if(idParent!=null) 
        {
            for (contentversion uploadedFileList :[SELECT Id, Title, description,Type__c,ContentDocumentId, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :idParent])
            {
                docUploadedList.add(new DealDocumentUploadWrapper(uploadedFileList.Type__c, uploadedFileList.Title ,uploadedFileList.description,uploadedFileList.ContentDocumentId, uploadedFileList.Id, uploadedFileList.FileExtension));
            }
            return docUploadedList;
        }
        else
        {
            return null;
        }
    }
    
    
    // This method set the description of attachment.
    
    
    @AuraEnabled
    public static Void setFileDetails(Id idParent , String description , String type) {
        if(!String.isEmpty(description)||(!String.isEmpty(type))){
            system.debug('setFileDetails'+description);
            list<contentversion> uploadedFileList = new list<contentversion>();
            
            if(!String.isEmpty(idParent)) 
            {
                for (contentversion recentfileList :[SELECT Id, isBrokerUploaded__c,Title,Type__c, description,ContentDocumentId, OwnerId FROM ContentVersion WHERE ContentDocumentId = :idParent limit 1])
                {
                    recentfileList.Type__c = type;
                    recentfileList.description = description;
                    recentfileList.isBrokerUploaded__c = true;
                    uploadedFileList.add(recentfileList); 
                }
                system.debug('setFileDetails'+uploadedFileList);
                update uploadedFileList;
            }
        }
    }
    
    // This method deletes the attachment from the Deal.
    
    @AuraEnabled
    public static Void deleteFiles(Id deleteRecord) {
        if(deleteRecord != null ) {
            list<ContentDocument> toBeDeletedContentDocList = new list<ContentDocument>();
            for (contentversion content : [SELECT Id, ContentDocumentId  FROM ContentVersion where ContentDocumentId =:deleteRecord ])
            {
                ContentDocument contentdoc = new ContentDocument();
                contentdoc.id = content.ContentDocumentId;
                toBeDeletedContentDocList.add(contentdoc);
            }
            
            Delete toBeDeletedContentDocList;
        }
    }
    
    
    
    public class DealDocumentUploadWrapper
    {
        
        @AuraEnabled public String fileType {get;set;}
        @AuraEnabled public String fileName {get;set;}
        @AuraEnabled public String fileDescription {get;set;}
        @AuraEnabled public Id fileId {get;set;}
        @AuraEnabled public Id versionId {get; set;}
        @AuraEnabled public String fileExtension {get; set;}
        
        
        public DealDocumentUploadWrapper(String fileType,String fileName,String fileDescription,Id fileId, Id versionId, String fileExtension){
            this.fileType = fileType;
            this.fileName = fileName;
            this.fileDescription = fileDescription;
            this.fileId = fileId;
            this.versionId = versionId;
            this.fileExtension = fileExtension;
            
        }
        
    }
    
    @AuraEnabled(cacheable=true)
    public static String getPortalURL(){
        CVF_Portal_URL__c portalURLSetting = CVF_Portal_URL__c.getOrgDefaults();
        if(!String.isEmpty(portalURLSetting.Portal_URL__c)){
            return portalURLSetting.Portal_URL__c;
        }else{
            return '';
        }
    }
    
}