@isTest
private class UserDAOTest {
    
    @isTest
    private static void testGetUserName()
    {
        CTLMTestUtil.getUser();
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                List<user> userList = UserDAO.getUser();
            Test.stopTest();  
            
            System.assertEquals(1, userList.size());
        }       
    }    
    
    @isTest
    private static void testInsertingUserSuccessfull()
    {
        Profile profileForUser = [Select Id From Profile Where Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profileForUser.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
        
        List<User> listUser = new List<User>();
        listUser.add(user);
        
        Test.startTest();
        List<User> listOfUser = UserDAO.insertUser(listUser);
        Test.stopTest();
        
        System.assertEquals(1, listOfUser.size());
    }
    
    @isTest
    private static void testInsertingUserUnsuccessfull()
    {
        Profile profileForUser = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profileForUser.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
        
        List<User> listUser = new List<User>();
        
        Test.startTest();
        List<User> listOfUser = UserDAO.insertUser(listUser);
        Test.stopTest();
        
        System.assertEquals(0, listOfUser.size() );
    }
    
    @isTest
    private static void testInsertUserFutureMethod()
    {
        Profile profileForUser = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profileForUser.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
        
        List<User> listUser = new List<User>();
        listUser.add(user);
        String listOfUserJSON = JSON.serialize(listUser);
        
        Test.startTest();
        UserDAO.insertUserFutureMethod(listOfUserJSON);
        Test.stopTest();
        
        //System.assertEquals(0, listOfUser.size() );
    }  
    
    @istest
    public static void testUpdateUser(){
        User myTestUser =  CTLMTestUtil.getUser();
        List<User> ListOfUserDetail = new List<User>();
        User userDetail = new User();
        userDetail.email = 'sh@rv.com';
        userDetail.firstName = 'test';
        userDetail.languagelocalekey = 'en_US';
        userDetail.lastName = 'User15948830254604123076';
        userDetail.localesidkey = 'en_US';
        userDetail.username = 's@trg.com';
        userDetail.timezonesidkey = 'America/New_York';
        userDetail.Id = myTestUser.Id;
        ListOfUserDetail.add(userDetail);
    
          System.runAs(myTestUser) {
        List<User> listOfUser = UserDAO.updateUser(ListOfUserDetail);
   
        System.assertEquals(1, listOfUser.size());
          }}
    
    @isTest
    private static void testGetUserToEdit()
    {
        CTLMTestUtil.getUser();
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                List<user> userList = UserDAO.getUserToEdit();
            Test.stopTest();  
            
            System.assertEquals(1, userList.size());
        }       
    }    
    
     @isTest
    private static void testGetLanguageLocaleKeyPicklist()
    {
        CTLMTestUtil.getUser();
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                List<PicklistWrapper> userList = UserDAO.getLanguageLocaleKeyPicklist();
            Test.stopTest();  
            
            System.assertEquals(true, userList.size() > 0,'Size is zero');
        }       
    }    
    
    @isTest
    private static void testGetLocaleSidKeyPicklist()
    {
        CTLMTestUtil.getUser();
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                List<PicklistWrapper> userList = UserDAO.getLocaleSidKeyPicklist();
            Test.stopTest();  
            
            System.assertEquals(true, userList.size() > 0,'Size is zero');
        }       
    }    
    
    @isTest
    private static void testGettimezonesidkeyPicklist()
    {
        CTLMTestUtil.getUser();
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                List<PicklistWrapper> userList = UserDAO.gettimezonesidkeyPicklist();
            Test.stopTest();  
            
            System.assertEquals(true, userList.size() > 0,'Size is zero');
        }       
    }    
    
    @isTest
    private static void testGetUserById()
    {
        Id ownerId = CTLMTestUtil.getUser().Id;
        List<User> listOfUsers = [Select Id, LastName From User];
        User user = listOfUsers[0];
        System.runAs(user)
        {
            Test.startTest();
                User userInstance = UserDAO.getUserById(ownerId);
            Test.stopTest();  
            
            System.assertEquals(ownerId, userInstance.Id, 'Id should not be different.');
        }       
    }
}