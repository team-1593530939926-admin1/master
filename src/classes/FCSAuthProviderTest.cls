@isTest(isParallel=true)
private  class FCSAuthProviderTest {
    
    private static final String CONTENT_TYPE_HEADER = 'Content-Type';
    private static final String CONTENT_TYPE_JSON = 'application/json';
    private static final String ACCESS_TOKEN = 'gv6iCx48OaH76ufiGXBUlRTRTXxx';
    private static final String EXPIRES_IN = '123';
    private static final String STATE = 'mocktestState'; 
    private static final String TOKEN_TYPE = 'Bearer'; 
    
    private static final String PROVIDER_NAME = 'UnitTestProvider'; 
    
    private static final String CALLBACK_URL_OVERRIDE = 
    System.URL.getSalesforceBaseUrl().getHost()+'/services/authcallback/' + PROVIDER_NAME; 

    private static final String TEST_USER_EMAIL = 'developer@example.com';
    private static final String KEY = 'testKey'; 
    private static final String SECRET = 'testSecret'; 
    private static final String SCOPE = 'View';
    private static final String USERNAME = 'userName';
    private static final String USER_PASSOWRD = 'passowrd';

    private static final String STATE_TO_PROPOGATE = 'testState'; 
    private static final String ACCESS_TOKEN_URL =  'http://www.dummyhost.com/accessTokenUri';
    private static final String EMPTY_VALUE = ''; 

    private static final String jsonGoodToken = '{'+
    '  \"token_type\" : \"Bearer\",'+
    '  \"access_token\" : \"gv6iCx48OaH76ufiGXBUlRTRTXxx\",'+
    '  \"expires_in\" : \"3600\"'+
    '}';
    
    private static final String jsonError1 = '{' + 
        '\"Message\":\"Authorization has been denied for this request.\"' +   
    '}';
    
    private static final String jsonError2 = '{' + 
      '\"ErrorCode\" : \"invalid_client\",' + 
      '\"Error\" :\"Client credentials are invalid\"' + 
    '}';

    private static final String jsonBadQueryParam = '{' + 
      '\"fault\":{' + 
            '\"faultstring\":\"Unresolved variable : request.header.Authorization\",' + 
            '\"detail\":{' + 
                '\"errorcode\":\"steps.basicauthentication.UnresolvedVariable\"' + 
             '}' +
        '}' + 
    '}';

    private static Map<String,String> setupAuthProviderConfig () 
    { 
        final Map<String,String> authProviderConfiguration = new Map<String,String>(); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_PROVIDER_NAME, PROVIDER_NAME); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_AUTHTOKEN_URL, ACCESS_TOKEN_URL); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_CALLBACK_URL, EMPTY_VALUE); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_CLIENT_ID, KEY); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_CLIENT_SECRET,SECRET); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_SCOPE,SCOPE);
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_USE_JSON, 'false');       
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_USERNAME, USERNAME); 
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_USER_PASSWORD, USER_PASSOWRD); 
         
       
        return authProviderConfiguration; 
    } 

     @isTest
     static void testMetadataType()
    {
        final FCSAuthProvider provider = new FCSAuthProvider(); 
        final String actual = provider.getCustomMetadataType();

        Test.startTest();
            final String expected = FCSAuthProvider.CUSTOM_MDT_NAME;
        Test.stopTest();

        System.assertEquals(expected, actual, 'Must return valid custom metadata type');
    }
    
    @isTest
    static void testInitiateMethod()
    {
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        final PageReference expectedUrl = new PageReference(authProviderConfiguration.get('Callback_URL__c'));
        expectedUrl.getParameters().put('state',STATE_TO_PROPOGATE);

        Test.startTest();
            final PageReference actualUrl = provider.initiate(authProviderConfiguration, STATE_TO_PROPOGATE); 
        Test.stopTest();

        System.assertEquals(expectedUrl.getParameters().get('state'), actualUrl.getParameters().get('state'), 'GET Url must be same as per expected');
    }
    
    @isTest
    static void testHandleCallback() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
       	final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonGoodToken,200)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
        
        Test.startTest();
            final Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN, SECRET, STATE); 
        Test.stopTest();

        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider, 'Auth provider must be same as expected'); 
        System.assertEquals(expectedAuthProvResponse.oauthToken, actualAuthProvResponse.oauthToken, 'Oauth Token must be same as expected'); 
    }

    @isTest
    static void testHandleCallbackJson() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        // override the USE_JSON config
        authProviderConfiguration.put(FCSAuthProvider.CMT_FIELD_USE_JSON,'True');

        final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonGoodToken,200)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
        
        Test.startTest();
            final Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN, SECRET, STATE); 
        Test.stopTest();

        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider, 'Expected must be same auth provider'); 
        System.assertEquals(expectedAuthProvResponse.oauthToken, actualAuthProvResponse.oauthToken, 'Expected to be get oauth token'); 
        System.assertEquals(expectedAuthProvResponse.state, actualAuthProvResponse.state,  'Expected to be same state'); 

    }

    @isTest
    static void testHandleCallbackError1() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonError1,401)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        try {
            final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
            System.Assert(false,'Should throw an Exception on bad http return code!');
        } catch (FCSAuthProvider.TokenException ex) {
            System.AssertEquals('Authorization has been denied for this request.',ex.getMessage(), 'Must thrown an excpetion');
        }
    }

    @isTest
    static void testHandleCallbackError2() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonError2,401)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        try {
            final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
            System.Assert(false,'Should throw an Exception on bad http return code!');
        } catch (FCSAuthProvider.TokenException ex) {
            System.AssertEquals('invalid_client',ex.getMessage(), 'Must throw exception for invalid token.');
        }
    } 

    @isTest
    static void testRefresh() {

        final Map<String,String> config = setupAuthProviderConfig(); 
        final FCSAuthProvider provider = new FCSAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonGoodToken,200)); 

        Test.startTest();
            final Auth.OAuthRefreshResult actual = provider.refresh(config,'myUnusedRefreshToken');        
        
            final Auth.OAuthRefreshResult expected = new Auth.OAuthRefreshResult(ACCESS_TOKEN, TOKEN_TYPE); 
        Test.stopTest();

        System.assertEquals(expected.accessToken, actual.accessToken, 'Expected access token on expire'); 
        System.assertEquals(expected.error,actual.error, 'Expected access token error for expire');
    } 
    
    @isTest
    static void testGetUserInfo() 
    { 
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        FCSAuthProvider provider = new FCSAuthProvider(); 
    
        Test.setMock(HttpCalloutMock.class, new FCSMockService(jsonGoodToken,200)); 
    
        Auth.AuthProviderTokenResponse response = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN ,'sampleOauthSecret', STATE); 
        Auth.UserData actualUserData = provider.getUserInfo(authProviderConfiguration, response) ; 
    
        Map<String,String> provMap = new Map<String,String>(); 
        provMap.put('key1', 'value1'); 
        provMap.put('key2', 'value2'); 

        Test.startTest();
            final Auth.UserData expectedUserData = new Auth.UserData(
                null // identifier
                , null // firstName
                , null // lastName
                , null  // fullName
                , null // email
                , null // link
                , null // userNAme
                , null  // locale
                , PROVIDER_NAME  // provider
                , null // siteLoginUrl
                , new Map<String,String>()); 
        Test.stopTest();

        System.assertNotEquals(actualUserData,null, 'Must return user'); 
        System.assertEquals(expectedUserData.provider, actualUserData.provider, 'Must return actual provider'); 
    } 

    @isTest
    static void testEncodingAsJSON() {
        final Map<String,String> config = setupAuthProviderConfig();

        final String output = new FCSAuthProvider().encodeParameters(config,FCSAuthProvider.ENCODING_JSON);

        Test.startTest();
            // Assert that we deserialize parse the output as JSON
            Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(output);
        Test.stopTest();

        System.AssertEquals('password', (String)result.get(FCSAuthProvider.GRANT_TYPE_PARAM),'Grant type did not match');
        System.AssertEquals(KEY, (String)result.get(FCSAuthProvider.CLIENT_ID_PARAM),'Client key did not match');
        System.AssertEquals(SECRET, (String)result.get(FCSAuthProvider.CLIENT_SECRET_PARAM), 'Client secret did not match');
        System.AssertEquals(SCOPE, (String)result.get(FCSAuthProvider.SCOPE_PARAM),'Scope did not match');
    }

    @isTest
    static void testTokenMembers() {
        Test.startTest();
            FCSAuthProvider.TokenResponse resp = (FCSAuthProvider.TokenResponse) JSON.deserialize(
               jsonGoodToken, FCSAuthProvider.TokenResponse.class);
        Test.stopTest();

        System.AssertEquals('Bearer',resp.token_type, 'Token type must be expected as Bearer');
        System.AssertEquals('gv6iCx48OaH76ufiGXBUlRTRTXxx',resp.access_token, 'Must get access token');
        System.AssertEquals('3600',resp.expires_in, 'Must be expire in 1 hour');
    }

    @isTest
    static void testErrorBad() {
        Test.startTest();
            FCSAuthProvider.TokenResponse resp = (FCSAuthProvider.TokenResponse) JSON.deserialize(
                jsonGoodToken, FCSAuthProvider.TokenResponse.class);
        Test.stopTest();

        System.AssertEquals(null,resp.getErrorMessage(), 'Error must be null for token response');
    }
    
     
    // Implement a mock http response generator for FCS. 
    public class FCSMockService implements HttpCalloutMock 
    {
        String jsonResponse;
        Integer httpCode;

        public FCSMockService(String json, Integer code) {
            this.jsonResponse=json;
            this.httpCode=code;
        }

        public HTTPResponse respond(HTTPRequest req) 
        { 
            if (req.getHeader(CONTENT_TYPE_HEADER)==CONTENT_TYPE_JSON) {
                // Check for Query Parameters on the Endpoint URL and if any are found, return a server error
                final PageReference endpoint = new PageReference(req.getEndpoint());
                if (endpoint.getParameters().size() > 0) {
                    this.httpCode=500;
                    this.jsonResponse= jsonBadQueryParam;
                }
            }
            // Create the response 
            HttpResponse res = new HttpResponse(); 
            res.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON); 
            res.setBody(jsonResponse); 
            res.setStatusCode(this.httpCode); 
            return res; 
        } 
    }
}