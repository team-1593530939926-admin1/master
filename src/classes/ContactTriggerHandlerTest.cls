@isTest
private class ContactTriggerHandlerTest {

    private static ContactTriggerHandler contactTriggerHandlerInstance = new ContactTriggerHandler();
    private static List<sObject> listOfNewSObject = new List<sObject>();
    private static Map<Id, sObject> mapOfOldObject = new Map<Id, sObject>();

    @TestSetup
    static void testSetup() {
        TestUtil.createContactData();
    }

    private static Contact getContactRecord() {
        return [SELECT Id, FirstName, LastName, Relationship_Type__c FROM Contact WHERE LastName = 'LastName' LIMIT 1];
    }

    // This method is used to test ContactTriggerHandler Apex Class afterInsert and afterUpdate methods.

    @isTest
    private static void testAfterInsertAndAfterUpdate()
    {
        List<User> listOfValidUser1 = [select Id ,UserRoleId from user];

        Test.startTest();
            TestUtil.CreatePartnerUserAccount();
        Test.stopTest();
        
        List<User> listOfValidUser2 = [select Id ,UserRoleId from user];
        
		System.assertEquals(listOfValidUser1.size()+1, listOfValidUser2.size());
    }
    
    @isTest 
    private static void beforeInsertTest() {

        Test.startTest();
            contactTriggerHandlerInstance.beforeInsert(listOfNewSObject);
        Test.stopTest();
    }

    /*@isTest
    private static void afterInsertTest() {
        Contact contactInstance = getContactRecord();

        Test.startTest();
            contactTriggerHandlerInstance.afterInsert(new List<Contact>{contactInstance});
        Test.stopTest();
    }*/

    @isTest
    private static void beforeUpdateTest() {

        Test.startTest();
            contactTriggerHandlerInstance.beforeUpdate(listOfNewSObject, mapOfOldObject);
        Test.stopTest();
    }

    /*@isTest
    private static void afterUpdateTest() {
        Contact contactInstance = getContactRecord();
        List<Contact> listOfContacts = new List<Contact>{contactInstance};
        Map<Id,Contact> mapOfContactsVsId = new Map<Id,Contact>(listOfContacts);

        Test.startTest();
            contactTriggerHandlerInstance.afterUpdate(listOfContacts, mapOfContactsVsId);
        Test.stopTest();
    }*/

    @isTest
    private static void beforeDeleteTest() {

        Test.startTest();
            contactTriggerHandlerInstance.beforeDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterDeleteTest() {

        Test.startTest();
            contactTriggerHandlerInstance.afterDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUnDeleteTest() {

        Test.startTest();
            contactTriggerHandlerInstance.afterUnDelete(listOfNewSObject);
        Test.stopTest();
    }
}