@isTest
private class LeadTriggerHandlerTest {

    private static LeadTriggerHandler leadTriggerInstance = new LeadTriggerHandler();
    private static List<sObject> listOfNewSObject = new List<sObject>();
    private static Map<Id, sObject> mapOfOldObject = new Map<Id, sObject>();
    
    
    @testSetup
    private static void testSetup() {
        List<Lead> listOfLeads = TestUtil.createLeadData(1, true);
        List<FCS_Entity_Information__c> listOfEntityInfo = FCSTestDataFactory.generateFcsEntityInfoRecord(1, false);
        listOfEntityInfo.get(0).Lead__c = listOfLeads.get(0).Id;
        insert listOfEntityInfo;
    }
    
    private static List<Lead> getLeadsRecord () {
        return [SELECT Id, LastName, Company, Street, PostalCode, City, State, Country, Source__c, Location__c, IsConverted, ConvertedAccountId FROM Lead];
    }
    
    @isTest
    private static void beforeInsertTest() {
        Test.startTest();
            leadTriggerInstance.beforeInsert(listOfNewSObject);
        Test.stopTest();
    }

    @isTest
    private static void afterInsertTest() {
        Test.startTest();
            leadTriggerInstance.afterInsert(listOfNewSObject);
        Test.stopTest();
    }

    @isTest
    private static void beforeUpdateTest() {
        Test.startTest();
            leadTriggerInstance.beforeUpdate(listOfNewSObject, mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUpdateTest() {
        List<Lead> listOfLeadRecords = getLeadsRecord();
        Map<Id, Lead> mapOfLeadVsId = new Map<Id, Lead> (listOfLeadRecords);
        Test.startTest();
            leadTriggerInstance.afterUpdate(listOfLeadRecords, mapOfLeadVsId);
        Test.stopTest();
        listOfLeadRecords = getLeadsRecord();
        List<FCS_Entity_Information__c> listOfEntityInfo = [SELECT Id, Name FROM FCS_Entity_Information__c WHERE Company__c =:listOfLeadRecords.get(0).ConvertedAccountId ];
        
        System.assertNotEquals(listOfEntityInfo, null, 'Account conversion cannot be null');
        System.assertEquals(1, listOfEntityInfo.size(), 'Entity Information must linked with Account');
    }

    @isTest
    private static void beforeDeleteTest() {
        Test.startTest();
            leadTriggerInstance.beforeDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterDeleteTest() {
        Test.startTest();
            leadTriggerInstance.afterDelete(mapOfOldObject);
        Test.stopTest();
    }

    @isTest
    private static void afterUnDeleteTest() {
        Test.startTest();
            leadTriggerInstance.afterUnDelete(listOfNewSObject);
        Test.stopTest();
    }
}