@isTest
private class CvfrelationshipsTest 
{
    @testSetup
    private static void testSetupData()
    {
        TestUtil.createRelationshipRelatedOpportunityData();
    }
    
    private static List<Opportunity> getOpportunity()
    {
        return [SELECT Id, Application_Id__c FROM Opportunity];
    }
    
    @isTest
    private static void testGetListOfRelationshipIfApplicationIdIsAccepted()
    {
        List<Opportunity> listOfOpportunity = getOpportunity();
        Opportunity opportunityRecord = listOfOpportunity[0];
        
        Test.startTest();
             List<Relationship__c> listOfrelationships = CvfRelationships.getListOfRelationship(opportunityRecord.Application_Id__c);
        Test.stopTest(); 
        System.assertEquals(1, listOfrelationships.size());
    } 
    
    @isTest
    private static void testGetListOfRelationshipIfApplicationIdIsNotAccepted()
    {
        Test.startTest();
             List<Relationship__c> listOfrelationships = CvfRelationships.getListOfRelationship('');
        Test.stopTest();  
        System.assertEquals(0, listOfrelationships.size());
    }    

}