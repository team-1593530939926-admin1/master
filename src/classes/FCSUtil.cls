public class FCSUtil {

    private static final String PATTERN_FOR_STRING_FCS_DATE = '(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s+\\d{1,2},\\s+\\d{4}';
    private static final String PATTERN_FOR_STRING_DATE_WITH_FULL_MONTH_NAME = '(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\\s+\\d{1,2},\\s+\\d{4}';
    
    /**
     * Method use to convert FCS Date with 'Jul 1, 2008' format to salesforce 
     * date format.
     */
    public static Date convertFCSDateInSFDate(string dateString) {
        Map < String, Integer > MapMonthList = new Map < String, Integer > {
            'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4,
            'May' => 5, 'Jun' => 6, 'Jul' => 7, 'Aug' => 8,
            'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12
        };

        if (String.isNotBlank(dateString) && isDateStringMatchPattern(dateString, PATTERN_FOR_STRING_FCS_DATE)) {
            String[] dateStringSplitArr = dateString.split(' ');
            String month = String.ValueOf(MapMonthList.get(dateStringSplitArr.get(0)));
            String day = dateStringSplitArr.get(1).replace(',', '');
            String year = dateStringSplitArr.get(2); 	 
            String sfDateFormat = month + '/' + day + '/' + year;
            return Date.parse(sfDateFormat);
        }
        return null;
	}

	private static Boolean isDateStringMatchPattern (String dateString, final String datePatternRegx) {
		Pattern datePattern = Pattern.compile(datePatternRegx);
		Matcher datePatternMatcher = datePattern.matcher(dateString);
		return datePatternMatcher.matches();
	}

	public static String generateRandomKey(String keyFor) {
		String hashString = keyFor + String.valueOf(Datetime.now().getTime()) + String.valueof((Math.random() * 10000));
		Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
		return hashString;
	}
}