public without sharing class BrokerApplicationFormController {
    
    private static SObjectDAO.ISObjectDAO sObjectDAOInstance = new SObjectDAO();
    private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    private static final AutomationApplicantInfoDAO.IAutomationApplicantInfoDAO automationDAOInstance = new AutomationApplicantInfoDAO();
    private static final ContactDAO.IContactDAO contactDAOInstance = new ContactDAO(); 
    private static final OpportunityDAO.IOpportunityDAO opportunityDAOInstance = new OpportunityDAO();
    private static final AdditionalAddressDAO.IAdditionalAddressDAO additionalAddressDAOInstance = new AdditionalAddressDAO();
    private static final RelationshipDAO.IRelationshipDAO relationshipDAOInstance = new RelationshipDAO();
    private static final BankReferenceDAO.IBankReferenceDAO bankReferenceDAO = new BankReferenceDAO();
    private static final FCSCommunicationAddressDAO.IFCSCommunicationAddressDAO fcsCommunicationAddressDAOInstance = new FCSCommunicationAddressDAO();
    private static final ContentVersionDAO.IContentVersionDAO contentVersionDAOInstance = new ContentVersionDAO();
    private static final Id BUSINESS_CONTACT_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Contact', 'Crestmark Contact');
    
    public class PicklistWrapper
    {
        @AuraEnabled
        public String label{get; set;}
        @AuraEnabled
        public String value{get; set;}
        
        public PicklistWrapper(String label, String value) {
            this.label = label;
            this.value = value; 
        }
    }   
    
    @AuraEnabled(cacheable=true)
    public static List<PicklistWrapper> getDescriptionOptionValues()
    {
        List<CVF_Equipment_Description__c> descOptions = CVFCustomSettings.getDescriptionOptionValues();
        List<PicklistWrapper> listOfOptions = new List<PicklistWrapper>();
        for(CVF_Equipment_Description__c Options : descOptions)
        {
            listOfOptions.add(new PicklistWrapper(Options.Name, Options.Name)); 
        }
        return listOfOptions;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<PicklistWrapper> getLeaseTermOptionValues()
    {
        List<PicklistWrapper> listOfOptions = new List<PicklistWrapper>();
        Schema.DescribeFieldResult fieldResult = Opportunity.Lease_Term_Request_1__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.getLabel() != 'Other'){
                listOfOptions.add(new PicklistWrapper(pickListVal.getLabel(), pickListVal.getLabel()));
            }
            
        }     
        return listOfOptions;
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String, String> getFileExtensionValues()
    {
        List<CVF_File_Extensions__c> fileExtensionList = CVFCustomSettings.getFilExtensionValues();
        System.debug('hi  :  '+fileExtensionList);
        Map<String, String> mapOfFileExtensions = new  Map<String, String>();
        
        for(CVF_File_Extensions__c fileExtList : fileExtensionList)
        {            
            mapOfFileExtensions.put(fileExtList.Name, fileExtList.File_Extension__c);            
        }
        return mapOfFileExtensions;
    }
    
    @AuraEnabled(cacheable=true)
    public static Map<String, List<PicklistWrapper>> getCountryAndStateValues()
    {
        List<Country_State_Mapping__c> stateCountryList = CVFCustomSettings.getCountryAndStateValues();
        Map<String, List<PicklistWrapper>> mapOfCountryStateValues = new Map<String, List<PicklistWrapper>>();
        
        for(Country_State_Mapping__c countryVar : stateCountryList)
        {
            if(mapOfCountryStateValues.containsKey(countryVar.Country_Name__c))
            {
                List<PicklistWrapper> tempStates = mapOfCountryStateValues.get(countryVar.Country_Name__c);
                tempStates.add(new PicklistWrapper(countryVar.State_Label__c,countryVar.State_Code__c));
                mapOfCountryStateValues.put(countryVar.Country_Name__c,tempStates);
                
            }
            else{
                List<PicklistWrapper> tempStates = new List<PicklistWrapper>();
                tempStates.add(new PicklistWrapper(countryVar.State_Label__c,countryVar.State_Code__c));
                mapOfCountryStateValues.put(countryVar.Country_Name__c,tempStates);            
            }
        }
        
        return mapOfCountryStateValues;
    }   
    
    
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getCurrentCompanyRecordTypeId()
    {
        RecordTypeWrappers recId = Accounts.getBusinessRecordTypeId();
        return recId;
    }    
    
    
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getCurrentDealRecordTypeId()
    {
        RecordTypeWrappers recId = Opportunities.getCvfRecordTypeId();
        return recId;
    } 
    
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getGuarantorRecordTypeId()
    {
        RecordTypeWrappers recId = getRecordTypeId();
        return recId;
    }
    
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getRecordTypeId()
    {
        RecordTypeWrappers wrapper = new RecordTypeWrappers();
        wrapper.recordTypeId = BUSINESS_CONTACT_RECORD_TYPE_ID;
        return wrapper;
    }
    
    @AuraEnabled
    public static ResultData submitBrokerApplication(string recordValues){
        if(String.isEmpty(recordValues)){
            return null;
        }
        // Declaration
        Savepoint sp;
        ResultData resultWrapper = new ResultData();
        resultWrapper.isPaynetApplicable = false;
        List<List<sObject>> recordsToInsert = new List<List<sObject>>();
        List<Account> listOfAccountToInsert = new List<Account>();
        List<Contact> listOfContactToInsert = new List<Contact>();
        List<Account> listOfAccountToUpsert = new List<Account>();
        List<Contact> listOfContactToUpsert = new List<Contact>();
        List<Relationship__c> listOfRelationshipToInsert = new List<Relationship__c>();
        List<Additional_Address__c> listOfAddAddressesToInsert = new List<Additional_Address__c>();
        List<Communication_Address__c> listOfCommAddressesToInsert = new List<Communication_Address__c>();
        List<Opportunity> listOfDealToInsert = new List<Opportunity>();
        List<Bank_Reference__c> listOfBankDetailsToInsert = new List<Bank_Reference__c>();
        Account companyRecord = new Account(); 
        Contact companyContactRecord = new Contact();
        Additional_Address__c companyAddressRecord = new Additional_Address__c();
        Opportunity equipmentRecord = new Opportunity(); 
        Communication_Address__c equipmentAddressRecord = new Communication_Address__c();
        
        BusinessApplicationWrapper businessApplicationRecord = (BusinessApplicationWrapper)JSON.deserialize(recordValues,BusinessApplicationWrapper.Class);
        //businessApplicationRecord.companyInformationWrapper.dateOfIncorporation = String.valueOf(getDateFromString(businessApplicationRecord.companyInformationWrapper.dateOfIncorporation));///////
        CompanyInformationWrapper companyRecordWrapper = new CompanyInformationWrapper(businessApplicationRecord.companyInformationWrapper);
        companyRecord = companyRecordWrapper.companyInformation;
        companyContactRecord = companyRecordWrapper.companyContactInformation;
        companyAddressRecord = companyRecordWrapper.companyAddressInformation;
        
        
        if(!String.isEmpty(companyContactRecord.LastName) && companyRecordWrapper.existingId == null){
            listOfContactToInsert.add(companyContactRecord);
        }  
        if(!String.isEmpty(companyAddressRecord.State__c) && companyRecordWrapper.existingId == null){
            listOfAddAddressesToInsert.add(companyAddressRecord);
        } 
        
        
        EquipmentInformationWrapper equipmentRecordWrapper = new EquipmentInformationWrapper(businessApplicationRecord.equipmentInformationWrapper, companyRecord);
        equipmentRecord = equipmentRecordWrapper.dealInformation;
        equipmentAddressRecord = equipmentRecordWrapper.otherDealAddress;
        
        Integer count = 1;
        listOfCommAddressesToInsert.add(equipmentAddressRecord);
        for(PartnerDetailsWrapper wraperInfo : businessApplicationRecord.partnerInformationWrapper){
            if(!String.isEmpty(wraperInfo.lastName) && (wraperInfo.existingId == null || String.isEmpty(wraperInfo.existingId))){
                listOfContactToInsert.add(new PartnerDetailsWrapper(wraperInfo, companyRecord).partnerInformation);
            }
        }   
        
        for(BankDetailsWrapper wraperInfo : businessApplicationRecord.bankInformationWrapper){
            if(!String.isEmpty(wraperInfo.accountNumber) && (wraperInfo.existingId == null || String.isEmpty(wraperInfo.existingId))){
                listOfBankDetailsToInsert.add(new BankDetailsWrapper(wraperInfo, companyRecord).dealBankDetails);
            }
        }
        GuarantorWrapper guarantorInfoInstance;
        count = 1;
        for(GuarantorWrapper wraperInfo : businessApplicationRecord.guarantorInformationWrapper){
            if(!String.isEmpty(wraperInfo.mailingStateGuarantor)){
                if(wraperInfo.guarantorType == 'Individual'){
                    guarantorInfoInstance = new GuarantorWrapper(wraperInfo, equipmentRecord.Name, true, 'G' + String.valueOf(count));
                    if(String.isEmpty(guarantorInfoInstance.existingId)){
                        guarantorInfoInstance.dealGuarantorInfo.Account = new Account(External_System_Id__c = companyRecord.External_System_Id__c);
                        listOfContactToInsert.add(guarantorInfoInstance.dealGuarantorInfo);
                        if(guarantorInfoInstance.gurantorAddress != null){
                            listOfCommAddressesToInsert.add(guarantorInfoInstance.gurantorAddress);
                        }
                    }else{
                        listOfContactToUpsert.add(guarantorInfoInstance.dealGuarantorInfo);
                        if(guarantorInfoInstance.gurantorAddress != null){
                            listOfCommAddressesToInsert.add(guarantorInfoInstance.gurantorAddress);
                        }
                    }                    
                    listOfRelationshipToInsert.add(guarantorInfoInstance.guarantorRelationship);
                }else{
                    guarantorInfoInstance = new GuarantorWrapper(wraperInfo, equipmentRecord.Name, false, 'G' + String.valueOf(count));
                    if(String.isEmpty(guarantorInfoInstance.existingId)){
                        listOfAccountToInsert.add(guarantorInfoInstance.dealCorporateGuarantorInfo);
                        if(guarantorInfoInstance.dealGuarantorInfo != null && !String.isEmpty(guarantorInfoInstance.dealGuarantorInfo.LastName)){
                            listOfContactToInsert.add(guarantorInfoInstance.dealGuarantorInfo);
                        }
                    }                    
                    listOfRelationshipToInsert.add(guarantorInfoInstance.guarantorRelationship);
                }
                count++;
            }
        }
        try{
            sp = Database.setSavepoint();
            
            //accountDAOInstance.insertAccounts(new List<Account>{companyRecord}); 
            upsert companyRecord;
            
            
            equipmentRecord.AccountId = companyRecord.Id;
            opportunityDAOInstance.insertOpportunities(new List<Opportunity>{equipmentRecord}); 
            
            
            TradeReferenceWrapper tradeWrapperInfo;
            count = 1;
            for(TradeReferenceWrapper wraperInfo : businessApplicationRecord.tradeInformationWrapper){
                if(!String.isEmpty(wraperInfo.lastNameTrade)){
                    tradeWrapperInfo = new TradeReferenceWrapper(wraperInfo, companyRecord, count);
                    if(String.isEmpty(tradeWrapperInfo.existingId) || tradeWrapperInfo.existingId == null){
                        listOfAccountToInsert.add(tradeWrapperInfo.dealTradeInfo);
                        if(!String.isEmpty(tradeWrapperInfo.dealTradeContactInfo.LastName)){
                            listOfContactToInsert.add(tradeWrapperInfo.dealTradeContactInfo);
                        } 
                    }else{
                        listOfAccountToUpsert.add(tradeWrapperInfo.dealTradeInfo);
                        if(!String.isEmpty(tradeWrapperInfo.dealTradeContactInfo.LastName)){
                            listOfContactToUpsert.add(tradeWrapperInfo.dealTradeContactInfo);
                        } 
                    }                   
                    count++;
                }
            }
            if(!listOfAccountToInsert.isEmpty())
            {
                accountDAOInstance.insertAccounts(listOfAccountToInsert);
            }              
            
            
            
            if(!listOfContactToInsert.isEmpty())
            {
                contactDAOInstance.insertContacts(listOfContactToInsert);
            }
            
            
            if(!listOfAddAddressesToInsert.isEmpty())
            {
                additionalAddressDAOInstance.insertAdditionalAddress(listOfAddAddressesToInsert);
            }
            
            if(!listOfCommAddressesToInsert.isEmpty())
            {
                fcsCommunicationAddressDAOInstance.saveCommunicationAddress(listOfCommAddressesToInsert);
            }
            
            
            if(!listOfRelationshipToInsert.isEmpty())
            {
                relationshipDAOInstance.insertRelationships(listOfRelationshipToInsert);
            }
            
            
            
            if(!listOfBankDetailsToInsert.isEmpty())
            {
                bankReferenceDAO.insertBankReference(listOfBankDetailsToInsert);
            }
            if(!listOfAccountToUpsert.isEmpty())
            {
                upsert listOfAccountToUpsert;
            }
            if(!listOfContactToUpsert.isEmpty())
            {
                upsert listOfContactToUpsert;
            }
            Opportunity dealData = Opportunities.getOpportunityById(equipmentRecord.Id);
            if(!Test.isRunningTest() 
               && businessApplicationRecord.entityInformation != null
               && businessApplicationRecord.entityInformation.listOfResultWrapper != null
               && companyRecordWrapper.ismultipleEntityPresent == false)
            {
                
                List<FCSEntityVerifications.FCSEntityVerificationWrapper> listOfFCSEntityVerificationWrapper = 
                    FCSEntityVerifications.generateEntityObjectFromWrapper(businessApplicationRecord.entityInformation);
                
                if (!listOfFCSEntityVerificationWrapper.isEmpty() ) 
                {
                    FCSEntityVerifications.saveEntityVerificationInformation(listOfFCSEntityVerificationWrapper, companyRecord.Id);
                } 
            }
            
            List<ContentVersion> listOfDocuments = new List<ContentVersion>();
            List<ContentDocumentLink> listOfContentDocumentLink = new List<ContentDocumentLink>();
            ContentDocumentLink cDocLink;
            if(!businessApplicationRecord.fileDetailData.isEmpty())
            {
                listOfDocuments = contentVersionDAOInstance.getListOfContentVersion(businessApplicationRecord.fileDetailData); 
                
                for(ContentVersion contentDocumentRecord : listOfDocuments )
                {
                    contentDocumentRecord.isBrokerUploaded__c = false;
                    cDocLink = new ContentDocumentLink();
                    cDocLink.ContentDocumentId = contentDocumentRecord.ContentDocumentId;//Add ContentDocumentId
                    cDocLink.LinkedEntityId = dealData.Id;
                    cDocLink.ShareType = 'V';
                    cDocLink.Visibility = 'AllUsers';
                    listOfContentDocumentLink.add(cDocLink);
                }
            }
            
            if(!listOfDocuments.isEmpty())
            {
                update listOfDocuments;
                insert listOfContentDocumentLink;
            }
            resultWrapper.applicationId = dealData.Application_Id__c;
            resultWrapper.companyId = dealData.AccountId;
            resultWrapper.dealId = dealData.Id;
            resultWrapper.companyCode = dealData.Account.Company_Code__c;
            if(dealData != null && !String.isEmpty(dealData.Account.TamarackPI__Paynet_Id__c)){
                resultWrapper.isPaynetApplicable = false;
                TC_StartServiceAutomation.start(dealData.Id);
            }else{
                resultWrapper.isPaynetApplicable = true;
            }
            
            //return 'Application ' + dealData.Application_Id__c;
            return resultWrapper;
        }
        catch(System.DmlException de)
        {
            Database.rollback(sp);
            TransactionLogHandler.doHandleException(de , 'BrokerApplicationFormController');
            throw new AuraHandledException(de.getMessage());
        }
        catch(System.Exception ex)
        {
            Database.rollback(sp);
            TransactionLogHandler.doHandleException(ex , 'BrokerApplicationFormController');
            throw new AuraHandledException(ex.getMessage());  
        }  
        
    }
    
    
    @AuraEnabled
    public static BusinessApplicationWrapper reviewBrokerApplication(string recordValues){
        if(String.isEmpty(recordValues))  {
            return null;
        }
        
        // Convert Json
        BusinessApplicationWrapper businessApplicationRecord = (BusinessApplicationWrapper)JSON.deserialize(recordValues,BusinessApplicationWrapper.Class);
        // Wrapper Declaration
        BusinessApplicationWrapper businessApplicationReview = new BusinessApplicationWrapper();
        // Wrapper Assignment
        businessApplicationReview.companyWrapper = new CompanyInformationWrapper(businessApplicationRecord.companyInformationWrapper, true);
        // businessApplicationReview.companyWrapper.dateOfIncorporation = String.valueOf(getDateFromString(businessApplicationReview.companyWrapper.dateOfIncorporation));///////
        List<PartnerDetailsWrapper> listOfPartnerDetailsWrapper = new List<PartnerDetailsWrapper>();
        List<TradeReferenceWrapper> listOfTradeDetailWrapper = new List<TradeReferenceWrapper>();
        List<BankDetailsWrapper> listOfBankDetailWrapper = new List<BankDetailsWrapper>();
        List<GuarantorWrapper> listOfGuarantorDetailWrapper = new List<GuarantorWrapper>();
        List<ContentVersion> listOfContentVersion = new List<ContentVersion>();
        
        for(PartnerDetailsWrapper wraperInfo : businessApplicationRecord.partnerInformationWrapper){
            if(!String.isEmpty(wraperInfo.lastName)){
                listOfPartnerDetailsWrapper.add(new PartnerDetailsWrapper(wraperInfo, true));
            }
        }
        for(TradeReferenceWrapper wraperInfo : businessApplicationRecord.tradeInformationWrapper){
            if(!String.isEmpty(wraperInfo.companyName)){
                listOfTradeDetailWrapper.add(new TradeReferenceWrapper(wraperInfo, true));
            }
        }
        for(BankDetailsWrapper wraperInfo : businessApplicationRecord.bankInformationWrapper){
            if(!String.isEmpty(wraperInfo.bankName)){
                listOfBankDetailWrapper.add(new BankDetailsWrapper(wraperInfo, true));
            }
        }
        for(GuarantorWrapper wraperInfo : businessApplicationRecord.guarantorInformationWrapper){
            if(!String.isEmpty(wraperInfo.mailingStateGuarantor)){
                listOfGuarantorDetailWrapper.add(new GuarantorWrapper(wraperInfo, true));
            }            
        }
        
        if (businessApplicationRecord.fileDetailData != null 
            && !businessApplicationRecord.fileDetailData.isEmpty()) {
                // need to call DAO to fetch content version with ids ;
                listOfContentVersion = contentVersionDAOInstance.getListOfContentVersion(businessApplicationRecord.fileDetailData); 
            }
        
        businessApplicationReview.partnerWrapper = listOfPartnerDetailsWrapper;
        businessApplicationReview.tradeWrapper = listOfTradeDetailWrapper;
        businessApplicationReview.bankWrapper = listOfBankDetailWrapper;
        businessApplicationReview.equipmentWrapper = new EquipmentInformationWrapper(businessApplicationRecord.equipmentInformationWrapper, true);
        businessApplicationReview.guarantorWrapper = listOfGuarantorDetailWrapper;
        businessApplicationReview.listOfDocuments = listOfContentVersion;
        return businessApplicationReview;
    }
    
    @AuraEnabled(cacheable=true)
    public static BusinessApplicationWrapper getCompanyRelatedDataByAddress(String companyName, String address, String city, String state, String Country, String zip)
    {
        
        return null;
    } 
    
    
    @AuraEnabled(cacheable=true)
    public static TradeReferenceWrapper getTradeInformationByCompanyCode( String dealCompanyCode, String count)
    {
        Account accountRecord = Accounts.getAccountRecordFromCompanyId(dealCompanyCode);
        TradeReferenceWrapper trInstance;
        trInstance = new TradeReferenceWrapper(accountRecord);
        trInstance.isDeleteRequire = true;
        trInstance.isClearButtonVisible = true;
        if(count == '0'){
            trInstance.isDeleteRequire = false;
            trInstance.isClearButtonVisible = false;
        }
        trInstance.rowId = Integer.valueOf(count);
        trInstance.count = Integer.valueOf(count)+ 1;
        
        trInstance.isDisable = true;
        return trInstance;
    } 
    
    @AuraEnabled(cacheable=true)
    public static BusinessApplicationWrapper getInformationByCompanyCode( String dealCompanyCode)
    {
        Account companyRecord = Accounts.getAccountRecordFromCompanyCode(dealCompanyCode);
        List<Account> listOfTrade = Accounts.getAccountRecordFromApplicationId(dealCompanyCode);
        
        //Initialize Wrappers
        BusinessApplicationWrapper instanceOfBusinessWrapper = new BusinessApplicationWrapper();        
        List<PartnerDetailsWrapper> listOfPartnerWrapper = new List<PartnerDetailsWrapper>(); 
        List<TradeReferenceWrapper> listOfTradeReferenceWrapper = new List<TradeReferenceWrapper>();
        List<BankDetailsWrapper> listOfBankDetailsWrapper = new List<BankDetailsWrapper>();
        Integer count = 0;
        // Assign Data In Instance
        if(companyRecord != null)
        {
            PartnerDetailsWrapper prInstance;
            count = 0;
            for(Contact contactRecord : companyRecord.Partners__r)
            {
                prInstance = new PartnerDetailsWrapper(contactRecord);
                prInstance.rowId = count;
                prInstance.count = count + 1;
                prInstance.isDisable = true;
                listOfPartnerWrapper.add(prInstance);
                count++;
            }
            count = 0;
            BankDetailsWrapper brInstance;
            for(Bank_Reference__c bankRecord : companyRecord.Bank_Details__r)
            {
                brInstance = new BankDetailsWrapper(bankRecord);
                brInstance.rowId = count;
                brInstance.count = count + 1;
                brInstance.isDisable = true;
                listOfBankDetailsWrapper.add(brInstance);
                count++;
            }
        }
        count = 0;
        TradeReferenceWrapper trInstance;
        for(Account accountRecord : listOfTrade){
            trInstance = new TradeReferenceWrapper(accountRecord);
            trInstance.isClearButtonVisible = true;
            trInstance.rowId = count;
            trInstance.count = count + 1;
            trInstance.isDisable = true;
            listOfTradeReferenceWrapper.add(trInstance);
            count++;
        }
        instanceOfBusinessWrapper.companyInformationWrapper = Accounts.getAccountRecordByCompanyCode(dealCompanyCode); 
        
        if (listOfPartnerWrapper.isEmpty()) {
            listOfPartnerWrapper.add(new PartnerDetailsWrapper(0));
        } 
        
        if (listOfTradeReferenceWrapper.isEmpty()) {
            listOfTradeReferenceWrapper.add(new TradeReferenceWrapper(0));
        }
        
        if (listOfBankDetailsWrapper.isEmpty()) {
            listOfBankDetailsWrapper.add(new BankDetailsWrapper(0));
        }
        instanceOfBusinessWrapper.partnerInformationWrapper = listOfPartnerWrapper;
        instanceOfBusinessWrapper.tradeInformationWrapper = listOfTradeReferenceWrapper;
        instanceOfBusinessWrapper.bankInformationWrapper = listOfBankDetailsWrapper;
        if(companyRecord!=null){
            if(!companyRecord.Entity_Informations__r.isEmpty()){
                instanceOfBusinessWrapper.companyInformationWrapper.ismultipleEntityPresent = true;
            }
            else{
                instanceOfBusinessWrapper.companyInformationWrapper.ismultipleEntityPresent = false;
            }
        }
        return instanceOfBusinessWrapper;
    }    
    
    @AuraEnabled(cacheable=true)
    public static GuarantorWrapper getGuarantorDetailsBasedOnSearch(String objName , String GuarantorId ,string rowId, String count){
        GuarantorWrapper guarantorRecord = new GuarantorWrapper();
        
        if(objName == 'Account'){
            guarantorRecord = Accounts.getGuarantorRecordFromGuarantorId(GuarantorId);
            guarantorRecord.isDeleteRequire = true;
            guarantorRecord.multipleGuarantor = true;
            if(count == '0'){
                guarantorRecord.isDeleteRequire = false;
                guarantorRecord.multipleGuarantor = false;
            }
            guarantorRecord.companyDataRequire = true;
            guarantorRecord.isGuarantor = true;
            guarantorRecord.isReviewEnabled = true;
            guarantorRecord.rowId = Integer.valueOf(count);
            guarantorRecord.count = Integer.valueOf(count)+ 1;
        }
        else if(objName == 'contact'){
            guarantorRecord = Contacts.getGuarantorRecordFromGuarantorId(GuarantorId);
            guarantorRecord.isDeleteRequire = true;
            guarantorRecord.multipleGuarantor = true;
            if(count == '0')
            {
                guarantorRecord.isDeleteRequire = false;
                guarantorRecord.multipleGuarantor = false;
            }
            guarantorRecord.individualDataRequire = true;
            guarantorRecord.isGuarantor = false;
            guarantorRecord.isReviewEnabled = true;
            guarantorRecord.rowId = Integer.valueOf(count);
            guarantorRecord.count = Integer.valueOf(count)+ 1;
        }
        return guarantorRecord;
    }
    
    @AuraEnabled(cacheable=true) 
    public static CompanyInformationWrapper getCompanyIdIfExists(CompanyInformationWrapper companyInfo){
        if(!String.isEmpty(companyInfo.dateOfIncorporation) && !companyInfo.dateOfIncorporation.contains('-')){
            companyInfo.dateOfIncorporation = String.valueOf(getDateFromString(companyInfo.dateOfIncorporation));
        }
        CompanyInformationWrapper companyRecordWrapper = new CompanyInformationWrapper(companyInfo);
        Account accData = accountDAOInstance.getExistingCopmanyDetails(companyRecordWrapper.companyInformation);
        if(accData != null){
            companyRecordWrapper.companyAlreadyExistId = accData.Id;
            if(!accData.Entity_Informations__r.isEmpty()){
                companyRecordWrapper.ismultipleEntityPresent = true;
            }
            else{
                companyRecordWrapper.ismultipleEntityPresent = false;
            }
        }
        return companyRecordWrapper;
        
    }
    
    public static Date getDateFromString(String stringDate){
        Map<String, Integer> mapOfMonth = new Map<String,Integer> {
            'January' => 1, 'February' =>2 , 'March' => 3, 'April' => 4, 'May' => 5, 'June' => 6, 'July' => 7, 'August' => 8, 
                'September' => 9, 'October' => 10, 'November' => 11, 'December' => 12, 'Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr' => 4,
                'Jun' => 5, 'Jul' => 7, 'Aug' => 8, 'Sept' => 9, 'Sep' => 9, 'Oct' => 10, 'Nov' => 11, 'Dec' => 12};
                    if(!String.isEmpty(stringDate)){
                        stringDate = stringDate.replaceAll(',','');
                        List<String> listOfString = stringDate.split(' ');
                        return Date.valueOf(listOfString[2] + '-' + mapOfMonth.get(listOfString[0]) + '-' + listOfString[1]);
                    }else{
                        return null;
                    }
    }
    
    @AuraEnabled
    public static Boolean returnRecordRelatedToDeal(String dealId){
        Boolean status;
        List<Automation_Applicant_Info__c> listOfAutomationApplicant = automationDAOInstance.getRecordRelatedToDeal(dealId , 'Complete');
        if(listOfAutomationApplicant.size() > 0)
        {
            status = true;
            return status;
        }
        else{
            status = false;
            return status;
        }
    }
    
    @AuraEnabled
    public static void updateOpportunity(String dealId){
        Opportunity opportunityRecord = new Opportunity(Id = dealId, Is_Automation_Completed__c = true);
        update opportunityRecord;
    }
    
    @AuraEnabled
    public static String returnRecordRelatedToDealPaynet(String dealId){
        String opportunityId;
        Set<String> setOfIds = new Set<String>();
        setOfIds.add(dealId);
        List<Opportunity> listOfOpportunity = automationDAOInstance.getOpportunityByAccountId(setOfIds);
            if(listOfOpportunity.size() > 0 && listOfOpportunity[0].Automation_Applicant_Infos__r.size() > 0)
            {
                opportunityId = listOfOpportunity[0].Id;
                return opportunityId;
            }
        
        else{
            opportunityId = 'false';
            return opportunityId;
        }
    }
    
    public class ResultData{
        @AuraEnabled
        public String applicationId{get; set;}
        @AuraEnabled
        public String companyId{get; set;}
        @AuraEnabled
        public String dealId{get; set;}
        @AuraEnabled
        public String companyCode{get; set;}
        @AuraEnabled
        public Boolean isPaynetApplicable{get; set;}
    }
    
}