public class CompanyInformationWrapper {
    @AuraEnabled
    public String existingId{get;set;}
    @AuraEnabled
    public String companyCode{get;set;}
    @AuraEnabled
    public String fullLegalCompanyName{get;set;}
    @AuraEnabled
    public String billingStreetAddress{get;set;}
    @AuraEnabled
    public String city{get;set;}
    @AuraEnabled
    public String county{get;set;}
    @AuraEnabled
    public String state{get;set;}
    @AuraEnabled
    public String zip{get;set;}
    @AuraEnabled
    public String phoneNumber{get;set;}
    @AuraEnabled
    public String fax{get;set;}
    @AuraEnabled
    public String federalTaxId{get;set;}
    @AuraEnabled
    public String natureOfBusiness{get;set;}
    @AuraEnabled
    public String dba{get;set;}
    @AuraEnabled
    public String companyType{get;set;}
    @AuraEnabled
    public String stateIncorporated{get;set;}
    @AuraEnabled
    public String otherStreetAddress{get;set;}
    @AuraEnabled
    public String otherCity{get;set;}
    @AuraEnabled
    public String otherCounty{get;set;}
    @AuraEnabled
    public String otherState{get;set;}
    @AuraEnabled
    public String otherZip{get;set;}
    @AuraEnabled
    public String contactFirstName{get;set;} 
    @AuraEnabled
    public String contactLastName{get;set;}
    @AuraEnabled
    public String emailAddress{get;set;}
    @AuraEnabled
    public String errorMessage{get;set;}
    @AuraEnabled
    public String dateOfIncorporation{get;set;}
    @AuraEnabled
    public Boolean nextByPassForSoleDisable{get;set;}
    @AuraEnabled
    public Boolean isCompanySolePropriotor{get;set;}
    @AuraEnabled
    public String companyAlreadyExistId{get;set;}
    @AuraEnabled
    public Boolean ismultipleEntityPresent{get;set;}
    @AuraEnabled
    public ContactInformationWrapper contactInfo{get;set;}
    @AuraEnabled
    public AddressInformationWrapper addressInfo{get;set;}
    public Account companyInformation {get;set;}
    public Contact companyContactInformation {get;set;}
    public Additional_Address__c companyAddressInformation {get;set;}
    public static RecordTypeDAO.IRecordTypeDAO recordTypeDAOInstance = new RecordTypeDAO();
    
    public CompanyInformationWrapper(CompanyInformationWrapper companyInfoWrapper){
        Account companyRecord = new Account();
        System.debug('from companyWrapper calling..   '+companyInfoWrapper);
        if(!String.isEmpty(companyInfoWrapper.existingId)){
            companyRecord.Id = companyInfoWrapper.existingId;
            this.existingId = companyInfoWrapper.existingId;
        }
        companyRecord.Name = companyInfoWrapper.fullLegalCompanyName;
        companyRecord.BillingStreet = companyInfoWrapper.billingStreetAddress;
        companyRecord.BillingCity = companyInfoWrapper.city;
        companyRecord.BillingCountry = companyInfoWrapper.county;
        companyRecord.BillingState = companyInfoWrapper.state;
        companyRecord.BillingPostalCode = companyInfoWrapper.zip;
        companyRecord.Phone = companyInfoWrapper.phoneNumber;
        companyRecord.Fax = companyInfoWrapper.fax;
        companyRecord.Federal_Tax_ID__c = companyInfoWrapper.federalTaxId;
        companyRecord.Industry = companyInfoWrapper.natureOfBusiness;
        companyRecord.DBA__c = companyInfoWrapper.dba;
        companyRecord.Entity_Type__c = companyInfoWrapper.companyType;
        companyRecord.State_Incorporated__c = companyInfoWrapper.stateIncorporated;
        companyRecord.External_System_Id__c = companyInfoWrapper.fullLegalCompanyName + String.valueOf(System.now()); 
         if(!String.isEmpty(companyInfoWrapper.dateOfIncorporation)){
            companyRecord.Date_of_Incorporation__c = date.valueOf(companyInfoWrapper.dateOfIncorporation);
        }
        companyRecord.RecordTypeId = recordTypeDAOInstance.recordTypeIdByObjectNameAndRecordTypeName('Account', 'Business');
        
        Contact contactInstance = new Contact(Account = new Account(External_System_Id__c = companyRecord.External_System_Id__c));        
        contactInstance.FirstName = companyInfoWrapper.contactFirstName;
        contactInstance.LastName = companyInfoWrapper.contactLastName;
        contactInstance.Email = companyInfoWrapper.emailAddress;
        contactInstance.MailingStreet = companyInfoWrapper.billingStreetAddress;
        contactInstance.MailingCity = companyInfoWrapper.city;
        contactInstance.MailingState = companyInfoWrapper.state;
        contactInstance.MailingCountry = companyInfoWrapper.county;
        contactInstance.MailingPostalCode = companyInfoWrapper.zip;
        this.companyContactInformation = contactInstance;
        
        Additional_Address__c addressInstance = new Additional_Address__c(Company__r = new Account(External_System_Id__c = companyRecord.External_System_Id__c));
        addressInstance.Street_Address_1__c = companyInfoWrapper.otherStreetAddress;
        addressInstance.City__c = companyInfoWrapper.otherCity;
        addressInstance.County__c = companyInfoWrapper.otherCounty;
        addressInstance.State__c = companyInfoWrapper.otherState;
        addressInstance.Postal_Code__c = companyInfoWrapper.otherZip;
        addressInstance.Name = 'Other Address';
        
        this.companyAddressInformation = addressInstance;            
        this.companyInformation = companyRecord;
        this.ismultipleEntityPresent = false;
        this.ismultipleEntityPresent = companyInfoWrapper.ismultipleEntityPresent;
    }
    
    
    public CompanyInformationWrapper(){}
    
    public CompanyInformationWrapper(String errorMessage){
        this.errorMessage = errorMessage;
    } 
    
    public CompanyInformationWrapper(Account accountRecord){
        //this.ismultipleEntityPresent = false;
        this.existingId = getValues(accountRecord.Id);
        this.companyCode = getValues(accountRecord.Company_Code__c);
        this.fullLegalCompanyName = getValues(accountRecord.Name);
        this.billingStreetAddress = getValues(accountRecord.BillingStreet);
        this.city = getValues(accountRecord.BillingCity);
        this.county = getValues(accountRecord.BillingCountry);
        this.state = getValues(accountRecord.BillingState);
        this.zip = getValues(accountRecord.BillingPostalCode);
        this.phoneNumber = getValues(accountRecord.Phone);
        this.fax = getValues(accountRecord.Fax);
        this.federalTaxId = getValues(accountRecord.Federal_Tax_ID__c);
        this.natureOfBusiness = getValues(accountRecord.Industry);
        this.dba = getValues(accountRecord.DBA__c);
        this.companyType = getValues(accountRecord.Entity_Type__c);
        this.dateOfIncorporation = accountRecord.Date_of_Incorporation__c != null ? String.valueOf(accountRecord.Date_of_Incorporation__c) : '';
        if(accountRecord.Entity_Type__c == 'Sole Proprietor' && accountRecord.Date_of_Incorporation__c != null)
        {        
            this.nextByPassForSoleDisable = true;
        }
        else{
             this.nextByPassForSoleDisable = false;
        }
        if(accountRecord.Entity_Type__c == 'Sole Proprietor'){
            this.isCompanySolePropriotor = true;
        }else{
            this.isCompanySolePropriotor = false;
        }
        this.stateIncorporated = getValues(accountRecord.State_Incorporated__c);
        
        if(!accountRecord.Contacts.isEmpty()){
            this.contactInfo = new ContactInformationWrapper(accountRecord.Contacts.get(0)); 
            this.contactFirstName = getValues(accountRecord.Contacts.get(0).FirstName);
            this.contactLastName = getValues(accountRecord.Contacts.get(0).LastName);
            this.emailAddress = getValues(accountRecord.Contacts.get(0).Email);
        }else{
            this.contactFirstName = '';
            this.contactLastName = '';
            this.emailAddress = ''; 
        }
        
        if(!accountRecord.Addresses__r.isEmpty()){
            this.otherStreetAddress = getValues(accountRecord.Addresses__r.get(0).Street_Address_1__c);
            this.otherCity = getValues(accountRecord.Addresses__r.get(0).City__c);
            this.otherCounty = getValues(accountRecord.Addresses__r.get(0).County__c);
            this.otherState = getValues(accountRecord.Addresses__r.get(0).State__c);
            this.otherZip = getValues(accountRecord.Addresses__r.get(0).Postal_Code__c); 
        }else{
            this.otherStreetAddress = '';
            this.otherCity = '';
            this.otherCounty = '';
            this.otherState = '';
            this.otherZip = ''; 
        }
        if(!accountRecord.Entity_Informations__r.isEmpty() && !String.isEmpty(this.dateOfIncorporation) && 
           accountRecord.Entity_Informations__r.get(0).Date_Of_Incorporation__c != null ){
            this.dateOfIncorporation = String.valueOf(accountRecord.Entity_Informations__r.get(0).Date_Of_Incorporation__c);
            
        }
        
    }    
    
    public CompanyInformationWrapper(CompanyInformationWrapper companyInfoWrapper, Boolean isReview){
        this.companyCode = companyInfoWrapper.companyCode;
        this.fullLegalCompanyName = companyInfoWrapper.fullLegalCompanyName;
        this.billingStreetAddress = companyInfoWrapper.billingStreetAddress;
        this.city = companyInfoWrapper.city;
        this.county = companyInfoWrapper.county;
        this.state = companyInfoWrapper.state;
        this.zip = companyInfoWrapper.zip;
        this.phoneNumber = companyInfoWrapper.phoneNumber;
        this.fax = companyInfoWrapper.fax;
        this.federalTaxId = companyInfoWrapper.federalTaxId;
        this.natureOfBusiness = companyInfoWrapper.natureOfBusiness;
        this.dba = companyInfoWrapper.dba;
        this.companyType = companyInfoWrapper.companyType;
        this.stateIncorporated = companyInfoWrapper.stateIncorporated;            
        this.contactFirstName = companyInfoWrapper.contactFirstName;
        this.contactLastName = companyInfoWrapper.contactLastName;
        this.emailAddress = companyInfoWrapper.emailAddress;
        this.otherStreetAddress = companyInfoWrapper.otherStreetAddress;
        this.otherCity = companyInfoWrapper.otherCity;
        this.otherCounty = companyInfoWrapper.otherCounty;
        this.otherState = companyInfoWrapper.otherState;
        this.otherZip = companyInfoWrapper.otherZip; 
        this.dateOfIncorporation = companyInfoWrapper.dateOfIncorporation;
        this.existingId = companyInfoWrapper.existingId;
        this.ismultipleEntityPresent = companyInfoWrapper.ismultipleEntityPresent;
    } 
    
    public class ContactInformationWrapper{
        @AuraEnabled
        public String firstName{get;set;}
        @AuraEnabled
        public String lastName{get;set;}
        @AuraEnabled
        public String email{get;set;}
        
        public ContactInformationWrapper(){
            this.firstName = '';
            this.lastName = '';
            this.email = '';
        }
        
        public ContactInformationWrapper(Contact contactRecord){
            this.firstName = contactRecord.FirstName;
            this.lastName = contactRecord.LastName;
            this.email = contactRecord.Email;
        }
    }
    
    private String getValues(String stringVal){
        if(!String.isEmpty(stringVal)){
            return stringVal;
        }else{
            return '';
        }
    }
}