public class LeadTriggerHandler implements TriggerInterface{ 
   
    public void beforeInsert(List<sObject> listOfNewSObject)
    {
        // before insert logic
    }

    public void afterInsert(List<sObject> listOfNewSObject)
    {
        // after insert logic
    }

    public void beforeUpdate(List<sObject> listOfNewSObject, Map<Id, sObject> oldObjMap)
    {
        // before update logic
    }

    public void afterUpdate(List<sObject> listOfNewSObject,  Map<Id, sObject> oldObjMap)
    {
        Leads.moveRelatedInfoToConvertedCompany(
            new Map<Id, Lead> ((List<Lead>)listOfNewSObject), (Map<Id, Lead>) oldObjMap);
    }

    public void beforeDelete(Map<Id, sObject> oldObjMap)
    {
        // before delete logic
    }

    public void afterDelete(Map<Id, sObject> oldObjMap)
    {
        // after delete logic
    }

    public void afterUnDelete(List<sObject> newObjList)
    {
        // after undelete logic
    }
}