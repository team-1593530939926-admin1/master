public with sharing class ProfileDAO implements IProfileDAO {
    
    public Interface IProfileDAO {
        List<Profile> getProfileByName(String profileName);
    }
    
    public static List<Profile> getProfileByName(String profileName){
        return [Select Id, Name From Profile Where Name = : profileName];
    }
}