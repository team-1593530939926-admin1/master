@isTest
private class CompanyInformationWrapperTest { 
    
    @testSetup
    private static void testSetupData() 
    {
        TestUtil.insertBusinessApplicationWrapper();
    }
    
    public static Account getCompany()
    {
        return [SELECT Id, Company_Code__c FROM Account WHERE Name='Test Business Account' LIMIT 1];
    }
    
    @isTest
    private static void testCompanyInformationWrapperIfDataExist()
    {     
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(accountRecord);
        Test.stopTest();
        System.assertEquals('Test Business Account', result.fullLegalCompanyName);
    }
    
    @isTest
    private static void testCompanyInformationWrapperIfDataNotExist()
    {     
        
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper();
        Test.stopTest();
        System.assertEquals(null, result.fullLegalCompanyName);
    }
    
    @isTest
    private static void testCompanyInformationWrapperIfDataIsNull()
    {            
        Account accountRecord = new Account();
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(accountRecord);
        Test.stopTest();
        System.assertEquals('', result.fullLegalCompanyName);
    }
    
    @isTest
    private static void testCompanyInformationWrapperIfErrorExist()
    {            
        String errorString = 'Comapny Code does not exist'; 
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(errorString);
        Test.stopTest();
        System.assertEquals('Comapny Code does not exist', result.errorMessage);
    }
    
    @isTest
    private static void testCompanyDataInputIfWrapperIsAccepted()
    {
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);      
        CompanyInformationWrapper companyInfoWrapper = new CompanyInformationWrapper(accountRecord);       
        
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(companyInfoWrapper);
        Test.stopTest();
        System.assertEquals('Test Business Account', result.companyInformation.Name);        
    }
    
    @isTest
    private static void testCompanyDataInputIfWrapperIsNotAccepted()
    {     
        CompanyInformationWrapper companyInfoWrapper = new CompanyInformationWrapper();       
        
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(companyInfoWrapper);
        Test.stopTest();
        System.assertEquals(null, result.fullLegalCompanyName);        
    }
    
    @isTest
    private static void testCompanyDataInputIfWrapperAndBooleanIsAccepted()
    {
        Account accountRecord =  AccountDAO.getAccountRecordFromCompanyCode(getCompany().Company_Code__c);      
        CompanyInformationWrapper companyInfoWrapper = new CompanyInformationWrapper(accountRecord);       
        
        Test.startTest();
            CompanyInformationWrapper result = new CompanyInformationWrapper(companyInfoWrapper,true);
        Test.stopTest();
        System.assertEquals('Test Business Account', result.fullLegalCompanyName);        
    }
    
}