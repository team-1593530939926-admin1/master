public class AccountTriggerHandler implements TriggerInterface {
    private static List<Account> listOfAccounts;     
    private static Map<Id, Account> oldAccountsMap = new Map<Id, Account>();
    
    public void beforeInsert(List<sObject> listOfNewSObject)
    {
        // before insert logic
    }
    
    public void afterInsert(List<sObject> listOfNewSObject)
    {
        // after insert logic
    }
    
    public void beforeUpdate(List<sObject> listOfNewSObject, Map<Id, sObject> oldObjMap)
    {
        // before update logic
        
    }
    
    public void afterUpdate(List<sObject> listOfNewSObject,  Map<Id, sObject> oldObjMap)
    {
         List<Account> oldAccountList = (List<Account>) oldObjMap.values();
        oldAccountsMap = new Map<Id, Account> (oldAccountList);
        listOfAccounts = (List<Account>)listOfNewSObject;
        System.debug('In Trigger Rakesh');
        Accounts.runTamarackProcessAfterPaynetIdUpdate(listOfAccounts, oldAccountsMap);
    }
    
    public void beforeDelete(Map<Id, sObject> oldObjMap)
    {
        // before delete logic
    }
    
    public void afterDelete(Map<Id, sObject> oldObjMap)
    {
        // after delete logic
    }
    
    public void afterUnDelete(List<sObject> newObjList)
    {
        // after undelete logic
    }
}