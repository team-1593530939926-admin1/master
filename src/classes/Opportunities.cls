public without sharing class Opportunities
{
    
    private static final Id OPPORTUNITY_CVF_RECORD_TYPE_ID = RecordTypeDAO.recordTypeIdByObjectNameAndRecordTypeName('Opportunity', 'CVF');
    
    @TestVisible
    private static final OpportunityDAO.IOpportunityDAO opportunityDAOInstance = new OpportunityDAO();
    private static final AccountDAO.IAccountDAO accountDAOInstance = new AccountDAO();
    
    
    @AuraEnabled(cacheable = true)
    public static RecordTypeWrappers getCvfRecordTypeId()
    {
        RecordTypeWrappers wrapper = new RecordTypeWrappers();
        wrapper.recordTypeId = OPPORTUNITY_CVF_RECORD_TYPE_ID;
        //    System.debug('wrapper from domain   ' + wrapper);
        return wrapper;
    }
    
    
    
    public static Opportunity getOpportunityDetailsByApplicationId(String applicationId)
    {
        List<Opportunity> listOfOpportunity = opportunityDAOInstance.getOpportunityDetailsByApplicationId(applicationId);
        if(!listOfOpportunity.isEmpty())
        {
            return listOfOpportunity[0];
        }
        else
        {
            return null;
        }
    }
    
    public static Opportunity getOpportunityById(Id opportunityId)
    {
        List<Opportunity> listOfOpportunity = opportunityDAOInstance.getOpportunityById(opportunityId);
        if(!listOfOpportunity.isEmpty())
        {
            return listOfOpportunity[0]; 
        }
        else
        {
            return null;
        }
    }
    
    public static void updateOpportunityStatus(List<Opportunity> listOfNewSObject, Map<Id, Opportunity> oldObjMap){
        Set<Id> setOfAccountIds = new Set<Id>();
        Map<Id, Date> mapOfDates = new Map<Id, Date>();
        List<Account> listOfAccounts = new List<Account>();
        for(Opportunity opportunityRecord:listOfNewSObject){
            if(opportunityRecord.RecordTypeId == OPPORTUNITY_CVF_RECORD_TYPE_ID 
                && opportunityRecord.AccountId != null
                && oldObjMap.containsKey(opportunityRecord.Id) 
                && opportunityRecord.Is_Automation_Completed__c
                && opportunityRecord.Is_Automation_Completed__c != oldObjMap.get(opportunityRecord.Id).Is_Automation_Completed__c){
                   setOfAccountIds.add(opportunityRecord.AccountId);
               }            
        }
        if(!setOfAccountIds.isEmpty()){
            listOfAccounts = accountDAOInstance.getListOfAccounts(setOfAccountIds);
            if(!listOfAccounts.isEmpty()){
                for(Account accountRecord : listOfAccounts){
                    mapOfDates.put(accountRecord.Id, accountRecord.Date_Of_Incorporation__c);
                }
            }
        }
        for(Opportunity opportunityRecord:listOfNewSObject){
            if(opportunityRecord.RecordTypeId == OPPORTUNITY_CVF_RECORD_TYPE_ID 
                && opportunityRecord.AccountId != null
                && oldObjMap.containsKey(opportunityRecord.Id) 
                && opportunityRecord.Is_Automation_Completed__c
                && (//opportunityRecord.Tier__c != oldObjMap.get(opportunityRecord.Id).Tier__c 
                //|| opportunityRecord.Rate_Card_Rate__c != oldObjMap.get(opportunityRecord.Id).Rate_Card_Rate__c
                 opportunityRecord.Is_Automation_Completed__c != oldObjMap.get(opportunityRecord.Id).Is_Automation_Completed__c)){
                if(mapOfDates.containskey(opportunityRecord.AccountId) 
                    && mapOfDates.get(opportunityRecord.AccountId) != null
                    && mapOfDates.get(opportunityRecord.AccountId) > System.today().addYears(-4)){
                    opportunityRecord.Deal_Status__c = 'Auto-Declined - Date of incorporation is less than 4 years from current date';
                    opportunityRecord.StageName = 'Prospect';
                }else if(opportunityRecord.Tier__c == '1' || opportunityRecord.Tier__c == '2' || opportunityRecord.Tier__c == '3'){
                    opportunityRecord.Deal_Status__c = 'Partial Approval';
                    opportunityRecord.StageName = 'Credit Underwriting';
                       
                }
                else if(opportunityRecord.Tier__c == '4'){
                    opportunityRecord.Deal_Status__c = 'Additional Review - Require More Information';
                    opportunityRecord.StageName = 'Credit Underwriting';
                }
                else if(opportunityRecord.Tier__c == 'Decline'){
                    opportunityRecord.Deal_Status__c = 'Auto Declined';
                    opportunityRecord.StageName = 'Prospect';
                }
            }
            
        }
    }
}