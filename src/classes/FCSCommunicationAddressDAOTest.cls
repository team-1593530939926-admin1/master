@isTest
private class FCSCommunicationAddressDAOTest {
  
    private static final String FOR_ENTITY = 'ENTITY_INFORMATION';
    private static final String FOR_PARTY_AGENT = 'ENTITY_PARTY_AGENT';
    private static final String SHOULD_GET_RESULT_ASSERT_MESSAGE = 'Should get result here.';
    
    @TestSetup 
    static void testSetup() {
         List<Communication_Address__c> listOfAddress = FCSTestDataFactory.generateCommunicationAddressForEntity(null, 1, true);
         List<FCS_Entity_Information__c> listOfEntityInformation = FCSTestDataFactory.generateFcsEntityInfoRecord(1,true);
         List<FCS_Entity_Party_Agent__c> listOfEntityPartyAgent = FCSTestDataFactory.generateFcsEntityPartyAgent(listOfEntityInformation.get(0).Id, 1, true);
         List<Communication_Address__c> listOfEntityPartyAddress = FCSTestDataFactory.generateCommunicationAddressForEntity(null, 2, false);
         listOfEntityPartyAddress.get(0).Entity_Party_Agent__c = listOfEntityPartyAgent.get(0).Id;
         listOfEntityPartyAddress.get(1).Entity_Information__c = listOfEntityInformation.get(0).Id;
         insert listOfEntityPartyAddress;
    }

    private static Communication_Address__c getCommunicationAddressFor(String addressFor) {
        if(String.isNotBlank(addressFor) && FOR_ENTITY.equalsIgnoreCase(addressFor)) { 
            return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
                        Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
                        Street_Address_2__c 
                        FROM Communication_Address__c WHERE Entity_Information__c <> null LIMIT 1];
        }

        if(String.isNotBlank(addressFor) && FOR_PARTY_AGENT.equalsIgnoreCase(addressFor)) { 
            return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
                     Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
                     Street_Address_2__c 
                     FROM Communication_Address__c WHERE Entity_Party_Agent__c <> null LIMIT 1];
        }

        return [SELECT Id, Name, City__c, Country_Parish__c, County__c, Entity_Information__c,
                     Entity_Party_Agent__c, Note__c, Phone__c, Postal_Code__c, State__c, Street_Address_1__c,
                     Street_Address_2__c 
                     FROM Communication_Address__c LIMIT 1];
    }


    @isTest
    private static void getCommunicationAddressById() {
        Communication_Address__c CommunicationAddressRecord = getCommunicationAddressFor(null);

        Test.startTest();
            List<Communication_Address__c> result = FCSCommunicationAddressDAO.getCommunicationAddressById(new Set<Id> {CommunicationAddressRecord.Id});
        Test.stopTest();

        System.assertEquals(1, result.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.City__c, result.get(0).City__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.County__c, result.get(0).County__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Postal_Code__c, result.get(0).Postal_Code__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Street_Address_1__c, result.get(0).Street_Address_1__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void getCommunicationAddressByEntityInformationIdTest() {
        Communication_Address__c communicationAddressRecord = getCommunicationAddressFor(FOR_ENTITY);

        Test.startTest();
            List<Communication_Address__c> result = FCSCommunicationAddressDAO.getCommunicationAddressByEntityInformationId(new Set<Id> {communicationAddressRecord.Entity_Information__c});
        Test.stopTest();

        System.assertEquals(1, result.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.City__c, result.get(0).City__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.County__c, result.get(0).County__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Postal_Code__c, result.get(0).Postal_Code__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Street_Address_1__c, result.get(0).Street_Address_1__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Street_Address_2__c, result.get(0).Street_Address_2__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Entity_Information__c, result.get(0).Entity_Information__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void getCommunicationAddressByPartyAgentIdTest() {
        Communication_Address__c communicationAddressRecord = getCommunicationAddressFor(FOR_PARTY_AGENT);

        Test.startTest();
            List<Communication_Address__c> result = FCSCommunicationAddressDAO.getCommunicationAddressByPartyAgentId(new Set<Id> {CommunicationAddressRecord.Entity_Party_Agent__c});
        Test.stopTest();

        System.assertEquals(1, result.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.City__c, result.get(0).City__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.County__c, result.get(0).County__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Postal_Code__c, result.get(0).Postal_Code__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Street_Address_1__c, result.get(0).Street_Address_1__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Street_Address_2__c, result.get(0).Street_Address_2__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(CommunicationAddressRecord.Entity_Party_Agent__c, result.get(0).Entity_Party_Agent__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }

    @isTest
    private static void saveCommunicationAddressTest() {
        Communication_Address__c addr = new Communication_Address__c(City__c = 'New York', 
         Country_Parish__c = null, County__c = 'USA', Postal_Code__c = '544212',
         State__c = 'AK', Street_Address_1__c = 'address2');
        List<Communication_Address__c> listOfAddress = FCSTestDataFactory.generateCommunicationAddressForPartyAgent(null, 1, false);
        listOfAddress.get(0).City__c = 'New York';
        listOfAddress.get(0).Country_Parish__c = null;
        listOfAddress.get(0).County__c = 'USA';
        listOfAddress.get(0).Postal_Code__c = '544212';
        listOfAddress.get(0).State__c = 'AK';
        listOfAddress.get(0).Street_Address_1__c = 'address2';

        Test.startTest();
            FCSCommunicationAddressDAO.saveCommunicationAddress(listOfAddress);
        Test.stopTest();

        List<Communication_Address__c> result = 
            [SELECT Id, City__c, County__c, Postal_Code__c, Street_Address_1__c
             FROM Communication_Address__c 
             WHERE Postal_Code__c = '544212' AND City__c = 'New York'];

        System.assertEquals(listOfAddress.size(), result.size(), SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfAddress.get(0).City__c, result.get(0).City__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfAddress.get(0).County__c, result.get(0).County__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfAddress.get(0).Postal_Code__c, result.get(0).Postal_Code__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
        System.assertEquals(listOfAddress.get(0).Street_Address_1__c, result.get(0).Street_Address_1__c, SHOULD_GET_RESULT_ASSERT_MESSAGE);
    }
    
}