@isTest
private class SecurityUtilTest 
{
    @isTest
    private static void testGetReadPermissionOfFieldsByObjectNameIfReadFieldPermissionExist()
    {
        List<String> fields = new List<String>{'Name','Type','Phone'};
        
        Test.startTest();
            List<String> listOfFields = SecurityUtil.getReadPermissionOfFieldsByObjectName('Account', fields);
        Test.stopTest();

        System.assertEquals(3, listOfFields.size(), 'Not having Read-field permission');
    }

    @isTest
    private static void testGetReadPermissionOfFieldsByObjectNameIfNoReadFieldPermissionExist()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user; 
        
        System.runAs(user)
        {
            List<String> fields = new List<String>{'City__c','Company__c','County__c'};
            
            Test.startTest();
                List<String> listOfFields = SecurityUtil.getReadPermissionOfFieldsByObjectName('Additional_Address__c', fields);
            Test.stopTest();

            System.assertEquals(0, listOfFields.size(), 'Having Read-field permission');               
        }
    }
    
    @isTest
    private static void testGetEditablePermissionOfFieldsByObjectNameIfEditFieldPermissionExist()
    {
        List<String> fields = new List<String>{'Name','Type','Phone'};
        
        Test.startTest();
            List<String> listOfFields = SecurityUtil.getEditablePermissionOfFieldsByObjectName('Account', fields);
        Test.stopTest();
        
        System.assertEquals(3, listOfFields.size(), 'Not having Edit-field permission');        
    }
    
    @isTest
    private static void testGetEditablePermissionOfFieldsByObjectNameIfNoEditFieldPermissionExist()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user;
        
        System.runAs(user)
        {
            List<String> fields = new List<String>{'City__c','Company__c','County__c'};
            
            Test.startTest();
                List<String> listOfFields = SecurityUtil.getEditablePermissionOfFieldsByObjectName('Additional_Address__c', fields);
            Test.stopTest();
            
            System.assertEquals(0, listOfFields.size(), 'Having Edit-field permission');        
        }
    }
    
    @isTest
    private static void testGetUpdatePermissionOfObjectIfExist()
    {
        Test.startTest();
            Boolean result = SecurityUtil.getUpdatePermissionOfObject('Account');
        Test.stopTest();

        System.assertEquals(true, result, 'Not having update permission');        
    }

    @isTest
    private static void testGetUpdatePermissionOfObjectIfNotExist()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user;    
        
        System.runAs(user)
        {
        Test.startTest();
            Boolean result = SecurityUtil.getUpdatePermissionOfObject('Additional_Address__c');
        Test.stopTest();

        System.assertEquals(false, result, 'Having Update permission');
        
        }       
    }
    
    @isTest
    private static void testGetCreatePermissionOfObjectIfExist()
    {
        Test.startTest();
            Boolean result = SecurityUtil.getCreatePermissionOfObject('Account');
        Test.stopTest();

        System.assertEquals(true, result, 'Not having Create permission');
    }

    @isTest
    private static void testGetCreatePermissionOfObjectIfNotExist()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user;  
        
        System.runAs(user)
        {
        Test.startTest();
            Boolean result = SecurityUtil.getCreatePermissionOfObject('Additional_Address__c');
        Test.stopTest();

        System.assertEquals(false, result, 'Having Create permission');
        }
    }
    
    @isTest
    private static void testGetDeletePermissionOfObjectIfExist()
    {
        Test.startTest();
            Boolean result = SecurityUtil.getDeletePermissionOfObject('Account');
        Test.stopTest();
        
        System.assertEquals(true, result, 'Not having Delete permission');        
    }

    @isTest
    private static void testGetDeletePermissionOfObjectIfNotExist()
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user;
        
        System.runAs(user)
        {
        Test.startTest();
            Boolean result = SecurityUtil.getDeletePermissionOfObject('Additional_Address__c');
        Test.stopTest();

        System.assertEquals(false, result, 'Having Delete permission');
        }
    }
    
    @isTest
    private static void testGetReadPermissionOfObjectIfExist()
    {
        Test.startTest();
           Boolean result = SecurityUtil.getReadPermissionOfObject('Account');
        Test.stopTest();
        
        System.assertEquals(true, result, 'Not having Read permission');
    }
    
    @isTest
    private static void testGetReadPermissionOfObjectIfNotExist()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Sales Coordinator'];
        User user = new User(Alias = 'standt', Email='standardUtil@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = p.Id, 
                             TimeZoneSidKey='America/Los_Angeles', UserName='testUtilSecurity@abc.com');  
                  
        insert user; 
        
        System.runAs(user)
        {
        Test.startTest();
            Boolean result = SecurityUtil.getReadPermissionOfObject('Additional_Address__c');
        Test.stopTest();

        System.assertEquals(false, result, 'Having Read permission');
        }
    }   
}