@isTest
private class ContactsTest 
{
    @TestSetup
    private static void setupTestData()
    {
        TestUtil.createContactData();
    }
    
    private static List<Account> getAccount()
    {
        return [SELECT Id , Name FROM Account];
    }
    
    private static List<Contact> getContact()
    {
        return [SELECT Id , Name FROM Contact];
    }

    @isTest
    private static void testCreatePartnerUserForBrokerContactIfContactExist()
    {
        List<User> listOfValidUser1 = [select Id ,UserRoleId from user];

        Test.startTest();
            TestUtil.CreatePartnerUserAccount();
        Test.stopTest();
        
        List<User> listOfValidUser2 = [select Id ,UserRoleId from user];
        
		System.assertNotEquals(listOfValidUser1.size(), listOfValidUser2.size());
    } 

	@isTest
    private static void getListOfPersonalGuarantorTest() {
        Contact contactInstance = getContact().get(0);
        
        Test.startTest();
        	List<contact> listOfContacts = Contacts.getListOfPersonalGuarantor(new Set<Id>{contactInstance.Id});
        Test.stopTest();
        
        System.assertEquals(1, listOfContacts.size());
    }
    
    @isTest
    private static void testGetGuarantorRecordFromGuarantorId()
    {        
        Test.startTest();
            GuarantorWrapper wrapperIstance = Contacts.getGuarantorRecordFromGuarantorId('test');
        Test.stopTest();
        
		System.assertEquals(wrapperIstance, null, 'No gurantor data found.');
    }
    
    @isTest
    private static void testUpdateContactToActive()
    {        
        List<Contact> listOFContact = [Select Id From Contact];
        Test.startTest();
            Contacts.updateContactToActive(listOFContact);
        Test.stopTest();
    }
    
    @isTest
    private static void testUpdateUserRole()
    {
        TestUtil.CreatePartnerUserAccount();
        List<Contact> listOFContact = [Select Id From Contact Where LastName = 'McTesty'];
        Test.startTest();
        for(Contact contactRecord : listOFContact){
            contactRecord.Primary_Contact__c = true;
        }
        update listOFContact;
        Test.stopTest();
    }

}