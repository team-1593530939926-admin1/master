import { LightningElement, track, wire, api } from 'lwc';
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import getCurrentCompanyRecordTypeId from '@salesforce/apex/BrokerApplicationFormController.getCurrentCompanyRecordTypeId';
import ACCOUNT_OBJECT from "@salesforce/schema/Account";
import { COMPANY_INFORMATION_LABELS } from "c/cvfLabelsUtil";
import { CompanyInformationModel } from "c/companyInformationModel";
import getCompanyIdIfExists from '@salesforce/apex/BrokerApplicationFormController.getCompanyIdIfExists';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';
import { UtilService } from 'c/utilService';

export default class CompanyInformation extends LightningElement {

  dependentPickListKey = {
    'state': 'state',
    'otherState': 'otherState'
  };

  label = COMPANY_INFORMATION_LABELS;
  @api isReadOnly = false
  @api companyValueSelected = false;
  @track recordTypeIdInfo;
  @track errorMessage;
  @track listOfEntities;
  @track companyCodeParam;
  @track isCompanyCodeError = false;
  @track isValidateButtonVisible = true;
  @api state;
  @track dateValue;
  @api companyName;
  @track error;
  @track businessOptions;
  @track initiateFCSFlow = false;
  @track entityVerificationResult;
  @track validate_button_label = 'Validate';
  @track IdFromServerIfExisting;
  @api fcsVerifiedResult = {};
  @api isCompanyModalOpen = false;
  @api isReviewEnabled = false;
  @track companyDetail = {};
  @track searchKey;
  /*renderedCallback() {
    const style = document.createElement('style'),
      targetComponent = this.template.querySelector('.container-style-tag');
    style.innerText = `
        .slds-input {
           height: 40px!important;
      }
      .slds-form-element__label {
        font-size: 15px!important;
    }`;

    if (targetComponent) {
      targetComponent.appendChild(style);
    }

    this.validateFieldByName('contactLastName');
  }*/


  @api
  get companyInformationDetail() {
    return this.companyInformationDetail;
    //return this.companyDetail;
  }

  set companyInformationDetail(value) {
    this.companyDetail = {};
    this.companyDetail = UtilService.updateDisableValueFormData({ ...value }, this, this.isEntityValidateSucess());
    if (value) {
      this.searchKey = value.companyName;
    }
    
    //  UtilService.reportValidity({ ...value }, this);
    //this.companyInformationDetail = value;
  }


  isEntityValidateSucess() {
    let isEntityValidate = false;
    if (this.entityVerificationResult != undefined && Object.keys(this.entityVerificationResult).length !== 0) {
      isEntityValidate = true;
    }
    return isEntityValidate;
  }

  @api
  get entityInformationDetail() {
    return this.entityVerificationResult;
  }

  set entityInformationDetail(value) {
    this.isReadOnly = false;
    if (value !== undefined && Object.keys(value).length !== 0) {
      this.validate_button_label = 'Verified';
      this.isReadOnly = true;
    }
    this.entityVerificationResult = value;
  }

  onComapnyClear() {
    this.entityVerificationResult = undefined;
    this.validate_button_label = 'Validate';
    this.handleFCSFlowToBrokerForm();
    this.clearCompanyCode();
    this.clearFormDataOnClear();
    this.companyDetail = new CompanyInformationModel();
  }


  clearFormDataOnClear() {
    const current = this,
      fcsRelatedDataEvent = new CustomEvent('fcscompanyrelateddata',
        {
          detail: { action: 'reset' }
        }),
      companyDetailData = { ...current.companyDetail };

    // reset partner trade and bank
    if (companyDetailData.existingId) {
      current.dispatchEvent(fcsRelatedDataEvent);
    }
  }


  connectedCallback() {
    this.companyDetail.county = 'United States';
    this.companyDetail.otherCounty = 'United States';
  }

  @wire(getCurrentCompanyRecordTypeId)
  recordTypeWired({ data, error }) {
    if (data) {
      this.recordTypeIdInfo = data.recordTypeId;
    }
    else if (error) {
      this.error = error;
    }
  }

  @wire(getPicklistValuesByRecordType, {
    objectApiName: ACCOUNT_OBJECT,
    recordTypeId: "$recordTypeIdInfo"
  })
  entityTypePicklistValues({ data, error }) {
    if (data) {
      this.listOfEntities = data.picklistFieldValues.Entity_Type__c.values;
      this.businessOptions = data.picklistFieldValues.Industry.values;

    }
    else if (error) {
      this.error = error;
    }
  }

  validateFormField() {
    const template = this;
    const ignoreKeyIndex = ["selectedName","companyTypeSoleProprietor", "otherZip", "isRequired", "companyCode", "contactInfo", "isFinalCompanyFormValid",
      "otherState", "existingId", "isFirstNameEntered", "isphoneNumberRequiDisable", "isBillingStreetAddressDisable",
      "ismultipleEntityPresent", "isCityDisable", "isCountyDisable", "isStateDisable", "isFullLegalCompanyNameDisable",
      "isContactFirstNameDisable", "isContactLastNameDisable", "isEmailAddressDisable", "isFaxDisable",
      "isFederalTaxIdDisable", "isNatureOfBusinessDisable", "isDbaDisable", "isCompanyTypeDisable",
      "isStateIncorporatedDisable", "isOtherStreetAddressDisable", "isOtherCityDisable", "isOtherZipDisable",
      "isOtherStateDisable", "isOtherCountyDisable", "isZipDisable", "isCompanySolePropriotor", "dateOfIncorporation"
    ];

    let isFormValid = true;
    this.companyDetail.isFinalCompanyFormValid = true;
    if (this.companyDetail.dateOfIncorporation === '') {
      ignoreKeyIndex.push('dateOfIncorporation');
    }
    else if (this.companyDetail.dateOfIncorporation !== '' && ignoreKeyIndex.includes('dateOfIncorporation')) {
      ignoreKeyIndex.pop('dateOfIncorporation');
    }
    ignoreKeyIndex.push('nextByPassForSoleDisable');
    ignoreKeyIndex.push('nextByPassForSole');
    for (const [key, value] of Object.entries(this.companyDetail)) {
      if (ignoreKeyIndex.indexOf(key) === -1) {
        //const disableKey = 'is' + key.charAt(0).toUpperCase() + key.slice(1) + 'Disable';
        if (key.indexOf('Disable') === -1) {
          const targetCmp = template.template.querySelector(`[data-id="${key}"]`);
          if (!targetCmp.checkValidity()) {
            targetCmp.reportValidity();
            isFormValid = false;
            this.companyDetail.isFinalCompanyFormValid = false;
          }
        }

      }
    }
    return isFormValid;
  }

  handleOnCommit(event) {
    let isSole = false;
    var newDate = new Date();
    const current = this;
    this.isCompanyCodeError = false;
    const id = event.target.dataset.id;
    this.handleToMakeDisable(id);
    this.dateValue = newDate.toISOString().substring(0, 10);
    if (id === 'phoneNumber') {
      event.target.setCustomValidity("");
      if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 10 && event.target.value.match(/^[0-9]*$/)) {
        const phoneNumber = event.target.value;
        const s = ("" + phoneNumber).replace(/\D/g, '');
        const m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        const formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        event.target.value = formattedPhone;
      }
      else if (event.target.value.length === 0) {
        event.target.setCustomValidity("");
      }
      else {
        event.target.setCustomValidity(COMPANY_INFORMATION_LABELS.invalidPhoneNumberFormat);
      }
      event.target.reportValidity();
    }


    if (id === 'fax') {
      event.target.setCustomValidity("");

      if ((event.target.value !== '' || event.target.value !== '() -') && !event.target.value.includes('@') && event.target.value.length === 10) {
        const phoneNumber = event.target.value;
        const s = ("" + phoneNumber).replace(/\D/g, '');
        const m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        const formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        // event.target.value = formattedPhone;
        if (formattedPhone === null) {
          event.target.setCustomValidity(COMPANY_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        else {
          event.target.value = formattedPhone;
        }

      }

      else if (!event.target.value.includes('@') && event.target.value.length !== 0) {
        event.target.setCustomValidity(COMPANY_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
      }

      if (event.target.value.includes('@')) {
        if (event.target.value.match(/^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$/)) {
          event.target.value = event.target.value;

        }
        else {
          event.target.setCustomValidity(COMPANY_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        event.target.reportValidity();
      }

      if (event.target.value === '() -') {
        event.target.value = '';
      }
      event.target.reportValidity();
    }


    if (id !== 'companyCode') {
      this.companyDetail[id] = event.target.value;
      this.isCompanyCodeError = false;
    }

    if (this.companyDetail.fullLegalCompanyName !== "" && this.companyDetail.state !== ""
      && this.companyDetail.billingStreetAddress !== "" && this.companyDetail.county !== ""
      && this.companyDetail.zip !== "" && this.companyDetail.city !== ""
      && this.companyDetail.stateIncorporated !== "") {
      this.isValidateButtonVisible = false;
      this.companyName = this.companyDetail.fullLegalCompanyName;
      this.state = this.companyDetail.state;
    }

    if (this.companyDetail.fullLegalCompanyName === "" || this.companyDetail.state === ""
      || this.companyDetail.billingStreetAddress === "" || this.companyDetail.county === ""
      || this.companyDetail.zip === "" || this.companyDetail.city === ""
      || this.companyDetail.stateIncorporated === "") {
      this.isValidateButtonVisible = true;
    }
    if (id === 'dateOfIncorporation' || id === 'companyType') {
      this.companyDetail[id] = event.target.value;
      if (this.companyDetail.companyType === 'Sole Proprietor') {
        this.companyDetail.isCompanySolePropriotor = true;
      }
      if (this.companyDetail.companyType !== 'Sole Proprietor') {
        this.companyDetail.isCompanySolePropriotor = false;
      }
      if (this.companyDetail.companyType === 'Sole Proprietor' && this.companyDetail.dateOfIncorporation !== "") {
        isSole = true;
        current.handleSoleProprietor(isSole);
      }
      if (this.companyDetail.companyType !== 'Sole Proprietor' || this.companyDetail.dateOfIncorporation === "") {
        isSole = false;
        current.handleSoleProprietor(isSole);
      }

    }
    if (id === 'contactFirstName' && event.target.value !== '') {
      this.companyDetail.isFirstNameEntered = true;
    }
    else if (id === 'contactFirstName' && event.target.value === '') {
      this.companyDetail.isFirstNameEntered = false;
    }

  }

  handleOnKeyUp(event) {
    event.target.setCustomValidity("");
    const charCode = event.which;
    if (charCode !== 92 && charCode !== 13 && (charCode < 48 || charCode > 57)) {
      // return event.target.value = event.target.value.substring(0,event.target.value.length - 1);
      event.target.setCustomValidity(COMPANY_INFORMATION_LABELS.invalidPhoneNumberFormat);
    }
    event.target.reportValidity();
  }

  handleOnFocus(event) {
    const id = event.target.dataset.id;
    if (id === 'phoneNumber' || id === 'fax') {
      event.target.value = event.target.value.replace(/[{( )}-]/g, '');
    }
    else if (id === 'fax' && event.target.value.length === 0) {
      event.target.setCustomValidity("");
    }
  }


  get isRequiredLastNameRequire() {
    var isRequired = false;
    if (this.companyDetail.contactFirstName && this.companyDetail.contactFirstName !== '') {
      isRequired = true;
    }
    return isRequired;
  }

  validateFieldByName(fieldName) {
    const targetCmp = this.template.querySelector(`[data-id="${fieldName}"]`);
    if (targetCmp && targetCmp !== null) {
      targetCmp.reportValidity();
    }
  }



  @api
  onCompanyDetailSave() {
    if ((this.validateFormField() && this.entityVerificationResult !== undefined) || (this.validateFormField() && this.companyDetail.companyType === 'Sole Proprietor' && this.companyDetail.dateOfIncorporation !== "")) {
      return {
        companyInformation: this.companyDetail,
        entityVerification: this.entityVerificationResult
      };
    }
    return null;
  }

  onCompanyInformationValidation() {
    this.isCompanyModalOpen = true;
  }

  onClosingCompanyModal(event) {
    const param = event.detail,
      fcsComponent = this.template.querySelector('[data-id="fcsValidationEntity"]');

    if (param.action === 'yes') {
      // invoke fcs verfication
      if (fcsComponent) {
        fcsComponent.invokeFCSFlow();
      }
    }
    this.isCompanyModalOpen = param.value;

  }

  @track countryControllingValues = [];
  @track stateDependentValues = [];
  @track defaultCountryAndStateData;
  @track countryVal;

  @wire(getCountryAndStateValues)
  countryStateData({ data, error }) {
    if (data) {
      let countyOptions = [{ label: '--None--', value: '--None--' }];
      let stateData = [{ label: '--None--', value: '--None--' }];
      for (let [key, value] of Object.entries(data)) {

        countyOptions.push({ label: key, value: key });
        stateData.push({ label: key, value: value });

      }

      this.defaultCountryAndStateData = stateData;
      this.countryControllingValues = countyOptions;
    }
    else if (error) {
      window.console.log(error);
    }
  }


  handleSelectDependentpicklist(event) {
    const param = event.detail;
    if (param.key) {
      this.companyDetail[param.key] = param.val;
      if (this.companyDetail.fullLegalCompanyName !== "" && this.companyDetail.state !== ""
        && this.companyDetail.billingStreetAddress !== "" && this.companyDetail.county !== ""
        && this.companyDetail.zip !== "" && this.companyDetail.city !== ""
        && this.companyDetail.stateIncorporated !== "") {
        this.isValidateButtonVisible = false;
        this.companyName = this.companyDetail.fullLegalCompanyName;
        this.state = this.companyDetail.state;
      }

      if (this.companyDetail.fullLegalCompanyName === "" || this.companyDetail.state === ""
        || this.companyDetail.billingStreetAddress === "" || this.companyDetail.county === ""
        || this.companyDetail.zip === "" || this.companyDetail.city === ""
        || this.companyDetail.stateIncorporated === "") {
        this.isValidateButtonVisible = true;
      }
    } 
  }

  handleAccountSelection(event) {
    const informationDislayEvent = new CustomEvent('companyidtoretrievedata', { detail: event.detail });
    this.dispatchEvent(informationDislayEvent);
    this.companyDetail.existingId = event.detail;
    this.companyValueSelected = true;
  }

  handleFCSFlowCallback(event) {
    const current = this,
      param = event.detail;

    let updatedCompanyData = { ...current.companyDetail };

    if (param.isSuccess && param.action === 'verified') {
      current.entityVerificationResult = param.result;
      current.validate_button_label = 'Verified';
      if (param.isFromFCS) {
        updatedCompanyData = current.assignDataAfterValidation(current.entityVerificationResult);
      }

      if (!param.isFromFCS) {
        updatedCompanyData = current.assignDataAfterValidationFromSF(current.entityVerificationResult);
      }

      if (Object.entries(updatedCompanyData).length > 0) {
        current.companyDetail = undefined;
        current.companyDetail = UtilService.updateDisableValueFormData({ ...updatedCompanyData }, this, true);
      }
      this.handleFCSFlowToBrokerForm();
    }
    if (this.companyDetail.existingId === null) {
      this.companyRelatedDataIfExistingIdNull();////////
    }

  }

  handleFCSFlowToBrokerForm() {
    const param = {
      isSuccess: true,
      entityVerificationResult: this.entityVerificationResult
    },
      customEvt = new CustomEvent('fcsverified', { detail: param });
    this.dispatchEvent(customEvt);
  }

  get isValidateButtonEnable() {
    const requiredFields = [
      'fullLegalCompanyName', 'state', 'billingStreetAddress', 'phoneNumber', 'fax',
      'county', 'zip', 'city', 'stateIncorporated', 'contactLastName', 'emailAddress'],
      current = this;

    let isButtonEnable = true;


    requiredFields.forEach(function (field) {
      const target = current.template.querySelector(`[data-id="${field}"]`);
      if (target && !target.checkValidity()) {
        isButtonEnable = false;
        return;
      }
    });
    isButtonEnable = isButtonEnable ? (current.entityVerificationResult == undefined || Object.keys(current.entityVerificationResult).length === 0) : false;

    let isSolePropritorNotSelected = true;
    if (current.companyDetail.companyType && current.companyDetail.companyType === 'Sole Proprietor') {
      isSolePropritorNotSelected = false;
      //  current.handleSoleProprietor(true); 
    } else if (current.companyDetail.companyType) {
      //  current.handleSoleProprietor(false); 
    }

    return !(isButtonEnable && isSolePropritorNotSelected);
  }

  get makeReadOnlyAfterEntityVerification() {
    var isReadonlyCmpForm = false;
    if (this.entityVerificationResult != undefined && Object.keys(this.entityVerificationResult).length !== 0) {
      isReadonlyCmpForm = true;
    }
    return isReadonlyCmpForm;
  }

  assignDataAfterValidation(result) {
    const currentTemplate = this;
    const wrapperRecord = result.listOfResultWrapper;
    let companyDetailData = currentTemplate.companyDetail;
    if (wrapperRecord.length > 0) {
      if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0]) {
        companyDetailData.fullLegalCompanyName = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].entityName;
      }
      if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0]) {
        if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].address1 !== "") {
          companyDetailData.billingStreetAddress = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].address1;
        }
        if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].city !== "") {
          companyDetailData.city = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].city;
        }
        if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].country !== "") {
          companyDetailData.county = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].country;
        }
        if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].stateOrProvince !== "") {
          companyDetailData.state = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].stateOrProvince;
        }
        if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].postalCode !== "") {
          companyDetailData.zip = wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0].postalCode;
        }
      }
      if (wrapperRecord[0].recordInfo.entityInfo.listOfDates[0].dateValue &&
        wrapperRecord[0].recordInfo.entityInfo.listOfDates[0].dateValue !== "" && wrapperRecord[0].recordInfo.entityInfo.listOfDates[0].dateValue.length > 3 &&
        wrapperRecord[0].recordInfo.entityInfo.listOfDates[0].dateType === "DateOfIncorporation") {
        companyDetailData.dateOfIncorporation = wrapperRecord[0].recordInfo.entityInfo.listOfDates[0].dateValue;
        companyDetailData.dateOfIncorporation = UtilService.formatDate(companyDetailData.dateOfIncorporation);

      }
      // this.handleFcsVerifiedNewCompanyRelatedData(companyDetailData);
    }
    return companyDetailData;
  }


  assignDataAfterValidationFromSF(result) {
    const current = this,
      companyFieldMapping = {
        'billingStreetAddress': 'Street_Address_1__c',
        'city': 'City__c',
        'county': 'County__c',
        'state': 'State__c',
        'zip': 'Postal_Code__c'
        // 'dateOfIncorporation':'Date_Of_Incorporation__c'
      };

    let companyDetail = { ...current.companyDetail };

    if (result.entityInformation) {
      companyDetail.fullLegalCompanyName = result.entityInformation.Entity_Name__c;
      if(result.entityInformation.Date_Of_Incorporation__c !== "" && result.entityInformation.Date_Of_Incorporation__c){
        companyDetail.dateOfIncorporation = result.entityInformation.Date_Of_Incorporation__c;
      }
    
    }

    if (result.listOfEntityCommunicationAddress &&
      result.listOfEntityCommunicationAddress.length > 1) {
      const companyAddress = result.listOfEntityCommunicationAddress[0];
      for (let [key, value] of Object.entries(companyFieldMapping)) {
        if (companyAddress[value]) {
          companyDetail[key] = companyAddress[value];
        }
      }
    }


    return companyDetail;
  }

  handleFcsVerifiedNewCompanyRelatedData(companyDetailData) {
    const fcsRelatedDataEvent = new CustomEvent('fcscompanyrelateddata', {
      detail: {
        companyName: companyDetailData.fullLegalCompanyName,
        address: companyDetailData.billingStreetAddress, city: companyDetailData.city,
        country: companyDetailData.county, state: companyDetailData.state, zip: companyDetailData.zip
      }
    });
    this.dispatchEvent(fcsRelatedDataEvent);
  }

  clearCompanyCode() {
    const current = this,
      cmp = current.template.querySelector('c-custom-lookup');

    if (cmp) {
      cmp.clearPill();
    }
  }

  clearFormFromCustomLookup() {
    let isSole = false;
    const current = this,
      fcsRelatedDataEvent = new CustomEvent('fcscompanyrelateddata',
        {
          detail: { action: 'reset' }
        }),
      companyDetailData = { ...current.companyDetail };

    //current.onComapnyClear();
    //current.clearCompanyCode();
    current.entityVerificationResult = undefined;
    current.validate_button_label = 'Validate';
    current.handleFCSFlowToBrokerForm();
    current.companyDetail = new CompanyInformationModel();
    if (companyDetailData.existingId) {
      current.dispatchEvent(fcsRelatedDataEvent);
    }
    current.companyValueSelected = false;
    current.handleSoleProprietor(isSole);

  }

  get makeReadOnlyAfterEntityVerificationClear() {
    var isReadonlyCmpForm = true;
    if (this.entityVerificationResult != undefined && Object.keys(this.entityVerificationResult).length !== 0) {
      isReadonlyCmpForm = false;
    }

    return isReadonlyCmpForm;
  }

  handleToMakeDisable(dataId) {
    let isFldDisable = false;
    if (this.companyDetail.existingId && this.companyDetail.existingId !== null) {
      if (this.companyDetail[dataId] && this.companyDetail[dataId] !== '') {
        isFldDisable = true;
      }
    }
    return isFldDisable;
  }

  get fullLegalCompanyNameDisable() {
    return this.isFormFieldNeedToDisable(this.companyDetail.fullLegalCompanyName);
  }

  get faxDisable() {
    return this.isFormFieldNeedToDisable(this.companyDetail.fax);
  }

  isFormFieldNeedToDisable(value) {
    let isFldDisable = false;
    if (this.companyDetail.existingId && this.companyDetail.existingId !== null) {
      if ((value && value !== '') || (this.entityVerificationResult !== undefined && Object.keys(this.entityVerificationResult).length !== 0)) {
        isFldDisable = true;
      }
    }
    return isFldDisable;
  }

  handleSoleProprietor(value) {
    const current = this;
    const companyTypeEvent = new CustomEvent('soleproprietor', { detail: { isSole: value, dateOfIncorporation: current.companyDetail.dateOfIncorporation } });
    this.dispatchEvent(companyTypeEvent);
  }

  companyRelatedDataIfExistingIdNull() {
    getCompanyIdIfExists({ companyInfo: this.companyDetail })
      .then(result => {
        if (result.companyAlreadyExistId) {
          this.IdFromServerIfExisting = result.companyAlreadyExistId;
          this.companyDetail.ismultipleEntityPresent = result.ismultipleEntityPresent;
          this.handlecompanyRelatedDataIfNotSearched(this.IdFromServerIfExisting, result.ismultipleEntityPresent);
        }
      })
  }


  handlecompanyRelatedDataIfNotSearched(value, isEntity) {
    const current = this;
    const companyDataEvent = new CustomEvent('companyrelateddataifnotsearched', { detail: { valueId: value, isEntityMultiple: isEntity } });
    this.dispatchEvent(companyDataEvent);
  }
}