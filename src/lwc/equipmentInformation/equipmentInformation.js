import { LightningElement, track, wire, api } from "lwc";
import getCurrentDealRecordTypeId from '@salesforce/apex/BrokerApplicationFormController.getCurrentDealRecordTypeId';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';
import getDescriptionOptionValues from '@salesforce/apex/BrokerApplicationFormController.getDescriptionOptionValues';
import { getPicklistValuesByRecordType } from 'lightning/uiObjectInfoApi';
import getLeaseTermOptionValues from '@salesforce/apex/BrokerApplicationFormController.getLeaseTermOptionValues';

import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import { EQUIQMENT_INFORMATION_LABELS } from "c/cvfLabelsUtil";
import { UtilService } from 'c/utilService';

const IGNORE_KEY_INDEX_ARR = ["isRequired", "isFinalEquipmentFormValid", "otherState"];

export default class EquipmentInformation extends LightningElement {

    label = EQUIQMENT_INFORMATION_LABELS;
    dependentPickListKey = {
        'otherState': 'otherState'
    };

    @track error;
    @track dealRecordTypeId;
    @track leaseTermOptions;
    @track leaseTypeOptions;
    @api isReviewEnabled = false;
    @track equipmentDetail = {};
    @track countryControllingValues = [];
    @track defaultCountryAndStateData;
    @track descriptionOptions;
    @track documentTypeOptions;

    // Need to remove this
    @api dealNameDisabled;

    @api
    get equipmentInformationDetail() {
        return this.equiqmentInformationDetail;
    }

    set equipmentInformationDetail(value) {
        this.equipmentDetail = { ...value };
    }

    @api
    onEquipmentDetailSave() {
        const current = this;
        current.equipmentDetail.dealName = current.dealNameDisabled;
        let equipmentDetailFormDataArr = [{ ...current.equipmentDetail }];
        const validatedFormDataResult = UtilService.validateFormData(IGNORE_KEY_INDEX_ARR, equipmentDetailFormDataArr, current, 'isFinalEquipmentFormValid');
        current.equipmentDetailData = validatedFormDataResult.result[0];
        if (validatedFormDataResult.isValid) {
            return current.equipmentDetail;
        }
        return null;
    }

    @wire(getCurrentDealRecordTypeId)
    recordTypeWired({ data, error }) {
        if (data) {
            this.dealRecordTypeId = data.recordTypeId;

            // Need to move this on broker
            /// const dateTimeStamp = UtilService.generateDateTimeStampInMMDDYYHHMM(" - ");
            //this.equipmentDetail.dealName = this.dealNameDisabled + dateTimeStamp;
        }
        else if (error) {
            this.error = error;
        }
    }

    @wire(getPicklistValuesByRecordType,
        { objectApiName: OPPORTUNITY_OBJECT, recordTypeId: '$dealRecordTypeId' })
    dealPicklistOptionsAsPerRecordType({ data, error }) {
        let leaseOptions = [];
        if (data) {
            if (data.picklistFieldValues.Lease_Term__c && data.picklistFieldValues.Lease_Type__c
                && data.picklistFieldValues.Documentation_Type__c) {
              //  leaseOptions = [{ ...data.picklistFieldValues.Lease_Term__c.values }];
                //this.removeValue(leaseOptions, 'Other');
               // this.leaseTermOptions = data.picklistFieldValues.Lease_Term__c.values;
               // this.leaseTermOptions.splice(5,1);
                this.leaseTypeOptions = data.picklistFieldValues.Lease_Type_Request_1__c.values;
                this.documentTypeOptions = data.picklistFieldValues.Documentation_Type__c.values;
            }
        }
        else if (error) {
            this.error = errror;
        }
    }

    @wire(getDescriptionOptionValues)
    getDescriptionOptionValues({ data, error }) {
        if (data) {

            let descOptions = [{ label: '--None--', value: '' }];

            data.forEach(function (item, index) {

                descOptions.push({ label: item.label, value: item.value });
               
            });
            this.descriptionOptions = descOptions;
            console.log('Description Picklist Data---->' + this.descriptionOptions);
        }

    }

    @wire(getLeaseTermOptionValues)
    getLeaseTermOptionValues({ data, error }) {
        if (data) {

            let descOptions = [];

            data.forEach(function (item, index) {

                descOptions.push({ label: item.label, value: item.value });
                
            });
            this.leaseTermOptions = descOptions;
            console.log('Description Picklist Data---->' + this.leaseTermOptions);
        }

    }

    handleOnCommit(event) {
        const current = this,
            id = event.target.dataset.id,
            dependentFieldJSONArr = {
                'endOfLeasePurchaseOption': {
                    'Other': ['totalCashPrice', 'otherText'],
                    '% of Total Cash Price': ['totalCashPrice', 'otherText']
                },
                'equipmentLocation': ['otherStreetAddress', 'otherCity', 'otherCounty', 'otherState', 'otherZip']
            },
            fieldValue = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

        if (id) {
            if (Object.keys(dependentFieldJSONArr).indexOf(id) !== -1) {
                const dependentFieldVal = dependentFieldJSONArr[id];

                if (Array.isArray(dependentFieldVal)) {
                    dependentFieldVal.forEach(function (fieldAPI) {
                        current.equipmentDetail[fieldAPI] = '';
                    });
                } else {
                    if (dependentFieldVal.hasOwnProperty(fieldValue)) {
                        dependentFieldVal[fieldValue].forEach(function (fieldAPI) {
                            current.equipmentDetail[fieldAPI] = '';
                        });
                    }
                }
            }
            current.equipmentDetail[id] = fieldValue;
        }
    }

    get isForOtherPurchaseOption() {
        const current = this,
            equipmentDetail = current.equipmentDetail;

        let isVisible = false;

        if (equipmentDetail.endOfLeasePurchaseOption === 'Other') {
            isVisible = true;
        }

        return isVisible;
    }

    get isForPercentTotalCashPricePurchaseOption() {
        const current = this,
            equipmentDetail = current.equipmentDetail;

        let isVisible = false;

        if (equipmentDetail.endOfLeasePurchaseOption === '% of Total Cash Price') {
            isVisible = true;
        }

        return isVisible;
    }

    get isEquipmentInfoCheck() {
        const current = this,
            equipmentDetail = current.equipmentDetail;

        return equipmentDetail.equipmentLocation;
    }

    @wire(getCountryAndStateValues)
    countryStateData({ data, error }) {
        if (data) {
            let countyOptions = [{ label: '--None--', value: '--None--' }];
            let stateData = [{ label: '--None--', value: '--None--' }];
            for (let [key, value] of Object.entries(data)) {

                countyOptions.push({ label: key, value: key });
                stateData.push({ label: key, value: value });

            }
            console.log(stateData + 'State dataaa');

            this.defaultCountryAndStateData = stateData;
            console.log(this.defaultCountryAndStateData + 'dataaa');
            this.countryControllingValues = countyOptions;
        }
        else if (error) {
            window.console.log(error);
        }
    }

    handleSelectDependentpicklist(event) {
        const param = event.detail;
        if (param.key) {
            this.equipmentDetail[param.key] = param.val;
        }
    }


    renderedCallback() {
        const style = document.createElement('style'),
            targetComponent = this.template.querySelector('.container-style-tag');
        style.innerText = `
        .slds-input {
           height: 40px!important;
      }
      .slds-form-element__label {
        font-size: 15px!important;
    }
    .slds-textarea {
    
        width: 100%;
        height: 77px!important;
       
    }`;

        if (targetComponent) {
            //     targetComponent.appendChild(style);
        }
    }

    removeValue(arr, val) {
        let options = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i].value !== val) {
                options.push(arr[i]);
                // this.leaseTermOptions
                //  arr.splice(i, 1);
                //arr.slice(0).splice(i, 1);
                //   return arr;
            }
        }
        this.leaseTermOptions = options;
    }
}