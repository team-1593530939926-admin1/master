import { LightningElement, track, api } from 'lwc';
import returnCreditConditionForOpportunity from '@salesforce/apex/CreditUnderwrittingDocumentController.returnCreditConditionForOpportunity';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import deleteRecord from '@salesforce/apex/DealDocumentUploadTableController.deleteFiles';
import relatedFiles from '@salesforce/apex/DealDocumentUploadTableController.relatedFiles';
import setFileDetails from '@salesforce/apex/DealDocumentUploadTableController.setFileDetails';
import handleSubmitOfDocuments from '@salesforce/apex/CreditUnderwrittingDocumentController.handleSubmitOfDocuments';
import { DEAL_DOCUMENT_UPLOAD_LABELS } from 'c/cvfLabelsUtil';
import { BROER_COMMENT_LABELS } from 'c/cvfLabelsUtil';
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';

const columns = [
    { label: DEAL_DOCUMENT_UPLOAD_LABELS.FileName, fieldName: 'fileName' },
    { label: DEAL_DOCUMENT_UPLOAD_LABELS.Description, fieldName: 'fileDescription' },
    { label: DEAL_DOCUMENT_UPLOAD_LABELS.Type, fieldName: 'fileType' },
    {
        label: DEAL_DOCUMENT_UPLOAD_LABELS.download, type: "button", typeAttributes: {
            label: 'Download',
            name: 'View',
            title: 'Download',
            disabled: false,
            value: 'view',
            iconPosition: 'center'
        }
    },
    {
        label: DEAL_DOCUMENT_UPLOAD_LABELS.Remove, type: "button", typeAttributes: {
            label: 'Delete',
            name: 'Del',
            title: 'Delete',
            disabled: false,
            value: 'delete',
            iconPosition: 'center'
        }
    }
];
const FILE_EXTENSION = ['.pdf', '.xlsx', '.docx', '.csv'];
export default class CreditFullfillmentDocumentUpload extends NavigationMixin(LightningElement) {
    @track creditDocumentList = [{
        creditConditions: '',
        opportunityId: '',
        rowId: '',
        fileUploader: '',
        dealName: '',
        companyName: '',
        equipmentDescription: '',
        Amount: ''

    }];
    label = BROER_COMMENT_LABELS;
    @track setOfRequiredData = new Set();
    @track setOfUploadedData = new Set();
    @track fileTable;
    @track fileMap = [];
    @track portalURL;
    @track columns = columns;
    @track data;
    @track isDocumentUploadModal;
    @track fileTypeOptions;
    @track isDisable = true;
    @track FileTypeAndDescriptionDetails = [];
    @track dealDetails = {
        dealName: '',
        companyName: '',
        equipmentDescription: '',
        Amount: ''
    }
    @track errorDetails = {
        isErrorVisible: false,
        headerMessage: "Error",
        errorContent: ''
    };
    @track isModalOpen = false;
    @track isButtonVisible = false;
    get acceptedFormats() {
        return FILE_EXTENSION;
    }

    get isDataAvailable() {
        let dataAvailable = false;
        if (this.data && this.data.length > 0) {
            dataAvailable = true;
        }
        return dataAvailable;
    }

    handleOnUploadFinished(event) {
        const current = this,
            id = event.target.dataset.id,
            index = event.target.dataset.index,
            uploadedFiles = event.detail.files;
        this.fileMap.push(uploadedFiles[0].documentId);
        this.setFileDescription(uploadedFiles[0].documentId, index);
        this.descriptionRequired = false;
    }

  /*  @wire(returnCreditConditionForOpportunity, { opportunityId: this.creditDocumentList[0].opportunityId })
    credits({ data, error }) {
        if (data) {
            this.creditDocumentList = data;
            this.dealDetails.dealName = this.creditDocumentList[0].dealName;
            this.dealDetails.companyName = this.creditDocumentList[0].companyName;
            this.dealDetails.equipmentDescription = this.creditDocumentList[0].equipmentDescription;
            this.dealDetails.Amount = this.creditDocumentList[0].Amount;
            this.isAllDocumentsUploaded(this.creditDocumentList);
        }

    }
*/
    setFileDescription(docId, index) {
        const current = this;
        setFileDetails({
            idParent: docId,
            description: current.creditDocumentList[index].creditConditions,
            type: 'Credit Condition'
        }).then(data => {
            current.getRelatedFiles(this.fileMap);
        }).catch(error => {
            current.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error.message,
                    variant: 'error',
                }),
            );
        })
    }

    getRelatedFiles(fileId) {
        const current = this;
        relatedFiles({ idParent: fileId })
            .then(data => {
                current.data = [...data];
                console.log(JSON.stringify(current.data));
                current.FileTypeAndDescriptionDetails = current.data;
            })
            .catch(error => {
                current.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error!!',
                        message: error.message,
                        variant: 'error',
                    }),
                );
            });
    }

    @track fileExtensionList = [];
    @track versionId;
    @track documentId;
    @track fileType;
    @track fileExtension;
    @track fileName;


    //It handles view and delete buttons.
    handleOnRowAction(event) {

        const current = this,
            recId = event.detail.row.fileId,
            actionName = event.detail.action.name;

        let fileDataRecords = [...current.data];

        current.versionId = event.detail.row.versionId;
        current.documentId = event.detail.row.fileId;
        current.fileType = event.detail.row.fileType;
        current.fileName = event.detail.row.fileName;

        for (let [key, value] of Object.entries(current.fileExtensionList)) {
            if (event.detail.row.fileExtension === key) {
                current.fileExtension = value;
            }
        }

        if (actionName === 'Del') {
            deleteRecord({ deleteRecord: recId })
                .then(() => {
                    const index = fileDataRecords.findIndex((element) => element.fileId === recId);
                    fileDataRecords.splice(index, 1);
                    current.data = fileDataRecords;
                    current.FileTypeAndDescriptionDetails = fileDataRecords;
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Record Deleted Successfully.',
                            variant: 'success'
                        })
                    );
                })
                .catch(error => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error deleting record',
                            message: error.body.message,
                            variant: 'error'
                        })
                    );
                });
        }

        else if (actionName === 'View') {

            var urlString = window.location.href;
            var baseURL = urlString.substring(0, urlString.indexOf("/s/"));
            const url = `${baseURL}/sfc/servlet.shepherd/document/download/${current.documentId}`;
            window.open(url);
        }
    }

    clear() {
        this.template.querySelectorAll('lightning-combobox').forEach(each => {
            each.value = '';
        });
        this.template.querySelectorAll('lightning-input').forEach(each => {
            each.value = '';
        });
        this.isDisable = true;
    }

    @api
    get fileDetailData() {
        return this.fileMap;
    }

    set fileDetailData(value) {
        if (value) {
            this.fileMap = undefined;
            this.getRelatedFiles(value)
            this.fileMap = [...value];
            this.FileTypeAndDescriptionDetails = [...value];
        }
    }

    navigateToHome() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'home'
            },
        });
    }

    handleOnSubmit() {
        const current = this;
        this.isAllRequiredDocuments(this.creditDocumentList);
        this.isAllRequiredDocumentsUploaded(this.FileTypeAndDescriptionDetails);
        if (current.setOfUploadedData.size === current.setOfRequiredData.size) {
        handleSubmitOfDocuments({ fileDetailData: current.fileMap, opportunityId: current.creditDocumentList[0].opportunityId })
            .then(result => {
                if (result === 'success') {
                    current.navigateToHome();
                }
            }).catch(error => {
                current.isLoaded = false;
                current.errorDetails.isErrorVisible = true;
                current.errorDetails.errorContent = error.body.message;

            });
        }
            else{
                current.isModalOpen = true;
                current.isButtonVisible = false;
            }
    }

    renderedCallback() {
        const current = this,
            testURL = window.location.href,
            newURL = current.returnIdFromUrl(testURL);
        current.creditDocumentList[0].opportunityId = newURL[0];
        console.log('hiiii' + current.creditDocumentList[0].opportunityId);
        returnCreditConditionForOpportunity({opportunityId:newURL[0]})
        .then(result=>{
            if(result){
                this.creditDocumentList = result;
                this.dealDetails.dealName = this.creditDocumentList[0].dealName;
                this.dealDetails.companyName = this.creditDocumentList[0].companyName;
                this.dealDetails.equipmentDescription = this.creditDocumentList[0].equipmentDescription;
                this.dealDetails.Amount = this.creditDocumentList[0].Amount;
            }
        })
    }

    returnIdFromUrl(value) {
        const newURL = value.match(/[a-z0-9]\w{4}0\w{12}|[a-z0-9]\w{4}0\w{9}/g);
        return newURL;
    }

    isAllRequiredDocuments(documentArray) {
        const current = this;
        documentArray.forEach(function (formData) {
            for (const [key, value] of Object.entries(formData)) {
                if (key === 'creditConditions') {
                    current.setOfRequiredData.add(value);
                }
            }
        });
    }

    isAllRequiredDocumentsUploaded(documentArray) {
        const current = this;
        documentArray.forEach(function (formData) {
            for (const [key, value] of Object.entries(formData)) {
                if (key === 'fileDescription') {
                    current.setOfUploadedData.add(value);
                }
            }
        });
       
    }

    onClosingModal(){
        const current = this;
        current.isModalOpen = false;
    }

    connectedCallback() {
        Promise.all([
            loadStyle(this, customCss)
        ]).then(() => {
            // alert('StyleSheet Implemented')
        })
        .catch(error => {
            alert(error.body.message);
        });
    }

}