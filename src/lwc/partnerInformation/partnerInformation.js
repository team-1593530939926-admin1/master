/* eslint-disable no-console */
import { LightningElement, track, api, wire } from 'lwc';
import { PARTNER_INFORMATION_LABELS } from 'c/cvfLabelsUtil';
import { PartnerInformationModel } from 'c/partnerInformationModel';
import { UtilService } from 'c/utilService';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';

const IGNORE_CHECK_IN_DATA_VALIDATE = ['isCreatedByMe','maskedFirstName', 'maskedMiddleName', 'maskedLastName', 'maskedHomePhone','maskedDob','maskedSocialSecurity','maskedMailingStreet','isRequired', 'existingId', 'rowId', 'isDeleteRequire', 'partnerId', 'isFinalPartnerFormValid', 'count', 'isDisable'];
const DUPLICATE_CHECK_KEYS = ["firstName","middleName","lastName","dOB","mailingStreet","mailingCountry","mailingState","mailingCity","mailingPostalCode","socialSecurity","homePhone"];
export default class PartnerInformation extends LightningElement {

    dependentPickListKey = {
        'mailingState': 'mailingState'
    };

    label = PARTNER_INFORMATION_LABELS;
    //private properties are declared here
    @track partnerDetailsList = [];
    @track dateValue;
    @track countryControllingValues = [];
    @track stateDependentValues = [];
    @track defaultCountryAndStateData;
    @track countryVal;
    @track errorDetails = {
        isErrorVisible: false,
        headerMessage: "Error",
        errorContent: ''
    };
    @api isReviewEnabled = false;
    @api
    get partnerDetailsListInformation() {
        return this.partnerDetailsList;
    }

    set partnerDetailsListInformation(value) {
        const current = this;
        current.partnerDetailsList = [];
        value.forEach(function (eachValue) {
            current.partnerDetailsList.push({ ...eachValue });
        })
    }

    connectedCallback() {
        Promise.all([
            loadStyle(this, customCss)
        ]).then(() => {
            //alert('StyleSheet Implemented')
        })
            .catch(error => {
                alert(error.body.message);
            });
    }

    onDeleteRow(event) {
        const sourceData = event.target.dataset,
            index = sourceData.index,
            currentIndex = this.partnerDetailsList.findIndex(element => element.rowId + '' === index);
        this.partnerDetailsList.splice(currentIndex, 1);
        for (let i = 0; i < this.partnerDetailsList.length; i++) {
            this.partnerDetailsList[i].count = i + 1;
            this.partnerDetailsList[i].rowId = i;
        }
    }

    createNewRow() {
        let currentSize = this.partnerDetailsList.length;
        if (currentSize <= 4) {
            let newRow = new PartnerInformationModel(currentSize);
            newRow.isCreatedByMe = true;
            newRow.isDeleteRequire = newRow.rowId !== 0;
            this.partnerDetailsList.push(newRow);
            for (let i = 0; i < this.partnerDetailsList.length; i++) {
                this.partnerDetailsList[i].count = i + 1;
                this.partnerDetailsList[i].rowId = i;
            }
        }
        else {
            this.dispatchEvent(new CustomEvent('multiplerowcallback', { detail: 'You can not add more than five rows.' }));
        }
    }

    handleOnFocus(event) {
        const id = event.target.dataset.id;
        if (id === 'homePhone') {
            event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
        }
        if (id === 'socialSecurity') {
            event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
        }
    }

    handleOnKeyUp(event) {
        event.target.setCustomValidity("");
        const charCode = event.which;
        if (charCode !== 92 && charCode !== 13 && (charCode < 48 || charCode > 57)) {
            event.target.setCustomValidity(PARTNER_INFORMATION_LABELS.invalidPhoneNumberFormat);
        }
        event.target.reportValidity();
    }

    onHandleChange(event) {
        const id = event.target.dataset.id,
            index = event.target.dataset.index;
        var newDate = new Date();

        if (id === 'homePhone') {
            event.target.setCustomValidity("");
            if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 10 && event.target.value.match(/^[0-9]*$/)) {
                const phoneNumber = event.target.value;
                event.target.value = UtilService.formatPhoneToUSFormat(phoneNumber);
            }
            else if (event.target.value.length === 0) {
                event.target.setCustomValidity("");
            }
            else{
                event.target.setCustomValidity(PARTNER_INFORMATION_LABELS.invalidPhoneNumberFormat);
            }
            event.target.reportValidity();
        }

        if (id === 'socialSecurity') {
            event.target.setCustomValidity("");
            if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 9 && event.target.value.match(/^[0-9]*$/)) {
                var ssnNumber = event.target.value.replace(/\D/g, '');
                ssnNumber = ssnNumber.replace(/^(\d{3})(\d{1,2})/, '$1-$2');
                ssnNumber = ssnNumber.replace(/^(\d{3})-(\d{2})(.+)/, '$1-$2-$3');
                event.target.value = ssnNumber.substring(0, 11);
            }
            else if (event.target.value.length === 0) {
                event.target.setCustomValidity("");
            }
            else{
                event.target.setCustomValidity("Invalid Social Security No. Format");
            }
            event.target.reportValidity();
        }

        this.dateValue = newDate.toISOString().substring(0, 10);
        this.partnerDetailsList[index][id] = event.target.value;
        let formData = Object.assign({}, this.partnerDetailsList[index]);
        formData = this.checkForRequired(formData);
        this.partnerDetailsList[index] = formData;
        this.isDisableRequire = false;

    }

    checkForRequired(formData) {
        let isRequiredFormValue = false;
        for (let [key, value] of Object.entries(formData)) {
            if (IGNORE_CHECK_IN_DATA_VALIDATE.indexOf(key) === -1 && value !== '') {
                isRequiredFormValue = true;
                break;
            }
        }
        formData.isRequired = isRequiredFormValue;
        return formData;
    }

    @api submitPartnerDetailData() {
        const current = this,
            PartnerInformationArr = current.partnerDetailsList,
            validatedFormDataResult = UtilService.validateFormData(
                IGNORE_CHECK_IN_DATA_VALIDATE, PartnerInformationArr, current, 'isFinalPartnerFormValid');
                const duplicateFormData = UtilService.checkDuplicateVariables(
                    IGNORE_CHECK_IN_DATA_VALIDATE, PartnerInformationArr,DUPLICATE_CHECK_KEYS);    
        current.partnerDetailsList = validatedFormDataResult.result;
        if (validatedFormDataResult.isValid) {
            if(duplicateFormData){
                return this.partnerDetailsList;
            }
            else{
                current.errorDetails.isErrorVisible = true;
                current.errorDetails.headerMessage = 'Duplicate Value';
                current.errorDetails.errorContent = 'Partner Information Section contains duplicate data';
                return null;
            }
        }
        return null;
    }

    onClearRows() {
        const current = this;
        current.isDisableRequire = true;
        current.partnerDetailsList[0] = new PartnerInformationModel(0);
        UtilService.reportValidity(current.partnerDetailsList[0], current);     
       
    }

    @wire(getCountryAndStateValues)
    getCountryStateData({ data, error }) {
        if (data) {
            let countyOptions = [{ label: '--None--', value: '--None--' }];
            let stateData = [{ label: '--None--', value: '--None--' }];
            for (let [key, value] of Object.entries(data)) {
                countyOptions.push({ label: key, value: key });
                stateData.push({ label: key, value: value });

            }
            this.defaultCountryAndStateData = stateData;
            console.log(this.defaultCountryAndStateData + 'dataaa');
            this.countryControllingValues = countyOptions;
        }

    }

    handleSelectDependentpicklist(event) {
        const param = event.detail;
        if (param.key) {
            this.partnerDetailsList[param.index][param.key] = param.val;
        }
    }
    handleErrorPopup() {
        this.errorDetails.isErrorVisible = false;
    }

}