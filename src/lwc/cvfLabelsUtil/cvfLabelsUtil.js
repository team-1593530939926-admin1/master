import companyName from "@salesforce/label/c.Text_Company_Name";
import companyAddress from "@salesforce/label/c.Text_Company_Address";
import city from "@salesforce/label/c.Text_City";
import country from "@salesforce/label/c.Text_Country";
import state from "@salesforce/label/c.Text_State";
import zip from "@salesforce/label/c.Text_Zip";
import documentationType from "@salesforce/label/c.Text_Documentation_Type";
import phoneNumber from "@salesforce/label/c.Text_Phone_Number";
import fax from "@salesforce/label/c.Text_Fax";
import firstName from "@salesforce/label/c.Text_Contact_First_Name";
import lastName from "@salesforce/label/c.Text_Contact_Last_Name";
import companyCode from "@salesforce/label/c.Text_Company_Code";
import addRow from '@salesforce/label/c.Text_Add_Row';
import deleteRow from '@salesforce/label/c.Text_Delete_Row';
import clearData from '@salesforce/label/c.Text_Clear_Data';
import HomePhone from '@salesforce/label/c.Text_Home_Phone';
import SocialSecurity from '@salesforce/label/c.Text_Social_Security';
import OwnershipPercentage from '@salesforce/label/c.Text_Ownership_Percentage';
import DOB from '@salesforce/label/c.Text_DOB';
import FirstName from '@salesforce/label/c.text_FirstName';
import MiddleName from '@salesforce/label/c.Text_MiddleName';
import LastName from '@salesforce/label/c.Text_LastName';
import MailingStreet from '@salesforce/label/c.Text_MailingStreet';
import MailingCity from '@salesforce/label/c.Text_MailingCity';
import MailingState from '@salesforce/label/c.Text_MailingState';
import ReviewApplication from '@salesforce/label/c.Text_Review_Application';
import MailingPostalCode from '@salesforce/label/c.Text_MailingPostalCode';
import MailingCountry from '@salesforce/label/c.Text_MailingCountry';
import bankName from '@salesforce/label/c.Text_Bank_Name';
import bankAddress from '@salesforce/label/c.Text_Bank_Address';
import bankContactPerson from '@salesforce/label/c.Text_Bank_Contact_Person';
import accountNumber from '@salesforce/label/c.Text_Account_Number_s';
import Guarantor from '@salesforce/label/c.Text_Guarantor';
import CompanyName from '@salesforce/label/c.Text_Company_Name';
import Prefix from '@salesforce/label/c.Text_Prefix';
import PrimaryGuarantor from '@salesforce/label/c.Text_Company_Guarantor';
import Contigent from '@salesforce/label/c.Text_Contigent';
import EmailAddress from '@salesforce/label/c.Text_Email_Address';
import Tin from '@salesforce/label/c.Text_Tin';
import PhoneNumber from '@salesforce/label/c.Text_Phone_Number';
import TaxID from '@salesforce/label/c.Text_Tax_ID';
import Fax from '@salesforce/label/c.Text_Fax';
import emailAddress from "@salesforce/label/c.Text_Email_Address";
import NatureOfBusiness from '@salesforce/label/c.Text_Nature_of_Business';
import FederalTaxId from '@salesforce/label/c.Text_Federal_Tax_ID';
import OtherStreetAddress from "@salesforce/label/c.Text_Office_Street_Address";
import OtherCity from "@salesforce/label/c.Text_Office_City";
import OtherCounty from "@salesforce/label/c.Text_Office_County";
import OtherState from "@salesforce/label/c.Text_Office_State";
import OtherZip from "@salesforce/label/c.Text_Office_Zip";
import companyInformation from "@salesforce/label/c.Text_COMPANY_INFORMATION";
import fullLegalCompanyName from "@salesforce/label/c.Text_Full_Legal_Company_Name";
import billingStreetAddress from "@salesforce/label/c.Text_Billing_Street_Address";
import county from "@salesforce/label/c.Text_County";
import federalTaxId from "@salesforce/label/c.Text_Federal_Tax_ID";
import natureOfBusiness from "@salesforce/label/c.Text_Nature_of_Business";
import stateOrJurisdictionOfIncorporationOrganization from "@salesforce/label/c.Text_STATE_or_JURISDICTION_OF_INCORPORATION_ORGANIZATION";
import entity from "@salesforce/label/c.Text_Entity";
import dba from "@salesforce/label/c.Text_DBA";
import contactFirstName from "@salesforce/label/c.Text_Contact_First_Name";
import contactLastName from "@salesforce/label/c.Text_Contact_Last_Name";
import otherStreetAddress from "@salesforce/label/c.Text_Other_Street_Address";
import otherCity from "@salesforce/label/c.Text_Other_City";
import otherCounty from "@salesforce/label/c.Text_Other_County";
import otherState from "@salesforce/label/c.Text_Other_State";
import otherZip from "@salesforce/label/c.Text_Other_Zip";
import companyType from "@salesforce/label/c.Text_Company_Type";
import confirmatioMessage from "@salesforce/label/c.Text_Do_you_want_to_continue_with_the_Company_Name_and_State_Code";
import popupHeaderMessage from "@salesforce/label/c.Text_Confirmation";
import applicationId from "@salesforce/label/c.Text_Application_Id";
import dealName from "@salesforce/label/c.Text_Deal_Name";
import supplierName from "@salesforce/label/c.Text_Supplier_Name";
import contactName from "@salesforce/label/c.Text_Contact_Name";
import leaseTermMonths from "@salesforce/label/c.Text_Lease_Term_Months";
import estimatedEquipmentCost from "@salesforce/label/c.Text_Estimated_Equipment_Cost";
import equipmentDescription from "@salesforce/label/c.Text_Equipment_Description";
import endOfLeasePurchaseOption from "@salesforce/label/c.Text_End_of_Lease_Purchase_Option";
import other from "@salesforce/label/c.Text_Other";
import percentOfTotalCashPrice from "@salesforce/label/c.Text_Percent_Of_Total_Cash_Price";
import equipmentLocation from "@salesforce/label/c.Text_Equipment_Location_if_different_from_above";
import pleaseEnterCostUptoTwoDecimalPlaces from '@salesforce/label/c.Text_Please_Enter_Cost_Upto_Two_Decimal_Places'
import pleaseEnterCorrectEquipmentCost from '@salesforce/label/c.Text_Please_Enter_Correct_Equipment_Cost'
import street from "@salesforce/label/c.Text_Street";
import yes from '@salesforce/label/c.Text_Yes';
import no from '@salesforce/label/c.Text_No';
import myApplication from '@salesforce/label/c.Text_My_Application';
import homePage from '@salesforce/label/c.Text_Home_Page';
import newApplication from '@salesforce/label/c.Text_New_Application';
import next from "@salesforce/label/c.Text_Next";
import submit from "@salesforce/label/c.Text_Submit";
import recordss from '@salesforce/label/c.Text_Records';
import previous from '@salesforce/label/c.Text_Previous';
import pleaseEnterCompanyCodeCompanyName from '@salesforce/label/c.Error_Please_Enter_Company_Code_Company_Name';
import searchApplication from '@salesforce/label/c.Text_Search_Application';
import errorPleaseEnterSomething from '@salesforce/label/c.Error_Please_Enter_Something';
import bankInformation from '@salesforce/label/c.Text_Bank_Information';
import tradeReferences from '@salesforce/label/c.Text_Trade_References';
import partnerInformation from '@salesforce/label/c.Text_Partner_Information';
import equipmentPopup from '@salesforce/label/c.Text_Equipment_Popup';
import equipmentInformation from '@salesforce/label/c.TEXT_EQUIPMENT_INFORMATION';
import validate from '@salesforce/label/c.Text_Validate';
import pendingApplications from '@salesforce/label/c.Text_Pending_Applications';
import guarantorInformation from '@salesforce/label/c.Text_Guarantor_Information';
import previousValue from '@salesforce/label/c.Text_Previous_Value';
import nextValue from "@salesforce/label/c.Text_Next_Value";
import review from "@salesforce/label/c.Text_Review";
import pleaseSelectAboveCriteriaToFetchRecords from '@salesforce/label/c.Text_Please_Select_Above_Criteria_To_Fetch_Records';
import selectAtleastOneGuarantorAsPrimaryGuarantor from '@salesforce/label/c.Text_Select_atleast_one_Guarantor_as_Primary_Guarantor';
import youCanNotSelectMultipleGuarantorAsPrimaryGuarantor from '@salesforce/label/c.Text_You_can_not_select_Multiple_Guarantor_as_Primary_Guarantor';
import businessPurpose from '@salesforce/label/c.Text_Business_Purpose';
import youTheCreditApplicant from '@salesforce/label/c.Text_By_Clicking_Submit';
import USApatriotActNotification from '@salesforce/label/c.Text_USA_PATRIOT_ACT_NOTIFICATION';
import toHelptheGovernment from '@salesforce/label/c.Text_It_Has_Recieved';
import ecoaNotice from '@salesforce/label/c.Text_ECOA_Notice';
import yorAreApplyingFor from '@salesforce/label/c.Text_it_will_comply';
import theFederalEqualCreditOpportunity from '@salesforce/label/c.Text_The_Federal_Equal_Credit_Opportunity';
import releaseAuthorization from '@salesforce/label/c.Text_RELEASE_AUTHORIZATION';
import toWhomItMayConcern from '@salesforce/label/c.Text_Broker_understands_and_acknowledges';
import Textattachedhereto from '@salesforce/label/c.Text_attached_hereto';
import Textithasreceivedwrittenauthorization from '@salesforce/label/c.Text_it_has_received_written_authorization';
import termsAndConditions from '@salesforce/label/c.Text_Terms_And_Conditions';
import iAgree from '@salesforce/label/c.Text_I_Agree';
import viewMode from '@salesforce/label/c.Text_View_Mode';
import close from '@salesforce/label/c.Text_Close';
import companyCodeToolTipMessage from '@salesforce/label/c.Text_Company_Code_Tool_Tip_Message';
import pleaseEnterTheCorrectFaxDetails from '@salesforce/label/c.Error_Please_Enter_The_Correct_Fax_Details';
import search from '@salesforce/label/c.Text_Search';
import companyOrApplicationSearch from '@salesforce/label/c.Text_Company_Or_Application_Search';
import invalidPhoneNumberFormat from '@salesforce/label/c.Error_Invalid_Phone_Number_Format';
import pendingApplication from '@salesforce/label/c.Text_Pending_Application';
import searchApplicationDescription from '@salesforce/label/c.TEXT_SEARCH_APPLICATION_DESCRIPTION';
import pendingApplicationDescription from '@salesforce/label/c.TEXT_PENDING_APPLICATION_DESCRIPTION';
import newApplicationDescription from '@salesforce/label/c.TEXT_NEW_APPLICATION_DESCRIPTION';
import myApplicationDescription from '@salesforce/label/c.TEXT_MY_APPLICATION_DESCRIPTION';
import textYoucannotaddmorethanfiverows from '@salesforce/label/c.Text_You_can_not_add_more_than_five_rows';
import SubmittedSuccessfullyPleaseWaitForApproval from '@salesforce/label/c.Text_Submitted_successfully_Please_wait_for_Approval';
import thetotalsumofownershipshouldbeequalto100 from '@salesforce/label/c.Text_The_total_sum_of_ownership_should_be_equal_to_100_00';
import TextThetotalsumofContigentshouldbeequalto100 from '@salesforce/label/c.Text_The_total_sum_of_Contigent_should_be_equal_to_100_00';
import companySearch from '@salesforce/label/c.Text_Company_Search';
import uploadDocuments from '@salesforce/label/c.Text_Upload_Documents';
import guarantorSearch from '@salesforce/label/c.Text_Guarantor_Search';
import searchIndividualGuarantor from '@salesforce/label/c.Text_Search_Individual_Guarantors';
import searchCorporateGuarantor from '@salesforce/label/c.Text_Search_Corporate_Guarantors';
import DocumentUpload from '@salesforce/label/c.Text_Document_Upload';
import Description from '@salesforce/label/c.Text_Description';
import AttachFiles from '@salesforce/label/c.Text_Attach_Files';
import Type from '@salesforce/label/c.Text_File_Type';
import View from '@salesforce/label/c.Text_View';
import FileName from '@salesforce/label/c.Text_File_Name';
import Remove from '@salesforce/label/c.Text_Remove';
import companyTypeValue from '@salesforce/label/c.Text_Company_Type_Value';
import noRecordsFound from '@salesforce/label/c.Text_No_Records_Found';
import uploadedDocuments from '@salesforce/label/c.Text_Uploaded_Documents';
import enterTheValue from '@salesforce/label/c.Text_Enter_The_Value';
import allowedFormats from '@salesforce/label/c.Text_Allowed_Formats';
import download from '@salesforce/label/c.Text_Download';
import noDocumentsUploaded from '@salesforce/label/c.Text_No_Documents_Uploaded';
import dateOfIncorporation from '@salesforce/label/c.Text_Date_of_Incorporation_Formation';
import clear from '@salesforce/label/c.Text_Clear';
import commentSubmittedSuccessfully from '@salesforce/label/c.Text_Your_Comment_Submitted_Successfully';
import success from '@salesforce/label/c.Text_Successful';
import ok from '@salesforce/label/c.Text_0k';
import commnenthasalreadybeenregistered from '@salesforce/label/c.Text_Your_commnent_has_already_been_registered';
import brokerComments from '@salesforce/label/c.Text_Broker_Comments';
import pleaseUploadAllTheRequiredDocuments from '@salesforce/label/c.Text_Please_Upload_All_The_Required_Document_Against_The_Section';
import creditConditionError from '@salesforce/label/c.Text_Credit_Condition_Error';
import note from '@salesforce/label/c.Text_Note';

const TERMS_AND_CONDITION =
{
  iAgree,
  submit

}
const PENDING_APPLICATION =
{
  recordss,
  previous,
  next,
  clear,
  noRecordsFound
}

const PENDING_DATA_TABLE =
{
  pendingApplications,
  homePage,
  newApplication,
  noRecordsFound
}

const BROKER_APPLICATION_FORM_LABEL =
{
  companyInformation,
  bankInformation,
  tradeReferences,
  partnerInformation,
  equipmentInformation,
  yes,
  no,
  equipmentPopup,
  guarantorInformation,
  previousValue,
  nextValue,
  review,
  submit,
  businessPurpose,
  youTheCreditApplicant,
  USApatriotActNotification,
  toHelptheGovernment,
  ecoaNotice,
  yorAreApplyingFor,
  theFederalEqualCreditOpportunity,
  releaseAuthorization,
  toWhomItMayConcern,
  termsAndConditions,
  viewMode,
  ReviewApplication,
  close,
  SubmittedSuccessfullyPleaseWaitForApproval,
  thetotalsumofownershipshouldbeequalto100,
  TextThetotalsumofContigentshouldbeequalto100,
  uploadDocuments,
  companyTypeValue,
  Textithasreceivedwrittenauthorization,
  Textattachedhereto

}

const BROER_COMMENT_LABELS = {
  commentSubmittedSuccessfully,
  success,
  ok,
  commnenthasalreadybeenregistered,
  fullLegalCompanyName,
  dealName,
  estimatedEquipmentCost,
  equipmentDescription,
  brokerComments,
  pleaseUploadAllTheRequiredDocuments,
  creditConditionError


}

const VIEW_APPLICATION_LABELS =
{
  close,
  submit,
  uploadedDocuments
}

const SEARCHING_APPLICATION =
{
  companyCode,
  recordss,
  previous,
  next,
  applicationId,
  errorPleaseEnterSomething,
  pleaseSelectAboveCriteriaToFetchRecords,
  search,
  companyOrApplicationSearch,
  noRecordsFound
}

const SEARCH_DATA_TABLE =
{
  searchApplication,
  homePage
}

const CUSTOM_DATA_TABLE =
{
  companyCode,
  recordss,
  previous,
  next,
  pleaseEnterCompanyCodeCompanyName,
  search,
  noRecordsFound,
  clear
}

const DEALS_DATA_TABLE_LABELS =
{
  myApplication,
  homePage,
  newApplication
}

const COMPANY_INFORMATION_LABELS =
{
  CompanyName,
  companyType,
  nextValue,
  dba,
  entity,
  companyCode,
  companyInformation,
  otherCity,
  otherState,
  otherZip,
  fullLegalCompanyName,
  otherCounty,
  otherStreetAddress,
  emailAddress,
  contactFirstName,
  contactLastName,
  billingStreetAddress,
  city,
  county,
  state,
  zip,
  phoneNumber,
  fax,
  federalTaxId,
  natureOfBusiness,
  stateOrJurisdictionOfIncorporationOrganization,
  confirmatioMessage,
  popupHeaderMessage,
  validate,
  no,
  companyCodeToolTipMessage,
  pleaseEnterTheCorrectFaxDetails,
  invalidPhoneNumberFormat,
  companySearch,
  enterTheValue,
  dateOfIncorporation

};

const TRADE_INFORMATION_LABELS = {
  addRow,
  deleteRow,
  clearData,
  companyCode,
  companyName,
  companyAddress,
  firstName,
  lastName,
  city,
  state,
  zip,
  phoneNumber,
  fax,
  country,
  companyCodeToolTipMessage,
  pleaseEnterTheCorrectFaxDetails,
  invalidPhoneNumberFormat,
  textYoucannotaddmorethanfiverows,
  companySearch,
  enterTheValue
};

const PARTNER_INFORMATION_LABELS = {
  addRow,
  deleteRow,
  clearData, FirstName, MiddleName, LastName, MailingStreet, MailingCity,
  MailingState, MailingPostalCode, MailingCountry,
  HomePhone, SocialSecurity, DOB,
  OwnershipPercentage, pleaseEnterCostUptoTwoDecimalPlaces,
  invalidPhoneNumberFormat,
  textYoucannotaddmorethanfiverows,
  enterTheValue
}

const BANK_INFORMATION_LABELS = {
  addRow,
  deleteRow,
  clearData,
  bankName,
  bankAddress,
  bankContactPerson,
  accountNumber,
  city,
  state,
  country,
  phoneNumber,
  zip,
  invalidPhoneNumberFormat,
  textYoucannotaddmorethanfiverows,
  enterTheValue
};

const EQUIQMENT_INFORMATION_LABELS = {
  applicationId,
  dealName,
  supplierName,
  contactName,
  leaseTermMonths,
  estimatedEquipmentCost,
  equipmentDescription,
  endOfLeasePurchaseOption,
  equipmentLocation,
  otherStreetAddress,
  otherCity,
  otherCounty,
  otherState,
  otherZip,
  other,
  percentOfTotalCashPrice,
  pleaseEnterCostUptoTwoDecimalPlaces,
  pleaseEnterCorrectEquipmentCost,
  enterTheValue,
  documentationType,
  note
};

const GUARANTOR_INFORMATION_LABELS = {
  Guarantor,
  CompanyName,
  Prefix,
  street,
  city,
  state,
  zip,
  country,
  PrimaryGuarantor,
  Contigent,
  EmailAddress,
  Tin,
  PhoneNumber,
  TaxID,
  Fax,
  NatureOfBusiness,
  FederalTaxId,
  FirstName,
  MiddleName,
  LastName,
  DOB,
  emailAddress,
  SocialSecurity,
  OtherStreetAddress,
  OtherCity,
  OtherCounty,
  OtherState,
  OtherZip,
  selectAtleastOneGuarantorAsPrimaryGuarantor,
  youCanNotSelectMultipleGuarantorAsPrimaryGuarantor,
  pleaseEnterCostUptoTwoDecimalPlaces,
  pleaseEnterTheCorrectFaxDetails,
  invalidPhoneNumberFormat,
  textYoucannotaddmorethanfiverows,
  guarantorSearch,
  searchIndividualGuarantor,
  searchCorporateGuarantor,
  enterTheValue
}

const POP_UP_LABELS = {
  yes,
  no
}

const APPLICATION_HOME_LABELS = {
  myApplication,
  searchApplication,
  pendingApplications,
  newApplication,
  searchApplicationDescription,
  pendingApplicationDescription,
  myApplicationDescription,
  newApplicationDescription
}

const DEAL_DOCUMENT_UPLOAD_LABELS =
{
  DocumentUpload,
  Description,
  AttachFiles,
  close,
  review,
  Type,
  View,
  FileName,
  Remove,
  close,
  submit,
  allowedFormats,
  download,
  noDocumentsUploaded

}

export {
  COMPANY_INFORMATION_LABELS, TRADE_INFORMATION_LABELS, PARTNER_INFORMATION_LABELS,
  BANK_INFORMATION_LABELS, EQUIQMENT_INFORMATION_LABELS, GUARANTOR_INFORMATION_LABELS,
  POP_UP_LABELS, DEALS_DATA_TABLE_LABELS, CUSTOM_DATA_TABLE, SEARCH_DATA_TABLE,
  SEARCHING_APPLICATION, BROKER_APPLICATION_FORM_LABEL, PENDING_DATA_TABLE,
  PENDING_APPLICATION, TERMS_AND_CONDITION, APPLICATION_HOME_LABELS, DEAL_DOCUMENT_UPLOAD_LABELS,
  VIEW_APPLICATION_LABELS, BROER_COMMENT_LABELS
};