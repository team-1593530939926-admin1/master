import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
export default class ShowToastMessage extends LightningElement {
renderedCallback(){
  /*  const current = this,
        testURL = window.location.href,
        newURL = current.returnIdFromUrl(testURL);*/
    const event = new ShowToastEvent({
        title: 'Success',
        message:'Application submitted successfully - Please wait for approval',
        variant:'success',
    });
    this.dispatchEvent(event);
}

/*returnIdFromUrl(value){
    const newURL = value.match(/[a-z0-9]\w{4}0\w{12}|[a-z0-9]\w{4}0\w{9}/g);
    return newURL;
}*/
}