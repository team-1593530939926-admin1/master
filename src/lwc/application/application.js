import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { APPLICATION_HOME_LABELS } from "c/cvfLabelsUtil";

export default class Application extends NavigationMixin(LightningElement) {

    label = APPLICATION_HOME_LABELS;

    navigateToMyApplication() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'my-application',
            },
        });
    }

    navigateToNewApplication() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'new-application',
            },
        });
    }

    navigateToSearchApplication() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'search-application',
            },
        });
    }

    navigateToPendingApplication() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'pending-application',
            },
        });
    }

    navigateToHome() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'home'
            },
        });
    }
}