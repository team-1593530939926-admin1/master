import { LightningElement, track, api, wire } from "lwc";
import { TRADE_INFORMATION_LABELS } from "c/cvfLabelsUtil";
import { TradeInformationModel } from "c/tradeInformationModel";
//import getTradeRecordFromCompanyCode from "@salesforce/apex/BrokerApplicationFormController.getTradeRecordFromCompanyCode";
import { UtilService } from 'c/utilService';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';
import getTradeInformationByCompanyCode from '@salesforce/apex/BrokerApplicationFormController.getTradeInformationByCompanyCode';
const IGNORE_CHECK_IN_DATA_VALIDATE = ["isRequired", "isClearButtonVisible", "existingId", "rowId", "isDeleteRequire", "partnerId", "isTradeInformationFormValid", "count", "isDisable"];
const KEYS_FOR_DUPLICATE = ["companyName","fullSearchName","addressLine1","firstNameTrade","lastNameTrade","cityTrade","stateTrade","phoneNumberTrade"
,"zipTrade","faxTrade","countryTrade"];

export default class TradeInformation extends LightningElement {

  dependentPickListKey = {
    'stateTrade': 'stateTrade'
  };
  label = TRADE_INFORMATION_LABELS;
  //private properties are declared here
  @track countryControllingValues = [];
  @track stateDependentValues = [];
  @track defaultCountryAndStateData;
  @track countryVal;
  @track tradeReferenceInformationList = [];
  @track tradeInformationCompanyCodeValue;
  @track errorDetails = {
    isErrorVisible: false,
    headerMessage: "Error",
    errorContent: ''
};

  @api multipleRowCallback;
  @api isReviewEnabled = false;
  @api
  get tradeReferenceInformationListData() {
    return this.tradeReferenceInformationList;
  }

  set tradeReferenceInformationListData(value) {
    const current = this;
    current.tradeReferenceInformationList = [];
    value.forEach(function (eachValue) {
      current.tradeReferenceInformationList.push({ ...eachValue });
    })
  }

  onDeleteRow(event) {
    const sourceData = event.target.dataset,
      index = sourceData.index,
      currentIndex = this.tradeReferenceInformationList.findIndex(
        (element) => element.rowId + "" === index);
    this.tradeReferenceInformationList.splice(currentIndex, 1);
    for (let i = 0; i < this.tradeReferenceInformationList.length; i++) {
      this.tradeReferenceInformationList[i].count = i + 1;
      this.tradeReferenceInformationList[i].rowId = i;
    }
  }

 /* createNewRow() {
    let currentSize = this.tradeReferenceInformationList.length;
    if (currentSize <= 4) {

      let newRow = new TradeInformationModel(currentSize, true);
      newRow.isDeleteRequire = newRow.rowId !== 0;
      this.tradeReferenceInformationList.push(newRow);
      for (let i = 0; i < this.tradeReferenceInformationList.length; i++) {
        this.tradeReferenceInformationList[i].count = i + 1;
        this.tradeReferenceInformationList[i].rowId = i;
      }
    }
    else {
      this.dispatchEvent(new CustomEvent('multiplerowcallback', { detail: TRADE_INFORMATION_LABELS.textYoucannotaddmorethanfiverows }));
    }
  }*/

  createNewRow() {                  //////
    const tradeListData = [...this.tradeReferenceInformationList];

    let newListData = [];


    if (tradeListData.length <= 4) {

      let newRow = new TradeInformationModel(this.getLastRowId(), true);
      newRow.isDeleteRequire = newRow.rowId !== 0;

      tradeListData.forEach(function (oldData) {
        newListData.push(oldData);
      })
      newListData.push(newRow); 

      this.tradeReferenceInformationList = newListData;
      for (let i = 0; i < this.tradeReferenceInformationList.length; i++) {
        this.tradeReferenceInformationList[i].count = i + 1;
        this.tradeReferenceInformationList[i].rowId = i;
       
      }
    }
    
    else {
      this.dispatchEvent(new CustomEvent('multiplerowcallback',
       { detail: TRADE_INFORMATION_LABELS.textYoucannotaddmorethanfiverows }));
    }

  }

  getLastRowId () {                //////
    let rowId = 0;
    this.tradeReferenceInformationList.forEach(function (p) {
      if (p.lookupId > rowId) {
        rowId = p.lookupId;
      }
    });
    return rowId + 1;
  }

  onHandleChange(event) {
    const id = event.target.dataset.id,
      current = this,
      index = Number(event.target.dataset.index);

    if (id === 'phoneNumberTrade') {
      event.target.setCustomValidity("");
      if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 10 && event.target.value.match(/^[0-9]*$/)) {
        const phoneNumber = event.target.value;
        event.target.value = UtilService.formatPhoneToUSFormat(phoneNumber);
      }
      else if (event.target.value.length === 0) {
        event.target.setCustomValidity("");
      }
      else{
        event.target.setCustomValidity(TRADE_INFORMATION_LABELS.invalidPhoneNumberFormat);
    }
      event.target.reportValidity();
    }

    if (id === 'faxTrade') {
      event.target.setCustomValidity("");

      if ((event.target.value !== '' || event.target.value !== '() -') && !event.target.value.includes('@') && event.target.value.length === 10) {
        const phoneNumber = event.target.value;
        const s = ("" + phoneNumber).replace(/\D/g, '');
        const m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
        const formattedPhone = (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
        if (formattedPhone === null) {
          event.target.setCustomValidity(TRADE_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        else {
          event.target.value = formattedPhone;
        }

      }

      else if (!event.target.value.includes('@') && event.target.value.length !== 0) {
        event.target.setCustomValidity(TRADE_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
      }
      if (event.target.value.includes('@')) {
        if (event.target.value.match(/^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$/)) {
          event.target.value = event.target.value;
        }
        else {
          event.target.setCustomValidity(TRADE_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        event.target.reportValidity();
      }

      if (event.target.value === '() -') {
        event.target.value = '';
      }
      event.target.reportValidity();
    }
    if (id !== "companyCode") {
      current.tradeReferenceInformationList[index][id] = event.target.value;
      let formData = Object.assign({}, this.tradeReferenceInformationList[index]);
      formData = this.checkForRequired(formData);
      current.tradeReferenceInformationList[index] = formData;
    }
    else {
      current.tradeInformationCompanyCodeValue = event.target.value;
    }
  }

  handleOnKeyUp(event) {
    event.target.setCustomValidity("");
    const charCode = event.which;
    if (charCode !== 92 && charCode !== 13 && (charCode < 48 || charCode > 57)) {
      event.target.setCustomValidity(TRADE_INFORMATION_LABELS.invalidPhoneNumberFormat);
    }
    event.target.reportValidity();

  }

  handleOnFocus(event) {
    const id = event.target.dataset.id;
    if (id === 'phoneNumberTrade' || id === 'faxTrade') {
      event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
    }
    else if (id === 'faxTrade' && event.target.value.length === 0) {
      event.target.setCustomValidity("");
    }
  }

  checkForRequired(formData) {
    let isRequiredFormValue = false;
    for (let [key, value] of Object.entries(formData)) {
      if (IGNORE_CHECK_IN_DATA_VALIDATE.indexOf(key) === -1 && value !== "") {
        isRequiredFormValue = true;
        break;
      }
    }
    formData.isRequired = isRequiredFormValue;
    return formData;
  }

  onClear() {
    const current = this;
    current.isDisableRequire = true;
    current.tradeReferenceInformationList[0] = new TradeInformationModel(0, false);
    UtilService.reportValidity(current.tradeReferenceInformationList[0], current);
  }

  @api submitTradeDetailData() {
    const current = this;
    const TradeDetailFormDataArr = current.tradeReferenceInformationList;
    const validatedFormDataResult = UtilService.validateFormData(IGNORE_CHECK_IN_DATA_VALIDATE, TradeDetailFormDataArr, current, 'isTradeInformationFormValid');
    const duplicateFormData = UtilService.checkDuplicateVariables(
      IGNORE_CHECK_IN_DATA_VALIDATE, TradeDetailFormDataArr,KEYS_FOR_DUPLICATE);   
    current.tradeReferenceInformationList = validatedFormDataResult.result;
    if (validatedFormDataResult.isValid) {
      if(duplicateFormData){
      return this.tradeReferenceInformationList;
      }
      else{
        current.errorDetails.isErrorVisible = true;
        current.errorDetails.headerMessage = 'Duplicate Value';
        current.errorDetails.errorContent = 'Trade Information Section contains duplicate data';
        return null;
    }
    }
    return null;
  }

  @wire(getCountryAndStateValues)
  countryStateData({ data, error }) {
      if (data) {
          let countyOptions = [{ label: '--None--', value: '--None--' }];
          let stateData = [{ label: '--None--', value: '--None--' }];
          for (let [key, value] of Object.entries(data)) {

              countyOptions.push({ label: key, value: key });
              stateData.push({label: key, value: value});
              
            }
            console.log(stateData + 'State dataaa');

          this.defaultCountryAndStateData = stateData;
          console.log(this.defaultCountryAndStateData + 'dataaa');
          this.countryControllingValues = countyOptions;
      }
      else if (error) {
          window.console.log(error);
      }
  }

  handleSelectDependentpicklist(event) {
    const param = event.detail;
    if (param.key) {
      this.tradeReferenceInformationList[param.index][param.key] = param.val;
    }
  }
 
  handleTradeSelection(event) { 
    const id = event.target.dataset.id,
    current = this,
    index = Number(event.target.dataset.rowId);


   getTradeInformationByCompanyCode({ dealCompanyCode: event.detail, count: index})
   .then(result => {
       current.tradeReferenceInformationList[index] = new TradeInformationModel(index, false);
       UtilService.reportValidity(current.tradeReferenceInformationList[index], current);

       current.tradeReferenceInformationList[index] = {...result};
      
       current.isLoaded = false;
       current.isReadOnly = true;
       current.tradeReferenceInformationList[index].isRecordSearch = true;
       current.tradeReferenceInformationList[index].existingId = event.detail; 
   })
  /* .catch(error => {
       current.isLoaded = false;
       current.errorDetails.isErrorVisible = true;
       current.errorDetails.errorContent = error.body.message;

   });   */
    
  }

  clearFormFromCustomLookup(event) {
    const current = this,
    index = Number(event.target.dataset.rowId);
    current.tradeReferenceInformationList[index] = new TradeInformationModel(index);
    if(index !== 0 && index){
      current.tradeReferenceInformationList[index].isDeleteRequire = true;
    }
  }

  handleErrorPopup() {
    this.errorDetails.isErrorVisible = false;
}

 
}