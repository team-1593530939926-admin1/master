import { LightningElement, track, api } from 'lwc';
import getFCSBusinessInfo from '@salesforce/apex/FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode';
import getFCSBusinessDetail from '@salesforce/apex/FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId';
import saveEntityInformation from '@salesforce/apex/FCSEntityVerificationController.saveEntityVerificationDetails';
import isNeedToFCSAPICall from '@salesforce/apex/FCSEntityVerificationController.isNeedToFCSAPICall';
import getEntityVerificationDetailByCompanyIdSF from '@salesforce/apex/FCSEntityVerificationController.getEntityVerificationDetailByCompanyIdSF';
import { FCS_ENTITY_VERIFICATION_LABELS } from "c/fcsLabelsUtil";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const VALIDATE_LABEL = 'validate';
const FCS_TOAST_TITLE = 'CVF Portal';

export default class FcsBrokerApplicationFormIntegration extends LightningElement {

    entityVerificationLabels = FCS_ENTITY_VERIFICATION_LABELS;

    title = this.entityVerificationLabels.entityVerificationLabel;
    message = this.entityVerificationLabels.doYouWantToContinueLabel;
    confirmLabel = this.entityVerificationLabels.yesLabel;
    cancelLabel = this.entityVerificationLabels.noLabel;
    originalMessage = ''
    selectedBusinessDetail;

    @api businessName;
    @api stateCode;
    @api recordId;
    @api invocationFrom;
    @api backToDetailCallback;

    // private attributes
    @track errorString;
    @track businessDetails;
    @track businessInformation;
    @track isBusinessInformation = false;
    @track isConfirmationPrompt = false;
    @track selectedBusiness;
    @track isModalOpen = false;
    @track isLoading = false;
    @track isBusinessDetailSFModel = false;
    @track entityBusinessDetailFromSF;

    connectedCallback() {
       
    }

    @api
    invokeFCSFlow() {
        this.checkIfFCSCallNeeded();
    }

    getBusinessInfo(event) {
        const current = this;
        current.closeBusinessInfoModal();
        current.isLoading = true;
        this.errorString = undefined;

        getFCSBusinessInfo({
            'businessName': this.businessName,
            'stateCode': this.stateCode
        })
            .then(result => {
                this.businessInformation = undefined;
                if (result.results.length > 0) {
                    if (result.results.length === 1) {
                        if (result.results[0].hasOwnProperty('entityID') && result.results[0].entityID === '') {
                            this.errorString = 'No Result Found.';
                            current.isLoading = false;
                            current.closeBusinessInfoModal();
                            current.showToast(FCS_TOAST_TITLE, 'No Result Found.', 'error');
                            return false;
                        } 
                    }
                    this.businessInformation = result;
                    current.openBusinessInfoModal();
                } 
              
                current.isLoading = false;
            })
            .catch(error => {
                this.errorString = error;
                current.isLoading = false;
                current.closeBusinessInfoModal();
                current.showToast(FCS_TOAST_TITLE, error.body, 'error');
            });
    }

    checkIfFCSCallNeeded(event) {
        const current = this;
        current.isLoading = true;
        this.errorString = undefined;

        isNeedToFCSAPICall({
            'recordId': current.recordId 
        })
        .then(result => {
            if (result) {
                current.getBusinessInfo();
             } 

             if (!result) {
                current.getBusinessDetailsFromSF();
             }
        })
        .catch(error => {
            this.errorString = error;
            current.isLoading = false;
            current.closeBusinessInfoModal();
        });
    }

    getBusinessDetailsFromSF(event) {
        const current = this;
        current.isLoading = true;
        this.errorString = undefined;

        getEntityVerificationDetailByCompanyIdSF({
            'recordId': current.recordId
        })
            .then(result => {
                current.entityBusinessDetailFromSF = undefined;
                current.entityBusinessDetailFromSF =  result;
                current.openBusinessDetailSFModal();
                current.isLoading = false;
            })
            .catch(error => {
                current.showToast(FCS_TOAST_TITLE, error.body, 'error');
                this.errorString = error;
                current.isLoading = false; 
            });
    }

    getBusinessDetail() {
        const current = this,
            selectedBusinessInfo = current.selectedBusiness,
            params = {
                transactionId : selectedBusinessInfo.transactionID, 
                entityId:selectedBusinessInfo.entityID, 
                additionalSearch: selectedBusinessInfo.additionalSearchParameters, 
                passThrough: selectedBusinessInfo.passThrough
            };

        current.closeBusinessDetailModal();

        current.isLoading = true;
        this.errorString = undefined;

        getFCSBusinessDetail(params)
            .then(result => {
                this.businessDetails = undefined;
                let isProcess = false;
                if (result && result.listOfResultWrapper) {
                    if (result.listOfResultWrapper.length > 0 && 
                        Object.keys(result.listOfResultWrapper[0]).length > 0) {
                            isProcess = true;
                           
                    }  
                }
               
                if (isProcess) {
                    current.businessDetails = result;
                    current.openBusinessDetailModal();
                    current.isLoading = false;
                } else {
                    current.errorString = {message:  'No Detail found for selected entitiy'};
                    current.showToast(FCS_TOAST_TITLE, 'No Detail found for selected entitiy', 'error');
                    current.closeBusinessInfoModal();
                    current.isLoading = false;
                }
            
            })
            .catch(error => {
                this.errorString = error;
                current.showToast(FCS_TOAST_TITLE, error.body, 'error');
                current.isLoading = false;
                current.closeBusinessInfoModal();
            });
    }

    openBusinessDetailModal() {
        this.isModalOpen = true;
    }

    closeBusinessDetailModal() {
        this.isModalOpen = false;
    }

    openBusinessDetailSFModal() {
        this.isBusinessDetailSFModel = true;
    }

    closeBusinessDetailSFModal() {
        this.isBusinessDetailSFModel = false;
    }

    openBusinessInfoModal() {
        this.isBusinessInformation = true;
    }

    closeBusinessInfoModal() {
        this.isBusinessInformation = false;
    }

    handleBusinessDetailAction(event) {
        const current = this,
            params = event.detail,
            actionName = params.actionName,
            action = params.action;


        if (actionName) {

            if (actionName === 'cancel') {
                current.closeBusinessDetailModal();
                current.openBusinessInfoModal();
            }

            if (actionName === 'certify') {
                const selectedBusinessDetails = params.businessDetails;
                current.selectedBusinessDetail = selectedBusinessDetails;
                //current.isLoading = true;
                current.handleFcsFlowCallback(true, 'verified', current.selectedBusinessDetail, true);
                current.showToast(FCS_TOAST_TITLE, 'Entity Verified Successfully!', 'success');
                //current.handleSaveEntityVerificationInfo();
            }
           
        }
        current.closeBusinessDetailModal();
    }

    handleBusinessDetailSF(event) {
        const current = this,
            params = event.detail,
            actionName = params.actionName,
            action = params.action;

            
        let selectedBusinessDetails, 
            status = 'pending';

        if (actionName) {

            if (actionName === 'cancel' || actionName === 'certify') {
                if (actionName === 'certify') {
                    selectedBusinessDetails = params.selectedBusinessDetail;
                    current.selectedBusinessDetail = selectedBusinessDetails;
                    status = 'verified';
                }
              
                //current.isLoading = true;
                current.handleFcsFlowCallback(true, status, selectedBusinessDetails, false);
                if (status === 'verified') {
                    current.showToast(FCS_TOAST_TITLE, 'Entity Verified Successfully!', 'success');
                }
            }
 
        }
        current.closeBusinessDetailSFModal();
    }


    get selectedBusinessDetailInformation() {
        return JSON.stringify(this.selectedBusinessDetail);
    }

    submitDetails() {
        this.isModalOpen = false;
    }

    handleHideBusinessInfo(event) {
        this.closeBusinessInfoModal();
    }

    handleBusinessInformationAction(event) {
        const current = this,
            detail = event.detail,
            actionName = detail.actionName;

        if (actionName === VALIDATE_LABEL) {
            current.selectedBusiness = detail.selectedBusinessInfo;
            current.isConfirmationPrompt = true;
        }

        if (actionName === 'cancel') {
            current.handleBackToDetail();
        }

        current.closeBusinessInfoModal();
    }

    handleConfirmModalClik(event) {
        const current = this;
        if (event.target) {

            if (event.target.name === 'openConfirmation') {
                this.originalMessage = 'test message';
                this.isDialogVisible = true;
            } else if (event.target.name === 'confirmModal') {

                if (event.detail !== 1) {
                    this.displayMessage = 'Status: ' + event.detail.status + '. Event detail: ' + JSON.stringify(event.detail.originalMessage) + '.';

                    if (event.detail.status === 'confirm') {
                        current.getBusinessDetail();
                    } else if (event.detail.status === 'cancel') {
                        current.openBusinessInfoModal();
                    }
                }
                //hides the component
                this.isConfirmationPrompt = false;
            }
        }
    }

    handleBackToDetail(event) {
        const current = this;
        if (current.backToDetailCallback !== undefined && typeof current.backToDetailCallback === 'function' ) {
            current.backToDetailCallback.call(current);
        }
    }

    handleSaveEntityVerificationInfo() {
        const current = this,
            params = {
            recordId: current.recordId,
            businessDetailWrapperString : JSON.stringify(current.businessDetails)
        };

        saveEntityInformation(params)
        .then(result => { 
            current.closeBusinessDetailModal();
            current.isLoading = false;
            current.handleBackToDetail();
        })
        .catch(error => {
            current.errorString = error;
            current.showToast(FCS_TOAST_TITLE, error.body, 'error');
            current.closeBusinessInfoModal();
            current.isLoading = false;
         });
    }

    showToast(heading, msg, toastType) {
        let message = 'Server is Busy!';

        if (typeof msg === 'object') {
            if (typeof msg.message === 'string' && msg.message.indexOf('{') !== -1) {
                const errorMsg = JSON.parse(msg.message);
                message = 'Server is Busy!';//errorMsg.Message;
            } else if (msg.message) {
                message = msg.message;
            }
        } else if (typeof msg === 'string') {
            message = msg;
        }

        const event = new ShowToastEvent({
            title: heading,
            message: message,
            variant: toastType,
            mode: 'dismissable'
        });
        this.dispatchEvent(event);
    }

    handleFcsFlowCallback (isSuccess, action, result, isFromFcs) {
        const param = {
            isSuccess: isSuccess,
            action: action,
            result: result,
            isFromFCS: isFromFcs
        },
        customEvent = new CustomEvent('fcsflowcallback', {detail: param});
        this.dispatchEvent(customEvent);
    }
}