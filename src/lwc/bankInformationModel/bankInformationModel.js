export class BankInformationModel {
    isDisable = false;
    existingId = null;
    isBankInformationFinalFormValid = true;
    isRequired = false;
    rowId = -1;
    bankName = "";
    addressLine1 = "";
    bankContactPerson = "";
    accountNumber = "";
    city = "";
    state = "";
    country = "";
    phoneNumber = "";
    zip = "";
    partnerId = "";
    count = "0";
    isCreatedByMe = true;
    maskedAccountNumber = "";
    constructor(rowId) {
        this.rowId = rowId;
        this.count = Number(rowId) + 1;
        this.isCreatedByMe = true;
    }
}