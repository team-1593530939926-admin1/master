import LightningDatatable from 'lightning/datatable';
import referenceTypeDataCell from './referenceTypeDataCell.html';

export default class CustomDataTypeMapping extends LightningDatatable  {
    static customTypes = {
        customReferenceCell: {
            template: referenceTypeDataCell,
            typeAttributes: ['referenceData', 'referenceKey'],
            standardCellLayout: true
        }
    }
}