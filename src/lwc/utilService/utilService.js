import { ShowToastEvent } from 'lightning/platformShowToastEvent';
class UtilService {

  static format(stringToFormat, ...formattingArguments) {
    if (typeof stringToFormat !== 'string') throw new Error('\'stringToFormat\' must be a String');
    // eslint-disable-next-line no-confusing-arrow
    return stringToFormat.replace(/{(\d+)}/gm, (match, index) =>
      (formattingArguments[index] === undefined ? '' : `${formattingArguments[index]}`));
  }

  static modifiableObject(element) {
    return { ...element };
  }

  static validateFormData(keysToIgnoreArr, formDataArr, parentReference, formDataValidateKeyField) {
    const template = this;
    let isFormDataValid = true;
    formDataArr.forEach(function (formData) {
      for (const [key, value] of Object.entries(formData)) {
        if(keysToIgnoreArr.includes("primaryGuarantor")){
          keysToIgnoreArr.pop("primaryGuarantor");
        }
        if (keysToIgnoreArr.indexOf(key) === -1) {
          const targetCmp = parentReference.template.querySelectorAll(`[data-id="${key}"]`);

          if (targetCmp) {
            targetCmp.forEach(function (target) {
              if (target && !target.checkValidity()) {
                target.reportValidity();
                formData[formDataValidateKeyField] = false;
                isFormDataValid = false;
              } else if (target) {
                target.reportValidity();
              }
            });
          }
        }
      }
    });
    return { result: formDataArr, isValid: isFormDataValid };
  }

  static generateDateTimeStampInMMDDYYHHMM(prefixString) {
    let currentdate = new Date();
    let datetime = ((prefixString && prefixString.trim().length > 0) ? prefixString : '')
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getDate() + "/"
      + currentdate.getFullYear() + " - "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes();

    return datetime;
  }

  static resetUSPhoneNumberFormat(phoneNumber) {
    if (phoneNumber && phoneNumber.trim().length > 0) {
      return phoneNumber.replace(/[{( )}-]/g, '');
    }
    return phoneNumber;
  }

  static formatPhoneToUSFormat(phoneNumber) {
    if (phoneNumber && phoneNumber.trim().length > 0) {
      const s = ("" + phoneNumber).replace(/\D/g, '');
      const m = s.match(/^(\d{3})(\d{3})(\d{4})$/);
      return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
    }
    return phoneNumber;
  }

  static updateDisableValueFormData(formData, parentReference, isEntityValidate) {
    if (formData) {
      if (formData.existingId != null || isEntityValidate) {
        Object.entries(formData).forEach(function ([key, value]) {
          const disableKey = 'is' + key.charAt(0).toUpperCase() + key.slice(1) + 'Disable';
          if ((value && value !== "") || isEntityValidate) {
            formData[disableKey] = true;
          }
        });


        Promise.resolve().then(() => {
          Object.entries(formData).forEach(function ([key, value]) {
            if (parentReference) {
              const targetCmp = parentReference.template.querySelectorAll(`[data-id="${key}"]`);
              if (targetCmp) {
                targetCmp.forEach(function (target) {

                  if (target) {
                    target.reportValidity();
                  }

                });
              }
            }
          });
        });
      }
    }
    return formData;
  }




  static reportValidity(formData, parentReference) {
    if (formData) {
      Promise.resolve().then(() => {
        Object.entries(formData).forEach(function ([key, value]) {
          if (parentReference) {
            const targetCmp = parentReference.template.querySelectorAll(`[data-id="${key}"]`);
            if (targetCmp) {
              targetCmp.forEach(function (target) {

                if (target) {
                  target.reportValidity();
                }

              });
            }
          }
        });

      });
    }
  }

  static showToast(heading, msg, toastType, parentReference) {
    const event = new ShowToastEvent({
      title: heading,
      message: (msg ? (typeof msg === 'object' ? msg.message : msg) : 'Something went wrong!'),
      variant: toastType,
      mode: 'dismissable'
    });
    parentReference.dispatchEvent(event);
  }

  /*
  onValidateForm() {
    const currentTemplate = this;
    let isAllFormValid = true,
      formData = this.tradeReferenceInformationList;
    formData.forEach(element => {
      element.isTradeInformationFormValid = true;
    });
 
    const requiredFormDataMissing = formData.map(function (formObject) {
      let missingValueFormObj = [];
      for (const [key, value] of Object.entries(formObject)) {
        if (IGNORE_CHECK_IN_DATA_VALIDATE.indexOf(key) === -1) {
          if (value === '') {
            missingValueFormObj.push(key);
          }
        }
        if (FORM_FIELD_TO_CHECK_PATTERN.indexOf(key) !== -1) {
          missingValueFormObj.push(key);
        }
      }
      return missingValueFormObj;
    });
 
    requiredFormDataMissing.forEach(function (partnerDetailData, index) {
      partnerDetailData.forEach(function (eachKey) {
        const targets = currentTemplate.template.querySelectorAll(
          `[data-id="${eachKey}"]`
        );
        targets.forEach(function (target) {
          if (!target.checkValidity()) {
            target.reportValidity();
            if (isAllFormValid) {
              isAllFormValid = false;
              currentTemplate.tradeReferenceInformationList[index].isTradeInformationFormValid = false;
            }
          }
        });
      });
    });
 
    return isAllFormValid;
  }

  checkForRequired(formData) {
    const currentTemplate = this;
    let isRequiredFormValue = false;
    for (let [key, value] of Object.entries(formData)) {
      if (IGNORE_CHECK_IN_DATA_VALIDATE.indexOf(key) === -1 && value !== "") {
        isRequiredFormValue = true;
        break;
      }
    }
    formData.isRequired = isRequiredFormValue;
    return formData;
  }

*/
  static checkDuplicateVariables(keysToIgnoreArr, formData, KEYS_FOR_DUPLICATE) {
    let duplicateNotPresent = true,
      setOfData = new Set(),
      wholeValue = null,
      convertedValue = null;
    if (formData.length > 1) {
      formData.forEach(function (dataPresent) {


   //   for (const [key, value] of Object.entries(dataPresent)) {
          KEYS_FOR_DUPLICATE.forEach(function (keyToCheck) {
           
            convertedValue =  dataPresent[keyToCheck];
            if(convertedValue !== undefined){
              convertedValue = convertedValue.toString().toLowerCase();
              wholeValue = wholeValue + convertedValue;
            }
           
          })
         
        /*  if (keysToIgnoreArr.indexOf(key) === -1) {
            convertedValue = value;
            convertedValue = convertedValue.toString().toLowerCase();
            wholeValue = wholeValue + convertedValue;

          }*/
      //  }
        setOfData.add(wholeValue);
        wholeValue = null;
      })

      if (formData.length !== setOfData.size) {
        duplicateNotPresent = false;
      }
    }
    return duplicateNotPresent;
  }

  static CheckDuplicateVariableGuarantor(keysToIgnoreArr, formData, KEYS_FOR_DUPLICATE) {
    let IndividualData = [],
      CorporateData = [],
      duplicateNotPresent = true,
      keysToIgnoreFinal = [];
      const template = this;
      
    if (formData.length > 1) {
      formData.forEach(function (dataPresent) {
        for (const [key, value] of Object.entries(dataPresent)) {
          if (key === 'guarantorType' && value === 'Individual') {
            IndividualData.push(dataPresent);
          }
          if (key === 'guarantorType' && value === 'Corporate') {
            CorporateData.push(dataPresent);
          }
        }

      })
      keysToIgnoreFinal = keysToIgnoreArr;
      keysToIgnoreFinal.push('contigent');
      keysToIgnoreFinal.push('primaryGuarantor','isIsDeleteRequireDisable','isRowIdDisable');
      if(IndividualData.length > 1){
        duplicateNotPresent = template.checkIndividualDuplicateData(keysToIgnoreFinal, IndividualData, KEYS_FOR_DUPLICATE);
        if(duplicateNotPresent === false){
          return duplicateNotPresent;
        }
      }
     if(CorporateData.length > 1){
      duplicateNotPresent = template.checkCorporateDuplicateData(keysToIgnoreFinal, CorporateData,  KEYS_FOR_DUPLICATE);
      if(duplicateNotPresent === false){
        return duplicateNotPresent;
      }
     }
      
    }
    
    return duplicateNotPresent;
  }


  static checkIndividualDuplicateData(keysToIgnoreArr, IndividualData, KEYS_FOR_DUPLICATE) {
    let setOfIndividualData = new Set(),
      wholeValue = null,
      duplicateNotPresent = true,
    convertedValue = null;
    if (IndividualData.length > 1) {
      IndividualData.forEach(function (indiviData) {
      //  for (const [key, Value] of Object.entries(indiviData)) {
      //    if (keysToIgnoreArr.indexOf(key) === -1) {
            //convertedValue = Value;
            KEYS_FOR_DUPLICATE.forEach(function (keyToCheck) {
              convertedValue =  indiviData[keyToCheck];
              if(convertedValue !== undefined){
            convertedValue = convertedValue.toString().toLowerCase();
            wholeValue = wholeValue + convertedValue;
          }
           
        })
       //   }
      //  }
        setOfIndividualData.add(wholeValue);
        wholeValue = null;
      })
      if (IndividualData.length !== setOfIndividualData.size) {
        duplicateNotPresent = false;
      }
    }
    return duplicateNotPresent;
  }


  static checkCorporateDuplicateData(keysToIgnoreArr, CorporateData, KEYS_FOR_DUPLICATE) {
    let setOfCorporateData = new Set(),
      wholeValue = null,
      duplicateNotPresent = true,
    convertedValue = null;
    if (CorporateData.length > 1) {
      CorporateData.forEach(function (corpData) {
        KEYS_FOR_DUPLICATE.forEach(function (keyToCheck) {
     //   for (const [key, Value] of Object.entries(corpData)) {
       //   if (keysToIgnoreArr.indexOf(key) === -1) {
        convertedValue =  corpData[keyToCheck];
        if(convertedValue !== undefined){
            //convertedValue = Value;
            convertedValue = convertedValue.toString().toLowerCase();
            wholeValue = wholeValue + convertedValue;

        //  }
      //  }
    }
           
  })
        setOfCorporateData.add(wholeValue);
        wholeValue = null;
      })
      if (CorporateData.length !== setOfCorporateData.size) {
        duplicateNotPresent = false;
      }
    }
    return duplicateNotPresent;
  }

  static formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}
}

export { UtilService };