import { LightningElement, track, api, wire } from "lwc";
//for Picklist import
import { getPicklistValuesByRecordType } from "lightning/uiObjectInfoApi";
import Contact from "@salesforce/schema/Contact";
import getCurrentRecordTypeId from "@salesforce/apex/BrokerApplicationFormController.getGuarantorRecordTypeId";
import { GUARANTOR_INFORMATION_LABELS } from "c/cvfLabelsUtil";
import { GuarantorInformationModel } from "c/guarantorInformationModel";
import getGuarantorDetailsBasedOnSearch from '@salesforce/apex/BrokerApplicationFormController.getGuarantorDetailsBasedOnSearch';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';
import { UtilService } from 'c/utilService';

const KEYS_FOR_DUPLICATE = 
[
  "companyName","firstNameGuarantor","middleNameGuarantor","lastNameGuarantor","mailingStreetGurantor","mailingCountryGuarantor","mailingCityGuarantor","mailingStateGuarantor",
  "mailingPostalCodeGuarantor","homePhoneGuarantor","emailAddressGuarantor","faxGuarantor","federalTaxId",
  "natureofBusiness","dOB","socialSecurity","otherStreetAddress","otherCity","otherCounty","otherState","otherZip"
 ];
const FORM_FIELD_TO_IGNORE =
 [ 
  "maskedOtherStreetAddress", "maskedHomePhoneGuarantor", "maskedEmailAddressGuarantor", 
  "maskedFirstNameGuarantor", "maskedMiddleNameGuarantor", "maskedLastNameGuarantor", 
  "ismaskedOtherStreetAddressRequired", "ismaskedHomePhoneGuarantorRequired", "ismaskedEmailAddressGuarantorRequired",
  "isRequired","ismaskedDobRequired","ismaskedSocialSecurityRequired","ismaskedMailingStreetRequired",
  "rowId",
  "isDeleteRequire",
  "partnerId",
  "isfinalFormValid",
  "isGuarantor","isCreatedByMe","maskedDob","maskedSocialSecurity","maskedMailingStreetGurantor",
  "companyDataRequire","multipleGuarantor",
  "individualDataRequire","fullSearchName",
  "count","searchType","existingId","isReviewEnabled","isContigentEnabled","searchKey", "lookupId","isRecordSearch"
];

const TODAY_DATE = new Date().toISOString().substring(0, 10);

//import the labels
export default class GuarantorInformation extends LightningElement {
  dependentPickListKey = {
    'mailingStateGuarantor': 'mailingStateGuarantor',
    'otherState': 'otherState'
  };
  label = GUARANTOR_INFORMATION_LABELS;
  maxDateLimit = TODAY_DATE;

  //@track multipleGuarantor = false;
  @track countryControllingValues = [];
  @track stateDependentValues = [];
  @track defaultCountryAndStateData;
  @track countryVal;
  @track businessOptions;
  @track guarantorList = [];
  @track gurantorTypes;
  @track PrefixTypes;
  @track primaryTypes;
  @track isMutiplePrimary = false;
  @track recordTypeIdInfo;
  @track searchComponent = 
  {
    selectedGuarantor:'contact',
    guarantorSearchLabel:GUARANTOR_INFORMATION_LABELS.guarantorSearch,
    searchPlaceHolder:GUARANTOR_INFORMATION_LABELS.searchIndividualGuarantor
  };
  @track errorPopupMessageDescription;
  @track conditionalMessageBody;
  @track conditionalPopupMessageBodyElse;
  @track errorMessages;
  @track guarantorSearchKey;
  @track errorDetails = {
    isErrorVisible: false, 
    headerMessage: "Error",
    errorContent: ''
};

  @api recordTypeId;
  @api objectApiName;
 
  @api isReviewEnabled = false;


  @api
  get guarantorDetailsInformation() {
    return this.guarantorList;
  }

  set guarantorDetailsInformation(value) {
    const current = this;
    value.forEach(function (eachValue) {
      current.guarantorList.push({ ...eachValue });
    });
  }

  @wire(getCurrentRecordTypeId)
  recordTypeWired({ data, error }) {
    if (data) {
      this.recordTypeIdInfo = data.recordTypeId;
    } else if (error) {
      this.errorMessage = error;
    }
  }

  @wire(getPicklistValuesByRecordType, {
    objectApiName: Contact,
    recordTypeId: "$recordTypeIdInfo"
  })
  natureOfBusinessPicklistValues({ data, error }) {
    if (data) {
      if (data.picklistFieldValues.Nature_of_Business__c) {
        this.businessOptions = data.picklistFieldValues.Nature_of_Business__c.values;
      }

      if (data.picklistFieldValues.Guarantor_Type__c) {
        this.gurantorTypes = data.picklistFieldValues.Guarantor_Type__c.values;
      }

      if (data.picklistFieldValues.Primary_Guarantor__c) {
        this.primaryTypes = data.picklistFieldValues.Primary_Guarantor__c.values;
      }

      if (data.picklistFieldValues.Salutation) {
        this.PrefixTypes = data.picklistFieldValues.Salutation.values;
      }
    } else if (error) {
      this.errorMessages = error;
    }
  }

  renderedCallback() {
    const style = document.createElement('style'),
      targetComponent = this.template.querySelector('.container-style-tag');

    style.innerText = `
       .slds-input {
          height: 40px!important;
          min-height: calc(1.875rem + (2px * 5));
       }
       .slds-form-element__label {
          font-size: 15px!important;
       }`;

    if (targetComponent) {
      targetComponent.appendChild(style);
    }
  }
  setSearchBoxToFetchAccounts(){
    const current = this;
    current.searchComponent.selectedGuarantor = 'Account';
    current.searchComponent.searchPlaceHolder = GUARANTOR_INFORMATION_LABELS.searchCorporateGuarantor;
  }

  setSearchBoxToFetchContacts(){
    const current = this;
current.searchComponent.selectedGuarantor = 'contact';
current.searchComponent.searchPlaceHolder = GUARANTOR_INFORMATION_LABELS.searchIndividualGuarantor;
  }

  handleSearchBoxSelection(event) {
    const current = this,
    index = Number(event.target.dataset.rowId);
    let objectName="Contact";
    if(current.guarantorList[index].guarantorType ==="Individual"){
      objectName = "Contact";
    }
    else{
      objectName = "Account";
     // current.searchComponent.selectedGuarantor
    }
    getGuarantorDetailsBasedOnSearch({objName:objectName,
      GuarantorId:event.detail,count:index})
      .then(result => {
        current.guarantorList[index].existingId = event.detail;
        current.guarantorList[index] = {...result};
        current.guarantorList[index].isRecordSearch = true;
        //current.guarantorList[index].searchKey = current.guarantorList[index].firstNameGuarantor;
        current.guarantorList[index]=UtilService.updateDisableValueFormData(current.guarantorList[index], current);
      if(this.guarantorList.length > 1){
        current.guarantorList[index].multipleGuarantor = true;
      }})
   
  }

  //changing UI based on selected Guarantor
  onHandleTypeOfGuarantor(event) {
    const current = this,
      sourceData = event.target.dataset,
      index = sourceData.index,
      id = sourceData.id,
      targetValue = event.target.value;

    let companyDataRequire = true,
      individualDataRequire = false,
      isGuarantor = true,
      gurantorListData = [...current.guarantorList];

    gurantorListData = current.clearDataOnHandleChange(index, gurantorListData);

    if (targetValue === "Individual") {
      isGuarantor = false;
      companyDataRequire = false;
      individualDataRequire = true;
    } else if (targetValue === "Corporate") {
      companyDataRequire = true;
      individualDataRequire = false;
    }

    gurantorListData[index][id] = event.target.value;
    gurantorListData[index].isGuarantor = isGuarantor;
    gurantorListData[index].companyDataRequire = companyDataRequire;
    gurantorListData[index].individualDataRequire = individualDataRequire;
    if (gurantorListData.length === 1) {
      gurantorListData[0].multipleGuarantor = false;
  } 
  else {
    gurantorListData[index].multipleGuarantor = true;
  }
    current.guarantorList = gurantorListData;
  }


  onDeleteRow(event) {
    const sourceData = event.target.dataset,
      index = sourceData.index,
      currentIndex = this.guarantorList.findIndex(
        element => element.rowId + "" === index
      );
    
    let gurantorListData = [...this.guarantorList];
    
    
   
   
   gurantorListData.splice(currentIndex, 1);

   /*gurantorListData.forEach(function (data, idx) {
      //data.rowId = idx;
      data.count = idx + 1;
   })*/
   this.guarantorList = gurantorListData;
    for (let i = 0; i < this.guarantorList.length; i++) {
      this.guarantorList[i].count = i + 1;
      this.guarantorList[i].rowId = i;
      if (gurantorListData.length === 1) {
        this.guarantorList[0].multipleGuarantor = false;
    } 
     
    }
  }

 /* createNewRow() {
    const currentSize = this.guarantorList.length;
    if (currentSize >= 1) {
      this.multipleGuarantor = true;
    }
    if (currentSize <= 4) {
      let newRow = new GuarantorInformationModel(this.getLastRowId());
      newRow.isDeleteRequire = newRow.rowId !== 0;
      this.guarantorList.push(newRow);
      for (let i = 0; i < this.guarantorList.length; i++) {
        this.guarantorList[i].count = i + 1;
        //this.guarantorList[i].rowId = i;
      //  this.setSearchBoxToFetchContacts();
      }
    }
    else {
      this.dispatchEvent(new CustomEvent('multiplerowcallback', { detail: GUARANTOR_INFORMATION_LABELS.textYoucannotaddmorethanfiverows }));
    }
  }  */

  getLastRowId () {
    let rowId = 0;
    this.guarantorList.forEach(function (p) {
      if (p.lookupId > rowId) {
        rowId = p.lookupId;
      }
    });
    return rowId + 1;
  }


 createNewRow() {
    const gurantorListData = [...this.guarantorList];

    let newListData = [];

   

    if (gurantorListData.length <= 4) {

      let newRow = new GuarantorInformationModel(this.getLastRowId());
      newRow.isDeleteRequire = newRow.rowId !== 0;
      newRow.isCreatedByMe = true;
      gurantorListData.forEach(function (oldData) {
        newListData.push(oldData);
      })
      newListData.push(newRow); 

      this.guarantorList = newListData;
      
      for (let i = 0; i < this.guarantorList.length; i++) {
        this.guarantorList[i].count = i + 1;
        this.guarantorList[i].rowId = i;
        if (gurantorListData.length >= 1) {
          this.guarantorList[i].multipleGuarantor = true;
        }
       
      }
    }
    
    else {
      this.dispatchEvent(new CustomEvent('multiplerowcallback',
       { detail: GUARANTOR_INFORMATION_LABELS.textYoucannotaddmorethanfiverows }));
    }

  }

  /*handleNumberOfPrimaryGuarantor() {
    const formData = this.guarantorList,
      currentTemplate = this;

    let numberOfPrimarGuarantor = [],
      numberofNoPrimaruGuarantor = [],
      numberOfRow = [];

    formData.map(function (formObject) {
      for (const [key, value] of Object.entries(formObject)) {

        if (key === "primaryGuarantor" && value === "Yes") {
          numberOfPrimarGuarantor.push(value);
          numberOfRow.push(value);
        }
        if (key === "primaryGuarantor" && value === "No") {
          numberofNoPrimaruGuarantor.push(value);
        
        }
        
        currentTemplate.numberOfPrimarGuarantors = numberOfPrimarGuarantor;
        currentTemplate.numberOfNoPrimarGuarantors = numberofNoPrimaruGuarantor;
      }
      currentTemplate.numberOfRows = numberOfRow;
    });
  } */

  getGurantorByType () {
    const current = this,
      formData = current.guarantorList;
    let result = {
      'primaryGaurantor': [],
      'nonPrimaryGaurantor': [],
      'count': formData.length
    };
    
    formData.forEach(function (guarantor) {
        if (guarantor.primaryGuarantor === 'Yes') {
          result.primaryGaurantor.push(guarantor);
        }

        if (guarantor.primaryGuarantor === 'No') {
          result.nonPrimaryGaurantor.push(guarantor);
        }
    });

    return result;
  }

  onClearRows() {
    const current = this,
          lookupComponent = current.template.querySelector("c-custom-lookup");
    current.isDisableRequire = true;
    current.multipleGuarantor = false;
    current.guarantorList[0] = new GuarantorInformationModel(0);
    lookupComponent.clearPill();
    UtilService.reportValidity(current.guarantorList[0], current); 
   
  }

  @api onGuarantorInformationSave() {
    const current = this,
    GuarantorDetailFormDataArr = current.guarantorList,
    gurantorTypeDetails = current.getGurantorByType(),
    validatedFormDataResult = UtilService.validateFormData(
      FORM_FIELD_TO_IGNORE, GuarantorDetailFormDataArr, current, 'isGuarantorFormValid');
      const duplicateFormData = UtilService.CheckDuplicateVariableGuarantor(FORM_FIELD_TO_IGNORE, GuarantorDetailFormDataArr,KEYS_FOR_DUPLICATE);
    current.GuarantorInformationList = validatedFormDataResult.result;
    if (validatedFormDataResult.isValid) {
      if (gurantorTypeDetails.primaryGaurantor.length > 1) {
        this.isMutiplePrimary = true;
        this.conditionalPopupMessageBodyElse = "";
        this.errorPopupMessageDescription =
          GUARANTOR_INFORMATION_LABELS.youCanNotSelectMultipleGuarantorAsPrimaryGuarantor;
        this.conditionalMessageBody =
          GUARANTOR_INFORMATION_LABELS.selectAtleastOneGuarantorAsPrimaryGuarantor;
        return null;
      }
      if (gurantorTypeDetails.nonPrimaryGaurantor.length === gurantorTypeDetails.count &&
          gurantorTypeDetails.count !== 0 && gurantorTypeDetails.nonPrimaryGaurantor.length !== 1) {
        this.isMutiplePrimary = true;
        this.conditionalPopupMessageBodyElse = "";
        this.errorPopupMessageDescription = "";
        this.conditionalMessageBody =
          GUARANTOR_INFORMATION_LABELS.selectAtleastOneGuarantorAsPrimaryGuarantor;
        return null;
      }
if(duplicateFormData){
  return this.guarantorList;
}
else{
  current.errorDetails.isErrorVisible = true;
  current.errorDetails.headerMessage = 'Duplicate Value';
  current.errorDetails.errorContent = 'Guarantor Information Section contains duplicate data';
  return null;
}
    }
    return null;
  }

  handleChange(event) {
    const current = this,
      id = event.target.dataset.id,
      index = event.target.dataset.index;

    let gaurantorListData = [...current.guarantorList];

      
    if (id === 'homePhoneGuarantor') {
      event.target.setCustomValidity("");
      if ((event.target.value !== '' || event.target.value !== '() -') && 
        event.target.value.length === 10 && 
          event.target.value.match(/^[0-9]*$/)) {
        const phoneNumber = event.target.value;
        event.target.value = UtilService.formatPhoneToUSFormat(phoneNumber);
      }
      else if (event.target.value.length === 0) {
        event.target.setCustomValidity("");
      }
      event.target.reportValidity();
    }
    if (id === 'faxGuarantor') {
      event.target.setCustomValidity("");
      if ((event.target.value !== '' || event.target.value !== '() -') && !event.target.value.includes('@') && event.target.value.length === 10) {
        const phoneNumber = event.target.value;
        event.target.value = UtilService.formatPhoneToUSFormat(phoneNumber);
        const formattedPhone = event.target.value;
        if (formattedPhone === null) {
          event.target.setCustomValidity(GUARANTOR_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        else {
          event.target.value = formattedPhone;
        }
      }
      else if (!event.target.value.includes('@') && event.target.value.length !== 0) {
        event.target.setCustomValidity(GUARANTOR_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
      }
      if (event.target.value.includes('@')) {
        if (event.target.value.match(/^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$/)) {
          event.target.value = event.target.value;
        }
        else {
          event.target.setCustomValidity(GUARANTOR_INFORMATION_LABELS.pleaseEnterTheCorrectFaxDetails);
        }
        event.target.reportValidity();
      }
      if (event.target.value === '() -') {
        event.target.value = '';
      }
      event.target.reportValidity();
    }

    if (id === 'socialSecurity') {
      event.target.setCustomValidity("");
      if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 9 && event.target.value.match(/^[0-9]*$/)) {
          var ssnNumber = event.target.value.replace(/\D/g, '');
          ssnNumber = ssnNumber.replace(/^(\d{3})(\d{1,2})/, '$1-$2');
          ssnNumber = ssnNumber.replace(/^(\d{3})-(\d{2})(.+)/, '$1-$2-$3');
          event.target.value = ssnNumber.substring(0, 11);
      }
      else if (event.target.value.length === 0) {
          event.target.setCustomValidity("");
      }
      else{
          event.target.setCustomValidity("Invalid Social Security No. Format");
      }
      event.target.reportValidity();
  }

    gaurantorListData[index][id] = event.target.value;
    current.guarantorList = undefined;
    current.guarantorList = gaurantorListData;
  }

  handleOnKeyUp(event) {
    event.target.setCustomValidity("");
    const charCode = event.which;
    if (charCode !== 92 && charCode !== 13 && (charCode < 48 || charCode > 57)) {
      // return event.target.value = event.target.value.substring(0,event.target.value.length - 1);
      event.target.setCustomValidity(GUARANTOR_INFORMATION_LABELS.invalidPhoneNumberFormat);
    }
    event.target.reportValidity();
  }

  handleOnFocus(event) { 
    const id = event.target.dataset.id;
    if (id === 'homePhoneGuarantor' || id === 'faxGuarantor') {
      event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
    }
    else if (id === 'socialSecurity') {
      event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
  }
    else if (id === 'fax' && event.target.value.length === 0) {
      event.target.setCustomValidity("");
    }
  }

  closeModal() {
   const current = this,
    gurantorTypeDetails = current.getGurantorByType();
    this.isMutiplePrimary = false;
    if (gurantorTypeDetails.count !== 0) {
      this.multipleGuarantor = true;
    }
  }

  clearDataOnHandleChange(index, gurantorList) {
      gurantorList[index] = new GuarantorInformationModel(index);
      gurantorList[index].isDeleteRequire = Number(index) !== 0;

      return gurantorList;
  }

  @wire(getCountryAndStateValues)
  getCountryStateData({ data, error }) {
    if (data) {
      let countyOptions = [{ label: '--None--', value: '' }];
      let stateData = [{ label: '--None--', value: '' }];
      for (let [key, value] of Object.entries(data)) {
        countyOptions.push({ label: key, value: key });
        stateData.push({ label: key, value: value });
      }
      this.defaultCountryAndStateData = stateData;
      this.countryControllingValues = countyOptions;
    }
    else if (error) {
      window.console.log(error);
    }
  }

  handleSelectDependentpicklist(event) {
    const param = event.detail;
    if (param.key) {
      this.guarantorList[param.index][param.key] = param.val;
    }
  }

  clearFormFromCustomLookup(event) {
    const current = this,
    index = Number(event.target.dataset.rowId);
    //this.guarantorList[index].guarantorSearchKey = '';
    current.guarantorList[index] = new GuarantorInformationModel(index);
    if(index !== 0 && index){
      current.guarantorList[index].isDeleteRequire = true;
    }
  // this.setSearchBoxToFetchContacts();
    
  }

  handleErrorPopup() {
    this.errorDetails.isErrorVisible = false;
}
}