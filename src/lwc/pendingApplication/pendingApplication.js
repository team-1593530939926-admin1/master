import { LightningElement, api, track, wire } from "lwc";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import getSubmittedApplicationOnView from "@salesforce/apex/DealDataTableController.getSubmittedApplicationOnView";
import { PENDING_APPLICATION } from "c/cvfLabelsUtil";
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';
import { UtilService } from 'c/utilService';

export default class PendingApplication extends LightningElement {
  label = PENDING_APPLICATION;
  // public property

  @api tableColumns;
  @api tableActionColumn;
  @api sObject;
  @api tableLabel;
  @api records;
  @api keyField;
  @api sortBy;
  @api sortDirection;
  @api offsetValue;
  @api isLastRecord;

  @api searchParam;
  @api options;
  @api pageSize;

  // private property
  @track isSearchError = true;
  @track columns;
  @track error;
  @track searchByDate = {
    from: '',
    to: ''
  }
  @track search;
  @track dateValue;
  @track maxToDate;
  @api isSearchErrorMessage = false;


  @wire(getObjectInfo, { objectApiName: "$sObject" })
  objectInfo({ data, error }) {
    if (data) {
      this.createColumnMapping(data.fields);
    } else if (error) {
      this.error = error;
    }
  }

  connectedCallback() {
    Promise.all([
      loadStyle(this, customCss)
    ]).then(() => {

    })
      .catch(error => {
        alert(error.body.message);
      });
  }

  createColumnMapping(objectFields) {
    let tableCol = [];
    if (this.tableColumns) {
      for (const [key, value] of Object.entries(this.tableColumns)) {
        let colConfig = {};
        if (objectFields.hasOwnProperty(key)) {
          colConfig = {
            label: value,
            fieldName: key,
            type:
              objectFields[key].dataType === "String"
                ? "text"
                : objectFields[key].dataType === "DateTime"
                  ? "date"
                  : objectFields[key].dataType,
            sortable: objectFields[key].sortable
          };
        } else if (key.split(".").length > 1) {
          const referencePathArr = key.split(".");
          colConfig = {
            label: value,
            fieldName: key,
            type: "customReferenceCell",
            typeAttributes: {
              referenceData: { fieldName: referencePathArr[0] },
              referenceKey: referencePathArr[1]
            },
            sortable: "true"
          };
        }
        tableCol.push(colConfig);
      }
    }

    if (this.tableActionColumn) {
      let actionConfig = {};
      for (const [key, value] of Object.entries(this.tableActionColumn)) {
        actionConfig = {
          label: value,
          type: "button",
          initialWidth: 135,
          typeAttributes: {
            label: value,
            name: value + "_action",
            title: "",
            variant: "neutral"
          },
          cellAttributes: {
            alignment: "center"
          }
        };
      }
      tableCol.push(actionConfig);
    }

    this.columns = tableCol;
  }

  handleSearchByDate(event) {
    const id = event.target.dataset.id;
    var newDate = new Date();
    this.dateValue = newDate.toISOString().substring(0, 10);

    if (id === "From") {
      this.searchByDate.from = event.target.value;
      this.maxToDate = this.searchByDate.from;
    }
    if (id === "To") {
      this.searchByDate.to = event.target.value;
    }
  }

  handleSearchBydateButton() {
    if (this.checkIfSearchInputValid()) {
      if (this.searchByDate.from !== '' && this.searchByDate.to !== '' && this.searchByDate.to <= this.dateValue) {
        const searchByDate = new CustomEvent("searchbydate", {
          detail: {
            fromDate: this.searchByDate.from,
            toDate: this.searchByDate.to,
            search: this.search
          }
        });
        this.dispatchEvent(searchByDate);
      }
    }
  }

  handleSearchBydateButtonOnClear() {
    const current = this;
    const temp = this.template.querySelector('[data-id="To"]');
    this.searchByDate.from = '';
    this.searchByDate.to = '';
    this.isSearchError = true;
    UtilService.reportValidity(this.searchByDate, current);
    const searchByDate = new CustomEvent("searchbydate", {
      detail: {
        fromDate: this.searchByDate.from,
        toDate: this.searchByDate.to,
        search: this.search
      }
    });
    this.dispatchEvent(searchByDate);

    Promise.resolve().then(() => {
      if (temp) {
        temp.reportValidity();
      }
    });

  }

  handleChange(event) {
    this.value = event.detail.value;
    const recordsOnPage = new CustomEvent("numberofrecordsonpage", {
      detail: this.value
    });
    this.dispatchEvent(recordsOnPage);
  }

  doSorting(event) {
    this.sortBy = event.detail.fieldName;
    this.sortDirection = event.detail.sortDirection;
    const dealsOnSorting = new CustomEvent("sortingdeals", {
      detail: { sortByField: this.sortBy, sortByDirection: this.sortDirection }
    });
    this.dispatchEvent(dealsOnSorting);
  }

  handleClick(event) {
    const actionName = event.target.name,
      handlePagination = new CustomEvent("paginateaction", {
        detail: {
          searchParam: this.searchParam,
          pageSize: this.pageSize,
          action: actionName
        }
      });
    this.dispatchEvent(handlePagination);
  }

  get isDisableForPrevious() {
    return this.offsetValue === "0";
  }

  get isDisableForLast() {
    return this.isLastRecord;
  }

  @track brokerApplicationFormDetails;
  @track isModalShowing = false;

  handleRowAction(event) {
    const current = this,
      applicationId = event.detail.row.Application_Id__c,
      companyCode = event.detail.row.Account.Company_Code__c;

    getSubmittedApplicationOnView({
      dealApplicationId: applicationId,
      dealCompanyCode: companyCode
    }).then(result => {
      current.brokerApplicationFormDetails = result;
      current.isModalShowing = true;
    });
  }

  handleCloseViewModel(event) {
    this.isModalShowing = false;
  }

  checkIfSearchInputValid() {
    const curentTemplate = this;
    if (curentTemplate.searchByDate.from !== '' && curentTemplate.searchByDate.to !== ''
      && curentTemplate.searchByDate.from !== null && curentTemplate.searchByDate.to !== null) {
      curentTemplate.isSearchError = true;
    }
    else {
      curentTemplate.isSearchError = false;
    }
    return curentTemplate.isSearchError;
  }
}