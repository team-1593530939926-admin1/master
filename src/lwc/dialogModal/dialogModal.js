import { LightningElement, api } from 'lwc';

export default class DialogModal extends LightningElement {

    //used to hide/show dialog
    @api visible;

    //modal title
    @api title; 

    //reference name of the component
    @api name; 

    //modal message
    @api message; 

     //confirm button label
    @api confirmLabel;

    //cancel button label
    @api cancelLabel; 

    //any event/message/detail to be published back to the parent component
    @api originalMessage;

    //handles button clicks
    handleClick(event) {

        //creates object which will be published to the parent component
        let finalEvent = {
            originalMessage: this.originalMessage,
            status: event.target.name ? event.target.name : event.currentTarget.name
        };

        //dispatch a 'click' event so the parent component can handle it
        this.dispatchEvent(new CustomEvent('modalactionclick', {detail: finalEvent}));
    }
}