export class PartnerInformationModel {
    isDisable = false;
    existingId = null;
    isFinalPartnerFormValid = true;
    isRequired = false;
    rowId = -1;
    dOB = "";
    socialSecurity = "";
    firstName = "";
    middleName = "";
    lastName = "";
    mailingStreet = "";
    mailingCountry = "";
    mailingCity = "";
    mailingState = "";
    mailingPostalCode = "";
    homePhone = "";
    ownershipPercentage = "";
    Id = "";
    isCreatedByMe = true;
    maskedDob = "";
    maskedSocialSecurity = "";
    maskedMailingStreet = ""; 
    maskedFirstName = "";
    maskedMiddleName = "";
    maskedLastName = "";
    maskedHomePhone = "";

    count = "0";
    constructor(rowId) {
        this.rowId = rowId;
        this.count = Number(rowId) + 1;
        this.isCreatedByMe = true;
    }
}