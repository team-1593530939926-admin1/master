import { LightningElement, track, api } from 'lwc';
import getFCSBusinessInfo from '@salesforce/apex/FCSEntityVerificationController.getBusinessInfoByBusinessNameAndStateCode';
import getFCSBusinessDetail from '@salesforce/apex/FCSEntityVerificationController.getBusinessDetailsByTransactionAndEntityId';
import saveEntityInformation from '@salesforce/apex/FCSEntityVerificationController.saveEntityVerificationDetails';
import isNeedToFCSAPICall from '@salesforce/apex/FCSEntityVerificationController.isNeedToFCSAPICall';
import getEntityVerificationDetailByCompanyIdSF from '@salesforce/apex/FCSEntityVerificationController.getEntityVerificationDetailByCompanyIdSF';
import { FCS_ENTITY_VERIFICATION_LABELS } from "c/fcsLabelsUtil";

const VALIDATE_LABEL = 'validate';

export default class FcsEntityVerification extends LightningElement {

    entityVerificationLabels = FCS_ENTITY_VERIFICATION_LABELS;

    title = this.entityVerificationLabels.entityVerificationLabel;
    message = this.entityVerificationLabels.doYouWantToContinueLabel;
    confirmLabel = this.entityVerificationLabels.yesLabel;
    cancelLabel = this.entityVerificationLabels.noLabel;
    originalMessage = ''
    selectedBusinessDetail;

    @api businessName;
    @api stateCode;
    @api companyId;
    @api recordId;
    @api invocationFrom;
    @api backToDetailCallback;

    // private attributes
    @track errorString;
    @track businessDetails;
    @track businessInformation;
    @track isBusinessInformation = false;
    @track isConfirmationPrompt = false;
    @track selectedBusiness;
    @track isModalOpen = false;
    @track isLoading = false;
    @track isBusinessDetailSFModel = false;
    @track entityBusinessDetailFromSF;

    connectedCallback() {
        this.checkIfFCSCallNeeded();
    }

    getBusinessInfo(event) {
        const current = this;
        current.closeBusinessInfoModal();
        current.isLoading = true;
        this.errorString = undefined;

        getFCSBusinessInfo({
            'businessName': this.businessName,
            'stateCode': this.stateCode
        })
            .then(result => {
                this.businessInformation = undefined;
                this.businessInformation = result;
                current.openBusinessInfoModal();
                current.isLoading = false;
            })
            .catch(error => {
                this.errorString = error;
                current.isLoading = false;
                current.closeBusinessInfoModal();
            });
    }

    checkIfFCSCallNeeded(event) {
        const current = this;
        current.isLoading = true;
        this.errorString = undefined;

        isNeedToFCSAPICall({
            'recordId': current.recordId 
        })
        .then(result => {
            if (result) {
                current.getBusinessInfo();
             } 

             if (!result) {
                current.getBusinessDetailsFromSF();
             }
        })
        .catch(error => {
            this.errorString = error;
            current.isLoading = false;
            current.closeBusinessInfoModal();
        });
    }

    getBusinessDetailsFromSF(event) {
        const current = this;
        current.isLoading = true;
        this.errorString = undefined;

        getEntityVerificationDetailByCompanyIdSF({
            'recordId': current.recordId
        })
            .then(result => {
                current.entityBusinessDetailFromSF = undefined;
                current.entityBusinessDetailFromSF =  result;
                current.openBusinessDetailSFModal();
                current.isLoading = false;
            })
            .catch(error => {
                this.errorString = error;
                current.isLoading = false; 
            });
    }

    getBusinessDetail() {
        const current = this,
            selectedBusinessInfo = current.selectedBusiness,
            params = {
                transactionId : selectedBusinessInfo.transactionID, 
                entityId:selectedBusinessInfo.entityID, 
                additionalSearch: selectedBusinessInfo.additionalSearchParameters, 
                passThrough: selectedBusinessInfo.passThrough
            };

        current.closeBusinessDetailModal();

        current.isLoading = true;
        current.errorString = undefined;

        getFCSBusinessDetail(params)
            .then(result => {
                current.businessDetails = undefined;
                let isProcess = false;
                if (result && result.listOfResultWrapper) {
                    if (result.listOfResultWrapper.length > 0 && 
                        Object.keys(result.listOfResultWrapper[0]).length > 0) {
                            isProcess = true;
                           
                    }  
                }
               
                if (isProcess) {
                    current.businessDetails = result;
                    current.openBusinessDetailModal();
                    current.isLoading = false;
                } else {
                    current.errorString = {message:  'No Detail found for selected entitiy'};
                    current.isLoading = false;
                    current.closeBusinessInfoModal();
                }
            })
            .catch(error => {
                current.errorString = error;
                current.isLoading = false;
                current.closeBusinessInfoModal();
            });
    }

    openBusinessDetailModal() {
        this.isModalOpen = true;
    }

    closeBusinessDetailModal() {
        this.isModalOpen = false;
    }

    openBusinessDetailSFModal() {
        this.isBusinessDetailSFModel = true;
    }

    closeBusinessDetailSFModal() {
        this.isBusinessDetailSFModel = false;
    }

    openBusinessInfoModal() {
        this.isBusinessInformation = true;
    }

    closeBusinessInfoModal() {
        this.isBusinessInformation = false;
    }

    handleBusinessDetailAction(event) {
        const current = this,
            params = event.detail,
            actionName = params.actionName,
            action = params.action;

        if (actionName) {

            if (actionName === 'cancel') {
                current.closeBusinessDetailModal();
                current.openBusinessInfoModal();
            }

            if (actionName === 'certify') {
                const selectedBusinessDetails = params.businessDetails;
                current.selectedBusinessDetail = selectedBusinessDetails;
                current.isLoading = true;
                current.handleSaveEntityVerificationInfo();
            }
           
        }
        current.closeBusinessDetailModal();
    }

    handleBusinessDetailSF(event) {
        const current = this,
            params = event.detail,
            actionName = params.actionName;

        if (actionName) {

            if (actionName === 'cancel' || actionName === 'certify') {
                current.handleBackToDetail();
            }
 
        }
        current.closeBusinessDetailSFModal();
    }


    get selectedBusinessDetailInformation() {
        return JSON.stringify(this.selectedBusinessDetail);
    }

    submitDetails() {
        this.isModalOpen = false;
    }

    handleHideBusinessInfo(event) {
        this.closeBusinessInfoModal();
    }

    handleBusinessInformationAction(event) {
        const current = this,
            detail = event.detail,
            actionName = detail.actionName;

        if (actionName === VALIDATE_LABEL) {
            current.selectedBusiness = detail.selectedBusinessInfo;
            current.isConfirmationPrompt = true;
        }

        if (actionName === 'cancel') {
            current.handleBackToDetail();
        }

        current.closeBusinessInfoModal();
    }

    handleConfirmModalClik(event) {
        const current = this;
        if (event.target) {

            if (event.target.name === 'openConfirmation') {
                this.originalMessage = 'test message';
                this.isDialogVisible = true;
            } else if (event.target.name === 'confirmModal') {

                if (event.detail !== 1) {
                    this.displayMessage = 'Status: ' + event.detail.status + '. Event detail: ' + JSON.stringify(event.detail.originalMessage) + '.';

                    if (event.detail.status === 'confirm') {
                        current.getBusinessDetail();
                    } else if (event.detail.status === 'cancel') {
                        current.openBusinessInfoModal();
                    }
                }
                //hides the component
                this.isConfirmationPrompt = false;
            }
        }
    }

    handleBackToDetail(event) {
        const current = this;
        if (current.backToDetailCallback !== undefined && typeof current.backToDetailCallback === 'function' ) {
            current.backToDetailCallback.call(current);
        }
    }

    handleSaveEntityVerificationInfo() {
        const current = this,
            params = {
            recordId: current.recordId,
            businessDetailWrapperString : JSON.stringify(current.businessDetails)
        };

        saveEntityInformation(params)
        .then(result => { 
            current.closeBusinessDetailModal();
            current.isLoading = false;
            current.handleBackToDetail();
        })
        .catch(error => {
            current.errorString = error;
            current.closeBusinessInfoModal();
            current.isLoading = false;
        });
    }


}