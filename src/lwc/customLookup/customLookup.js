import lookUp from '@salesforce/apex/CustomLookupController.search';
import { api, LightningElement, track, wire } from 'lwc';


export default class customLookUp extends LightningElement {
    @api disableCompany = false;
    @api objName;
    @api iconName;
    @api filter = '';
    @api searchPlaceholder='Search';
    @api helpTextValue;    
    @api searchBox;
    @api rowId;
    @api searchLabel;
    @api clearPill() {
        this.handleRemovePill();
    }

    @track selectedName;
    @track records;
    @track isValueSelected;
    @track blurTimeout;

    @api searchTerm;
    //css
    @track boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    @track inputClass = '';

    @wire(lookUp, {searchTerm : '$searchTerm', myObject : '$objName', filter : '$filter'})
    wiredRecords({ error, data }) {
        if (data) {
            this.error = undefined;
            this.records = data;
        } else if (error) {
            this.error = error;
            this.records = undefined;
        }
    }


    handleClick() {
        this.searchTerm = '';
        this.inputClass = 'slds-has-focus';
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open';
    }

    onBlur() {
        this.blurTimeout = setTimeout(() =>  {this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus'}, 300);
    }

   onSelect(event) {
        let selectedId = event.currentTarget.dataset.id;
        let selectedName = event.currentTarget.dataset.name;
        const valueSelectedEvent = new CustomEvent('lookupselected', {detail:  selectedId });
        this.dispatchEvent(valueSelectedEvent);
        this.isValueSelected = true;
        this.selectedName = selectedName;
        if(this.blurTimeout) {
            clearTimeout(this.blurTimeout);
        }
        this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
    }

    handleRemovePill() {
        this.isValueSelected = false; 
        this.selectedName = undefined;
        this.searchTerm = '';
        this.fireClearEvent();
    }

    onChange(event) {
        this.searchTerm = event.target.value;
    }

    get pillContainerStyle () {
        let container__ = 'height:34px;';
        if (this.disableCompany || this.isValueSelected) {
            //container__ = container__ + 'background-color: hsl(0deg 0% 0% / 16%);'
        }
        return container__;
    }

    fireClearEvent () {
        const current = this,
            evt = new CustomEvent('clearformdata', {detail: true});
        
        current.dispatchEvent(evt);
    }
   
}