import { LightningElement, track, wire } from 'lwc';
import getListOfDealRecordBySearchParam from '@salesforce/apex/DealDataTableController.getListOfPendingDealRecordBySearchKey';
import getListOfPendingDealRecord from '@salesforce/apex/DealDataTableController.getListOfPendingDealRecord';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import { NavigationMixin } from 'lightning/navigation';
import {PENDING_DATA_TABLE} from 'c/cvfLabelsUtil';

export default class PendingDataTable extends NavigationMixin(LightningElement) {
    label = PENDING_DATA_TABLE;
    
    opportunityObject = OPPORTUNITY_OBJECT;
    dataTableColumn;
    actionColumn;

    @track displaySpinner = true;
    @track searchErrorMessage = false;
    @track tableData;
    @track pageSizeOptions;
    @track defaultSortField;
    @track pageSize;
    @track offsetValue;
   // @track sortDirection = ' ASC';
    @track sortDirection;
    @track sortField;
    @track search;
    @track defaultSortDirection;
    @track isLastRecord = false;
    
    @track errorMessage;
    @track searchByDate = {
        fromDate:'',
        toDate:''
    }


    @wire(getListOfPendingDealRecord)
    listOfDeals({ data, error }) {
        if (data) {
            this.initilizeTableData(data);
        } else if (error) {
            this.errorMessage = error;
            this.tableData = undefined;
            this.hideSpinner();
        }
    }

    initilizeTableData(data) {
        this.dataTableColumn = data.dataTableColumn;
        this.pageSizeOptions = data.pageSizeOptions;
        this.actionColumn = data.dataTableActionColumn;
        this.pageSize = data.defaultPageSize;
        this.defaultSortField = data.defaultSortField;
        this.tableData = data.data;
        this.offsetValue = data.defaultOffset;
        this.defaultSortDirection = data.defaultSortDirection;
        if(this.tableData.length < this.pageSize)
        {
            this.isLastRecord = true;
        }
       
        this.hideSpinner();
    }


    handleRecordsOnPage(event) {
        this.isLastRecord = false;
        this.offsetValue = '0';
        this.pageSize = event.detail;
        this.showSpinner();
        getListOfDealRecordBySearchParam({
            searchkey: this.search, pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        }).then(result => {
            this.tableData = result;
            if(this.tableData.length < this.pageSize)
                {
                 this.isLastRecord = true;
                }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handleSorting(event) {
        this.sortDirection = event.detail.sortByDirection;
        this.sortField = event.detail.sortByField;
        getListOfDealRecordBySearchParam({
            searchkey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        }).then(result => {
            this.tableData = result;
            if(this.tableData.length < this.pageSize)
            {
              this.isLastRecord = true;
            }
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    showSpinner() {
        this.displaySpinner = true;
    }

    hideSpinner() {
        this.displaySpinner = false;
    }

    handleSearchByDate(event)
    {

        this.isLastRecord = false;
        this.offsetValue = '0';
        this.search = event.detail.search;
        this.searchByDate.fromDate = event.detail.fromDate;
        this.searchByDate.toDate = event.detail.toDate;
        this.showSpinner();

        getListOfDealRecordBySearchParam({
            searchkey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        }).then(result => {
            if(result.length !== 0){
                this.searchErrorMessage = false;
                this.tableData = result;
                if(this.tableData.length < this.pageSize)
                {
                  this.isLastRecord = true;
                }
            }
                
            
            else{
                this.searchErrorMessage = true;
            }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handlePaginationAction(event) {
        const searchParam = event.detail.searchParam,
            pageSize = event.detail.pageSize,
            action = event.detail.action;

        let currentOffset = parseInt(this.offsetValue),
            currentPageSize = parseInt(pageSize);

        this.showSpinner();
        if (action === 'next') {
            this.offsetValue = (currentOffset + currentPageSize) + '';
        } else if (action === 'previous') {
            this.offsetValue = (currentOffset - currentPageSize) + '';
            if (parseInt(this.offsetValue) < 0) {
                this.offsetValue = '0';
            }
        }

        getListOfDealRecordBySearchParam({
           searchkey: searchParam,
           pageSize: pageSize,
           offset: this.offsetValue,
           // sortOrderField: this.defaultSortField,
           // sortDirection: this.defaultSortDirection
           sortOrderField: this.sortField,
           sortDirection: this.sortDirection,
           fromDate:this.searchByDate.fromDate,
           toDate:this.searchByDate.toDate
        }).then(result => {
            if (result.length > 0) {
                this.isLastRecord = false;
                this.tableData = result;
                if (result.length < this.pageSize) {
                    this.isLastRecord = true;
                }
                
            } else {
                this.isLastRecord = true;
            }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handleClick(event) {
        const sourceName = event.target.name;

        if (sourceName === 'backToHome') {
            this[NavigationMixin.Navigate]({
                "type": "comm__namedPage",
                attributes: {
                    pageName: 'home',
                },
            });
    
        }

    }
}