import { LightningElement, api, track, wire } from "lwc";
import { NavigationMixin } from 'lightning/navigation'; 
import getPortalURL from '@salesforce/apex/DealDocumentUploadTableController.getPortalURL';
import getFileExtensionValues from '@salesforce/apex/BrokerApplicationFormController.getFileExtensionValues';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {
  COMPANY_INFORMATION_LABELS,
  PARTNER_INFORMATION_LABELS,
  TRADE_INFORMATION_LABELS,
  BANK_INFORMATION_LABELS,
  EQUIQMENT_INFORMATION_LABELS,
  GUARANTOR_INFORMATION_LABELS,
  BROKER_APPLICATION_FORM_LABEL,
  VIEW_APPLICATION_LABELS
} from "c/cvfLabelsUtil";


const columns = [
  {label: 'File Name', fieldName: 'Title'},
  {label: 'Description', fieldName: 'Description',  wrapText: true},
  {label: 'Type', fieldName: 'Type__c'},
  {label: 'Download',type: "button", typeAttributes: {  
    label: 'Download',  
    name: 'View',  
    title: 'Download',  
    disabled: false,  
    value: 'view',  
    iconPosition: 'center'  
}}

];

export default class ViewApplication extends NavigationMixin(LightningElement) {
  @track portalURL;

  @wire(getPortalURL)
  gettingPortalURL({data, error})
  {
      if(data)
      {
          this.portalURL = data;
      }

  };
    companyLabel = COMPANY_INFORMATION_LABELS;
    partnerInfoLabels = PARTNER_INFORMATION_LABELS;
    tradeInfoLabels = TRADE_INFORMATION_LABELS;
    bankInfoLabels = BANK_INFORMATION_LABELS;
    equipmentInformationLabels = EQUIQMENT_INFORMATION_LABELS;
    guarantorInfoLabels = GUARANTOR_INFORMATION_LABELS;
    brokerAppLabel = BROKER_APPLICATION_FORM_LABEL;
    viewApplicationLabels = VIEW_APPLICATION_LABELS;
  
    @api brokerApplicationFormDetails;
    @api isReviewCall = false;

    @track columns = columns;
    @track isPreviewModalOpen = false;


  openmodal() {
      this.isPreviewModalOpen = true
  }
  closeModal() {
      this.isPreviewModalOpen = false
  } 

    get companyInfo() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.companyWrapper !== undefined && this.brokerApplicationFormDetails.companyWrapper.length !== 0 ) {
        return this.brokerApplicationFormDetails.companyWrapper;
      }
      return false;
    }
  
    get partnerInfo() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.partnerWrapper !== undefined && this.brokerApplicationFormDetails.partnerWrapper.length !== 0 ) {
        return this.brokerApplicationFormDetails.partnerWrapper;
      }
      return false;
    }
  
    get tradeInfo() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.tradeWrapper !== undefined && this.brokerApplicationFormDetails.tradeWrapper.length !== 0) {
        return this.brokerApplicationFormDetails.tradeWrapper;
      }
      return false;
    }
  
    get bankInfo() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.bankWrapper !== undefined && this.brokerApplicationFormDetails.bankWrapper.length !== 0) {
        return this.brokerApplicationFormDetails.bankWrapper;
      }
      return false; 
    }
  
    get equipmentInformation() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.equipmentWrapper !== undefined) {
        return this.brokerApplicationFormDetails.equipmentWrapper;
      }
      return false ;
    }
  
    
    get guarantorInfo() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.guarantorWrapper !== undefined && this.brokerApplicationFormDetails.guarantorWrapper.length !== 0) {
        return this.brokerApplicationFormDetails.guarantorWrapper;
      }
      return false;
    }

    get documentInformation() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.listOfDocuments !== undefined && this.brokerApplicationFormDetails.listOfDocuments.length !== 0) {
        return this.brokerApplicationFormDetails.listOfDocuments;
      }
      return false;
    }    
    
    get creditDocuments() {
      if (this.brokerApplicationFormDetails && this.brokerApplicationFormDetails.listOfCreditDocuments !== undefined && this.brokerApplicationFormDetails.listOfCreditDocuments.length !== 0) {
        return this.brokerApplicationFormDetails.listOfCreditDocuments;
      }
      return false;
    }    

    handleOnCloseModel() {
      const current = this;
      current.dispatchEvent(new CustomEvent('closeviewmodel', {
          detail: true
      }));
    }
  
    renderedCallback() {
      const style = document.createElement("style");
      style.innerText = `
              .accordion-background .slds-accordion__section .slds-accordion__summary {
                  background: #eaeaea!important;
                  padding: 5px;
                  font-weight: bold;
              }`;
        //this.template.querySelector('.container-style-tag').appendChild(style);
    }
    
    handleReviewAccepted(){
      const current = this;
      current.dispatchEvent(new CustomEvent('clickingreviewsubmit', {
          detail: true
      }));
    }



    @wire(getFileExtensionValues)
    getListOfFileExtensions({data,error})
    {
      if(data)
      {
          this.fileExtensionList = data;
      }
    }


    @track fileExtensionList = [];
    @track versionId;
    @track documentId;
    @track fileType;
    @track fileExtension;
    @track fileName;

    handleOnRowAction( event ) {  
                
      const current = this;

          current.versionId = event.detail.row.Id;
          current.documentId = event.detail.row.ContentDocumentId;
          current.fileType = event.detail.row.Type__c;
          current.fileName = event.detail.row.Name;
         // current.fileExtension = event.detail.row.FileExtension;

          for(let [key, value] of Object.entries(current.fileExtensionList))
          {
            if(event.detail.row.FileExtension === key)
            {
              current.fileExtension = value;
            }
          }

          var urlString = window.location.href;
          var baseURL = urlString.substring(0, urlString.indexOf("/s/"));

          
        //  const url =  `${baseURL}/sfc/servlet.shepherd/version/renditionDownload?rendition=${current.fileExtension}&versionId=${current.versionId}`;
          
          const url =  `${baseURL}/sfc/servlet.shepherd/document/download/${current.documentId}`;
          window.open(url);
         /*
          const xhr = new XMLHttpRequest();
          xhr.open('GET', url, true);
  
          xhr.onload = function() {
              if(this.status === 202) {
                  current.isPreviewModalOpen = false;
                  const response = JSON.parse(this.responseText);
                  const event = new ShowToastEvent({
                      title: 'CVF Broker Preview Result',
                      message: 'File preview is in process, please try after some time.',
                      variant: 'info',
                      mode: 'dismissable'
                  });
                  current.dispatchEvent(event); 
              } 
              if (this.status === 200) { 
        
                  if(current.documentId !== null)
                  {
                    current.isPreviewModalOpen = true;
                  }
              }
          };
  
          xhr.send(); 
          */

  }


  }