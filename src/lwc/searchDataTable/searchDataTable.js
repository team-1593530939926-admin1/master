import { LightningElement, track, wire } from 'lwc';
import getListOfDealRecordBySearchParam from '@salesforce/apex/DealDataTableController.getListOfDealRecordBySearchKey';
import getListOfSearchRecord from '@salesforce/apex/DealDataTableController.getListOfSearchRecord';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import { NavigationMixin } from 'lightning/navigation';
import { SEARCH_DATA_TABLE } from 'c/cvfLabelsUtil';

export default class SearchDataTable extends NavigationMixin(LightningElement) {

    label = SEARCH_DATA_TABLE;
    opportunityObject = OPPORTUNITY_OBJECT;
    dataTableColumn;
    actionColumn;

    @track displaySpinner = true;
    @track tableData;
    @track pageSizeOptions;
    @track defaultSortField;
    @track pageSize;
    @track offsetValue;
    @track sortDirection;
    @track sortField;
    @track search;
    @track defaultSortDirection;
    @track isLastRecord = false;
 // @track optionsForSearching;
    @track errorMessage;
    @track isRecordFound = false;

    @wire(getListOfSearchRecord)
    listOfDeals({ data, error }) {
        if (data) {
            this.initilizeTableData(data);
        } else if (error) {
            this.errorMessage = error;
            this.tableData = undefined;
            this.hideSpinner();
        }
    }

    initilizeTableData(data) {
        this.dataTableColumn = data.dataTableColumn;
        this.pageSizeOptions = data.pageSizeOptions;
        this.actionColumn = data.dataTableActionColumn;
        this.pageSize = data.defaultPageSize;
        this.defaultSortField = data.defaultSortField;
        this.tableData = data.data;
        this.offsetValue = data.defaultOffset;
        this.defaultSortDirection = data.defaultSortDirection;
    //  this.optionsForSearching = data.searchOptions;

        this.hideSpinner();
    }

    handleSearchKey(event) {
        this.isLastRecord = false;
        this.offsetValue = '0';
        this.search = event.detail;
        this.showSpinner();
        this.isRecordFound = false;

        if (this.search != null) {
            getListOfDealRecordBySearchParam({
                searchKey: this.search,
                pageSize: this.pageSize,
                offset: this.offsetValue,
                sortOrderField: this.defaultSortField,
                sortDirection: this.defaultSortDirection
            })
                .then(result => {
                    this.tableData = result;
                    this.isRecordFound = true;
                    if (this.tableData.length < this.pageSize) {
                        this.isLastRecord = true;
                    }
                    if (this.tableData.length === 0) {
                        this.tableData = undefined;
                    }
                    this.hideSpinner();
                }).catch(error => {
                    this.errorMessage = error;
                });
        }
        else {
            this.tableData = undefined;
            this.hideSpinner();
        }
    }

    handleRecordsOnPage(event) {
        this.isLastRecord = false;
        this.offsetValue = '0';
        this.pageSize = event.detail;
        this.showSpinner();

        getListOfDealRecordBySearchParam({
            searchKey: this.search, 
            pageSize: this.pageSize,
            offset: this.offsetValue, 
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
        }).then(result => {
            this.tableData = result;
            if (this.tableData.length < this.pageSize) {
                this.isLastRecord = true;
            }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handleSorting(event) {
        this.sortDirection = event.detail.sortByDirection;
        this.sortField = event.detail.sortByField;

        getListOfDealRecordBySearchParam({
            searchKey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection
        }).then(result => {
            this.tableData = result;
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    showSpinner() {
        this.displaySpinner = true;
    }

    hideSpinner() {
        this.displaySpinner = false;
    }

    handlePaginationAction(event) {
        const searchParam = event.detail.searchParam,
              pageSize = event.detail.pageSize,
              action = event.detail.action;

        let currentOffset = parseInt(this.offsetValue),
            currentPageSize = parseInt(pageSize);

        this.showSpinner();
        if (action === 'next') {
            this.offsetValue = (currentOffset + currentPageSize) + '';
        } else if (action === 'previous') {
            this.offsetValue = (currentOffset - currentPageSize) + '';
            if (parseInt(this.offsetValue) < 0) {
                this.offsetValue = '0';
            }
        }

        getListOfDealRecordBySearchParam({
            searchKey: searchParam, 
            pageSize: pageSize,
            offset: this.offsetValue,
           // sortOrderField: this.defaultSortField,
           // sortDirection: this.defaultSortDirection
           sortOrderField: this.sortField,
           sortDirection: this.sortDirection,
        }).then(result => {
            if (result.length > 0) {
                this.isLastRecord = false;
                this.tableData = result;
                if (result.length < this.pageSize) {
                    this.isLastRecord = true;
                }

            } 
            else {
                this.isLastRecord = true;
            }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handleClick(event) {
        const sourceName = event.target.name;

        if (sourceName === 'backToHome') {
            this[NavigationMixin.Navigate]({
                "type": "comm__namedPage",
                attributes: {
                    pageName: 'home',
                },
            });
        }
    }
}