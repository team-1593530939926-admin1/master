import entityNameLabel from '@salesforce/label/c.Text_Entity_Name';
import entityInformationLabel from '@salesforce/label/c.Text_Entity_Information';
import entityTypeLabel from '@salesforce/label/c.Text_Entity_Type';
import partyTypeLabel from '@salesforce/label/c.Text_Party_Type';
import titleLabel from '@salesforce/label/c.Text_Title';
import standingLabel from '@salesforce/label/c.Text_Standing';
import entityIdLabel from '@salesforce/label/c.Text_Entity_Id';
import filingDateLabel from '@salesforce/label/c.Text_Filing_Date';
import dateOfIncorporationLabel from '@salesforce/label/c.Text_Date_of_Incorporation';
import domicileStateLabel from '@salesforce/label/c.Text_Domicile_State'; 
import domicileCountryLabel from '@salesforce/label/c.Text_Domicile_Country'; 
import addressTypeLabel from '@salesforce/label/c.Text_Address_Type'; 
import addressLabel from '@salesforce/label/c.Text_Address'; 
import partiesInformationLabel from '@salesforce/label/c.Text_Parties_Information'; 
import agentInformationLabel from '@salesforce/label/c.Text_Agent_Information'; 
import cityLabel from '@salesforce/label/c.Text_City'; 
import stateLabel from '@salesforce/label/c.Text_State'; 
import countryLabel from '@salesforce/label/c.Text_Country';
import postalCodeLabel from '@salesforce/label/c.Text_Postal_Code';  
import closeLabel from '@salesforce/label/c.Text_Close';  
import cancelLabel from '@salesforce/label/c.Text_Cancel';  
import iCertifyLabel from '@salesforce/label/c.Text_I_Certify';  
import entityStandingLabel from '@salesforce/label/c.Text_Entity_Standing';
import nameTypeLabel from '@salesforce/label/c.Text_Name_Type';
import continueLabel from '@salesforce/label/c.Text_Continue';
import NALabel from '@salesforce/label/c.Text_N_A';

import selectedResultLabel from '@salesforce/label/c.Text_Selected_Result';
import stateCodeLabel from '@salesforce/label/c.Text_State_Code';
import businessNameLabel from '@salesforce/label/c.Text_Business_Name';
import entityVerificationLabel from '@salesforce/label/c.Text_Entity_Verification';
import doYouWantToContinueLabel from '@salesforce/label/c.Text_Do_You_Want_To_Continue';
import yesLabel from '@salesforce/label/c.Text_Yes';
import noLabel from '@salesforce/label/c.Text_No';
import warningLabel from '@salesforce/label/c.Text_Warning';
import noPartiesFoundLabel from '@salesforce/label/c.Text_No_Parties_Found';
import noAgentsFoundLabel from '@salesforce/label/c.Text_No_Agents_Found';
import companyNameLabel from '@salesforce/label/c.Text_Company_Name';
import backToDetailLabel from '@salesforce/label/c.Text_Back_To_Detail';
import validateLabel from '@salesforce/label/c.Text_Validate';
import countryOrParishLabel from '@salesforce/label/c.Text_Country_Or_Parish';
import timeInServiceLabel from '@salesforce/label/c.Text_Time_In_Service';

const FCS_ENTITY_VERIFICATION_LABELS = {
    selectedResultLabel,
    stateCodeLabel,
    businessNameLabel,
    entityVerificationLabel,
    doYouWantToContinueLabel,
    yesLabel,
    noLabel,
    companyNameLabel,
    backToDetailLabel
};

const FCS_BUSINESS_DETAILS_LABELS = {
    entityNameLabel,
    entityInformationLabel,
    domicileCountryLabel,
    domicileStateLabel,
    partyTypeLabel,
    titleLabel,
    entityTypeLabel,
    filingDateLabel,
    dateOfIncorporationLabel,
    agentInformationLabel,
    partiesInformationLabel,
    closeLabel,
    cancelLabel,
    iCertifyLabel,
    entityIdLabel,
    standingLabel,
    NALabel,
    timeInServiceLabel,
    ADDRESS: {
        addressTypeLabel,
        addressLabel,
        cityLabel,
        stateLabel,
        countryLabel,
        postalCodeLabel,
        countryOrParishLabel
    },
    warningLabel,
    noPartiesFoundLabel,
    noAgentsFoundLabel
};

const FCS_BUSINESS_INFORMATION_LABELS = {
    stateLabel,
    closeLabel,
    entityInformationLabel,
    entityIdLabel,
    entityStandingLabel,
    entityTypeLabel,
    nameTypeLabel,
    cancelLabel,
    continueLabel,
    companyNameLabel,
    validateLabel
};

export {FCS_BUSINESS_DETAILS_LABELS, FCS_BUSINESS_INFORMATION_LABELS, FCS_ENTITY_VERIFICATION_LABELS};