import { LightningElement, track, wire } from 'lwc';
import getListOfDealRecordBySearchParam from '@salesforce/apex/DealDataTableController.getListOfDealRecordBySearchKey';
import getListOfDealRecord from '@salesforce/apex/DealDataTableController.getListOfDealRecord';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import { NavigationMixin } from 'lightning/navigation';
import { DEALS_DATA_TABLE_LABELS } from "c/cvfLabelsUtil";
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';


export default class DealsDataTable extends NavigationMixin(LightningElement) {

    label = DEALS_DATA_TABLE_LABELS;
    opportunityObject = OPPORTUNITY_OBJECT;
    dataTableColumn;
    actionColumn;

    @track displaySpinner = true;
    @track tableData;
    @track pageSizeOptions;
    @track defaultSortField;
    @track pageSize;
    @track offsetValue;
    @track sortDirection;
    @track sortField;
    @track search;
    @track defaultSortDirection;
    @track isLastRecord = false;
    @track errorDetails = {
        messageDetail:'',
        headerMessage:'Error',
        isErrorVisible:false

    };
    @track searchByDate = {
        fromDate:'',
        toDate:''
    }

    @wire(getListOfDealRecord)
    listOfDeals({ data, error }) {
        if (data) {
            this.initilizeTableData(data);
        } 
        else if (error) {
            this.errorMessage.messageDetail = error;
            this.tableData = undefined;
            this.hideSpinner();
        }
    }

    connectedCallback() {
        Promise.all([
            loadStyle(this, customCss)
        ]).then(() => {
            
        })
            .catch(error => {
                alert(error.body.message);
            });
    }

    initilizeTableData(data) {
        this.dataTableColumn = data.dataTableColumn;
        this.pageSizeOptions = data.pageSizeOptions;
        this.actionColumn = data.dataTableActionColumn;
        this.pageSize = data.defaultPageSize;
        this.defaultSortField = data.defaultSortField;
        this.tableData = data.data;
        this.offsetValue = data.defaultOffset;
        this.defaultSortDirection = data.defaultSortDirection;
        if (this.tableData.length < this.pageSize) {
            this.isLastRecord = true;
        }
        this.hideSpinner(); 
    }

    handleSearchKey(event) {
        this.isLastRecord = false;
        this.offsetValue = '0';
        if( event.detail === null)
        {
            this.searchByDate.fromDate = '';
            this.searchByDate.toDate = '';
        }
        this.search = event.detail;

        this.showSpinner();

        getListOfDealRecordBySearchParam({ 
            searchKey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.defaultSortField,
            sortDirection: this.defaultSortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        })
            .then(result => {
                this.tableData = result;
                if (this.tableData.length < this.pageSize) {
                    this.isLastRecord = true;
                }
                if (this.tableData.length === 0) {
                    this.tableData = undefined;
                }
                this.hideSpinner();
            }).catch(error => {
                this.errorMessage.messageDetail = error;
            });
    }

    handleSearchByDate(event){
        this.isLastRecord = false;
        this.offsetValue = '0';
        this.search = event.detail.searchParam;
        this.searchByDate.fromDate = event.detail.fromDate;
        this.searchByDate.toDate = event.detail.toDate;
        this.showSpinner();

        getListOfDealRecordBySearchParam({
            searchKey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.defaultSortField,
            sortDirection: this.defaultSortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        })
            .then(result => {
                this.tableData = result;
                if (this.tableData.length < this.pageSize) {
                    this.isLastRecord = true;
                }
                if (this.tableData.length === 0) {
                    this.tableData = undefined;
                }
                this.hideSpinner();
            }).catch(error => {
                this.errorMessage.messageDetail = error;
                this.errorDetails.isErrorVisible = true;
                this.errorDetails.errorContent = error.body.message;
                this.hideSpinner();
            });
    }

    handleRecordsOnPage(event) {
        this.isLastRecord = false;
        this.offsetValue = '0';
        this.pageSize = event.detail;
        this.showSpinner();
        
        getListOfDealRecordBySearchParam({
            searchKey: this.search, 
            pageSize: this.pageSize,
            offset: this.offsetValue, 
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        })
            .then(result => {
                this.tableData = result;
                if (this.tableData.length < this.pageSize) {
                    this.isLastRecord = true;
                }
                this.hideSpinner();
        }).catch(error => {
            this.errorMessage.messageDetail = error;
        });
    }

    handleSorting(event) {
        this.sortDirection = event.detail.sortByDirection;
        this.sortField = event.detail.sortByField;

        getListOfDealRecordBySearchParam({
            searchKey: this.search,
            pageSize: this.pageSize,
            offset: this.offsetValue,
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        })
            .then(result => {
                this.tableData = result;
                if (this.tableData.length < this.pageSize) {
                    this.isLastRecord = true;
                }
        }).catch(error => {
            this.errorMessage.messageDetail = error;
        });
    }

    showSpinner() {
        this.displaySpinner = true;
    }

    hideSpinner() {
        this.displaySpinner = false;
    }

    handlePaginationAction(event) {
        const searchParam = event.detail.searchParam,
              pageSize = event.detail.pageSize,
              action = event.detail.action;

        let currentOffset = parseInt(this.offsetValue),
            currentPageSize = parseInt(pageSize);

        this.showSpinner();
        if (action === 'next') {
            this.offsetValue = (currentOffset + currentPageSize) + '';
        } 
        else if (action === 'previous') {
            this.offsetValue = (currentOffset - currentPageSize) + '';
            if (parseInt(this.offsetValue) < 0) {
                this.offsetValue = '0';
            }
        }

        getListOfDealRecordBySearchParam({
            searchKey: searchParam, 
            pageSize: pageSize,
            offset: this.offsetValue,
            //sortOrderField: this.defaultSortField,
            //sortDirection: this.defaultSortDirection
            sortOrderField: this.sortField,
            sortDirection: this.sortDirection,
            fromDate:this.searchByDate.fromDate,
            toDate:this.searchByDate.toDate
        }).then(result => {
            if (result.length > 0) {
                this.isLastRecord = false;
                this.tableData = result;
                if (result.length < this.pageSize) {
                    this.isLastRecord = true;
                }
            }
            else {
                this.isLastRecord = true;
            }
            this.hideSpinner();
        }).catch(error => {
            this.errorMessage = error;
        });
    }

    handleClick(event) {
        const sourceName = event.target.name;

        if (sourceName === 'backToHome') {
            this[NavigationMixin.Navigate]({
                "type": "comm__namedPage",
                attributes: {
                    pageName: 'home',
                },
            });
        }
        if (sourceName === 'newApplication') {
            this[NavigationMixin.Navigate]({
                type: 'comm__namedPage',
                attributes: {
                    pageName: 'new-application',
                },
            });
        }
    }
}