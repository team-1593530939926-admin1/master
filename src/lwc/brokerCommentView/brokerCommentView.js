import { LightningElement,api } from 'lwc';

export default class BrokerCommentView extends LightningElement {
    @api comment;
}