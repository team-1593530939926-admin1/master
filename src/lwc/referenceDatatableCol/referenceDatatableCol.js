import { LightningElement, api, track } from 'lwc';
// For the render() method

export default class ReferenceDatatableCol extends LightningElement {
    @api referenceData;
    @api referenceKey;
    @api referenceValue;
    
    
    get cellValue() {
        if(this.referenceData)
        {
            return this.referenceData[this.referenceKey];
        }
        
    }
}