import { LightningElement , api } from 'lwc';
import yes from '@salesforce/label/c.Text_Yes';
import no from '@salesforce/label/c.Text_No';

export default class PopUpModal extends LightningElement {

    @api showPopup;
    @api label1;
    @api label2;
    @api valuesIncomming;
    @api valuesIncomming2;
    @api confirmation;
    @api headermessage;
    label = {
        yes,
        no
    }

    closeModal()
    {
        this.showPopup = false;
        const popEvent = new CustomEvent("popupclose", {
            detail: this.showPopup
          });
      
          // Dispatches the event.
          this.dispatchEvent(popEvent);
    }
}