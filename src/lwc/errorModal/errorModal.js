import { LightningElement ,api} from 'lwc';

export default class ErrorModal extends LightningElement {
    @api headerMessage;
    @api errorContent;
    @api isErrorVisible;
    closeModal() {
        this.isErrorVisible = false;
        const errorEvent = new CustomEvent("errordisplay", {
            detail: this.isPopupVisible
        });

        // Dispatches the event.
        this.dispatchEvent(errorEvent);
    }
}