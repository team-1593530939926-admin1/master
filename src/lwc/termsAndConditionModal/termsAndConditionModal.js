import { LightningElement,api } from 'lwc';
import { TERMS_AND_CONDITION } from 'c/cvfLabelsUtil';
export default class TermsAndConditionModal extends LightningElement {
    @api firstHeaderTitle;
    @api firstHeaderTitleContent;
    @api secondHeaderTitle;
    @api secondHeaderTitleContent;
    @api thirdHeaderTitle;
    @api thirdHeaderTitleContent;
    @api thirdHeaderTitleContentRemaining;
    @api fourthHeaderTitle;
    @api fourthHeaderTitleContent;
    @api fifthHeaderTitleContent;
    @api sixthHeaderTitleContent;
    @api headerMessage;
    @api isTermsAndConditionVisible = false;
 label = TERMS_AND_CONDITION;
    closeModal() {
        this.isTermsAndConditionVisible = false;
        const termsAndConditionEvent = new CustomEvent("termsandconditionrejected", {
            detail: this.isPopupVisible
        });

        // Dispatches the event.
        this.dispatchEvent(termsAndConditionEvent);
    }
    onSubmit(){
        this.isTermsAndConditionVisible = false;
        const termsAndConditionAcceptedEvent = new CustomEvent("termsandconditionaccepted", {
            detail: this.isPopupVisible
        });

        // Dispatches the event.
        this.dispatchEvent(termsAndConditionAcceptedEvent);
    }
}