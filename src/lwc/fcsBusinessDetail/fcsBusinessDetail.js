import { LightningElement, track, api } from 'lwc';
import { FCS_BUSINESS_DETAILS_LABELS } from 'c/fcsLabelsUtil';
import { UtilService } from 'c/utilService';

const ACTIVE_ACCORDION = 'entityInformation';
const FILING_DATE = 'filingdate';
const DATE_OF_INCORPORATION = 'dateofincorporation';
const EMPTY_DATE_PLACEHOLDER = '-';

export default class FcsBusinessDetail extends LightningElement {
     // public attributes
     @api businessDetails;

     // private attributes
     entityDetailLabels = FCS_BUSINESS_DETAILS_LABELS;
     @track activeSections = ACTIVE_ACCORDION;

     renderedCallback() {
          
     }

     closeModal() {
          this.closeModalDialog();
     }

     submitDetails() {
          this.closeModalDialog();
     }

     handleClick(event) {
          const selectedBusinessDetail = this.businessDetails,
               target = event.target;
          
          let params = {
               action: false,
               actionName: target.name ? target.name : event.currentTarget.name
          };

          if (target) { 
               if (target.name === 'certify') {
                    params.businessDetails = selectedBusinessDetail;
               }
          }
          this.dispatchEvent(new CustomEvent('businessdetailaction',
               { detail: params }));
     }

     get entityName() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].entityName) {
                    return wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].entityName;
               }
          }
          return null;
     }

     get entityType() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.entityType.entityType) {
                    return wrapperRecord[0].recordInfo.entityInfo.entityType.entityType;
               }
          }
          return null;
     }

     get entityInfo() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               return wrapperRecord[0].recordInfo.entityInfo.entityType;
          }
          return null;
     }

     get partyInfo() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.listOfParties 
                    && wrapperRecord[0].recordInfo.entityInfo.listOfParties.length > 0) {
                    return wrapperRecord[0].recordInfo.entityInfo.listOfParties;
               }
          }
          return undefined;
     }

     get agentInfo() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.listOfAgents 
                    && wrapperRecord[0].recordInfo.entityInfo.listOfAgents.length > 0) {
                    return wrapperRecord[0].recordInfo.entityInfo.listOfAgents;
               }
          }
          return undefined;
     }

     get dateOfIncorporation() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          let dateOfIncorporation = EMPTY_DATE_PLACEHOLDER;
          if (wrapperRecord.length > 0) {
               wrapperRecord[0].recordInfo.entityInfo.listOfDates.forEach(dateObj => {
                    if (dateObj.dateType.toLowerCase() === DATE_OF_INCORPORATION) {
                         dateOfIncorporation = dateObj.dateValue;
                         return false;
                    }
               });
          }
          return dateOfIncorporation;
     }

     get dateOfFiling() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          let dateOfFiling = EMPTY_DATE_PLACEHOLDER;
          if (wrapperRecord.length > 0) {
               wrapperRecord[0].recordInfo.entityInfo.listOfDates.forEach(dateObj => {
                    if (dateObj.dateType.toLowerCase() === FILING_DATE) {
                         dateOfFiling = dateObj.dateValue;
                         return false;
                    }
               });
          }
          return dateOfFiling;
     }

     get domicileInfo() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.entityType.domicileDetails) {
                    return wrapperRecord[0].recordInfo.entityInfo.entityType.domicileDetails;
               }
          }
          return null;
     }

     get addressInfo() {
          const wrapperRecord = this.businessDetails.listOfResultWrapper;
          if (wrapperRecord.length > 0) {
               if (wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0]) {
                    return wrapperRecord[0].recordInfo.entityInfo.entityType.listOfEntityName[0].listOfAddress[0];
               }
          }
          return null;
     }

     get addresOneLabel() {
          return UtilService.format(this.entityDetailLabels.ADDRESS.addressLabel, 1);
     }

     get addresTwoLabel() {
          return UtilService.format(this.entityDetailLabels.ADDRESS.addressLabel, 2);
     }
}