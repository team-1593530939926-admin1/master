import { LightningElement, api, track } from 'lwc';
import { PREVIEW_CONTENT_FILE_LABELS } from 'c/cvfLabelsUtil';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class PreviewContentFile extends LightningElement {
    @api contentVersionId;
    @api contentDocumentId;
    @api contentType;
    @api portalUrl;

    // List of all potential img urls
    @track imgUrls = [];

    // Page num used for tracking pages
    pageNum = 0;

    // Idicator when loading should be finished, because of failure
    loadFailed = false;

    theIframe;
    filePreviewUrl;
    @track isProcessed = false;

    connectedCallback() {
       this.generatePreviewUrl();
        this.isPreviewAvailable();
    }
  

    get fullUrl() {
        return this.filePreviewUrl;//return `${PREVIEW_CONTENT_FILE_LABELS.currentInstance}/crestmarkbrokercommunity/sfc/servlet.shepherd/version/renditionDownload?rendition=${this.contentType}&versionId=${this.contentVersionId}`;
    }

    generatePreviewUrl () {
        const portalURL = this.portalUrl;
        const url = `${portalURL}/sfc/servlet.shepherd/version/renditionDownload?rendition=${this.contentType}&versionId=${this.contentVersionId}`;
        this.filePreviewUrl = url;
    }
   

    isPreviewAvailable () {
        const current = this;
        const xhr = new XMLHttpRequest();
        const url =  this.filePreviewUrl;
        xhr.open('GET', url, true);

        xhr.onload = function() {
            if(this.status === 202) {
                current.isProcessed = false;
                const response = JSON.parse(this.responseText);
                const event = new ShowToastEvent({
                    title: 'CVF Broker Preview Result',
                    message: 'File preview is in process, please try after some time.',
                    variant: 'info',
                    mode: 'dismissable'
                });
                current.dispatchEvent(event); 
            } 
            if (this.status === 200) {
                current.isProcessed = true;
            }
        };

        xhr.send();
    }

    showloading(event) {
        alert('File preview not available immidately please wait!');
    }


}