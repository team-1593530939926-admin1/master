import { LightningElement, track } from 'lwc';
import addingBrokerComment from '@salesforce/apex/BrokerCommentInformationController.addingBrokerComment';
import checkForBrokerComment from '@salesforce/apex/BrokerCommentInformationController.checkForBrokerComment';
import { NavigationMixin } from 'lightning/navigation';
import { BROER_COMMENT_LABELS } from 'c/cvfLabelsUtil';
import getSubmittedApplicationOnView from "@salesforce/apex/DealDataTableController.getSubmittedApplicationOnView";
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';

export default class BrokerCommentInformation extends NavigationMixin(LightningElement) {
    @track brokerInput = {
        comment: '',
        applicationId:'',
        companyCode:'',
        dealCode:'',
        companyName:'',
        dealName:'',
        equipmentDescription:'',
        Amount:''
    };
    @track errorDetails = {
        isErrorVisible: false,
        headerMessage: "Error",
        errorContent: ''
    };
    @track brokerApplicationFormDetails;
    @track isLoaded = false;
    @track fieldEmpty = false;
    @track isModalOpen = false;
    @track isButtonVisible = false;
    label = BROER_COMMENT_LABELS;
    handleSubmittedComment(event) {
        const current = this,
        testURL = window.location.href,
        newURL = current.returnIdFromUrl(testURL);
        current.brokerInput.applicationId = newURL[0];
        if(current.validateFields()){
           addingBrokerComment({brokerInput:current.brokerInput})
           .then(result=>{
               if(result === 'true'){
                current.isModalOpen = true;
                current.isButtonVisible = false;
               }
           })
           .catch(error => {
            current.isLoaded = false;
            current.errorDetails.isErrorVisible = true;
            current.errorDetails.errorContent = error.body.message;

        });
         
           
        }

    }

    
    handleErrorPopup() {
        this.errorDetails.isErrorVisible = false;
        this.isCompanyInformationView = true;
    }

    handleBrokerComments(event) {
        const current = this;
        current.brokerInput.comment = event.target.value;
    }

    validateFields() {

        let isValid = true;
        this.template.querySelectorAll('lightning-textarea').forEach(element => {
           if(!element.reportValidity()){
            isValid = false;
           }
        });
        return isValid;
    }

    returnIdFromUrl(value){
        const newURL = value.match(/[a-z0-9]\w{4}0\w{12}|[a-z0-9]\w{4}0\w{9}/g);
        return newURL;
    }


    loadStyleFromCSS() {
        Promise.all([
            loadStyle(this, customCss)
        ]).then(() => {
            // alert('StyleSheet Implemented')
        })
        .catch(error => {
            alert(error.body.message);
        });
    }

    connectedCallback(){


        const current = this,
        testURL = window.location.href,
        newURL = current.returnIdFromUrl(testURL);
        current.brokerInput.applicationId = newURL[0];
        current.loadStyleFromCSS();
        console.log('hiiii'+current.brokerInput.applicationId);
        checkForBrokerComment({brokerInput:current.brokerInput})
        .then(result=>{
            if(result.dealCode){
                current.brokerInput = result;
                current.fieldEmpty = false;
            }
            else{
                current.fieldEmpty = true;
            }
           
        }) 
        .catch(error => {
            current.isLoaded = false;
            current.errorDetails.isErrorVisible = true;
            current.errorDetails.errorContent = error.body.message;

        });
    }

    navigateToHome() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'home'
            },
        });
    }

    onClosingModal(){
        const current = this;
        current.isModalOpen = false;
        current.navigateToHome();
    }
}