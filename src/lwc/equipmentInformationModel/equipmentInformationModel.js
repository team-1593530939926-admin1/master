export class EquipmentInformationModel{
    isFinalEquipmentFormValid = true;
    isRequired = false;
    dealName = "";
    supplierName = "";
    contactName = "";
    leaseTermMonths = "";
    estimatedEquipmentCost = "";
    equipmentDescription = "";
    endOfLeasePurchaseOption = "";
    equipmentLocation = false;
    totalCashPrice = "";
    otherText = "";
    otherStreetAddress = "";
    otherCity = "";
    otherCounty = "United States";
    otherState = "";
    otherZip = "";
    comment = "";
    documentationType="";
    note="";
    constructor(){

    }
}