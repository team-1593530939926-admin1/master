import { LightningElement,track } from 'lwc';
import returnRecordRelatedToDealPaynet from '@salesforce/apex/BrokerApplicationFormController.returnRecordRelatedToDealPaynet';
import updateOpportunity from '@salesforce/apex/BrokerApplicationFormController.updateOpportunity';

export default class CompanyDetailPageLoader extends LightningElement {
    @track dataId = '';
    @track progress = 5000;  
    @track isLoaded = false;

    connectedCallback() {
        const current = this,
            testURL = window.location.href,
            newURL = current.returnIdFromUrl(testURL);
        current.dataId = newURL[0];
        if(current.dataId !== ''){
            current.handleCallBack(current.dataId);
        }
       
    }


    returnIdFromUrl(value) {
        const newURL = value.match(/[a-z0-9]\w{4}0\w{12}|[a-z0-9]\w{4}0\w{9}/g);
        return newURL;
    }

     handleCallBack(value) {   
         this._interval = setInterval(() => {  
             this.isLoaded = true;
              
             returnRecordRelatedToDealPaynet({dealId:value})
             .then(result =>{
                 if (result !== 'false'){
                     this.isLoaded = false;
                     this.handleOpportunityUpdate(result);
                     clearInterval(this._interval); 
                 }
                 if(result === 'false'){
                     this.progress = this.progress + 5000;
                 }   
             })  
         }, this.progress);  
   
     }  

     handleOpportunityUpdate(value){
        updateOpportunity({dealId:value})
     }
}