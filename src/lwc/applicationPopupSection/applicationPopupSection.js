import { LightningElement, api } from 'lwc';
import { POP_UP_LABELS } from "c/cvfLabelsUtil";

export default class ApplicationPopupSection extends LightningElement {

    @api isPopupVisible;
    @api isDetailVisible;
    @api customlabelOuter;
    @api customlabelInside;
    @api displayOuterInfo;
    @api displayInsideInfo;
    @api confirmationMessage;
    @api headermessage;
    @api customButtonLabel;
    label = POP_UP_LABELS;
    @api isButtonVisible = false;

    handleBtnClick(event) {
        const current = this,
        param = {
            action: event.target.name ? event.target.name : event.currentTarget.name,
            value: current.isPopupVisible = false
        };
        this.isPopupVisible = false;
        const popUpEvent = new CustomEvent("popupclose", {
            detail: param
        });

        // Dispatches the event.
        this.dispatchEvent(popUpEvent);
    }
}