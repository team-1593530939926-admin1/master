export class TradeInformationModel{
    isClearButtonVisible = false;
    searchBox = "";
    isDisable = false;
    existingId = null;
    isTradeInformationFormValid =  true;
    isRequired =  false;
    rowId =  -1;
    companyCode =  "";
    companyName =  "";
    addressLine1 =  "";
    firstNameTrade =  "";
    lastNameTrade =  "";
    cityTrade =  "";
    stateTrade =  "";
    phoneNumberTrade =  "";
    zipTrade =  "";
    faxTrade =  "";
    countryTrade =  "";
    isDeleteRequire = true;
    count = "0";
    fullSearchName ="";
    isRecordSearch = false;
    constructor(rowId, isDeleteRequire) {
        this.rowId = rowId;
        this.isDeleteRequire = isDeleteRequire === undefined ? true : isDeleteRequire;
        this.count = Number(rowId) + 1;
    } 
     
}