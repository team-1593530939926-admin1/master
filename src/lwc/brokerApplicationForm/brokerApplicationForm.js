import { LightningElement, track } from "lwc";
import { NavigationMixin } from 'lightning/navigation';
import { TradeInformationModel } from 'c/tradeInformationModel';
import { BankInformationModel } from 'c/bankInformationModel';
import { CompanyInformationModel } from 'c/companyInformationModel';
import { PartnerInformationModel } from 'c/partnerInformationModel';
import { EquipmentInformationModel } from 'c/equipmentInformationModel';
import { GuarantorInformationModel } from 'c/guarantorInformationModel';
import insertBrokerRecord from '@salesforce/apex/BrokerApplicationFormController.submitBrokerApplication';
import reviewAccountRecord from '@salesforce/apex/BrokerApplicationFormController.reviewBrokerApplication';
import getInformationByCompanyCode from '@salesforce/apex/BrokerApplicationFormController.getInformationByCompanyCode';
import returnRecordRelatedToDeal from '@salesforce/apex/BrokerApplicationFormController.returnRecordRelatedToDeal';
import updateOpportunity from '@salesforce/apex/BrokerApplicationFormController.updateOpportunity';
import { BROKER_APPLICATION_FORM_LABEL } from 'c/cvfLabelsUtil';
import { UtilService } from 'c/utilService';
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';
import getPortalURL from '@salesforce/apex/DealDocumentUploadTableController.getPortalURL';

export default class BrokerApplicationForm extends NavigationMixin(LightningElement) {
    brokerApplication = {
        companyInformationWrapper: new CompanyInformationModel(),
        partnerInformationWrapper: [new PartnerInformationModel(0)],
        tradeInformationWrapper: [new TradeInformationModel(0, false)],
        bankInformationWrapper: [new BankInformationModel(0)],
        equipmentInformationWrapper: new EquipmentInformationModel(),
        guarantorInformationWrapper: [new GuarantorInformationModel(0)],
        entityInformation: {},
        fileDetailData: [],
        listOfDocuments: []
    };


    partnerFormValid = true;
    bankFormValid = true;
    tradeFormValid = true;
    @track documentUploadInitiateFrom;
    @track isReviewApplicationForm = false;
    @track isEquipmentInformationModalOpen = false;
    @track isCompanyInformationView = true;
    @track isTermsandConditionVisible = false;
    @track activeTabValue = "companyInformation";
    @track dealNameEquipment;
    @track progressValue = 0;
    @track isDocumentUploadModal = false;
    @track nextByPassForSoleDisable = false;
    @track submit = {
        SubmittedApplicationId: '',
        isSubmitted: false,
        onAppSubmit: true,
        failure: ''
    };
    @track errorDetails = {
        isErrorVisible: false,
        headerMessage: "Error",
        errorContent: ''
    };
    @track resultData = {
        applicationId: '',
        companyId: '',
        dealId: '',
        companyCode: '',
        isPaynetApplicable: false
    }
    @track isLoaded = false;
    @track reviewFormData = [];
    @track isReadOnly = false
    @track isExistingCompanyAfterValidate = false;
    @track multiDialogBox = {
        title: 'Broker Application Error',
        message: '',
        okLabel: 'OK',
        isVisible: false
    };
    label = BROKER_APPLICATION_FORM_LABEL;
    @track companySearched = false;

    @track entityVerificationData = {};

    connectedCallback() {
        Promise.all([
            loadStyle(this, customCss)
        ]).then(() => {
            // alert('StyleSheet Implemented')
        })
            .catch(error => {
                alert(error.body.message);
            });
    }


    handleActive(event) {
        this.isCompanyInformationView = true;
        this.template.querySelector('lightning-tabset').activeTabValue = 'companyInformation';
        this.activeTabValue = this.template.querySelector('lightning-tabset').activeTabValue;
    }

    handleMultipleRowException(event) {
        const currentTemplate = this,
            message = event.detail;
        currentTemplate.multiDialogBox.message = message;
        currentTemplate.multiDialogBox.isVisible = true;

    }

    handleSaveCompanyInformation(event) {
        const current = this;

        if (!current.handleCompanyInformationData()) {
            return false;
        }

        if (!current.handlePartnerInformationData()) {
            return false;
        }

        if (!current.handleTradeInformationData()) {
            return false;
        }
        if (!current.handleBankInformationData()) {
            return false;
        }
        if (!current.handleTotalPercent()) {
            return false;
        }

        current.template.querySelector('lightning-tabset').activeTabValue = 'Equipment_Information';

        current.isCompanyInformationView = false;
        current.progressValue = 33.33;
        current.activeTabValue = current.template.querySelector('lightning-tabset').activeTabValue;
    }



    handleCompanyInformationData() {
        const current = this,
            companyInformationComponent = this.template.querySelector("c-company-information");

        if (companyInformationComponent) {
            let companyInformationFormData = companyInformationComponent.onCompanyDetailSave();

            if (companyInformationFormData == null) {
                return false;
            }

            current.brokerApplication.companyInformationWrapper = companyInformationFormData.companyInformation;
            current.entityVerificationData = companyInformationFormData.entityVerification;
            current.brokerApplication.equipmentInformationWrapper.dealName = companyInformationFormData.companyInformation.fullLegalCompanyName + UtilService.generateDateTimeStampInMMDDYYHHMM('-');
            current.dealNameEquipment = companyInformationFormData.companyInformation.fullLegalCompanyName + UtilService.generateDateTimeStampInMMDDYYHHMM('-');
        }
        return true;
    }

    handlePartnerInformationData() {
        const current = this,
            partnerInformationComponent = this.template.querySelector("c-partner-information");

        if (partnerInformationComponent) {
            let partnerInformationFormData = partnerInformationComponent.submitPartnerDetailData();

            if (partnerInformationFormData == null) {
                return false;
            }

            current.brokerApplication.partnerInformationWrapper = partnerInformationFormData;
            return true;
        }
        return false;
    }

    handleTradeInformationData() {
        const current = this,
            tradeInformationComponent = this.template.querySelector("c-trade-information");

        if (tradeInformationComponent) {
            let tradeInformationFormData = tradeInformationComponent.submitTradeDetailData();
            if (tradeInformationFormData == null) {
                return false;
            }
            current.brokerApplication.tradeInformationWrapper = tradeInformationFormData;
            return true;

        }
        return false;
    }

    handleBankInformationData() {
        const current = this,
            bankInformationComponent = this.template.querySelector("c-bank-references-information");

        if (bankInformationComponent) {
            let bankInformationFormData = bankInformationComponent.submitBankDetailData();
            if (bankInformationFormData == null) {
                return false;
            }
            current.brokerApplication.bankInformationWrapper = bankInformationFormData;
            return true;

        }
        return false;
    }

    handleEquiqmentInformationData() {
        const current = this,
            equipmentInformationComponent = this.template.querySelector("c-equipment-information");
        if (equipmentInformationComponent) {
            let equipmentInformationFormData = equipmentInformationComponent.onEquipmentDetailSave();
            if (equipmentInformationFormData == null) {
                return false;
            }
            current.brokerApplication.equipmentInformationWrapper = equipmentInformationFormData;
        }
        return true;
    }

    handleGuarantorInformationData() {
        const current = this,
            guarantorInformationComponent = this.template.querySelector("c-guarantor-information");

        if (guarantorInformationComponent) {
            let guarantorInformationFormData = guarantorInformationComponent.onGuarantorInformationSave();
            if (guarantorInformationFormData == null) {
                return false;
            }
            if (current.handleTotalPercentGuarantor(guarantorInformationFormData)) {
                current.isReviewApplicationForm = true;
                current.brokerApplication.guarantorInformationWrapper = guarantorInformationFormData;
                return true;
            }

        }
        return false;
    }

    onGuarantorReviewTab() {
        this.guarantorsList = this.template.querySelector("c-guarantor-information").onGuarantorInformationSave();

    }


    onBackToCompanyPage() {
        this.template.querySelector('lightning-tabset').activeTabValue = 'companyInformation';
        this.isCompanyInformationView = true;
        this.progressValue = 0;
        this.activeTabValue = this.template.querySelector('lightning-tabset').activeTabValue;
        if (this.brokerApplication.companyInformationWrapper.existingId && this.brokerApplication.companyInformationWrapper.existingId !== null) {
            this.companySearched = true;
        }
        else{
            this.companySearched = false;
        }
    }

    onBackToEquiqmentPage() {
        const currentTemplate = this;
        currentTemplate.activeTabValue = 'Equipment_Information';
        currentTemplate.progressValue = 33.33;
        current.documentUploadInitiateFrom = 'EQUIPMENT_INFORMATION';
    }

    onSubmittingEquipmentDetails() {
        const current = this;
        if (!current.handleEquiqmentInformationData()) {
            return false;
        } else {
            current.isEquipmentInformationModalOpen = true;
        }
    }

    closeEquipmentInformationModal() {

        this.isEquipmentInformationModalOpen = false;
        this.progressValue = 33.33;
    }

    submitEquipmentInformationModal() {
        const currentTemplate = this;
        currentTemplate.isEquipmentInformationModalOpen = false;
        currentTemplate.activeTabValue = 'Guarantor_Information';
        currentTemplate.progressValue = 66.66;
        currentTemplate.documentUploadInitiateFrom = 'EQUIPMENT_INFORMATION';
    }

    onBackToGuarantorPageAfterReview() {
        this.isReviewApplicationForm = false;
    }

    closeTermsandCondition() {
        this.istermsAndConditionVisible = false;
    }


    handleOnCloseReviewModel() {
        const currentTemplate = this;
        currentTemplate.isReviewApplicationForm = false;
        currentTemplate.isEquipmentInformationModalOpen = false;
        currentTemplate.brokerApplication.guarantorInformationWrapper.forEach(function (guarantorDetail) {
            if (guarantorDetail.mailingStateGuarantor !== "") {
                currentTemplate.activeTabValue = 'Guarantor_Information';
                currentTemplate.progressValue = 66.66;
            }
            else {
                currentTemplate.activeTabValue = 'Equipment_Information'
                currentTemplate.progressValue = 33.33;
            }
        })

    }

    handledDocumentUploadOpenModalOnGuarantor() {
        const currentTemplate = this,
            guarantorInformationComponent = currentTemplate.template.querySelector("c-guarantor-information");
        currentTemplate.isDocumentUploadModal = false;
        if (guarantorInformationComponent) {
            let guarantorInformationData = guarantorInformationComponent.onGuarantorInformationSave();
            if (guarantorInformationData == null) {
                currentTemplate.isDocumentUploadModal = false;
            }
            else {
                currentTemplate.isDocumentUploadModal = true;
            }
        }
    }

    handledDocumentUploadOpenModalOnEquipmentPopup() {
        const currentTemplate = this;
        currentTemplate.isEquipmentInformationModalOpen = false;
        currentTemplate.isDocumentUploadModal = true;
    }

    handleDocumentUploadCloseModal(event) {
        const current = this,
            param = event.detail;

        current.brokerApplication.fileDetailData = param.fileList;
        current.isDocumentUploadModal = false;
        current.isEquipmentInformationModalOpen = false;
    }

    onDocumentUploadCallback(event) {
        const current = this,
            param = event.detail;

        if (current.brokerApplication.companyInformationWrapper.companyType === 'Sole Proprietor') {
            if (current.isCompanyTypePropriotorPresent(param.FileTypeAndDescriptionDetails)) {
                return false;
            }
        }

        current.isDocumentUploadModal = false;
        current.isEquipmentInformationModalOpen = false;
        if (param && param.fileList) {
            current.brokerApplication.fileDetailData = param.fileList;
        }

        //if (current.handleEquiqmentInformationData()) {       
        if (param.invokeFrom === 'Guarantor_Information') {
            if (current.handleGuarantorInformationData()) {
                if (current.handleEquiqmentInformationData()) {
                    current.handleReviewDataFromServer();
                }
            }
        } else if (param.invokeFrom === 'Equipment_Information') {
            current.handleReviewDataFromServer();
        }

    }

    handleReviewDataFromServer() {
        const current = this,
            guarantorInformationComponent = current.template.querySelector("c-guarantor-information");
        let guarantorInformationData = [];
        if (guarantorInformationComponent) {
            guarantorInformationData = guarantorInformationComponent.onGuarantorInformationSave();
        }
        if (guarantorInformationData !== current.brokerApplication.guarantorInformationWrapper) {
            current.brokerApplication.guarantorInformationWrapper = [new GuarantorInformationModel(0)];
        }
        reviewAccountRecord({ recordValues: JSON.stringify(current.brokerApplication) })
            .then(result => {
                current.isReviewApplicationForm = true;
                current.isEquipmentInformationModalOpen = false;
                current.reviewFormData = result;
                current.progressValue = 100;
            })
    }


    onGuarantorReview() {
        const current = this;
        current.isReviewApplicationForm = false;
        if (current.handleGuarantorInformationData()) {
            if (current.handleEquiqmentInformationData()) {
                current.isReviewApplicationForm = true;
                reviewAccountRecord({ recordValues: JSON.stringify(this.brokerApplication) })
                    .then(result => {
                        current.reviewFormData = result;
                        current.isEquipmentInformationModalOpen = false;
                    })
                current.progressValue = 100;
            }
        }
    }

    onEqipmentPageSubmission() {
        this.isTermsandConditionVisible = true;
        this.isReviewApplicationForm = false;
        this.isEquipmentInformationModalOpen = false;

    }
    onCloseTermsandCondition() {
        const currentTemplate = this;
        currentTemplate.isTermsandConditionVisible = false;
        currentTemplate.brokerApplication.guarantorInformationWrapper.forEach(function (guarantorDetail) {
            if (guarantorDetail.mailingStateGuarantor !== "") {
                currentTemplate.activeTabValue = 'Guarantor_Information';
                currentTemplate.progressValue = 66.66;
            }
            else {
                currentTemplate.activeTabValue = 'Equipment_Information'
                currentTemplate.progressValue = 33.33;
            }
        })
    }
    onSubmitTheForm() {
        const currentTemplate = this;
        currentTemplate.isLoaded = true;
        this.brokerApplication.entityInformation = this.entityVerificationData;
        insertBrokerRecord({ recordValues: JSON.stringify(this.brokerApplication) })
            .then(result => {
                if (result) {
                    if (result.isPaynetApplicable === false) {

                        currentTemplate.submit.SubmittedApplicationId = result.applicationId + ' Submitted successfully  - Please wait for Approval'
                        currentTemplate.handleCallBack(result.dealId);

                        //  currentTemplate.submit.isSubmitted = true;
                        // currentTemplate.submit.onAppSubmit = false;
                        //  currentTemplate.isLoaded = false;
                    }
                    else if (result.isPaynetApplicable === true) {
                        currentTemplate.resultData.applicationId = result.applicationId;
                        currentTemplate.resultData.companyId = result.companyId;
                        currentTemplate.resultData.dealId = result.dealId;
                        currentTemplate.resultData.companyCode = result.companyCode;
                        currentTemplate.resultData.isPaynetApplicable = result.isPaynetApplicable;
                        if (currentTemplate.resultData.companyId) {
                            currentTemplate.navigateToPaynet(currentTemplate.resultData.companyId);
                        }

                    }
                }
            })
            .catch(error => {
                currentTemplate.isLoaded = false;
                currentTemplate.errorDetails.isErrorVisible = true;
                currentTemplate.errorDetails.errorContent = error.body.message;

            });
        currentTemplate.isTermsandConditionVisible = false;
    }
    /* 
        onSubmitTheForm() {
            const currentTemplate = this;
            currentTemplate.isLoaded = true;
            this.brokerApplication.entityInformation = this.entityVerificationData;
            insertBrokerRecord({ recordValues: JSON.stringify(this.brokerApplication) })
                .then(result => {
                    if (result) {
                        if (result.isPaynetApplicable === false) {
                            currentTemplate.submit.SubmittedApplicationId = result.applicationId + ' Submitted successfully  - Please wait for Approval';
                            currentTemplate.submit.isSubmitted = true;
                            currentTemplate.submit.onAppSubmit = false;
                            currentTemplate.isLoaded = false;
                        }
                        else if (result.isPaynetApplicable === true) {
                            currentTemplate.resultData.applicationId = result.applicationId;
                            currentTemplate.resultData.companyId = result.companyId;
                            currentTemplate.resultData.dealId = result.dealId;
                            currentTemplate.resultData.companyCode = result.companyCode;
                            currentTemplate.resultData.isPaynetApplicable = result.isPaynetApplicable;
                            if (currentTemplate.resultData.companyId) {
                                currentTemplate.navigateToPaynet(currentTemplate.resultData.companyId);
                            }
    
                        }
                    }
                })
                .catch(error => {
                    currentTemplate.isLoaded = false;
                    currentTemplate.errorDetails.isErrorVisible = true;
                    currentTemplate.errorDetails.errorContent = error.body.message;
    
                });
            currentTemplate.isTermsandConditionVisible = false;
        }
    */


    navigateToPaynet(value) {
        let urlValue;
        getPortalURL()
            .then(result => {
                if (result) {
                    urlValue = result;

                }
            })

        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: urlValue + '/s/paynet?recordId=' + value
                //url: 'https://ncino-crestmarkcommunity.cs10.force.com/crestmark/s/paynet?recordId=' + value
            }
        }).then(generatedUrl => {
            generatedUrl = generatedUrl.replace('undefined/s/', '');
            window.open(generatedUrl, '_self');
        });
    }

    handleErrorModalClick(event) {
        this.multiDialogBox.isVisible = false;
    }


    navigateToHome() {
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                pageName: 'home'
            },
        });
    }

    handleErrorPopup() {
        this.errorDetails.isErrorVisible = false;
        this.isCompanyInformationView = true;
    }

    handleTotalPercent() {
        this.isCompanyInformationView = false;
        const current = this;
        let validityOfPercent = true;
        let totalPercent = 0.00;
        current.brokerApplication.partnerInformationWrapper.forEach(function (percent) {
            if (percent.ownershipPercentage !== '') {
                totalPercent = parseFloat(percent.ownershipPercentage) + totalPercent;
            }
        })

        if (totalPercent !== 0.00 && totalPercent > 100.00) {
            current.errorDetails.isErrorVisible = true;
            current.errorDetails.errorContent = 'The total sum of ownership% should be equal to 100.00';
            validityOfPercent = false;
        }

        return validityOfPercent;
    }
    handleTotalPercentGuarantor(gurantorInfoData) {
        const current = this;
        let validityOfPercent = true;
        let totalPercentGuarantor = 0.00;

        gurantorInfoData.forEach(function (percentGuarantor) {
            if (percentGuarantor.contigent && percentGuarantor.contigent !== null && percentGuarantor.contigent !== '') {
                totalPercentGuarantor = parseFloat(percentGuarantor.contigent) + totalPercentGuarantor;
            }

        })

        if (totalPercentGuarantor !== 100.00 && totalPercentGuarantor !== 0.00) {
            current.errorDetails.isErrorVisible = true;
            current.errorDetails.errorContent = 'The total sum of Contigent% should be equal to 100.00';
            validityOfPercent = false;
            return validityOfPercent;
        }
        return validityOfPercent;
    }

    get isProceedToNext() {
        const current = this;
        let isProceedToNext = false,
            isByPass = current.nextByPassForSoleDisable;
        if (current.entityVerificationData !== undefined && Object.keys(current.entityVerificationData).length !== 0) {
            isProceedToNext = true;
        }
        if (current.isExistingCompanyAfterValidate === true) {
            isByPass = false;

        }

        return !(isProceedToNext || isByPass);
    }

    handleFCSVerified(event) {
        const param = event.detail;
        if (param) {
            if (param.isSuccess) {
                this.entityVerificationData = param.entityVerificationResult;
            }

        }
    }

    /*
    handleRetrieveTradeReferenceDataIfExist(event) {
        const current = this,        
             tradeId = event.detail.tradeId,
             rowId = event.detail.rowId;

        getTradeInformationByCompanyCode({ dealCompanyCode: tradeId, count: rowId})
            .then(result => {
                current.brokerApplication.tradeInformationWrapper[rowId] = undefined;
                current.brokerApplication.tradeInformationWrapper[rowId] = result;
                current.isLoaded = false;
                current.isReadOnly = true
            })
            .catch(error => {
                current.isLoaded = false;
                current.errorDetails.isErrorVisible = true;
                current.errorDetails.errorContent = error.body.message;

            });
    }  */

    handleRetrieveApplicationDataIfExist(event) {
        const current = this,
            brokerApplicationFormData = { ...current.brokerApplication };
        current.nextByPassForSoleDisable = false;
        current.isExistingCompanyAfterValidate = false;
        let brokerInformationData = { ...current.brokerApplication };
        getInformationByCompanyCode({ dealCompanyCode: event.detail })
            .then(result => {
                current.isLoaded = false;
                current.isReadOnly = true;

                current.nextByPassForSoleDisable = result.companyInformationWrapper.nextByPassForSoleDisable;
                brokerInformationData.companyInformationWrapper = { ...result.companyInformationWrapper };
                brokerInformationData.companyInformationWrapper.ismultipleEntityPresent = result.companyInformationWrapper.ismultipleEntityPresent;
                brokerInformationData.companyInformationWrapper.selectedName = result.companyInformationWrapper.fullLegalCompanyName;
                if (result.tradeInformationWrapper[0].companyName) {
                    brokerInformationData.tradeInformationWrapper = [...result.tradeInformationWrapper];
                }
                if (result.bankInformationWrapper[0].bankName) {
                    brokerInformationData.bankInformationWrapper = [...result.bankInformationWrapper];
                }
                if (result.partnerInformationWrapper[0].lastName) {
                    brokerInformationData.partnerInformationWrapper = [...result.partnerInformationWrapper];
                }
                current.brokerApplication = brokerInformationData;

            })
            .catch(error => {
                current.isLoaded = false;
                current.errorDetails.isErrorVisible = true;
                current.errorDetails.errorContent = error.body.message;

            });
        // current.handleCompanyTypeSoleProprietorFromSearch(brokerApplicationFormData.companyInformationWrapper);
        /* if(brokerApplicationFormData.companyInformationWrapper.companyType === "Sole Proprietor" &&
          brokerApplicationFormData.companyInformationWrapper.dateOfIncorporation !== ""){
             
          }
          else{
              current.handleCompanyTypeSoleProprietorFromSearch(brokerApplicationFormData.companyInformationWrapper);
          }*/
    }


    handleDealNameOnEquipment(event) {
        this.dealNameEquipment = event.detail;
    }

    handleCompanyRelatedDataAfterFcsVerification(event) {
        const current = this,
            param = event.detail,
            action = param.action;
        current.nextByPassForSoleDisable = false;
        if (action === 'reset') {
            let brokerApplication = { ...current.brokerApplication };
            brokerApplication.partnerInformationWrapper = [new PartnerInformationModel(0)];
            brokerApplication.tradeInformationWrapper = [new TradeInformationModel(0, false)];
            brokerApplication.bankInformationWrapper = [new BankInformationModel(0)];
            current.brokerApplication = brokerApplication;
            //current.isExistingCompanyAfterValidate = true;
        }
    }

    handleCompanyTypeSoleProprietor(event) {
        const current = this,
            param = event.detail;
        let nextVal = false;
        let brokerInformationData = { ...current.brokerApplication };
        current.nextByPassForSoleDisable = undefined;
        if ((param.isSole && param.dateOfIncorporation !== "") || (brokerInformationData.companyInformationWrapper.companyType === "Sole Proprietor" &&
            brokerInformationData.companyInformationWrapper.dateOfIncorporation !== null)) {
            nextVal = true;
        }
        current.nextByPassForSoleDisable = nextVal;

    }

    handleCompanyTypeSoleProprietorFromSearch(value) {
        const current = this;
        let nextVal = false;
        current.nextByPassForSoleDisable = undefined;
        if (value.companyType === "Sole Proprietor" &&
            value.dateOfIncorporation !== null) {
            nextVal = true;
        }
        current.nextByPassForSoleDisable = nextVal;

    }

    isCompanyTypePropriotorPresent(DocTypevalue) {
        let current = this,
            docTypePresent = false;

        DocTypevalue.forEach(function (docType) {
            if (docType.fileType === 'Proof of Time in Business') {
                docTypePresent = true;
                return false;
            }
            return true;
        });

        if (!docTypePresent) {
            UtilService.showToast('CVF Broker Application',
                "Document type " + BROKER_APPLICATION_FORM_LABEL.companyTypeValue + " is not uploaded",
                'error', current);
            return true;
        }
        current.isDocumentUploadModal = false;


    }

    handleRetrieveApplicationDataIfExistingIdNotExist(event) {
        const current = this,
            isMultipleEntity = event.detail.isEntityMultiple,
            brokerApplicationFormData = { ...current.brokerApplication };
        current.nextByPassForSoleDisable = false;
        current.isExistingCompanyAfterValidate = false;
        let brokerInformationData = { ...current.brokerApplication };
        getInformationByCompanyCode({ dealCompanyCode: event.detail.valueId })
            .then(result => {
                current.isLoaded = false;
                current.isReadOnly = true;
                brokerInformationData.companyInformationWrapper = { ...result.companyInformationWrapper };
                brokerInformationData.companyInformationWrapper.existingId = event.detail.valueId;
                brokerInformationData.companyInformationWrapper.ismultipleEntityPresent = isMultipleEntity;
                if (result.tradeInformationWrapper[0].companyName) {
                    brokerInformationData.tradeInformationWrapper = [...result.tradeInformationWrapper];
                }
                if (result.bankInformationWrapper[0].bankName) {
                    brokerInformationData.bankInformationWrapper = [...result.bankInformationWrapper];
                }
                if (result.partnerInformationWrapper[0].lastName) {
                    brokerInformationData.partnerInformationWrapper = [...result.partnerInformationWrapper];
                }
                brokerInformationData.entityInformation = { ...current.brokerApplication.entityInformation };
                brokerInformationData.fileDetailData = [...current.brokerApplication.fileDetailData];
                brokerInformationData.listOfDocuments = [...current.brokerApplication.listOfDocuments];
                current.brokerApplication = brokerInformationData;
                current.isExistingCompanyAfterValidate = true;
                UtilService.showToast('Updated',
                    "Company already exists. Hence, pulling the Contact details, Partner Information, Trade references and Bank References",
                    'success', current);
            })
    }


    @track progress = 5000;
    handleCallBack(value) {
        const currentTemplate = this;
        this._interval = setInterval(() => {
            returnRecordRelatedToDeal({ dealId: value })
                .then(result => {
                    if (result === true) {
                        currentTemplate.submit.isSubmitted = true;
                        currentTemplate.submit.onAppSubmit = false;
                        currentTemplate.isLoaded = false;
                        currentTemplate.handleUpdateOpportunity(value);
                        clearInterval(this._interval);
                    }
                    if (result === false) {
                        this.progress = this.progress + 5000;
                    }

                })
        }, this.progress);

    }

    handleUpdateOpportunity(value) {
        updateOpportunity({ dealId: value })
    }
}