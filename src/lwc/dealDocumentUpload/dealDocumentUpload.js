import { LightningElement, api ,track ,wire} from 'lwc';
import relatedFiles from '@salesforce/apex/DealDocumentUploadTableController.relatedFiles';
import setFileDetails from '@salesforce/apex/DealDocumentUploadTableController.setFileDetails';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import CONTENTVERSION_OBJECT from '@salesforce/schema/ContentVersion';
import TYPE_FIELD from '@salesforce/schema/ContentVersion.Type__c'; 
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';  
import deleteRecord from '@salesforce/apex/DealDocumentUploadTableController.deleteFiles';
import { DEAL_DOCUMENT_UPLOAD_LABELS } from 'c/cvfLabelsUtil';
import getFileExtensionValues from '@salesforce/apex/BrokerApplicationFormController.getFileExtensionValues';
import getPortalURL from '@salesforce/apex/DealDocumentUploadTableController.getPortalURL';

const columns = [
    {label: DEAL_DOCUMENT_UPLOAD_LABELS.FileName, fieldName: 'fileName'},
    {label: DEAL_DOCUMENT_UPLOAD_LABELS.Description, fieldName: 'fileDescription' },
    {label: DEAL_DOCUMENT_UPLOAD_LABELS.Type, fieldName: 'fileType'},
    {label: DEAL_DOCUMENT_UPLOAD_LABELS.download,type: "button", typeAttributes: {  
        label: 'Download',  
        name: 'View',  
        title: 'Download',  
        disabled: false,  
        value: 'view',  
        iconPosition: 'center'  
    }},
    {label: DEAL_DOCUMENT_UPLOAD_LABELS.Remove,type: "button", typeAttributes: {  
        label: 'Delete',  
        name: 'Del',  
        title: 'Delete',  
        disabled: false,  
        value: 'delete',  
        iconPosition: 'center'  
    }}  
];

const FILE_EXTENSION = ['.pdf', '.xlsx', '.docx', '.csv'];


export default class dealDocumentUpload extends NavigationMixin(LightningElement) {

    @track isViewModalOpen = false;
    @track portalURL;
    @track descriptionRequired = false;
    @wire(getPortalURL)
    gettingPortalURL({data, error})
    {
        if(data)
        {
            this.portalURL = data;
        }

    };


    closeModal() {
        this.isViewModalOpen = false;
    } 




    @api recordId ;
    label = DEAL_DOCUMENT_UPLOAD_LABELS;

    @api 
    get fileDetailData() {
        return this.fileMap;
    }

    set fileDetailData(value) {
        if (value) {
            this.fileMap = undefined;
            this.getRelatedFiles(value)
            this.fileMap = [...value];
            this.FileTypeAndDescriptionDetails = [...value];
        }
    }


    @track columns = columns;
    @track data;
    @track isDocumentUploadModal ;
    @track fileTypeOptions ;
    @api objectApiName;
    @api initiateFrom;
    @track isDisable = true;
    //@track objectInfo; 
    @track fileMap = [];
    @track FileTypeAndDescriptionDetails = [];
    @track fileParam = {
        type: '',
        description: ''
    };
     
    get acceptedFormats() {
        return FILE_EXTENSION;
    }

    @wire(getObjectInfo, { objectApiName: CONTENTVERSION_OBJECT })
    objectInfo;

    @wire(getPicklistValues, {
        fieldApiName: TYPE_FIELD,
        recordTypeId: '$objectInfo.data.defaultRecordTypeId'
    })
     entityTypePicklistValues({ data, error }) {
        if (data) {
          this.fileTypeOptions = data.values;
        }
        else if (error) {
          this.error = error;
        }
      }

      handleOnTypeChange(event) {
        const current = this;
        current.fileParam.type = event.detail.value;
        current.isDisable = false;
        current.descriptionRequired = false;
        if (event.detail.value !== 'Other') {
            current.isDisable = false;
            Promise.resolve().then(() => {
                let inputEle = this.template.querySelector('lightning-input');
                inputEle.reportValidity();
            });
        }
        if (event.detail.value === 'Other') {
            current.descriptionRequired = true;
            current.isDisable = true;
        }
    } 

    // When description is added.
    handleOnDescriptionChange(event) {  
        this.fileParam.description = event.detail.value;  
        if( this.fileParam.description !== '' && this.fileParam.type === 'Other'){
            this.isDisable = false;
        }
    }  

  /*   isUploadEnable() {
        const current = this;

        
        if (current.fileParam.type !== '') {
            current.isDisable = false;
        }
        else
        {
            current.isDisable = true;
        }
        return current.isDisable;
    }*/

    handleOnUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        this.fileMap.push(uploadedFiles[0].documentId);
        this.setFileDescription(uploadedFiles[0].documentId);
        this.descriptionRequired = false;
    }

    // It is used to set the type and description field on attachment.
    setFileDescription(docId) {
        const current = this;
        setFileDetails({
            idParent: docId , 
            description: current.fileParam.description ,
            type: current.fileParam.type
        }).then(data => {
            current.getRelatedFiles(this.fileMap);
        }).catch(error => {
            current.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error.message,
                    variant: 'error',
                }),
            );
        })
    }

    get isDataAvailable() {
        let dataAvailable = false;
        if (this.data && this.data.length > 0) {
            dataAvailable = true;
        }
        return dataAvailable;
    }

     // Getting releated files of the current record.
     getRelatedFiles(fileId) {
         const current = this;
        relatedFiles({idParent: fileId})
        .then(data => {
            current.data = [...data];
            console.log(JSON.stringify(current.data));
            current.FileTypeAndDescriptionDetails = current.data;
            current.clear();
        })
        .catch(error => {
            current.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: error.message,
                    variant: 'error',
                }),
            );
        });
    } 


    renderedCallback() {
        const style = document.createElement('style'),
        targetComponent = this.template.querySelector('.container-style-tag');
        style.innerText = `
           .slds-dropdown-trigger .slds-dropdown {
                height: 180px;
                overflow: auto;
            }`;

        if (targetComponent) {
            //targetComponent.appendChild(style);
        } 
  }


    @wire(getFileExtensionValues)
    getListOfFileExtensions({data,error})
    {
      if(data)
      {
          this.fileExtensionList = data;
      }
    }


    @track fileExtensionList = [];
    @track versionId;
    @track documentId;
    @track fileType;
    @track fileExtension;
    @track fileName;


    //It handles view and delete buttons.
    handleOnRowAction( event ) {  
          
        const current = this,
              recId =  event.detail.row.fileId, 
              actionName = event.detail.action.name;

        let fileDataRecords = [...current.data];

        current.versionId = event.detail.row.versionId;
        current.documentId = event.detail.row.fileId;
        current.fileType = event.detail.row.fileType;
        current.fileName = event.detail.row.fileName;

        for(let [key, value] of Object.entries(current.fileExtensionList))
        {
          if(event.detail.row.fileExtension === key)
          {
            current.fileExtension = value;
          }
        } 

        if ( actionName === 'Del' ) {  
              deleteRecord({deleteRecord : recId})
            .then(() => {
                const index = fileDataRecords.findIndex((element) => element.fileId === recId);
                fileDataRecords.splice(index,1);
                current.data = fileDataRecords;
                current.FileTypeAndDescriptionDetails = fileDataRecords;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record Deleted Successfully.',
                        variant: 'success'
                    })
                );
            })
            .catch(error => {
                 this.dispatchEvent(
                 new ShowToastEvent({
                 title: 'Error deleting record',
                 message: error.body.message,
                 variant: 'error'
                })
              );
            });     
        }

         else if ( actionName === 'View') {  

            var urlString = window.location.href;
            var baseURL = urlString.substring(0, urlString.indexOf("/s/"));

            
            
           // const url =  `${baseURL}/sfc/servlet.shepherd/version/renditionDownload?rendition=${current.fileExtension}&versionId=${current.versionId}`;
           const url =  `${baseURL}/sfc/servlet.shepherd/document/download/${current.documentId}`;
           window.open(url);
        /*   
           const xhr = new XMLHttpRequest();
           xhr.open('GET', url, true);
    
            xhr.onload = function() {
                if(this.status === 202) {
                    current.isViewModalOpen = false;
                    const response = JSON.parse(this.responseText);
                    const event = new ShowToastEvent({
                        title: 'CVF Broker Preview Result',
                        message: 'File preview is in process, please try after some time.',
                        variant: 'info',
                        mode: 'dismissable'
                    });
                    current.dispatchEvent(event); 
                } 
                if (this.status === 200) { 
          
                    if(current.documentId !== null)
                    {
                      //current.isViewModalOpen = true;
                      alert('Hiii');
                    }
                }
            };
    
            xhr.send(); */
  
        }          
  
    }


    

//It creates a custom event to close the popup.

handleOnCloseModel() {

    const closeEvt = new CustomEvent('closemodal',{detail : 
        {
            fileList : [...this.fileMap]
        }
    });
    this.dispatchEvent (closeEvt);
}

clear() {
    this.template.querySelectorAll('lightning-combobox').forEach(each => {
        each.value = '';
    });
    this.template.querySelectorAll('lightning-input').forEach(each=> {
        each.value = '';
    });
    this.isDisable=true;
}

onEqipmentReview() {
    if (this.validateFields()) {
        this.dispatchEvent(
            new CustomEvent(
                'documentcalback', {
                    detail:
                    {
                        invokeFrom: this.initiateFrom,
                        fileList: [...this.fileMap],
                        FileTypeAndDescriptionDetails: [...this.FileTypeAndDescriptionDetails]
                    }
                }
            ));
    }
}
validateFields() {

    let isValid = true;
    this.template.querySelectorAll('lightning-input').forEach(element => {
        if (!element.reportValidity()) {
            isValid = false;
        }
    });
    return isValid;
}


}