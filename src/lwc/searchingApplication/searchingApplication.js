import { LightningElement, api, track, wire } from 'lwc';
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import {SEARCHING_APPLICATION} from 'c/cvfLabelsUtil'; 
import getSubmittedApplicationOnView from "@salesforce/apex/DealDataTableController.getSubmittedApplicationOnView";
import { loadStyle } from 'lightning/platformResourceLoader';
import customCss from '@salesforce/contentAssetUrl/CvfCustomCss';

export default class SearchingApplication extends LightningElement {

  label = SEARCHING_APPLICATION;
  
  @api tableColumns;
  @api tableActionColumn;
  @api sObject;
  @api tableLabel;
  @api records;
  @api keyField;
  @api sortBy;
  @api sortDirection;
  @api offsetValue;
  @api isLastRecord;
  @api searchParam;
  @api options;
  @api pageSize;
  @api isRecordFound;

  @track columns;
  @track error;
  @track isSearchBoxEmpty = false;
//@track isSearchByCompanyCode = false;
//@track isSearchByApplicationId = false;
  @track value;
  @track selectedDateFilter = false;

  @wire(getObjectInfo, { objectApiName: "$sObject" })
  objectInfo({ data, error }) {
    if (data) {
      this.createColumnMapping(data.fields);
    } else if (error) {
      this.error = error;
    }
  }

  connectedCallback() {
    Promise.all([
        loadStyle(this, customCss)
    ]).then(() => {
      
    })
        .catch(error => {
            alert(error.body.message);
        });
}

  createColumnMapping(objectFields) {
    let tableCol = [];
    if (this.tableColumns) {
        for (const [key, value] of Object.entries(this.tableColumns)) {
            let colConfig = {};
            if (objectFields.hasOwnProperty(key)) {
                colConfig = {
                label: value,
                fieldName: key,
                type: objectFields[key].dataType === 'String' ? 'text' : (objectFields[key].dataType === 'DateTime' ? 'date' : objectFields[key].dataType),
                sortable: objectFields[key].sortable
            };
           }
            else if (key.split(".").length > 1) {
                const referencePathArr = key.split(".");
                colConfig = {
                label: value,
                fieldName: key,
                type: "customReferenceCell",
                typeAttributes: {
                referenceData: { fieldName: referencePathArr[0] },
                referenceKey: referencePathArr[1]
                },
                sortable: "true"
              };
            }
        tableCol.push(colConfig);
      }
    }

    if (this.tableActionColumn) {
        let actionConfig = {};
            for (const [key, value] of Object.entries(this.tableActionColumn)) {
                actionConfig = {
                label: value,
                type: 'button',
                initialWidth: 135,
                typeAttributes: {
                label: value,
                name: value + '_action',
                title: '',
                variant: 'neutral'
               },
             cellAttributes: {
                  alignment: 'center' 
             }
           };
         }
      tableCol.push(actionConfig);
    }
    this.columns = tableCol;
  }

  handleSearchItem(event) {
    this.isSearchBoxEmpty = false;
    this.searchParam = event.target.value;

    if (!event.target.checkValidity()) {
        event.target.reportValidity();
    } 
    else {
        event.target.reportValidity();
        if (event.which === 13) {
          
            if (!(this.searchParam.length < 3)) {
                const searchEvent = new CustomEvent("searchopportunity", {
                    detail: this.searchParam
            });
          this.dispatchEvent(searchEvent);
          this.isValueSelected = true;
            }
        else if (this.searchParam.length < 1) {
            this.isSearchBoxEmpty = true;
            const searchEvent = new CustomEvent("searchopportunity", {
                detail: null
          });
          this.dispatchEvent(searchEvent);
        }
      }
    }   
  }

  handleChange(event) {
    this.value = event.detail.value;
    const recordsOnPage = new CustomEvent("numberofrecordsonpage", {
        detail: this.value
    });
    this.dispatchEvent(recordsOnPage);
  }

  doSorting(event) {
      this.sortBy = event.detail.fieldName;
      this.sortDirection = event.detail.sortDirection;
      const dealsOnSorting = new CustomEvent("sortingdeals", {
          detail: { sortByField: this.sortBy, sortByDirection: this.sortDirection }
      });
    this.dispatchEvent(dealsOnSorting);
  }

  handleClick(event) {
      const actionName = event.target.name,
      handlePagination = new CustomEvent("paginateaction", {
          detail: { searchParam: this.searchParam, pageSize: this.pageSize, action: actionName }
      });
      this.dispatchEvent(handlePagination);
  }

  get isDisableForPrevious() {
      return this.offsetValue === "0";
  }

  get isDisableForLast() {
    return this.isLastRecord;
  }
  

  @track brokerApplicationFormDetails;
  @track isModalShowing = false;

  handleRowAction(event) {
    const current = this,
      applicationId = event.detail.row.Application_Id__c,
      companyCode = event.detail.row.Account.Company_Code__c;

    getSubmittedApplicationOnView({
      dealApplicationId: applicationId,
      dealCompanyCode: companyCode
    }).then(result => {
      current.brokerApplicationFormDetails = result;
      current.isModalShowing = true;
    });
  }

  handleCloseViewModel(event) {
    this.isModalShowing = false;
  }



  @track isValueSelected = false; 

  handleRemovePill() {
    this.isValueSelected = false; 
    this.fireClearEvent();
}

fireClearEvent () {
  const current = this,
      evt = new CustomEvent('cleartabledata', {detail: null});
  
  current.dispatchEvent(evt);
}

}