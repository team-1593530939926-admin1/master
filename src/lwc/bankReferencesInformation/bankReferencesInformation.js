import { LightningElement, track, api, wire } from 'lwc';
import { BANK_INFORMATION_LABELS } from "c/cvfLabelsUtil";
import { BankInformationModel } from "c/bankInformationModel";
import { UtilService } from 'c/utilService';
import getCountryAndStateValues from '@salesforce/apex/BrokerApplicationFormController.getCountryAndStateValues';

const DUPLICATE_CHECK_KEYS = ["bankName","addressLine1","accountNumber","bankContactPerson","country","state","city","zip","phoneNumber"];
const IGNORE_FORM_KEY_ARR = ['isCreatedByMe','maskedAccountNumber','isRequired', 'existingId', 'rowId', 'isDeleteRequire', 'partnerId', 'isBankInformationFinalFormValid', 'count'];

export default class BankReferencesInformation extends LightningElement { 

    dependentPickListKey = {
        'state': 'state'
    };
    label = BANK_INFORMATION_LABELS;

    @track countryControllingValues = [];
    @track stateDependentValues = [];
    @track defaultCountryAndStateData;
    @track countryVal;
    @track bankInformationList = [];
    @track errorDetails = {
        isErrorVisible: false,
        headerMessage: "Error",
        errorContent: ''
    };
    
    @api isReviewEnabled = false;
    @api
    get bankDetailsListInformation() {
        return this.bankInformationList;
    }

    set bankDetailsListInformation(value) {
        const current = this;
        current.bankInformationList = [];        
        value.forEach(function (eachValue) {
            current.bankInformationList.push({ ...eachValue });
        })
    }

    onDeleteRow(event) {
        const current = this,
            sourceData = event.target.dataset,
            index = sourceData.index,
            currentIndex = this.bankInformationList.findIndex(element => element.rowId + '' === index);
        
        current.bankInformationList.splice(currentIndex, 1);
        for (let i = 0; i < this.bankInformationList.length; i++) {
            current.bankInformationList[i].count = i + 1;
            current.bankInformationList[i].rowId = i;
        }
    }

    createNewRow() {
        const current = this;
        let currentSize = this.bankInformationList.length;
        if (currentSize <= 4) {
            let newRow = new BankInformationModel(currentSize);
            newRow.isDeleteRequire = newRow.rowId !== 0;
            current.bankInformationList.push(newRow);
            for (let i = 0; i < this.bankInformationList.length; i++) {
                current.bankInformationList[i].count = i + 1;
                current.bankInformationList[i].rowId = i;
            }
        }
        else {
            this.dispatchEvent(new CustomEvent('multiplerowcallback', { detail: BANK_INFORMATION_LABELS.textYoucannotaddmorethanfiverows }));
        }
    }

    onHandleChange(event) {
        const current = this,
            id = event.target.dataset.id,
            index = event.target.dataset.index;

        if (id === 'phoneNumber') {
            event.target.setCustomValidity("");
            if ((event.target.value !== '' || event.target.value !== '() -') && event.target.value.length === 10 && event.target.value.match(/^[0-9]*$/)) {
                const phoneNumber = event.target.value;
                event.target.value = UtilService.formatPhoneToUSFormat(phoneNumber);
            }
            else if (event.target.value.length === 0) {
                event.target.setCustomValidity("");
            }
            else{
                event.target.setCustomValidity(BANK_INFORMATION_LABELS.invalidPhoneNumberFormat);
            }
            event.target.reportValidity();
        }

        current.bankInformationList[index][id] = event.target.value;
        current.bankInformationList[index] = current.checkForRequired(
            Object.assign({}, current.bankInformationList[index]));;
    }

    handleOnKeyUp(event) {
        event.target.setCustomValidity("");
        const charCode = event.which;
        if (charCode !== 92 && charCode !== 13 && (charCode < 48 || charCode > 57)) {
            event.target.setCustomValidity(BANK_INFORMATION_LABELS.invalidPhoneNumberFormat);
        }
    }

    handleOnFocus(event) {
        const id = event.target.dataset.id;
        if (id === 'phoneNumber') {
            event.target.value = UtilService.resetUSPhoneNumberFormat(event.target.value);
        }
    }

    checkForRequired(formData) {
        let isRequiredFormValue = false;
        for (let [key, value] of Object.entries(formData)) {
            if (IGNORE_FORM_KEY_ARR.indexOf(key) === -1 && value !== '') {
                isRequiredFormValue = true;
                break;
            }
        }
        formData.isRequired = isRequiredFormValue;
        formData.isBankInformationFinalFormValid = isRequiredFormValue;
        return formData;
    }

    onClearRows() {
        const current = this;
        current.isDisableRequire = true;
        current.bankInformationList[0] = new BankInformationModel(0);
        UtilService.reportValidity(current.bankInformationList[0], current);

    }

    @api 
    submitBankDetailData() {
        const current = this;
        let bankDetailFormDataArr = [...current.bankInformationList];
        const validatedFormDataResult = UtilService.validateFormData(IGNORE_FORM_KEY_ARR, bankDetailFormDataArr, current);
        const duplicateFormData = UtilService.checkDuplicateVariables(IGNORE_FORM_KEY_ARR, bankDetailFormDataArr,DUPLICATE_CHECK_KEYS);
        current.bankInformationList = validatedFormDataResult.result;
        if (validatedFormDataResult.isValid) {
            if(duplicateFormData){
            return current.bankInformationList;
        }
    else{
        current.errorDetails.isErrorVisible = true;
        current.errorDetails.headerMessage = 'Duplicate Value';
        current.errorDetails.errorContent = 'Bank Information Section contains duplicate data';
        return null;
    }
}
        return null;
    }


    @wire(getCountryAndStateValues)
    countryStateData({ data, error }) {
        if (data) {
            let countyOptions = [{ label: '--None--', value: '--None--' }];
            let stateData = [{ label: '--None--', value: '--None--' }];
            for (let [key, value] of Object.entries(data)) {

                countyOptions.push({ label: key, value: key });
                stateData.push({label: key, value: value});
                
              }
              console.log(stateData + 'State dataaa');

            this.defaultCountryAndStateData = stateData;
            console.log(this.defaultCountryAndStateData + 'dataaa');
            this.countryControllingValues = countyOptions;
        }
        else if (error) {
            window.console.log(error);
        }
    }

    handleSelectDependentpicklist(event) {
        const param = event.detail;
        if (param.key) {
            this.bankInformationList[param.index][param.key] = param.val;
        }
    }

    handleErrorPopup() {
        this.errorDetails.isErrorVisible = false;
    }


}