import { LightningElement, track, api } from 'lwc';

export default class DynamicSelectComponent extends LightningElement {

    // private
    @track dynamicDependentOptions = [];
    parentDependentValue;

    customOptions__;

    // public
    @api selectedValue;
    @api 
    get customOptions() {
        return this.customOptions__;
    }

    set customOptions(value) {
        if (value) {
            this.customOptions__ = value;
            this.handleCreateOptions(this.parentDependentValue);
        }
    }
  
    @api 
    get dependentValue() {
        return this.parentDependentValue;
    }

    set dependentValue(value) {
        if (value) {
            this.handleCreateOptions(value);
            this.parentDependentValue = value;
        } 
    }

    @api customLabel; 
    @api currentIndex;
    @api keyParam;
    @api isRequired = false;
    @api isReadOnly = false;
    @api isDisable = false;

    handleCreateOptions (dependentValueParam) {
        if (this.customOptions && dependentValueParam){
           const dependentOptions = Object.values(this.customOptions).filter(value => value.label === dependentValueParam);
           let dependentOpt = [{ label: "-- None --", value: "" }];
           if (dependentOptions) {
                dependentOptions.forEach(function (state) {
                    state.value.forEach(stateOpt =>  dependentOpt.push({ label: stateOpt.label, value: stateOpt.value }));
                });
           }
           this.dynamicDependentOptions = dependentOpt;
        }
   }
 

    handleOnChange(event) {
        const param = {
            key: this.keyParam,
            index: this.currentIndex,
            val: event.target.value
        },
        selectedEvent = new CustomEvent('selectdependentpicklist', { detail: param });

        this.selectedValue = event.target.value;
        this.dispatchEvent(selectedEvent);
    }

    @api
    checkValidity() {
        if (this.isRequired) {
            return this.selectedValue && this.selectedValue !== '' ? true : false;
        } else {
            return true;
        }
    }
    
    @api
    reportValidity() {
        const cmp = this.template.querySelector(`[data-id="${this.keyParam}"]`);
        if (cmp) {
            cmp.reportValidity();
        }
    }
}