import { LightningElement, track, api } from 'lwc';
import { FCS_BUSINESS_DETAILS_LABELS } from 'c/fcsLabelsUtil';
import { UtilService } from 'c/utilService';

const ACTIVE_ACCORDION = 'entityInformation';
const EMPTY_DATE_PLACEHOLDER = '-';


export default class FcsEntityBusinessDetailInfoSF extends LightningElement {

     @api entityBusinessDetailInfoSF;

     // private attributes
     entityDetailLabels = FCS_BUSINESS_DETAILS_LABELS;
     @track activeSections = ACTIVE_ACCORDION;

      

     get entityInfo() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          if (wrapperRecord) {
               if (wrapperRecord.entityInformation) {
                    return wrapperRecord.entityInformation;
               }
          }
          return null;
     }



     get partyInfo() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          if (wrapperRecord) {
               if (wrapperRecord.listOfPartySF && wrapperRecord.listOfPartySF.length > 0) {
                    return wrapperRecord.listOfPartySF;
               }
          }
          return null;
     }

     get agentInfo() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          if (wrapperRecord) {
               if (wrapperRecord.listOfAgentSF && wrapperRecord.listOfAgentSF.length > 0) {
                    return wrapperRecord.listOfAgentSF;
               }
          }
          return null;
     }

     get dateOfIncorporation() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          let dateOfIncorporation = EMPTY_DATE_PLACEHOLDER;
          if (wrapperRecord) {
               if (wrapperRecord.entityInformation) {
                    if (wrapperRecord.entityInformation.Date_Of_Incorporation__c) {
                         dateOfIncorporation = wrapperRecord.entityInformation.Date_Of_Incorporation__c;
                    }
               }
          }

          return dateOfIncorporation;
     }

     get dateOfFiling() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          let dateOfFiling = EMPTY_DATE_PLACEHOLDER;
          if (wrapperRecord) {
               if (wrapperRecord.entityInformation) {
                    if (wrapperRecord.entityInformation.Date_of_Filing__c) {
                         dateOfFiling = wrapperRecord.entityInformation.Date_of_Filing__c;
                    }
               }
          }

          return dateOfFiling;
     }


     get addressInfo() {
          const wrapperRecord = this.entityBusinessDetailInfoSF;
          if (this.entityBusinessDetailInfoSF) {
               if (wrapperRecord.listOfEntityCommunicationAddress) {
                    return wrapperRecord.listOfEntityCommunicationAddress[0];
               }
          }
          return null;
     }

     get addresOneLabel() {
          return UtilService.format(this.entityDetailLabels.ADDRESS.addressLabel, 1);
     }

     get addresTwoLabel() {
          return UtilService.format(this.entityDetailLabels.ADDRESS.addressLabel, 2);
     }

     handleClick(event) {
          const current = this,
               target = event.target;
          let params = {
               action: false,
               actionName: target.name ? target.name : event.currentTarget.name,
               selectedBusinessDetail: {...current.entityBusinessDetailInfoSF}
          };

          current.dispatchEvent(new CustomEvent('sfbusinessdetailaction', {
               detail: params
          }));
     }
}