import { LightningElement, api, track } from "lwc";
import { FCS_BUSINESS_INFORMATION_LABELS } from "c/fcsLabelsUtil";
import { UtilService } from "c/utilService";

export default class FcsEntityInformation extends LightningElement {
  entityInformationResponse;
  businessInfoLabel = FCS_BUSINESS_INFORMATION_LABELS;
  selectedBusinessInformation;

  @api
  get entityInformation() {
    return this.entityInformationResponse;
  }

  set entityInformation(value) {
    this.entityInformationResponse = value;
    this.createBusinessInformationArray(value.results);
  }

  @track listOfEntity;

  get modalDialogSize() {
    let modalDefaultSize = "slds-modal slds-fade-in-open slds-modal_large";
    if (this.listOfEntity && this.listOfEntity.length === 1) {
      modalDefaultSize = "slds-modal slds-fade-in-open slds-modal_small";
    }
    return modalDefaultSize;
  }

  get cardLayoutBlockModel() {
    let cardBlockFormat =
      "slds-p-horizontal_small slds-size_1-of-1 slds-medium-size_1-of-2";
    if (this.listOfEntity && this.listOfEntity.length === 1) {
      cardBlockFormat =
        "slds-p-horizontal_small slds-size_1-of-1 slds-medium-size_1-of-1";
    }
    return cardBlockFormat;
  }

  createBusinessInformationArray(responseArr) {
    let businessInfoArr = [];
    responseArr.forEach(info => {
      businessInfoArr.push(UtilService.modifiableObject(info));
    });
    this.listOfEntity = businessInfoArr;
  }

  handleSelectChange(event) {
    const entityId = event.currentTarget.dataset.entityId,
      cmp = event.target,
      entityRecordIndex = this.listOfEntity.findIndex(function(businessInfo) {
        return businessInfo.entityID === entityId;
      });
    this.listOfEntity = this.deSelectAllBusinessInfo();
    if (cmp.checked !== undefined) {
      cmp.checked = true;
    }
    this.listOfEntity[entityRecordIndex].isSelected = true;
    this.selectedBusinessInfoClass(entityRecordIndex, true);
    this.selectedBusinessInformation = this.listOfEntity[entityRecordIndex];
  }

  deSelectAllBusinessInfo() {
    const current = this,
      businessInfoArray = this.listOfEntity.map(function(entity, index) {
        entity.isSelected = false;
        current.selectedBusinessInfoClass(index, false);
        return entity;
      });
    return businessInfoArray;
  }

  selectedBusinessInfoClass(selectedBusinessInfoIndex, isBusinessSelected) {
    const selectedBussinessInfo = this.listOfEntity[selectedBusinessInfoIndex],
      divComponent = this.template.querySelector(
        `[data-div-selection="${selectedBussinessInfo.entityID}"]`
      );

    if (divComponent) {
      if (isBusinessSelected) {
        divComponent.classList.add("selected-business-info");
      } else if (!isBusinessSelected) {
        divComponent.classList.remove("selected-business-info");
      }
    }
  }

  handleClick(event) {
    const current = this,
      target = event.target,
      selectedBusinessInfo = current.selectedBusinessInformation,
      selectedDtails = {
        ...selectedBusinessInfo,
        ...current.entityInformationResponse.header
      },
      params = {
        action: false,
        actionName:
          target.name !== undefined ? target.name : event.currentTarget.name
      };

    if (params.actionName === "validate") {
      params.selectedBusinessInfo = selectedDtails;
    }

    current.dispatchEvent(
      new CustomEvent("handlebusinessinfo", {
        detail: params
      })
    );
  }

  get isBusinessInfoSelected() {
    return this.selectedBusinessInformation == undefined;
  }
}