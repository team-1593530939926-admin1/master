<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Broker_Comment_Submitted</fullName>
        <description>Broker Comment Submitted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_For_Submitted_Comment</template>
    </alerts>
    <alerts>
        <fullName>Broker_Credit_Condition_Document_Uploaded</fullName>
        <description>Broker Credit Condition Document Uploaded</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Broker_Credit_Conditiont_Uploaded</template>
    </alerts>
    <alerts>
        <fullName>CVF_broker_Deal_Auto_Declined</fullName>
        <description>CVF broker Deal Auto Declined</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/CVF_Broker_Deal_Auto_Declined</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Credit_Conditions</fullName>
        <description>Email Alert For Credit Conditions</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_Template_For_Credit_Condition</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_For_Partial_Approval</fullName>
        <description>Email Alert For Partial Approval</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_Template_For_Partial_Approval</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Additional_Review</fullName>
        <description>Email Alert for Additional Review</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_Alert_for_Additional_Review</template>
    </alerts>
    <alerts>
        <fullName>Email_Template_For_Deal_Declined</fullName>
        <description>Email Template For Deal Declined</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_Template_For_Deal_Declined</template>
    </alerts>
    <alerts>
        <fullName>Email_Template_with_Rate</fullName>
        <description>Email Template with Rate</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/Email_Template_with_Rate</template>
    </alerts>
    <alerts>
        <fullName>NOTIFYING_BROKER_PRELIMINARY_APPROVAL_HAS_BEEN_DECLINED</fullName>
        <description>NOTIFYING BROKER PRELIMINARY APPROVAL HAS BEEN DECLINED</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/NOTIFYING_BROKER_APPROVAL_BEEN_DECLINED</template>
    </alerts>
    <alerts>
        <fullName>NOTIFYING_BROKER_THE_DEAL_HAS_BEEN_DECLINED</fullName>
        <description>NOTIFYING BROKER – THE DEAL HAS BEEN DECLINED</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/NOTIFYING_BROKER_DEAL_HAS_BEEN_DECLINED</template>
    </alerts>
    <alerts>
        <fullName>NOTIFYING_CREDIT_TEAM_PRELIMINARILY_APPROVED</fullName>
        <description>NOTIFYING CREDIT TEAM  - PRELIMINARILY APPROVED</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>CVF_Portal_Email_Template/NOTIFYING_TEAM_PRELIMINARILY_APPROVED</template>
    </alerts>
</Workflow>
